package com.softelite.testapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class SwipeAdapter extends FragmentStatePagerAdapter {
    public SwipeAdapter(FragmentManager fm) {
        super(fm);
    }

    public Fragment getItem(int i) {
        Fragment fragment = new PageFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("count", i + 1);
        bundle.putString("menu1", "Admissions");
        bundle.putString("menu2", "Programs");
        bundle.putString("menu3", "News/Events");
        bundle.putString("menu4", "Maps/Directions");
        bundle.putString("menu5", "Directory");
        bundle.putString("menu6", "IntellZone");
        bundle.putString("menu7", "About Intell");
        bundle.putString("menu8", "My Exams");
        bundle.putString("menu9", "Media");
        bundle.putString("menu10", "INTELL on\nFacebook");
        bundle.putString("menu11", "Website");
        bundle.putString("menu12", "Results");
        fragment.setArguments(bundle);
        return fragment;
    }

    public int getCount() {
        return 2;
    }
}
