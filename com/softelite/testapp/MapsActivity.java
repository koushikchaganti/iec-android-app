package com.softelite.testapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {
    private GoogleMap mMap;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) C0577R.layout.activity_maps);
        activitybar();
        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(C0577R.id.map)).getMapAsync(this);
    }

    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;
        LatLng intell = new LatLng(14.681798d, 77.545039d);
        this.mMap.setMapType(4);
        this.mMap.addMarker(new MarkerOptions().position(intell).title("INTELL Engg. College"));
        this.mMap.moveCamera(CameraUpdateFactory.newLatLng(intell));
        this.mMap.animateCamera(CameraUpdateFactory.zoomTo(17.0f));
        this.mMap.getUiSettings().setZoomControlsEnabled(true);
    }

    public void activitybar() {
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle((CharSequence) "INTELL Engg. College");
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setLogo((int) C0577R.drawable.intellenggicon);
    }
}
