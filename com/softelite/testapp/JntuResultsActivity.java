package com.softelite.testapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class JntuResultsActivity extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(1);
        setContentView((int) C0577R.layout.activity_jntu_results);
        activitybar();
        WebView webView = (WebView) findViewById(C0577R.id.webView2);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("https://jntuaresults.azurewebsites.net/");
        webView.setWebViewClient(new WebViewClient());
    }

    public void activitybar() {
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle((CharSequence) "INTELL Engg. College");
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setLogo((int) C0577R.drawable.intellenggicon);
    }
}
