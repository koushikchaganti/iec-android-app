package com.softelite.testapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class AboutActivity extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(1);
        setContentView((int) C0577R.layout.activity_about);
        activitybar();
    }

    public void activitybar() {
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle((CharSequence) "INTELL Engg. College");
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setLogo((int) C0577R.drawable.intellenggicon);
    }
}
