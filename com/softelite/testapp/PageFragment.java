package com.softelite.testapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class PageFragment extends Fragment {

    /* renamed from: com.softelite.testapp.PageFragment.1 */
    class C05681 implements OnClickListener {
        C05681() {
        }

        public void onClick(View v) {
            PageFragment.this.admissions();
        }
    }

    /* renamed from: com.softelite.testapp.PageFragment.2 */
    class C05692 implements OnClickListener {
        C05692() {
        }

        public void onClick(View v) {
        }
    }

    /* renamed from: com.softelite.testapp.PageFragment.3 */
    class C05703 implements OnClickListener {
        C05703() {
        }

        public void onClick(View v) {
        }
    }

    /* renamed from: com.softelite.testapp.PageFragment.4 */
    class C05714 implements OnClickListener {
        C05714() {
        }

        public void onClick(View v) {
            PageFragment.this.maps();
        }
    }

    /* renamed from: com.softelite.testapp.PageFragment.5 */
    class C05725 implements OnClickListener {
        C05725() {
        }

        public void onClick(View v) {
        }
    }

    /* renamed from: com.softelite.testapp.PageFragment.6 */
    class C05736 implements OnClickListener {
        C05736() {
        }

        public void onClick(View v) {
        }
    }

    /* renamed from: com.softelite.testapp.PageFragment.7 */
    class C05747 implements OnClickListener {
        C05747() {
        }

        public void onClick(View v) {
            PageFragment.this.aboutIntell();
        }
    }

    /* renamed from: com.softelite.testapp.PageFragment.8 */
    class C05758 implements OnClickListener {
        C05758() {
        }

        public void onClick(View v) {
        }
    }

    /* renamed from: com.softelite.testapp.PageFragment.9 */
    class C05769 implements OnClickListener {
        C05769() {
        }

        public void onClick(View v) {
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(C0577R.layout.fragment_page, container, false);
        TextView tv1 = (TextView) view.findViewById(C0577R.id.textView2);
        TextView tv2 = (TextView) view.findViewById(C0577R.id.textView3);
        TextView tv3 = (TextView) view.findViewById(C0577R.id.textView4);
        TextView tv4 = (TextView) view.findViewById(C0577R.id.textView5);
        TextView tv5 = (TextView) view.findViewById(C0577R.id.textView6);
        TextView tv6 = (TextView) view.findViewById(C0577R.id.textView7);
        TextView tv7 = (TextView) view.findViewById(C0577R.id.textView8);
        TextView tv8 = (TextView) view.findViewById(C0577R.id.textView9);
        TextView tv9 = (TextView) view.findViewById(C0577R.id.textView10);
        ImageView im1 = (ImageView) view.findViewById(C0577R.id.imageView2);
        ImageView im2 = (ImageView) view.findViewById(C0577R.id.imageView3);
        ImageView im3 = (ImageView) view.findViewById(C0577R.id.imageView4);
        ImageView im4 = (ImageView) view.findViewById(C0577R.id.imageView6);
        ImageView im5 = (ImageView) view.findViewById(C0577R.id.imageView7);
        ImageView im6 = (ImageView) view.findViewById(C0577R.id.imageView5);
        ImageView im7 = (ImageView) view.findViewById(C0577R.id.imageView8);
        ImageView im8 = (ImageView) view.findViewById(C0577R.id.imageView10);
        ImageView im9 = (ImageView) view.findViewById(C0577R.id.imageView9);
        Bundle bundle = getArguments();
        String message1 = bundle.getString("menu1");
        String message2 = bundle.getString("menu2");
        String message3 = bundle.getString("menu3");
        String message4 = bundle.getString("menu4");
        String message5 = bundle.getString("menu5");
        String message6 = bundle.getString("menu6");
        String message7 = bundle.getString("menu7");
        String message8 = bundle.getString("menu8");
        String message9 = bundle.getString("menu9");
        String message10 = bundle.getString("menu10");
        String message11 = bundle.getString("menu11");
        String message12 = bundle.getString("menu12");
        int count = bundle.getInt("count");
        if (count == 1) {
            im1.setImageResource(C0577R.drawable.admissions);
            tv1.setText(message1);
            im2.setImageResource(C0577R.drawable.programs);
            tv2.setText(message2);
            im3.setImageResource(C0577R.drawable.news);
            tv3.setText(message3);
            im4.setImageResource(C0577R.drawable.maps);
            tv4.setText(message4);
            im5.setImageResource(C0577R.drawable.directory);
            tv5.setText(message5);
            im6.setImageResource(C0577R.drawable.myintell);
            tv6.setText(message6);
            im7.setImageResource(C0577R.drawable.intellengg);
            tv7.setText(message7);
            im8.setImageResource(C0577R.drawable.exams);
            tv8.setText(message8);
            im9.setImageResource(C0577R.drawable.media);
            tv9.setText(message9);
            im1.setOnClickListener(new C05681());
            im2.setOnClickListener(new C05692());
            im3.setOnClickListener(new C05703());
            im4.setOnClickListener(new C05714());
            im5.setOnClickListener(new C05725());
            im6.setOnClickListener(new C05736());
            im7.setOnClickListener(new C05747());
            im8.setOnClickListener(new C05758());
            im9.setOnClickListener(new C05769());
        } else if (count == 2) {
            im1.setImageResource(C0577R.drawable.facebook);
            tv1.setText(message10);
            im2.setImageResource(C0577R.drawable.web);
            tv2.setText(message11);
            im3.setImageResource(C0577R.drawable.results);
            tv3.setText(message12);
            im1.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    PageFragment.this.intellFacebook();
                }
            });
            im2.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    PageFragment.this.website();
                }
            });
            im3.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    PageFragment.this.jntuResults();
                }
            });
        }
        return view;
    }

    public void admissions() {
        startActivity(new Intent(getActivity(), AdmissionsActivity.class));
    }

    public void website() {
        startActivity(new Intent(getActivity(), WebsiteActivity.class));
    }

    public void jntuResults() {
        startActivity(new Intent(getActivity(), JntuResultsActivity.class));
    }

    public void intellFacebook() {
        startActivity(new Intent(getActivity(), FacebookActivity.class));
    }

    public void maps() {
        startActivity(new Intent(getActivity(), MapsActivity.class));
    }

    public void aboutIntell() {
        startActivity(new Intent(getActivity(), AboutActivity.class));
    }
}
