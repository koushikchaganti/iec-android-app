package com.softelite.testapp;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

public class MainActivity extends FragmentActivity {
    ViewPager viewPager;

    /* renamed from: com.softelite.testapp.MainActivity.1 */
    class C05671 extends CountDownTimer {
        C05671(long x0, long x1) {
            super(x0, x1);
        }

        public void onTick(long millisUntilFinished) {
        }

        public void onFinish() {
            MainActivity.this.setContentView(C0577R.layout.activity_main);
            MainActivity.this.viewPager = (ViewPager) MainActivity.this.findViewById(C0577R.id.viewPager);
            MainActivity.this.viewPager.setAdapter(new SwipeAdapter(MainActivity.this.getSupportFragmentManager()));
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(1);
        setContentView(C0577R.layout.splash);
        new C05671(2000, 1000).start();
    }
}
