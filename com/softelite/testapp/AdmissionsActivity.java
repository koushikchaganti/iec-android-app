package com.softelite.testapp;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;

public class AdmissionsActivity extends AppCompatActivity {

    /* renamed from: com.softelite.testapp.AdmissionsActivity.1 */
    class C05611 implements OnClickListener {
        C05611() {
        }

        public void onClick(View view) {
            Snackbar.make(view, (CharSequence) "Replace with your own action", 0).setAction((CharSequence) "Action", null).show();
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(1);
        setContentView((int) C0577R.layout.activity_admissions);
        setSupportActionBar((Toolbar) findViewById(C0577R.id.toolbar));
        activitybar();
        ((FloatingActionButton) findViewById(C0577R.id.fab)).setOnClickListener(new C05611());
    }

    public void activitybar() {
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle((CharSequence) "INTELL Engg. College");
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setLogo((int) C0577R.drawable.intellenggicon);
    }
}
