package com.softelite.testapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebsiteActivity extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(1);
        setContentView((int) C0577R.layout.activity_website);
        activitybar();
        WebView webView = (WebView) findViewById(C0577R.id.webView);
        webView.loadUrl("http://intellengg.ac.in/");
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());
    }

    public void activitybar() {
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle((CharSequence) "INTELL Engg. College");
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setLogo((int) C0577R.drawable.intellenggicon);
    }
}
