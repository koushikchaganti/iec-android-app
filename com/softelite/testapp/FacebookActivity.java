package com.softelite.testapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class FacebookActivity extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) C0577R.layout.activity_facebook);
        activitybar();
        WebView webView = (WebView) findViewById(C0577R.id.webView3);
        webView.loadUrl("https://www.facebook.com/intellengineeringcollege");
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());
    }

    public void activitybar() {
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle((CharSequence) "INTELL Engg. College");
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setLogo((int) C0577R.drawable.intellenggicon);
    }
}
