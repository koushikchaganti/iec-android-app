package com.google.android.gms.wearable;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions.Optional;
import com.google.android.gms.common.api.Api.zza;
import com.google.android.gms.common.api.Api.zzc;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.wearable.internal.zzbc;
import com.google.android.gms.wearable.internal.zzbm;
import com.google.android.gms.wearable.internal.zzbo;
import com.google.android.gms.wearable.internal.zzcb;
import com.google.android.gms.wearable.internal.zzce;
import com.google.android.gms.wearable.internal.zzcg;
import com.google.android.gms.wearable.internal.zze;
import com.google.android.gms.wearable.internal.zzg;
import com.google.android.gms.wearable.internal.zzj;
import com.google.android.gms.wearable.internal.zzl;
import com.google.android.gms.wearable.internal.zzw;
import com.google.android.gms.wearable.internal.zzx;

public class Wearable {
    public static final Api<WearableOptions> API;
    public static final CapabilityApi CapabilityApi;
    public static final ChannelApi ChannelApi;
    public static final DataApi DataApi;
    public static final MessageApi MessageApi;
    public static final NodeApi NodeApi;
    public static final zzc<zzce> zzTo;
    private static final zza<zzce, WearableOptions> zzTp;
    public static final LargeAssetApi zzbma;
    public static final zzc zzbmb;
    public static final zza zzbmc;
    public static final zzf zzbmd;
    public static final zzj zzbme;
    public static final zzl zzbmf;

    /* renamed from: com.google.android.gms.wearable.Wearable.1 */
    static class C08571 extends zza<zzce, WearableOptions> {
        C08571() {
        }

        public zzce zza(Context context, Looper looper, zzf com_google_android_gms_common_internal_zzf, WearableOptions wearableOptions, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            if (wearableOptions == null) {
                WearableOptions wearableOptions2 = new WearableOptions(null);
            }
            return new zzce(context, looper, connectionCallbacks, onConnectionFailedListener, com_google_android_gms_common_internal_zzf);
        }
    }

    public static final class WearableOptions implements Optional {

        public static class Builder {
            public WearableOptions build() {
                return new WearableOptions();
            }
        }

        private WearableOptions(Builder builder) {
        }
    }

    static {
        DataApi = new zzx();
        CapabilityApi = new zzj();
        MessageApi = new zzbm();
        NodeApi = new zzbo();
        ChannelApi = new zzl();
        zzbma = new zzbc();
        zzbmb = new zzg();
        zzbmc = new zze();
        zzbmd = new zzw();
        zzbme = new zzcb();
        zzbmf = new zzcg();
        zzTo = new zzc();
        zzTp = new C08571();
        API = new Api("Wearable.API", zzTp, zzTo);
    }

    private Wearable() {
    }
}
