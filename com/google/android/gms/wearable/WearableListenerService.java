package com.google.android.gms.wearable;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.internal.zznt;
import com.google.android.gms.wearable.CapabilityApi.CapabilityListener;
import com.google.android.gms.wearable.ChannelApi.ChannelListener;
import com.google.android.gms.wearable.DataApi.DataListener;
import com.google.android.gms.wearable.MessageApi.MessageListener;
import com.google.android.gms.wearable.NodeApi.NodeListener;
import com.google.android.gms.wearable.internal.AmsEntityUpdateParcelable;
import com.google.android.gms.wearable.internal.AncsNotificationParcelable;
import com.google.android.gms.wearable.internal.CapabilityInfoParcelable;
import com.google.android.gms.wearable.internal.ChannelEventParcelable;
import com.google.android.gms.wearable.internal.LargeAssetQueueStateChangeParcelable;
import com.google.android.gms.wearable.internal.LargeAssetSyncRequestPayload;
import com.google.android.gms.wearable.internal.MessageEventParcelable;
import com.google.android.gms.wearable.internal.NodeParcelable;
import com.google.android.gms.wearable.internal.zzax;
import com.google.android.gms.wearable.internal.zzay;
import com.google.android.gms.wearable.internal.zzbj;
import java.io.File;
import java.util.List;

public abstract class WearableListenerService extends Service implements CapabilityListener, ChannelListener, DataListener, MessageListener, NodeListener, com.google.android.gms.wearable.NodeApi.zza {
    public static final String BIND_LISTENER_INTENT_ACTION = "com.google.android.gms.wearable.BIND_LISTENER";
    private boolean zzOR;
    private String zzSp;
    private IBinder zzaiT;
    private Handler zzbmg;
    private final Object zzbmh;

    private final class zza extends com.google.android.gms.wearable.internal.zzba.zza {
        private volatile int zzaiR;
        final /* synthetic */ WearableListenerService zzbmi;

        /* renamed from: com.google.android.gms.wearable.WearableListenerService.zza.10 */
        class AnonymousClass10 implements Runnable {
            final /* synthetic */ zza zzbmk;
            final /* synthetic */ AmsEntityUpdateParcelable zzbmt;

            AnonymousClass10(zza com_google_android_gms_wearable_WearableListenerService_zza, AmsEntityUpdateParcelable amsEntityUpdateParcelable) {
                this.zzbmk = com_google_android_gms_wearable_WearableListenerService_zza;
                this.zzbmt = amsEntityUpdateParcelable;
            }

            public void run() {
                ((zzk) this.zzbmk.zzbmi).zza(this.zzbmt);
            }
        }

        /* renamed from: com.google.android.gms.wearable.WearableListenerService.zza.11 */
        class AnonymousClass11 implements Runnable {
            final /* synthetic */ zza zzbmk;
            final /* synthetic */ ChannelEventParcelable zzbmu;

            AnonymousClass11(zza com_google_android_gms_wearable_WearableListenerService_zza, ChannelEventParcelable channelEventParcelable) {
                this.zzbmk = com_google_android_gms_wearable_WearableListenerService_zza;
                this.zzbmu = channelEventParcelable;
            }

            public void run() {
                this.zzbmu.zza(this.zzbmk.zzbmi);
            }
        }

        /* renamed from: com.google.android.gms.wearable.WearableListenerService.zza.1 */
        class C05491 implements Runnable {
            final /* synthetic */ DataHolder zzbmj;
            final /* synthetic */ zza zzbmk;

            C05491(zza com_google_android_gms_wearable_WearableListenerService_zza, DataHolder dataHolder) {
                this.zzbmk = com_google_android_gms_wearable_WearableListenerService_zza;
                this.zzbmj = dataHolder;
            }

            public void run() {
                DataEventBuffer dataEventBuffer = new DataEventBuffer(this.zzbmj);
                try {
                    this.zzbmk.zzbmi.onDataChanged(dataEventBuffer);
                } finally {
                    dataEventBuffer.release();
                }
            }
        }

        /* renamed from: com.google.android.gms.wearable.WearableListenerService.zza.2 */
        class C05502 implements Runnable {
            final /* synthetic */ zza zzbmk;
            final /* synthetic */ LargeAssetQueueStateChangeParcelable zzbml;

            C05502(zza com_google_android_gms_wearable_WearableListenerService_zza, LargeAssetQueueStateChangeParcelable largeAssetQueueStateChangeParcelable) {
                this.zzbmk = com_google_android_gms_wearable_WearableListenerService_zza;
                this.zzbml = largeAssetQueueStateChangeParcelable;
            }

            public void run() {
                try {
                    ((zzk) this.zzbmk.zzbmi).zza(this.zzbml);
                } finally {
                    this.zzbml.release();
                }
            }
        }

        /* renamed from: com.google.android.gms.wearable.WearableListenerService.zza.3 */
        class C05513 implements Runnable {
            final /* synthetic */ zza zzbmk;
            final /* synthetic */ LargeAssetSyncRequestPayload zzbmm;
            final /* synthetic */ zzay zzbmn;

            C05513(zza com_google_android_gms_wearable_WearableListenerService_zza, LargeAssetSyncRequestPayload largeAssetSyncRequestPayload, zzay com_google_android_gms_wearable_internal_zzay) {
                this.zzbmk = com_google_android_gms_wearable_WearableListenerService_zza;
                this.zzbmm = largeAssetSyncRequestPayload;
                this.zzbmn = com_google_android_gms_wearable_internal_zzay;
            }

            public void run() {
                zzh com_google_android_gms_wearable_internal_zzbj = new zzbj(this.zzbmm, this.zzbmn);
                ((zzk) this.zzbmk.zzbmi).zza(com_google_android_gms_wearable_internal_zzbj);
                try {
                    com_google_android_gms_wearable_internal_zzbj.zzGX();
                } catch (Throwable e) {
                    Log.w("WearableLS", "Failed to respond to LargeAssetRequest", e);
                }
            }
        }

        /* renamed from: com.google.android.gms.wearable.WearableListenerService.zza.4 */
        class C05524 implements Runnable {
            final /* synthetic */ zza zzbmk;
            final /* synthetic */ MessageEventParcelable zzbmo;

            C05524(zza com_google_android_gms_wearable_WearableListenerService_zza, MessageEventParcelable messageEventParcelable) {
                this.zzbmk = com_google_android_gms_wearable_WearableListenerService_zza;
                this.zzbmo = messageEventParcelable;
            }

            public void run() {
                this.zzbmk.zzbmi.onMessageReceived(this.zzbmo);
            }
        }

        /* renamed from: com.google.android.gms.wearable.WearableListenerService.zza.5 */
        class C05535 implements Runnable {
            final /* synthetic */ zza zzbmk;
            final /* synthetic */ NodeParcelable zzbmp;

            C05535(zza com_google_android_gms_wearable_WearableListenerService_zza, NodeParcelable nodeParcelable) {
                this.zzbmk = com_google_android_gms_wearable_WearableListenerService_zza;
                this.zzbmp = nodeParcelable;
            }

            public void run() {
                this.zzbmk.zzbmi.onPeerConnected(this.zzbmp);
            }
        }

        /* renamed from: com.google.android.gms.wearable.WearableListenerService.zza.6 */
        class C05546 implements Runnable {
            final /* synthetic */ zza zzbmk;
            final /* synthetic */ NodeParcelable zzbmp;

            C05546(zza com_google_android_gms_wearable_WearableListenerService_zza, NodeParcelable nodeParcelable) {
                this.zzbmk = com_google_android_gms_wearable_WearableListenerService_zza;
                this.zzbmp = nodeParcelable;
            }

            public void run() {
                this.zzbmk.zzbmi.onPeerDisconnected(this.zzbmp);
            }
        }

        /* renamed from: com.google.android.gms.wearable.WearableListenerService.zza.7 */
        class C05557 implements Runnable {
            final /* synthetic */ zza zzbmk;
            final /* synthetic */ List zzbmq;

            C05557(zza com_google_android_gms_wearable_WearableListenerService_zza, List list) {
                this.zzbmk = com_google_android_gms_wearable_WearableListenerService_zza;
                this.zzbmq = list;
            }

            public void run() {
                this.zzbmk.zzbmi.onConnectedNodes(this.zzbmq);
            }
        }

        /* renamed from: com.google.android.gms.wearable.WearableListenerService.zza.8 */
        class C05568 implements Runnable {
            final /* synthetic */ zza zzbmk;
            final /* synthetic */ CapabilityInfoParcelable zzbmr;

            C05568(zza com_google_android_gms_wearable_WearableListenerService_zza, CapabilityInfoParcelable capabilityInfoParcelable) {
                this.zzbmk = com_google_android_gms_wearable_WearableListenerService_zza;
                this.zzbmr = capabilityInfoParcelable;
            }

            public void run() {
                this.zzbmk.zzbmi.onCapabilityChanged(this.zzbmr);
            }
        }

        /* renamed from: com.google.android.gms.wearable.WearableListenerService.zza.9 */
        class C05579 implements Runnable {
            final /* synthetic */ zza zzbmk;
            final /* synthetic */ AncsNotificationParcelable zzbms;

            C05579(zza com_google_android_gms_wearable_WearableListenerService_zza, AncsNotificationParcelable ancsNotificationParcelable) {
                this.zzbmk = com_google_android_gms_wearable_WearableListenerService_zza;
                this.zzbms = ancsNotificationParcelable;
            }

            public void run() {
                ((zzk) this.zzbmk.zzbmi).zza(this.zzbms);
            }
        }

        private zza(WearableListenerService wearableListenerService) {
            this.zzbmi = wearableListenerService;
            this.zzaiR = -1;
        }

        private void zzGA() throws SecurityException {
            int callingUid = Binder.getCallingUid();
            if (callingUid != this.zzaiR) {
                if (GooglePlayServicesUtil.zze(this.zzbmi, callingUid)) {
                    this.zzaiR = callingUid;
                    return;
                }
                throw new SecurityException("Caller is not GooglePlayServices");
            }
        }

        private boolean zza(Runnable runnable, String str, Object obj) {
            return !(this.zzbmi instanceof zzk) ? false : zzb(runnable, str, obj);
        }

        private boolean zzb(Runnable runnable, String str, Object obj) {
            if (Log.isLoggable("WearableLS", 3)) {
                Log.d("WearableLS", String.format("%s: %s %s", new Object[]{str, this.zzbmi.zzSp, obj}));
            }
            zzGA();
            synchronized (this.zzbmi.zzbmh) {
                if (this.zzbmi.zzOR) {
                    return false;
                }
                this.zzbmi.zzbmg.post(runnable);
                return true;
            }
        }

        public void onConnectedNodes(List<NodeParcelable> connectedNodes) {
            zzb(new C05557(this, connectedNodes), "onConnectedNodes", connectedNodes);
        }

        public void zza(AmsEntityUpdateParcelable amsEntityUpdateParcelable) {
            zza(new AnonymousClass10(this, amsEntityUpdateParcelable), "onEntityUpdate", (Object) amsEntityUpdateParcelable);
        }

        public void zza(AncsNotificationParcelable ancsNotificationParcelable) {
            zza(new C05579(this, ancsNotificationParcelable), "onNotificationReceived", (Object) ancsNotificationParcelable);
        }

        public void zza(CapabilityInfoParcelable capabilityInfoParcelable) {
            zzb(new C05568(this, capabilityInfoParcelable), "onConnectedCapabilityChanged", capabilityInfoParcelable);
        }

        public void zza(ChannelEventParcelable channelEventParcelable) {
            zzb(new AnonymousClass11(this, channelEventParcelable), "onChannelEvent", channelEventParcelable);
        }

        public void zza(LargeAssetQueueStateChangeParcelable largeAssetQueueStateChangeParcelable) {
            try {
                if (!zza(new C05502(this, largeAssetQueueStateChangeParcelable), "onLargeAssetStateChanged", (Object) largeAssetQueueStateChangeParcelable)) {
                }
            } finally {
                largeAssetQueueStateChangeParcelable.release();
            }
        }

        public void zza(LargeAssetSyncRequestPayload largeAssetSyncRequestPayload, zzay com_google_android_gms_wearable_internal_zzay) {
            zza(new C05513(this, largeAssetSyncRequestPayload, com_google_android_gms_wearable_internal_zzay), "onLargeAssetSyncRequest", (Object) largeAssetSyncRequestPayload);
        }

        public void zza(MessageEventParcelable messageEventParcelable) {
            zzb(new C05524(this, messageEventParcelable), "onMessageReceived", messageEventParcelable);
        }

        public void zza(NodeParcelable nodeParcelable) {
            zzb(new C05535(this, nodeParcelable), "onPeerConnected", nodeParcelable);
        }

        public void zza(zzax com_google_android_gms_wearable_internal_zzax, String str, int i) {
            ParcelFileDescriptor parcelFileDescriptor = null;
            if (this.zzbmi instanceof zzk) {
                if (Log.isLoggable("WearableLS", 3)) {
                    Log.d("WearableLS", String.format("openFileDescriptor: %s (mode=%s)", new Object[]{str, Integer.valueOf(i)}));
                }
                zzGA();
                synchronized (this.zzbmi.zzbmh) {
                    if (this.zzbmi.zzOR) {
                        return;
                    }
                    try {
                        File parentFile = new File(str).getParentFile();
                        if (parentFile.exists() || parentFile.mkdirs()) {
                            try {
                                parcelFileDescriptor = ParcelFileDescriptor.open(new File(str), 134217728 | i);
                                com_google_android_gms_wearable_internal_zzax.zzb(parcelFileDescriptor);
                                zznt.zza(parcelFileDescriptor);
                                return;
                            } catch (Throwable e) {
                                Log.w("WearableLS", "Failed to open file: " + str, e);
                                com_google_android_gms_wearable_internal_zzax.zzb(null);
                                zznt.zza(parcelFileDescriptor);
                                return;
                            }
                        }
                        Log.w("WearableLS", "Unable to create directory: " + parentFile.getAbsolutePath());
                        com_google_android_gms_wearable_internal_zzax.zzb(null);
                    } catch (Throwable e2) {
                        Log.w("WearableLS", "Failed to set file descriptor", e2);
                    } finally {
                        zznt.zza(parcelFileDescriptor);
                    }
                }
            }
        }

        public void zzag(DataHolder dataHolder) {
            try {
                if (!zzb(new C05491(this, dataHolder), "onDataItemChanged", dataHolder)) {
                }
            } finally {
                dataHolder.close();
            }
        }

        public void zzb(NodeParcelable nodeParcelable) {
            zzb(new C05546(this, nodeParcelable), "onPeerDisconnected", nodeParcelable);
        }
    }

    public WearableListenerService() {
        this.zzbmh = new Object();
    }

    public final IBinder onBind(Intent intent) {
        return BIND_LISTENER_INTENT_ACTION.equals(intent.getAction()) ? this.zzaiT : null;
    }

    public void onCapabilityChanged(CapabilityInfo capabilityInfo) {
    }

    public void onChannelClosed(Channel channel, int closeReason, int appSpecificErrorCode) {
    }

    public void onChannelOpened(Channel channel) {
    }

    public void onConnectedNodes(List<Node> list) {
    }

    public void onCreate() {
        super.onCreate();
        if (Log.isLoggable("WearableLS", 3)) {
            Log.d("WearableLS", "onCreate: " + getPackageName());
        }
        this.zzSp = getPackageName();
        HandlerThread handlerThread = new HandlerThread("WearableListenerService");
        handlerThread.start();
        this.zzbmg = new Handler(handlerThread.getLooper());
        this.zzaiT = new zza();
    }

    public void onDataChanged(DataEventBuffer dataEvents) {
    }

    public void onDestroy() {
        synchronized (this.zzbmh) {
            this.zzOR = true;
            if (this.zzbmg == null) {
                throw new IllegalStateException("onDestroy: mServiceHandler not set, did you override onCreate() but forget to call super.onCreate()?");
            }
            this.zzbmg.getLooper().quit();
        }
        super.onDestroy();
    }

    public void onInputClosed(Channel channel, int closeReason, int appSpecificErrorCode) {
    }

    public void onMessageReceived(MessageEvent messageEvent) {
    }

    public void onOutputClosed(Channel channel, int closeReason, int appSpecificErrorCode) {
    }

    public void onPeerConnected(Node peer) {
    }

    public void onPeerDisconnected(Node peer) {
    }
}
