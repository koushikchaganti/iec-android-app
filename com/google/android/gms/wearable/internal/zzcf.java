package com.google.android.gms.wearable.internal;

import android.content.IntentFilter;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzmn;
import com.google.android.gms.internal.zzmn.zzb;
import com.google.android.gms.wearable.CapabilityApi.CapabilityListener;
import com.google.android.gms.wearable.ChannelApi.ChannelListener;
import com.google.android.gms.wearable.DataApi.DataListener;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.LargeAssetApi;
import com.google.android.gms.wearable.MessageApi.MessageListener;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.NodeApi.NodeListener;
import com.google.android.gms.wearable.internal.zzba.zza;
import com.google.android.gms.wearable.zzc;
import java.util.List;

final class zzcf<T> extends zza {
    private zzmn<MessageListener> zzaWe;
    private zzmn<com.google.android.gms.wearable.zza.zza> zzboV;
    private zzmn<zzc.zza> zzboW;
    private zzmn<DataListener> zzboX;
    private zzmn<NodeListener> zzboY;
    private zzmn<NodeApi.zza> zzboZ;
    private final IntentFilter[] zzbou;
    private zzmn<ChannelListener> zzbpa;
    private zzmn<LargeAssetApi.zza> zzbpb;
    private zzmn<CapabilityListener> zzbpc;
    private final String zzbpd;

    /* renamed from: com.google.android.gms.wearable.internal.zzcf.10 */
    static class AnonymousClass10 implements zzb<LargeAssetApi.zza> {
        final /* synthetic */ LargeAssetQueueStateChangeParcelable zzbml;

        AnonymousClass10(LargeAssetQueueStateChangeParcelable largeAssetQueueStateChangeParcelable) {
            this.zzbml = largeAssetQueueStateChangeParcelable;
        }

        public void zza(LargeAssetApi.zza com_google_android_gms_wearable_LargeAssetApi_zza) {
            com_google_android_gms_wearable_LargeAssetApi_zza.zza(this.zzbml);
            this.zzbml.release();
        }

        public void zzpb() {
            this.zzbml.release();
        }

        public /* synthetic */ void zzs(Object obj) {
            zza((LargeAssetApi.zza) obj);
        }
    }

    /* renamed from: com.google.android.gms.wearable.internal.zzcf.1 */
    static class C08601 implements zzb<zzc.zza> {
        final /* synthetic */ AncsNotificationParcelable zzbms;

        C08601(AncsNotificationParcelable ancsNotificationParcelable) {
            this.zzbms = ancsNotificationParcelable;
        }

        public void zza(zzc.zza com_google_android_gms_wearable_zzc_zza) {
            com_google_android_gms_wearable_zzc_zza.zza(this.zzbms);
        }

        public void zzpb() {
        }

        public /* synthetic */ void zzs(Object obj) {
            zza((zzc.zza) obj);
        }
    }

    /* renamed from: com.google.android.gms.wearable.internal.zzcf.2 */
    static class C08612 implements zzb<CapabilityListener> {
        final /* synthetic */ CapabilityInfoParcelable zzbpe;

        C08612(CapabilityInfoParcelable capabilityInfoParcelable) {
            this.zzbpe = capabilityInfoParcelable;
        }

        public void zza(CapabilityListener capabilityListener) {
            capabilityListener.onCapabilityChanged(this.zzbpe);
        }

        public void zzpb() {
        }

        public /* synthetic */ void zzs(Object obj) {
            zza((CapabilityListener) obj);
        }
    }

    /* renamed from: com.google.android.gms.wearable.internal.zzcf.3 */
    static class C08623 implements zzb<com.google.android.gms.wearable.zza.zza> {
        final /* synthetic */ AmsEntityUpdateParcelable zzbmt;

        C08623(AmsEntityUpdateParcelable amsEntityUpdateParcelable) {
            this.zzbmt = amsEntityUpdateParcelable;
        }

        public void zza(com.google.android.gms.wearable.zza.zza com_google_android_gms_wearable_zza_zza) {
            com_google_android_gms_wearable_zza_zza.zza(this.zzbmt);
        }

        public void zzpb() {
        }

        public /* synthetic */ void zzs(Object obj) {
            zza((com.google.android.gms.wearable.zza.zza) obj);
        }
    }

    /* renamed from: com.google.android.gms.wearable.internal.zzcf.4 */
    static class C08634 implements zzb<DataListener> {
        final /* synthetic */ DataHolder zzbmj;

        C08634(DataHolder dataHolder) {
            this.zzbmj = dataHolder;
        }

        public void zza(DataListener dataListener) {
            try {
                dataListener.onDataChanged(new DataEventBuffer(this.zzbmj));
            } finally {
                this.zzbmj.close();
            }
        }

        public void zzpb() {
            this.zzbmj.close();
        }

        public /* synthetic */ void zzs(Object obj) {
            zza((DataListener) obj);
        }
    }

    /* renamed from: com.google.android.gms.wearable.internal.zzcf.5 */
    static class C08645 implements zzb<MessageListener> {
        final /* synthetic */ MessageEventParcelable zzbmo;

        C08645(MessageEventParcelable messageEventParcelable) {
            this.zzbmo = messageEventParcelable;
        }

        public void zza(MessageListener messageListener) {
            messageListener.onMessageReceived(this.zzbmo);
        }

        public void zzpb() {
        }

        public /* synthetic */ void zzs(Object obj) {
            zza((MessageListener) obj);
        }
    }

    /* renamed from: com.google.android.gms.wearable.internal.zzcf.6 */
    static class C08656 implements zzb<NodeListener> {
        final /* synthetic */ NodeParcelable zzbmp;

        C08656(NodeParcelable nodeParcelable) {
            this.zzbmp = nodeParcelable;
        }

        public void zza(NodeListener nodeListener) {
            nodeListener.onPeerConnected(this.zzbmp);
        }

        public void zzpb() {
        }

        public /* synthetic */ void zzs(Object obj) {
            zza((NodeListener) obj);
        }
    }

    /* renamed from: com.google.android.gms.wearable.internal.zzcf.7 */
    static class C08667 implements zzb<NodeListener> {
        final /* synthetic */ NodeParcelable zzbmp;

        C08667(NodeParcelable nodeParcelable) {
            this.zzbmp = nodeParcelable;
        }

        public void zza(NodeListener nodeListener) {
            nodeListener.onPeerDisconnected(this.zzbmp);
        }

        public void zzpb() {
        }

        public /* synthetic */ void zzs(Object obj) {
            zza((NodeListener) obj);
        }
    }

    /* renamed from: com.google.android.gms.wearable.internal.zzcf.8 */
    static class C08678 implements zzb<NodeApi.zza> {
        final /* synthetic */ List zzbmq;

        C08678(List list) {
            this.zzbmq = list;
        }

        public void zza(NodeApi.zza com_google_android_gms_wearable_NodeApi_zza) {
            com_google_android_gms_wearable_NodeApi_zza.onConnectedNodes(this.zzbmq);
        }

        public void zzpb() {
        }

        public /* synthetic */ void zzs(Object obj) {
            zza((NodeApi.zza) obj);
        }
    }

    /* renamed from: com.google.android.gms.wearable.internal.zzcf.9 */
    static class C08689 implements zzb<ChannelListener> {
        final /* synthetic */ ChannelEventParcelable zzbmu;

        C08689(ChannelEventParcelable channelEventParcelable) {
            this.zzbmu = channelEventParcelable;
        }

        public void zzb(ChannelListener channelListener) {
            this.zzbmu.zza(channelListener);
        }

        public void zzpb() {
        }

        public /* synthetic */ void zzs(Object obj) {
            zzb((ChannelListener) obj);
        }
    }

    private zzcf(IntentFilter[] intentFilterArr, String str) {
        this.zzbou = (IntentFilter[]) zzx.zzy(intentFilterArr);
        this.zzbpd = str;
    }

    private static zzb<NodeApi.zza> zzF(List<NodeParcelable> list) {
        return new C08678(list);
    }

    public static zzcf<ChannelListener> zza(zzmn<ChannelListener> com_google_android_gms_internal_zzmn_com_google_android_gms_wearable_ChannelApi_ChannelListener, String str, IntentFilter[] intentFilterArr) {
        zzcf<ChannelListener> com_google_android_gms_wearable_internal_zzcf = new zzcf(intentFilterArr, (String) zzx.zzy(str));
        com_google_android_gms_wearable_internal_zzcf.zzbpa = (zzmn) zzx.zzy(com_google_android_gms_internal_zzmn_com_google_android_gms_wearable_ChannelApi_ChannelListener);
        return com_google_android_gms_wearable_internal_zzcf;
    }

    public static zzcf<DataListener> zza(zzmn<DataListener> com_google_android_gms_internal_zzmn_com_google_android_gms_wearable_DataApi_DataListener, IntentFilter[] intentFilterArr) {
        zzcf<DataListener> com_google_android_gms_wearable_internal_zzcf = new zzcf(intentFilterArr, null);
        com_google_android_gms_wearable_internal_zzcf.zzboX = (zzmn) zzx.zzy(com_google_android_gms_internal_zzmn_com_google_android_gms_wearable_DataApi_DataListener);
        return com_google_android_gms_wearable_internal_zzcf;
    }

    private static zzb<DataListener> zzai(DataHolder dataHolder) {
        return new C08634(dataHolder);
    }

    private static zzb<com.google.android.gms.wearable.zza.zza> zzb(AmsEntityUpdateParcelable amsEntityUpdateParcelable) {
        return new C08623(amsEntityUpdateParcelable);
    }

    private static zzb<zzc.zza> zzb(AncsNotificationParcelable ancsNotificationParcelable) {
        return new C08601(ancsNotificationParcelable);
    }

    private static zzb<CapabilityListener> zzb(CapabilityInfoParcelable capabilityInfoParcelable) {
        return new C08612(capabilityInfoParcelable);
    }

    private static zzb<ChannelListener> zzb(ChannelEventParcelable channelEventParcelable) {
        return new C08689(channelEventParcelable);
    }

    private static zzb<LargeAssetApi.zza> zzb(LargeAssetQueueStateChangeParcelable largeAssetQueueStateChangeParcelable) {
        return new AnonymousClass10(largeAssetQueueStateChangeParcelable);
    }

    private static zzb<MessageListener> zzb(MessageEventParcelable messageEventParcelable) {
        return new C08645(messageEventParcelable);
    }

    public static zzcf<MessageListener> zzb(zzmn<MessageListener> com_google_android_gms_internal_zzmn_com_google_android_gms_wearable_MessageApi_MessageListener, IntentFilter[] intentFilterArr) {
        zzcf<MessageListener> com_google_android_gms_wearable_internal_zzcf = new zzcf(intentFilterArr, null);
        com_google_android_gms_wearable_internal_zzcf.zzaWe = (zzmn) zzx.zzy(com_google_android_gms_internal_zzmn_com_google_android_gms_wearable_MessageApi_MessageListener);
        return com_google_android_gms_wearable_internal_zzcf;
    }

    private static zzb<NodeListener> zzc(NodeParcelable nodeParcelable) {
        return new C08656(nodeParcelable);
    }

    public static zzcf<NodeListener> zzc(zzmn<NodeListener> com_google_android_gms_internal_zzmn_com_google_android_gms_wearable_NodeApi_NodeListener, IntentFilter[] intentFilterArr) {
        zzcf<NodeListener> com_google_android_gms_wearable_internal_zzcf = new zzcf(intentFilterArr, null);
        com_google_android_gms_wearable_internal_zzcf.zzboY = (zzmn) zzx.zzy(com_google_android_gms_internal_zzmn_com_google_android_gms_wearable_NodeApi_NodeListener);
        return com_google_android_gms_wearable_internal_zzcf;
    }

    private static zzb<NodeListener> zzd(NodeParcelable nodeParcelable) {
        return new C08667(nodeParcelable);
    }

    public static zzcf<ChannelListener> zzd(zzmn<ChannelListener> com_google_android_gms_internal_zzmn_com_google_android_gms_wearable_ChannelApi_ChannelListener, IntentFilter[] intentFilterArr) {
        zzcf<ChannelListener> com_google_android_gms_wearable_internal_zzcf = new zzcf(intentFilterArr, null);
        com_google_android_gms_wearable_internal_zzcf.zzbpa = (zzmn) zzx.zzy(com_google_android_gms_internal_zzmn_com_google_android_gms_wearable_ChannelApi_ChannelListener);
        return com_google_android_gms_wearable_internal_zzcf;
    }

    public static zzcf<CapabilityListener> zze(zzmn<CapabilityListener> com_google_android_gms_internal_zzmn_com_google_android_gms_wearable_CapabilityApi_CapabilityListener, IntentFilter[] intentFilterArr) {
        zzcf<CapabilityListener> com_google_android_gms_wearable_internal_zzcf = new zzcf(intentFilterArr, null);
        com_google_android_gms_wearable_internal_zzcf.zzbpc = (zzmn) zzx.zzy(com_google_android_gms_internal_zzmn_com_google_android_gms_wearable_CapabilityApi_CapabilityListener);
        return com_google_android_gms_wearable_internal_zzcf;
    }

    private static void zze(zzmn<?> com_google_android_gms_internal_zzmn_) {
        if (com_google_android_gms_internal_zzmn_ != null) {
            com_google_android_gms_internal_zzmn_.clear();
        }
    }

    public void clear() {
        zze(this.zzboV);
        this.zzboV = null;
        zze(this.zzboW);
        this.zzboW = null;
        zze(this.zzboX);
        this.zzboX = null;
        zze(this.zzaWe);
        this.zzaWe = null;
        zze(this.zzboY);
        this.zzboY = null;
        zze(this.zzboZ);
        this.zzboZ = null;
        zze(this.zzbpa);
        this.zzbpa = null;
        zze(this.zzbpb);
        this.zzbpb = null;
        zze(this.zzbpc);
        this.zzbpc = null;
    }

    public void onConnectedNodes(List<NodeParcelable> connectedNodes) {
        if (this.zzboZ != null) {
            this.zzboZ.zza(zzF(connectedNodes));
        }
    }

    public IntentFilter[] zzGZ() {
        return this.zzbou;
    }

    public String zzHa() {
        return this.zzbpd;
    }

    public void zza(AmsEntityUpdateParcelable amsEntityUpdateParcelable) {
        if (this.zzboV != null) {
            this.zzboV.zza(zzb(amsEntityUpdateParcelable));
        }
    }

    public void zza(AncsNotificationParcelable ancsNotificationParcelable) {
        if (this.zzboW != null) {
            this.zzboW.zza(zzb(ancsNotificationParcelable));
        }
    }

    public void zza(CapabilityInfoParcelable capabilityInfoParcelable) {
        if (this.zzbpc != null) {
            this.zzbpc.zza(zzb(capabilityInfoParcelable));
        }
    }

    public void zza(ChannelEventParcelable channelEventParcelable) {
        if (this.zzbpa != null) {
            this.zzbpa.zza(zzb(channelEventParcelable));
        }
    }

    public void zza(LargeAssetQueueStateChangeParcelable largeAssetQueueStateChangeParcelable) {
        if (this.zzbpb != null) {
            this.zzbpb.zza(zzb(largeAssetQueueStateChangeParcelable));
        } else {
            largeAssetQueueStateChangeParcelable.release();
        }
    }

    public void zza(LargeAssetSyncRequestPayload largeAssetSyncRequestPayload, zzay com_google_android_gms_wearable_internal_zzay) {
        throw new UnsupportedOperationException("onLargeAssetSyncRequest not supported on live listener");
    }

    public void zza(MessageEventParcelable messageEventParcelable) {
        if (this.zzaWe != null) {
            this.zzaWe.zza(zzb(messageEventParcelable));
        }
    }

    public void zza(NodeParcelable nodeParcelable) {
        if (this.zzboY != null) {
            this.zzboY.zza(zzc(nodeParcelable));
        }
    }

    public void zza(zzax com_google_android_gms_wearable_internal_zzax, String str, int i) {
        throw new UnsupportedOperationException("openFileDescriptor not supported on live listener");
    }

    public void zzag(DataHolder dataHolder) {
        if (this.zzboX != null) {
            this.zzboX.zza(zzai(dataHolder));
        } else {
            dataHolder.close();
        }
    }

    public void zzb(NodeParcelable nodeParcelable) {
        if (this.zzboY != null) {
            this.zzboY.zza(zzd(nodeParcelable));
        }
    }
}
