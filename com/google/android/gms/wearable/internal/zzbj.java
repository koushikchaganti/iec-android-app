package com.google.android.gms.wearable.internal;

import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zznt;
import com.google.android.gms.wearable.zzh;

public class zzbj implements zzh {
    private final LargeAssetSyncRequestPayload zzbog;
    private final zzay zzboh;
    private boolean zzboi;
    private ParcelFileDescriptor zzboj;
    private long zzbok;
    private int zzbol;

    public zzbj(LargeAssetSyncRequestPayload largeAssetSyncRequestPayload, zzay com_google_android_gms_wearable_internal_zzay) {
        this.zzbog = (LargeAssetSyncRequestPayload) zzx.zzy(largeAssetSyncRequestPayload);
        this.zzboh = (zzay) zzx.zzy(com_google_android_gms_wearable_internal_zzay);
        zzx.zzy(largeAssetSyncRequestPayload.zzbnK);
        zzx.zzb(largeAssetSyncRequestPayload.zzbom >= 0, "Got negative offset: %s", Long.valueOf(largeAssetSyncRequestPayload.zzbom));
    }

    public void zzGX() throws RemoteException {
        try {
            zzx.zza(this.zzboi, (Object) "Received onLargeAssetSyncRequest but didn't set a response.");
            if (this.zzboj != null) {
                this.zzboh.zza(this.zzboj, this.zzbok);
            } else {
                this.zzboh.zzli(this.zzbol);
            }
            if (this.zzboj != null) {
                zznt.zza(this.zzboj);
            }
        } catch (Throwable th) {
            if (this.zzboj != null) {
                zznt.zza(this.zzboj);
            }
        }
    }

    public void zzli(int i) {
        zzx.zza(!this.zzboi, (Object) "createOutputFileDescriptor called when response already set");
        this.zzbol = i;
        this.zzboi = true;
    }
}
