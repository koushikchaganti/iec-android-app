package com.google.android.gms.wearable.internal;

import android.content.IntentFilter;
import android.net.Uri;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzmn;
import com.google.android.gms.wearable.CapabilityApi;
import com.google.android.gms.wearable.CapabilityApi.AddLocalCapabilityResult;
import com.google.android.gms.wearable.CapabilityApi.CapabilityListener;
import com.google.android.gms.wearable.CapabilityApi.GetAllCapabilitiesResult;
import com.google.android.gms.wearable.CapabilityApi.GetCapabilityResult;
import com.google.android.gms.wearable.CapabilityApi.RemoveLocalCapabilityResult;
import com.google.android.gms.wearable.CapabilityInfo;
import com.google.android.gms.wearable.Node;
import java.util.Map;
import java.util.Set;

public class zzj implements CapabilityApi {

    /* renamed from: com.google.android.gms.wearable.internal.zzj.5 */
    static class C08695 implements zza<CapabilityListener> {
        final /* synthetic */ IntentFilter[] zzbmK;

        C08695(IntentFilter[] intentFilterArr) {
            this.zzbmK = intentFilterArr;
        }

        public void zza(zzce com_google_android_gms_wearable_internal_zzce, com.google.android.gms.internal.zzlx.zzb<Status> com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_common_api_Status, CapabilityListener capabilityListener, zzmn<CapabilityListener> com_google_android_gms_internal_zzmn_com_google_android_gms_wearable_CapabilityApi_CapabilityListener) throws RemoteException {
            com_google_android_gms_wearable_internal_zzce.zza((com.google.android.gms.internal.zzlx.zzb) com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_common_api_Status, capabilityListener, (zzmn) com_google_android_gms_internal_zzmn_com_google_android_gms_wearable_CapabilityApi_CapabilityListener, this.zzbmK);
        }
    }

    private static class zzb implements CapabilityListener {
        final CapabilityListener zzbmL;
        final String zzbmM;

        zzb(CapabilityListener capabilityListener, String str) {
            this.zzbmL = capabilityListener;
            this.zzbmM = str;
        }

        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            zzb com_google_android_gms_wearable_internal_zzj_zzb = (zzb) o;
            return this.zzbmL.equals(com_google_android_gms_wearable_internal_zzj_zzb.zzbmL) ? this.zzbmM.equals(com_google_android_gms_wearable_internal_zzj_zzb.zzbmM) : false;
        }

        public int hashCode() {
            return (this.zzbmL.hashCode() * 31) + this.zzbmM.hashCode();
        }

        public void onCapabilityChanged(CapabilityInfo capabilityInfo) {
            this.zzbmL.onCapabilityChanged(capabilityInfo);
        }
    }

    public static class zzc implements CapabilityInfo {
        private final String mName;
        private final Set<Node> zzbmN;

        public zzc(CapabilityInfo capabilityInfo) {
            this(capabilityInfo.getName(), capabilityInfo.getNodes());
        }

        public zzc(String str, Set<Node> set) {
            this.mName = str;
            this.zzbmN = set;
        }

        public String getName() {
            return this.mName;
        }

        public Set<Node> getNodes() {
            return this.zzbmN;
        }
    }

    public static class zza implements AddLocalCapabilityResult, RemoveLocalCapabilityResult {
        private final Status zzTA;

        public zza(Status status) {
            this.zzTA = status;
        }

        public Status getStatus() {
            return this.zzTA;
        }
    }

    public static class zzd implements GetAllCapabilitiesResult {
        private final Status zzTA;
        private final Map<String, CapabilityInfo> zzbmO;

        public zzd(Status status, Map<String, CapabilityInfo> map) {
            this.zzTA = status;
            this.zzbmO = map;
        }

        public Map<String, CapabilityInfo> getAllCapabilities() {
            return this.zzbmO;
        }

        public Status getStatus() {
            return this.zzTA;
        }
    }

    public static class zze implements GetCapabilityResult {
        private final Status zzTA;
        private final CapabilityInfo zzbmP;

        public zze(Status status, CapabilityInfo capabilityInfo) {
            this.zzTA = status;
            this.zzbmP = capabilityInfo;
        }

        public CapabilityInfo getCapability() {
            return this.zzbmP;
        }

        public Status getStatus() {
            return this.zzTA;
        }
    }

    /* renamed from: com.google.android.gms.wearable.internal.zzj.1 */
    class C12591 extends zzi<GetCapabilityResult> {
        final /* synthetic */ String zzbmH;
        final /* synthetic */ int zzbmI;
        final /* synthetic */ zzj zzbmJ;

        C12591(zzj com_google_android_gms_wearable_internal_zzj, GoogleApiClient googleApiClient, String str, int i) {
            this.zzbmJ = com_google_android_gms_wearable_internal_zzj;
            this.zzbmH = str;
            this.zzbmI = i;
            super(googleApiClient);
        }

        protected void zza(zzce com_google_android_gms_wearable_internal_zzce) throws RemoteException {
            com_google_android_gms_wearable_internal_zzce.zzg(this, this.zzbmH, this.zzbmI);
        }

        protected GetCapabilityResult zzbk(Status status) {
            return new zze(status, null);
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzbk(status);
        }
    }

    /* renamed from: com.google.android.gms.wearable.internal.zzj.2 */
    class C12602 extends zzi<GetAllCapabilitiesResult> {
        final /* synthetic */ int zzbmI;
        final /* synthetic */ zzj zzbmJ;

        C12602(zzj com_google_android_gms_wearable_internal_zzj, GoogleApiClient googleApiClient, int i) {
            this.zzbmJ = com_google_android_gms_wearable_internal_zzj;
            this.zzbmI = i;
            super(googleApiClient);
        }

        protected void zza(zzce com_google_android_gms_wearable_internal_zzce) throws RemoteException {
            com_google_android_gms_wearable_internal_zzce.zzb(this, this.zzbmI);
        }

        protected GetAllCapabilitiesResult zzbl(Status status) {
            return new zzd(status, null);
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzbl(status);
        }
    }

    /* renamed from: com.google.android.gms.wearable.internal.zzj.3 */
    class C12613 extends zzi<AddLocalCapabilityResult> {
        final /* synthetic */ String zzbmH;
        final /* synthetic */ zzj zzbmJ;

        C12613(zzj com_google_android_gms_wearable_internal_zzj, GoogleApiClient googleApiClient, String str) {
            this.zzbmJ = com_google_android_gms_wearable_internal_zzj;
            this.zzbmH = str;
            super(googleApiClient);
        }

        protected void zza(zzce com_google_android_gms_wearable_internal_zzce) throws RemoteException {
            com_google_android_gms_wearable_internal_zzce.zzr(this, this.zzbmH);
        }

        protected AddLocalCapabilityResult zzbm(Status status) {
            return new zza(status);
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzbm(status);
        }
    }

    /* renamed from: com.google.android.gms.wearable.internal.zzj.4 */
    class C12624 extends zzi<RemoveLocalCapabilityResult> {
        final /* synthetic */ String zzbmH;
        final /* synthetic */ zzj zzbmJ;

        C12624(zzj com_google_android_gms_wearable_internal_zzj, GoogleApiClient googleApiClient, String str) {
            this.zzbmJ = com_google_android_gms_wearable_internal_zzj;
            this.zzbmH = str;
            super(googleApiClient);
        }

        protected void zza(zzce com_google_android_gms_wearable_internal_zzce) throws RemoteException {
            com_google_android_gms_wearable_internal_zzce.zzs(this, this.zzbmH);
        }

        protected RemoveLocalCapabilityResult zzbn(Status status) {
            return new zza(status);
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzbn(status);
        }
    }

    private static final class zzf extends zzi<Status> {
        private CapabilityListener zzbmL;

        private zzf(GoogleApiClient googleApiClient, CapabilityListener capabilityListener) {
            super(googleApiClient);
            this.zzbmL = capabilityListener;
        }

        protected void zza(zzce com_google_android_gms_wearable_internal_zzce) throws RemoteException {
            com_google_android_gms_wearable_internal_zzce.zza((com.google.android.gms.internal.zzlx.zzb) this, this.zzbmL);
            this.zzbmL = null;
        }

        public Status zzb(Status status) {
            this.zzbmL = null;
            return status;
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzb(status);
        }
    }

    private PendingResult<Status> zza(GoogleApiClient googleApiClient, CapabilityListener capabilityListener, IntentFilter[] intentFilterArr) {
        return zzb.zza(googleApiClient, zza(intentFilterArr), capabilityListener);
    }

    private static zza<CapabilityListener> zza(IntentFilter[] intentFilterArr) {
        return new C08695(intentFilterArr);
    }

    public PendingResult<Status> addCapabilityListener(GoogleApiClient client, CapabilityListener listener, String capability) {
        zzx.zzb(capability != null, (Object) "capability must not be null");
        CapabilityListener com_google_android_gms_wearable_internal_zzj_zzb = new zzb(listener, capability);
        IntentFilter zzfY = zzcc.zzfY(CapabilityApi.ACTION_CAPABILITY_CHANGED);
        if (!capability.startsWith("/")) {
            capability = "/" + capability;
        }
        zzfY.addDataPath(capability, 0);
        return zza(client, com_google_android_gms_wearable_internal_zzj_zzb, new IntentFilter[]{zzfY});
    }

    public PendingResult<Status> addListener(GoogleApiClient client, CapabilityListener listener, Uri uri, int filterType) {
        zzx.zzb(uri != null, (Object) "uri must not be null");
        boolean z = filterType == 0 || filterType == 1;
        zzx.zzb(z, (Object) "invalid filter type");
        return zza(client, listener, new IntentFilter[]{zzcc.zza(CapabilityApi.ACTION_CAPABILITY_CHANGED, uri, filterType)});
    }

    public PendingResult<AddLocalCapabilityResult> addLocalCapability(GoogleApiClient client, String capability) {
        return client.zza(new C12613(this, client, capability));
    }

    public PendingResult<GetAllCapabilitiesResult> getAllCapabilities(GoogleApiClient client, int nodeFilter) {
        boolean z = true;
        if (!(nodeFilter == 0 || nodeFilter == 1)) {
            z = false;
        }
        zzx.zzab(z);
        return client.zza(new C12602(this, client, nodeFilter));
    }

    public PendingResult<GetCapabilityResult> getCapability(GoogleApiClient client, String capability, int nodeFilter) {
        boolean z = true;
        if (!(nodeFilter == 0 || nodeFilter == 1)) {
            z = false;
        }
        zzx.zzab(z);
        return client.zza(new C12591(this, client, capability, nodeFilter));
    }

    public PendingResult<Status> removeCapabilityListener(GoogleApiClient client, CapabilityListener listener, String capability) {
        return client.zza(new zzf(new zzb(listener, capability), null));
    }

    public PendingResult<Status> removeListener(GoogleApiClient client, CapabilityListener listener) {
        return client.zza(new zzf(listener, null));
    }

    public PendingResult<RemoveLocalCapabilityResult> removeLocalCapability(GoogleApiClient client, String capability) {
        return client.zza(new C12624(this, client, capability));
    }
}
