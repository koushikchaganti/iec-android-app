package com.google.android.gms.wearable.internal;

import android.content.IntentFilter;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.os.ParcelFileDescriptor.AutoCloseInputStream;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.internal.zzmn;
import com.google.android.gms.wearable.Asset;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataApi.DataItemResult;
import com.google.android.gms.wearable.DataApi.DataListener;
import com.google.android.gms.wearable.DataApi.DeleteDataItemsResult;
import com.google.android.gms.wearable.DataApi.GetFdForAssetResult;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataItemAsset;
import com.google.android.gms.wearable.DataItemBuffer;
import com.google.android.gms.wearable.PutDataRequest;
import java.io.IOException;
import java.io.InputStream;

public final class zzx implements DataApi {

    /* renamed from: com.google.android.gms.wearable.internal.zzx.8 */
    static class C08738 implements zza<DataListener> {
        final /* synthetic */ IntentFilter[] zzbmK;

        C08738(IntentFilter[] intentFilterArr) {
            this.zzbmK = intentFilterArr;
        }

        public void zza(zzce com_google_android_gms_wearable_internal_zzce, com.google.android.gms.internal.zzlx.zzb<Status> com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_common_api_Status, DataListener dataListener, zzmn<DataListener> com_google_android_gms_internal_zzmn_com_google_android_gms_wearable_DataApi_DataListener) throws RemoteException {
            com_google_android_gms_wearable_internal_zzce.zza((com.google.android.gms.internal.zzlx.zzb) com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_common_api_Status, dataListener, (zzmn) com_google_android_gms_internal_zzmn_com_google_android_gms_wearable_DataApi_DataListener, this.zzbmK);
        }
    }

    public static class zza implements DataItemResult {
        private final Status zzTA;
        private final DataItem zzbnq;

        public zza(Status status, DataItem dataItem) {
            this.zzTA = status;
            this.zzbnq = dataItem;
        }

        public DataItem getDataItem() {
            return this.zzbnq;
        }

        public Status getStatus() {
            return this.zzTA;
        }
    }

    public static class zzb implements DeleteDataItemsResult {
        private final Status zzTA;
        private final int zzbnr;

        public zzb(Status status, int i) {
            this.zzTA = status;
            this.zzbnr = i;
        }

        public int getNumDeleted() {
            return this.zzbnr;
        }

        public Status getStatus() {
            return this.zzTA;
        }
    }

    public static class zzc implements GetFdForAssetResult {
        private volatile boolean mClosed;
        private final Status zzTA;
        private volatile InputStream zzbnc;
        private volatile ParcelFileDescriptor zzbns;

        public zzc(Status status, ParcelFileDescriptor parcelFileDescriptor) {
            this.mClosed = false;
            this.zzTA = status;
            this.zzbns = parcelFileDescriptor;
        }

        public ParcelFileDescriptor getFd() {
            if (!this.mClosed) {
                return this.zzbns;
            }
            throw new IllegalStateException("Cannot access the file descriptor after release().");
        }

        public InputStream getInputStream() {
            if (this.mClosed) {
                throw new IllegalStateException("Cannot access the input stream after release().");
            } else if (this.zzbns == null) {
                return null;
            } else {
                if (this.zzbnc == null) {
                    this.zzbnc = new AutoCloseInputStream(this.zzbns);
                }
                return this.zzbnc;
            }
        }

        public Status getStatus() {
            return this.zzTA;
        }

        public void release() {
            if (this.zzbns != null) {
                if (this.mClosed) {
                    throw new IllegalStateException("releasing an already released result.");
                }
                try {
                    if (this.zzbnc != null) {
                        this.zzbnc.close();
                    } else {
                        this.zzbns.close();
                    }
                    this.mClosed = true;
                    this.zzbns = null;
                    this.zzbnc = null;
                } catch (IOException e) {
                }
            }
        }
    }

    /* renamed from: com.google.android.gms.wearable.internal.zzx.1 */
    class C12641 extends zzi<DataItemResult> {
        final /* synthetic */ PutDataRequest zzbnk;
        final /* synthetic */ zzx zzbnl;

        C12641(zzx com_google_android_gms_wearable_internal_zzx, GoogleApiClient googleApiClient, PutDataRequest putDataRequest) {
            this.zzbnl = com_google_android_gms_wearable_internal_zzx;
            this.zzbnk = putDataRequest;
            super(googleApiClient);
        }

        protected void zza(zzce com_google_android_gms_wearable_internal_zzce) throws RemoteException {
            com_google_android_gms_wearable_internal_zzce.zza((com.google.android.gms.internal.zzlx.zzb) this, this.zzbnk);
        }

        public DataItemResult zzbr(Status status) {
            return new zza(status, null);
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzbr(status);
        }
    }

    /* renamed from: com.google.android.gms.wearable.internal.zzx.2 */
    class C12652 extends zzi<DataItemResult> {
        final /* synthetic */ Uri zzaYf;
        final /* synthetic */ zzx zzbnl;

        C12652(zzx com_google_android_gms_wearable_internal_zzx, GoogleApiClient googleApiClient, Uri uri) {
            this.zzbnl = com_google_android_gms_wearable_internal_zzx;
            this.zzaYf = uri;
            super(googleApiClient);
        }

        protected void zza(zzce com_google_android_gms_wearable_internal_zzce) throws RemoteException {
            com_google_android_gms_wearable_internal_zzce.zza((com.google.android.gms.internal.zzlx.zzb) this, this.zzaYf);
        }

        protected DataItemResult zzbr(Status status) {
            return new zza(status, null);
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzbr(status);
        }
    }

    /* renamed from: com.google.android.gms.wearable.internal.zzx.3 */
    class C12663 extends zzi<DataItemBuffer> {
        final /* synthetic */ zzx zzbnl;

        C12663(zzx com_google_android_gms_wearable_internal_zzx, GoogleApiClient googleApiClient) {
            this.zzbnl = com_google_android_gms_wearable_internal_zzx;
            super(googleApiClient);
        }

        protected void zza(zzce com_google_android_gms_wearable_internal_zzce) throws RemoteException {
            com_google_android_gms_wearable_internal_zzce.zzo(this);
        }

        protected DataItemBuffer zzbs(Status status) {
            return new DataItemBuffer(DataHolder.zzbJ(status.getStatusCode()));
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzbs(status);
        }
    }

    /* renamed from: com.google.android.gms.wearable.internal.zzx.4 */
    class C12674 extends zzi<DataItemBuffer> {
        final /* synthetic */ Uri zzaYf;
        final /* synthetic */ zzx zzbnl;
        final /* synthetic */ int zzbnm;

        C12674(zzx com_google_android_gms_wearable_internal_zzx, GoogleApiClient googleApiClient, Uri uri, int i) {
            this.zzbnl = com_google_android_gms_wearable_internal_zzx;
            this.zzaYf = uri;
            this.zzbnm = i;
            super(googleApiClient);
        }

        protected void zza(zzce com_google_android_gms_wearable_internal_zzce) throws RemoteException {
            com_google_android_gms_wearable_internal_zzce.zza((com.google.android.gms.internal.zzlx.zzb) this, this.zzaYf, this.zzbnm);
        }

        protected DataItemBuffer zzbs(Status status) {
            return new DataItemBuffer(DataHolder.zzbJ(status.getStatusCode()));
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzbs(status);
        }
    }

    /* renamed from: com.google.android.gms.wearable.internal.zzx.5 */
    class C12685 extends zzi<DeleteDataItemsResult> {
        final /* synthetic */ Uri zzaYf;
        final /* synthetic */ zzx zzbnl;
        final /* synthetic */ int zzbnm;

        C12685(zzx com_google_android_gms_wearable_internal_zzx, GoogleApiClient googleApiClient, Uri uri, int i) {
            this.zzbnl = com_google_android_gms_wearable_internal_zzx;
            this.zzaYf = uri;
            this.zzbnm = i;
            super(googleApiClient);
        }

        protected void zza(zzce com_google_android_gms_wearable_internal_zzce) throws RemoteException {
            com_google_android_gms_wearable_internal_zzce.zzb(this, this.zzaYf, this.zzbnm);
        }

        protected DeleteDataItemsResult zzbt(Status status) {
            return new zzb(status, 0);
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzbt(status);
        }
    }

    /* renamed from: com.google.android.gms.wearable.internal.zzx.6 */
    class C12696 extends zzi<GetFdForAssetResult> {
        final /* synthetic */ zzx zzbnl;
        final /* synthetic */ Asset zzbnn;

        C12696(zzx com_google_android_gms_wearable_internal_zzx, GoogleApiClient googleApiClient, Asset asset) {
            this.zzbnl = com_google_android_gms_wearable_internal_zzx;
            this.zzbnn = asset;
            super(googleApiClient);
        }

        protected void zza(zzce com_google_android_gms_wearable_internal_zzce) throws RemoteException {
            com_google_android_gms_wearable_internal_zzce.zza((com.google.android.gms.internal.zzlx.zzb) this, this.zzbnn);
        }

        protected GetFdForAssetResult zzbu(Status status) {
            return new zzc(status, null);
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzbu(status);
        }
    }

    /* renamed from: com.google.android.gms.wearable.internal.zzx.7 */
    class C12707 extends zzi<GetFdForAssetResult> {
        final /* synthetic */ zzx zzbnl;
        final /* synthetic */ DataItemAsset zzbno;

        C12707(zzx com_google_android_gms_wearable_internal_zzx, GoogleApiClient googleApiClient, DataItemAsset dataItemAsset) {
            this.zzbnl = com_google_android_gms_wearable_internal_zzx;
            this.zzbno = dataItemAsset;
            super(googleApiClient);
        }

        protected void zza(zzce com_google_android_gms_wearable_internal_zzce) throws RemoteException {
            com_google_android_gms_wearable_internal_zzce.zza((com.google.android.gms.internal.zzlx.zzb) this, this.zzbno);
        }

        protected GetFdForAssetResult zzbu(Status status) {
            return new zzc(status, null);
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzbu(status);
        }
    }

    /* renamed from: com.google.android.gms.wearable.internal.zzx.9 */
    class C12719 extends zzi<Status> {
        final /* synthetic */ zzx zzbnl;
        final /* synthetic */ DataListener zzbnp;

        C12719(zzx com_google_android_gms_wearable_internal_zzx, GoogleApiClient googleApiClient, DataListener dataListener) {
            this.zzbnl = com_google_android_gms_wearable_internal_zzx;
            this.zzbnp = dataListener;
            super(googleApiClient);
        }

        protected void zza(zzce com_google_android_gms_wearable_internal_zzce) throws RemoteException {
            com_google_android_gms_wearable_internal_zzce.zza((com.google.android.gms.internal.zzlx.zzb) this, this.zzbnp);
        }

        public Status zzb(Status status) {
            return status;
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzb(status);
        }
    }

    private PendingResult<Status> zza(GoogleApiClient googleApiClient, DataListener dataListener, IntentFilter[] intentFilterArr) {
        return zzb.zza(googleApiClient, zza(intentFilterArr), dataListener);
    }

    private static zza<DataListener> zza(IntentFilter[] intentFilterArr) {
        return new C08738(intentFilterArr);
    }

    private void zza(Asset asset) {
        if (asset == null) {
            throw new IllegalArgumentException("asset is null");
        } else if (asset.getDigest() == null) {
            throw new IllegalArgumentException("invalid asset");
        } else if (asset.getData() != null) {
            throw new IllegalArgumentException("invalid asset");
        }
    }

    public PendingResult<Status> addListener(GoogleApiClient client, DataListener listener) {
        return zza(client, listener, new IntentFilter[]{zzcc.zzfY(DataApi.ACTION_DATA_CHANGED)});
    }

    public PendingResult<Status> addListener(GoogleApiClient client, DataListener listener, Uri uri, int filterType) {
        com.google.android.gms.common.internal.zzx.zzb(uri != null, (Object) "uri must not be null");
        boolean z = filterType == 0 || filterType == 1;
        com.google.android.gms.common.internal.zzx.zzb(z, (Object) "invalid filter type");
        return zza(client, listener, new IntentFilter[]{zzcc.zza(DataApi.ACTION_DATA_CHANGED, uri, filterType)});
    }

    public PendingResult<DeleteDataItemsResult> deleteDataItems(GoogleApiClient client, Uri uri) {
        return deleteDataItems(client, uri, 0);
    }

    public PendingResult<DeleteDataItemsResult> deleteDataItems(GoogleApiClient client, Uri uri, int filterType) {
        boolean z = false;
        com.google.android.gms.common.internal.zzx.zzb(uri != null, (Object) "uri must not be null");
        if (filterType == 0 || filterType == 1) {
            z = true;
        }
        com.google.android.gms.common.internal.zzx.zzb(z, (Object) "invalid filter type");
        return client.zza(new C12685(this, client, uri, filterType));
    }

    public PendingResult<DataItemResult> getDataItem(GoogleApiClient client, Uri uri) {
        return client.zza(new C12652(this, client, uri));
    }

    public PendingResult<DataItemBuffer> getDataItems(GoogleApiClient client) {
        return client.zza(new C12663(this, client));
    }

    public PendingResult<DataItemBuffer> getDataItems(GoogleApiClient client, Uri uri) {
        return getDataItems(client, uri, 0);
    }

    public PendingResult<DataItemBuffer> getDataItems(GoogleApiClient client, Uri uri, int filterType) {
        boolean z = false;
        com.google.android.gms.common.internal.zzx.zzb(uri != null, (Object) "uri must not be null");
        if (filterType == 0 || filterType == 1) {
            z = true;
        }
        com.google.android.gms.common.internal.zzx.zzb(z, (Object) "invalid filter type");
        return client.zza(new C12674(this, client, uri, filterType));
    }

    public PendingResult<GetFdForAssetResult> getFdForAsset(GoogleApiClient client, Asset asset) {
        zza(asset);
        return client.zza(new C12696(this, client, asset));
    }

    public PendingResult<GetFdForAssetResult> getFdForAsset(GoogleApiClient client, DataItemAsset asset) {
        return client.zza(new C12707(this, client, asset));
    }

    public PendingResult<DataItemResult> putDataItem(GoogleApiClient client, PutDataRequest request) {
        return client.zza(new C12641(this, client, request));
    }

    public PendingResult<Status> removeListener(GoogleApiClient client, DataListener listener) {
        return client.zza(new C12719(this, client, listener));
    }
}
