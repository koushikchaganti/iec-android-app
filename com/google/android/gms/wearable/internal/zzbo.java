package com.google.android.gms.wearable.internal;

import android.content.IntentFilter;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.zzmn;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.NodeApi.GetConnectedNodesResult;
import com.google.android.gms.wearable.NodeApi.GetLocalNodeResult;
import com.google.android.gms.wearable.NodeApi.NodeListener;
import java.util.ArrayList;
import java.util.List;

public final class zzbo implements NodeApi {

    /* renamed from: com.google.android.gms.wearable.internal.zzbo.3 */
    static class C08593 implements zza<NodeListener> {
        final /* synthetic */ IntentFilter[] zzbmK;

        C08593(IntentFilter[] intentFilterArr) {
            this.zzbmK = intentFilterArr;
        }

        public void zza(zzce com_google_android_gms_wearable_internal_zzce, com.google.android.gms.internal.zzlx.zzb<Status> com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_common_api_Status, NodeListener nodeListener, zzmn<NodeListener> com_google_android_gms_internal_zzmn_com_google_android_gms_wearable_NodeApi_NodeListener) throws RemoteException {
            com_google_android_gms_wearable_internal_zzce.zza((com.google.android.gms.internal.zzlx.zzb) com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_common_api_Status, nodeListener, (zzmn) com_google_android_gms_internal_zzmn_com_google_android_gms_wearable_NodeApi_NodeListener, this.zzbmK);
        }
    }

    public static class zza implements GetConnectedNodesResult {
        private final Status zzTA;
        private final List<Node> zzbox;

        public zza(Status status, List<Node> list) {
            this.zzTA = status;
            this.zzbox = list;
        }

        public List<Node> getNodes() {
            return this.zzbox;
        }

        public Status getStatus() {
            return this.zzTA;
        }
    }

    public static class zzb implements GetLocalNodeResult {
        private final Status zzTA;
        private final Node zzboy;

        public zzb(Status status, Node node) {
            this.zzTA = status;
            this.zzboy = node;
        }

        public Node getNode() {
            return this.zzboy;
        }

        public Status getStatus() {
            return this.zzTA;
        }
    }

    /* renamed from: com.google.android.gms.wearable.internal.zzbo.1 */
    class C12561 extends zzi<GetLocalNodeResult> {
        final /* synthetic */ zzbo zzbov;

        C12561(zzbo com_google_android_gms_wearable_internal_zzbo, GoogleApiClient googleApiClient) {
            this.zzbov = com_google_android_gms_wearable_internal_zzbo;
            super(googleApiClient);
        }

        protected void zza(zzce com_google_android_gms_wearable_internal_zzce) throws RemoteException {
            com_google_android_gms_wearable_internal_zzce.zzp(this);
        }

        protected GetLocalNodeResult zzbw(Status status) {
            return new zzb(status, null);
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzbw(status);
        }
    }

    /* renamed from: com.google.android.gms.wearable.internal.zzbo.2 */
    class C12572 extends zzi<GetConnectedNodesResult> {
        final /* synthetic */ zzbo zzbov;

        C12572(zzbo com_google_android_gms_wearable_internal_zzbo, GoogleApiClient googleApiClient) {
            this.zzbov = com_google_android_gms_wearable_internal_zzbo;
            super(googleApiClient);
        }

        protected void zza(zzce com_google_android_gms_wearable_internal_zzce) throws RemoteException {
            com_google_android_gms_wearable_internal_zzce.zzq(this);
        }

        protected GetConnectedNodesResult zzbx(Status status) {
            return new zza(status, new ArrayList());
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzbx(status);
        }
    }

    /* renamed from: com.google.android.gms.wearable.internal.zzbo.4 */
    class C12584 extends zzi<Status> {
        final /* synthetic */ zzbo zzbov;
        final /* synthetic */ NodeListener zzbow;

        C12584(zzbo com_google_android_gms_wearable_internal_zzbo, GoogleApiClient googleApiClient, NodeListener nodeListener) {
            this.zzbov = com_google_android_gms_wearable_internal_zzbo;
            this.zzbow = nodeListener;
            super(googleApiClient);
        }

        protected void zza(zzce com_google_android_gms_wearable_internal_zzce) throws RemoteException {
            com_google_android_gms_wearable_internal_zzce.zza((com.google.android.gms.internal.zzlx.zzb) this, this.zzbow);
        }

        public Status zzb(Status status) {
            return status;
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzb(status);
        }
    }

    private static zza<NodeListener> zza(IntentFilter[] intentFilterArr) {
        return new C08593(intentFilterArr);
    }

    public PendingResult<Status> addListener(GoogleApiClient client, NodeListener listener) {
        return zzb.zza(client, zza(new IntentFilter[]{zzcc.zzfY("com.google.android.gms.wearable.NODE_CHANGED")}), listener);
    }

    public PendingResult<GetConnectedNodesResult> getConnectedNodes(GoogleApiClient client) {
        return client.zza(new C12572(this, client));
    }

    public PendingResult<GetLocalNodeResult> getLocalNode(GoogleApiClient client) {
        return client.zza(new C12561(this, client));
    }

    public PendingResult<Status> removeListener(GoogleApiClient client, NodeListener listener) {
        return client.zza(new C12584(this, client, listener));
    }
}
