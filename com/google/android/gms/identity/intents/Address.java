package com.google.android.gms.identity.intents;

import android.app.Activity;
import android.content.Context;
import android.os.Looper;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions.HasOptions;
import com.google.android.gms.common.api.Api.zzc;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzqe;

public final class Address {
    public static final Api<AddressOptions> API;
    static final zzc<zzqe> zzTo;
    private static final com.google.android.gms.common.api.Api.zza<zzqe, AddressOptions> zzTp;

    /* renamed from: com.google.android.gms.identity.intents.Address.1 */
    static class C07291 extends com.google.android.gms.common.api.Api.zza<zzqe, AddressOptions> {
        C07291() {
        }

        public zzqe zza(Context context, Looper looper, zzf com_google_android_gms_common_internal_zzf, AddressOptions addressOptions, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            zzx.zzb(context instanceof Activity, (Object) "An Activity must be used for Address APIs");
            if (addressOptions == null) {
                addressOptions = new AddressOptions();
            }
            return new zzqe((Activity) context, looper, com_google_android_gms_common_internal_zzf, addressOptions.theme, connectionCallbacks, onConnectionFailedListener);
        }
    }

    public static final class AddressOptions implements HasOptions {
        public final int theme;

        public AddressOptions() {
            this.theme = 0;
        }

        public AddressOptions(int theme) {
            this.theme = theme;
        }
    }

    private static abstract class zza extends com.google.android.gms.internal.zzlx.zza<Status, zzqe> {
        public zza(GoogleApiClient googleApiClient) {
            super(Address.zzTo, googleApiClient);
        }

        public Status zzb(Status status) {
            return status;
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzb(status);
        }
    }

    /* renamed from: com.google.android.gms.identity.intents.Address.2 */
    static class C12262 extends zza {
        final /* synthetic */ UserAddressRequest zzaJL;
        final /* synthetic */ int zzaJM;

        C12262(GoogleApiClient googleApiClient, UserAddressRequest userAddressRequest, int i) {
            this.zzaJL = userAddressRequest;
            this.zzaJM = i;
            super(googleApiClient);
        }

        protected void zza(zzqe com_google_android_gms_internal_zzqe) throws RemoteException {
            com_google_android_gms_internal_zzqe.zza(this.zzaJL, this.zzaJM);
            zzb(Status.zzaeX);
        }
    }

    static {
        zzTo = new zzc();
        zzTp = new C07291();
        API = new Api("Address.API", zzTp, zzTo);
    }

    public static void requestUserAddress(GoogleApiClient googleApiClient, UserAddressRequest request, int requestCode) {
        googleApiClient.zza(new C12262(googleApiClient, request, requestCode));
    }
}
