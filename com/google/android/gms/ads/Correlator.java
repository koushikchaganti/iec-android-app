package com.google.android.gms.ads;

import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.internal.zzha;

@zzha
public final class Correlator {
    private zzm zzoz;

    public Correlator() {
        this.zzoz = new zzm();
    }

    public void reset() {
        this.zzoz.zzcS();
    }

    public zzm zzaH() {
        return this.zzoz;
    }
}
