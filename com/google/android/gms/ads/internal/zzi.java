package com.google.android.gms.ads.internal;

import android.content.Context;
import android.support.v4.util.SimpleArrayMap;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzo;
import com.google.android.gms.ads.internal.client.zzp.zza;
import com.google.android.gms.ads.internal.client.zzv;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.internal.zzcx;
import com.google.android.gms.internal.zzcy;
import com.google.android.gms.internal.zzcz;
import com.google.android.gms.internal.zzda;
import com.google.android.gms.internal.zzew;
import com.google.android.gms.internal.zzha;
import com.google.android.gms.internal.zzip;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

@zzha
public class zzi extends zza {
    private final Context mContext;
    private final zzcx zzpA;
    private final zzcy zzpB;
    private final SimpleArrayMap<String, zzda> zzpC;
    private final SimpleArrayMap<String, zzcz> zzpD;
    private final NativeAdOptionsParcel zzpE;
    private final List<String> zzpF;
    private final zzv zzpG;
    private final String zzpH;
    private final VersionInfoParcel zzpI;
    private WeakReference<zzn> zzpJ;
    private final Object zzpK;
    private final zzew zzpd;
    private final zzo zzpz;

    /* renamed from: com.google.android.gms.ads.internal.zzi.1 */
    class C02471 implements Runnable {
        final /* synthetic */ AdRequestParcel zzpL;
        final /* synthetic */ zzi zzpM;

        C02471(zzi com_google_android_gms_ads_internal_zzi, AdRequestParcel adRequestParcel) {
            this.zzpM = com_google_android_gms_ads_internal_zzi;
            this.zzpL = adRequestParcel;
        }

        public void run() {
            synchronized (this.zzpM.zzpK) {
                zzn zzbl = this.zzpM.zzbl();
                this.zzpM.zzpJ = new WeakReference(zzbl);
                zzbl.zzb(this.zzpM.zzpA);
                zzbl.zzb(this.zzpM.zzpB);
                zzbl.zza(this.zzpM.zzpC);
                zzbl.zza(this.zzpM.zzpz);
                zzbl.zzb(this.zzpM.zzpD);
                zzbl.zza(this.zzpM.zzbk());
                zzbl.zzb(this.zzpM.zzpE);
                zzbl.zza(this.zzpM.zzpG);
                zzbl.zzb(this.zzpL);
            }
        }
    }

    zzi(Context context, String str, zzew com_google_android_gms_internal_zzew, VersionInfoParcel versionInfoParcel, zzo com_google_android_gms_ads_internal_client_zzo, zzcx com_google_android_gms_internal_zzcx, zzcy com_google_android_gms_internal_zzcy, SimpleArrayMap<String, zzda> simpleArrayMap, SimpleArrayMap<String, zzcz> simpleArrayMap2, NativeAdOptionsParcel nativeAdOptionsParcel, zzv com_google_android_gms_ads_internal_client_zzv) {
        this.zzpK = new Object();
        this.mContext = context;
        this.zzpH = str;
        this.zzpd = com_google_android_gms_internal_zzew;
        this.zzpI = versionInfoParcel;
        this.zzpz = com_google_android_gms_ads_internal_client_zzo;
        this.zzpB = com_google_android_gms_internal_zzcy;
        this.zzpA = com_google_android_gms_internal_zzcx;
        this.zzpC = simpleArrayMap;
        this.zzpD = simpleArrayMap2;
        this.zzpE = nativeAdOptionsParcel;
        this.zzpF = zzbk();
        this.zzpG = com_google_android_gms_ads_internal_client_zzv;
    }

    private List<String> zzbk() {
        List<String> arrayList = new ArrayList();
        if (this.zzpB != null) {
            arrayList.add("1");
        }
        if (this.zzpA != null) {
            arrayList.add("2");
        }
        if (this.zzpC.size() > 0) {
            arrayList.add("3");
        }
        return arrayList;
    }

    public String getMediationAdapterClassName() {
        synchronized (this.zzpK) {
            if (this.zzpJ != null) {
                zzn com_google_android_gms_ads_internal_zzn = (zzn) this.zzpJ.get();
                String mediationAdapterClassName = com_google_android_gms_ads_internal_zzn != null ? com_google_android_gms_ads_internal_zzn.getMediationAdapterClassName() : null;
                return mediationAdapterClassName;
            }
            return null;
        }
    }

    public boolean isLoading() {
        synchronized (this.zzpK) {
            if (this.zzpJ != null) {
                zzn com_google_android_gms_ads_internal_zzn = (zzn) this.zzpJ.get();
                boolean isLoading = com_google_android_gms_ads_internal_zzn != null ? com_google_android_gms_ads_internal_zzn.isLoading() : false;
                return isLoading;
            }
            return false;
        }
    }

    protected void runOnUiThread(Runnable runnable) {
        zzip.zzKO.post(runnable);
    }

    protected zzn zzbl() {
        return new zzn(this.mContext, AdSizeParcel.zzt(this.mContext), this.zzpH, this.zzpd, this.zzpI);
    }

    public void zzf(AdRequestParcel adRequestParcel) {
        runOnUiThread(new C02471(this, adRequestParcel));
    }
}
