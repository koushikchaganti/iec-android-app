package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.internal.reward.client.zzf;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.internal.zzdb;
import com.google.android.gms.internal.zzha;

@zzha
public class zzl {
    private static final Object zzqf;
    private static zzl zzud;
    private final zza zzue;
    private final zze zzuf;
    private final zzad zzug;
    private final zzdb zzuh;
    private final zzf zzui;

    static {
        zzqf = new Object();
        zza(new zzl());
    }

    protected zzl() {
        this.zzue = new zza();
        this.zzuf = new zze();
        this.zzug = new zzad();
        this.zzuh = new zzdb();
        this.zzui = new zzf();
    }

    protected static void zza(zzl com_google_android_gms_ads_internal_client_zzl) {
        synchronized (zzqf) {
            zzud = com_google_android_gms_ads_internal_client_zzl;
        }
    }

    private static zzl zzcM() {
        zzl com_google_android_gms_ads_internal_client_zzl;
        synchronized (zzqf) {
            com_google_android_gms_ads_internal_client_zzl = zzud;
        }
        return com_google_android_gms_ads_internal_client_zzl;
    }

    public static zza zzcN() {
        return zzcM().zzue;
    }

    public static zze zzcO() {
        return zzcM().zzuf;
    }

    public static zzad zzcP() {
        return zzcM().zzug;
    }

    public static zzdb zzcQ() {
        return zzcM().zzuh;
    }

    public static zzf zzcR() {
        return zzcM().zzui;
    }
}
