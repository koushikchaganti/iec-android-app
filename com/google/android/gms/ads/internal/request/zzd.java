package com.google.android.gms.ads.internal.request;

import android.content.Context;
import android.os.Binder;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Looper;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzp;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.internal.zzbs;
import com.google.android.gms.internal.zzbz;
import com.google.android.gms.internal.zzha;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzhc;
import com.google.android.gms.internal.zzir;
import com.google.android.gms.internal.zzjg;
import com.google.android.gms.internal.zzjg.zzc;

@zzha
public abstract class zzd implements com.google.android.gms.ads.internal.request.zzc.zza, zzir<Void> {
    private final zzjg<AdRequestInfoParcel> zzGi;
    private final com.google.android.gms.ads.internal.request.zzc.zza zzGj;
    private final Object zzpK;

    /* renamed from: com.google.android.gms.ads.internal.request.zzd.1 */
    class C06711 implements zzc<AdRequestInfoParcel> {
        final /* synthetic */ zzj zzGk;
        final /* synthetic */ zzd zzGl;

        C06711(zzd com_google_android_gms_ads_internal_request_zzd, zzj com_google_android_gms_ads_internal_request_zzj) {
            this.zzGl = com_google_android_gms_ads_internal_request_zzd;
            this.zzGk = com_google_android_gms_ads_internal_request_zzj;
        }

        public void zzc(AdRequestInfoParcel adRequestInfoParcel) {
            if (!this.zzGl.zza(this.zzGk, adRequestInfoParcel)) {
                this.zzGl.zzge();
            }
        }
    }

    /* renamed from: com.google.android.gms.ads.internal.request.zzd.2 */
    class C06722 implements com.google.android.gms.internal.zzjg.zza {
        final /* synthetic */ zzd zzGl;

        C06722(zzd com_google_android_gms_ads_internal_request_zzd) {
            this.zzGl = com_google_android_gms_ads_internal_request_zzd;
        }

        public void run() {
            this.zzGl.zzge();
        }
    }

    @zzha
    public static final class zza extends zzd {
        private final Context mContext;

        public zza(Context context, zzjg<AdRequestInfoParcel> com_google_android_gms_internal_zzjg_com_google_android_gms_ads_internal_request_AdRequestInfoParcel, com.google.android.gms.ads.internal.request.zzc.zza com_google_android_gms_ads_internal_request_zzc_zza) {
            super(com_google_android_gms_internal_zzjg_com_google_android_gms_ads_internal_request_AdRequestInfoParcel, com_google_android_gms_ads_internal_request_zzc_zza);
            this.mContext = context;
        }

        public /* synthetic */ Object zzfR() {
            return super.zzfO();
        }

        public void zzge() {
        }

        public zzj zzgf() {
            return zzhc.zza(this.mContext, new zzbs((String) zzbz.zzvg.get()), zzhb.zzgn());
        }
    }

    @zzha
    public static class zzb extends zzd implements ConnectionCallbacks, OnConnectionFailedListener {
        private Context mContext;
        private zzjg<AdRequestInfoParcel> zzGi;
        private final com.google.android.gms.ads.internal.request.zzc.zza zzGj;
        protected zze zzGm;
        private boolean zzGn;
        private VersionInfoParcel zzpI;
        private final Object zzpK;

        public zzb(Context context, VersionInfoParcel versionInfoParcel, zzjg<AdRequestInfoParcel> com_google_android_gms_internal_zzjg_com_google_android_gms_ads_internal_request_AdRequestInfoParcel, com.google.android.gms.ads.internal.request.zzc.zza com_google_android_gms_ads_internal_request_zzc_zza) {
            Looper zzhk;
            super(com_google_android_gms_internal_zzjg_com_google_android_gms_ads_internal_request_AdRequestInfoParcel, com_google_android_gms_ads_internal_request_zzc_zza);
            this.zzpK = new Object();
            this.mContext = context;
            this.zzpI = versionInfoParcel;
            this.zzGi = com_google_android_gms_internal_zzjg_com_google_android_gms_ads_internal_request_AdRequestInfoParcel;
            this.zzGj = com_google_android_gms_ads_internal_request_zzc_zza;
            if (((Boolean) zzbz.zzvF.get()).booleanValue()) {
                this.zzGn = true;
                zzhk = zzp.zzbJ().zzhk();
            } else {
                zzhk = context.getMainLooper();
            }
            this.zzGm = new zze(context, zzhk, this, this, this.zzpI.zzLG);
            connect();
        }

        protected void connect() {
            this.zzGm.zzqp();
        }

        public void onConnected(Bundle connectionHint) {
            zzfO();
        }

        public void onConnectionFailed(ConnectionResult result) {
            com.google.android.gms.ads.internal.util.client.zzb.zzaF("Cannot connect to remote service, fallback to local instance.");
            zzgg().zzfR();
            Bundle bundle = new Bundle();
            bundle.putString("action", "gms_connection_failed_fallback_to_local");
            zzp.zzbx().zzb(this.mContext, this.zzpI.afmaVersion, "gmob-apps", bundle, true);
        }

        public void onConnectionSuspended(int cause) {
            com.google.android.gms.ads.internal.util.client.zzb.zzaF("Disconnected from remote ad request service.");
        }

        public /* synthetic */ Object zzfR() {
            return super.zzfO();
        }

        public void zzge() {
            synchronized (this.zzpK) {
                if (this.zzGm.isConnected() || this.zzGm.isConnecting()) {
                    this.zzGm.disconnect();
                }
                Binder.flushPendingCommands();
                if (this.zzGn) {
                    zzp.zzbJ().zzhl();
                    this.zzGn = false;
                }
            }
        }

        public zzj zzgf() {
            zzj zzgj;
            synchronized (this.zzpK) {
                try {
                    zzgj = this.zzGm.zzgj();
                } catch (IllegalStateException e) {
                    zzgj = null;
                    return zzgj;
                } catch (DeadObjectException e2) {
                    zzgj = null;
                    return zzgj;
                }
            }
            return zzgj;
        }

        zzir zzgg() {
            return new zza(this.mContext, this.zzGi, this.zzGj);
        }
    }

    public zzd(zzjg<AdRequestInfoParcel> com_google_android_gms_internal_zzjg_com_google_android_gms_ads_internal_request_AdRequestInfoParcel, com.google.android.gms.ads.internal.request.zzc.zza com_google_android_gms_ads_internal_request_zzc_zza) {
        this.zzpK = new Object();
        this.zzGi = com_google_android_gms_internal_zzjg_com_google_android_gms_ads_internal_request_AdRequestInfoParcel;
        this.zzGj = com_google_android_gms_ads_internal_request_zzc_zza;
    }

    public void cancel() {
        zzge();
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    boolean zza(com.google.android.gms.ads.internal.request.zzj r5, com.google.android.gms.ads.internal.request.AdRequestInfoParcel r6) {
        /*
        r4 = this;
        r1 = 0;
        r0 = 1;
        r2 = new com.google.android.gms.ads.internal.request.zzg;	 Catch:{ RemoteException -> 0x000b, NullPointerException -> 0x0024, SecurityException -> 0x0032, Throwable -> 0x0040 }
        r2.<init>(r4);	 Catch:{ RemoteException -> 0x000b, NullPointerException -> 0x0024, SecurityException -> 0x0032, Throwable -> 0x0040 }
        r5.zza(r6, r2);	 Catch:{ RemoteException -> 0x000b, NullPointerException -> 0x0024, SecurityException -> 0x0032, Throwable -> 0x0040 }
    L_0x000a:
        return r0;
    L_0x000b:
        r2 = move-exception;
        r3 = "Could not fetch ad response from ad request service.";
        com.google.android.gms.ads.internal.util.client.zzb.zzd(r3, r2);
        r3 = com.google.android.gms.ads.internal.zzp.zzbA();
        r3.zzb(r2, r0);
    L_0x0018:
        r0 = r4.zzGj;
        r2 = new com.google.android.gms.ads.internal.request.AdResponseParcel;
        r2.<init>(r1);
        r0.zzb(r2);
        r0 = r1;
        goto L_0x000a;
    L_0x0024:
        r2 = move-exception;
        r3 = "Could not fetch ad response from ad request service due to an Exception.";
        com.google.android.gms.ads.internal.util.client.zzb.zzd(r3, r2);
        r3 = com.google.android.gms.ads.internal.zzp.zzbA();
        r3.zzb(r2, r0);
        goto L_0x0018;
    L_0x0032:
        r2 = move-exception;
        r3 = "Could not fetch ad response from ad request service due to an Exception.";
        com.google.android.gms.ads.internal.util.client.zzb.zzd(r3, r2);
        r3 = com.google.android.gms.ads.internal.zzp.zzbA();
        r3.zzb(r2, r0);
        goto L_0x0018;
    L_0x0040:
        r2 = move-exception;
        r3 = "Could not fetch ad response from ad request service due to an Exception.";
        com.google.android.gms.ads.internal.util.client.zzb.zzd(r3, r2);
        r3 = com.google.android.gms.ads.internal.zzp.zzbA();
        r3.zzb(r2, r0);
        goto L_0x0018;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.internal.request.zzd.zza(com.google.android.gms.ads.internal.request.zzj, com.google.android.gms.ads.internal.request.AdRequestInfoParcel):boolean");
    }

    public void zzb(AdResponseParcel adResponseParcel) {
        synchronized (this.zzpK) {
            this.zzGj.zzb(adResponseParcel);
            zzge();
        }
    }

    public Void zzfO() {
        zzj zzgf = zzgf();
        if (zzgf == null) {
            this.zzGj.zzb(new AdResponseParcel(0));
            zzge();
        } else {
            this.zzGi.zza(new C06711(this, zzgf), new C06722(this));
        }
        return null;
    }

    public /* synthetic */ Object zzfR() {
        return zzfO();
    }

    public abstract void zzge();

    public abstract zzj zzgf();
}
