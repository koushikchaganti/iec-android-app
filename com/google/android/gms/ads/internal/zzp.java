package com.google.android.gms.ads.internal;

import android.os.Build.VERSION;
import com.google.android.gms.ads.internal.overlay.zze;
import com.google.android.gms.ads.internal.purchase.zzi;
import com.google.android.gms.ads.internal.request.zza;
import com.google.android.gms.internal.zzbw;
import com.google.android.gms.internal.zzbx;
import com.google.android.gms.internal.zzby;
import com.google.android.gms.internal.zzcc;
import com.google.android.gms.internal.zzdv;
import com.google.android.gms.internal.zzed;
import com.google.android.gms.internal.zzes;
import com.google.android.gms.internal.zzgq;
import com.google.android.gms.internal.zzha;
import com.google.android.gms.internal.zzhj;
import com.google.android.gms.internal.zzig;
import com.google.android.gms.internal.zzip;
import com.google.android.gms.internal.zziq;
import com.google.android.gms.internal.zziv;
import com.google.android.gms.internal.zzjp;
import com.google.android.gms.internal.zznl;
import com.google.android.gms.internal.zzno;

@zzha
public class zzp {
    private static final Object zzqf;
    private static zzp zzqu;
    private final zzjp zzqA;
    private final zziq zzqB;
    private final zzig zzqC;
    private final zznl zzqD;
    private final zzcc zzqE;
    private final zzhj zzqF;
    private final zzbx zzqG;
    private final zzbw zzqH;
    private final zzby zzqI;
    private final zzi zzqJ;
    private final zzed zzqK;
    private final zziv zzqL;
    private final zzes zzqM;
    private final zzdv zzqN;
    private final zza zzqv;
    private final com.google.android.gms.ads.internal.overlay.zza zzqw;
    private final zze zzqx;
    private final zzgq zzqy;
    private final zzip zzqz;

    static {
        zzqf = new Object();
        zza(new zzp());
    }

    protected zzp() {
        this.zzqv = new zza();
        this.zzqw = new com.google.android.gms.ads.internal.overlay.zza();
        this.zzqx = new zze();
        this.zzqy = new zzgq();
        this.zzqz = new zzip();
        this.zzqA = new zzjp();
        this.zzqB = zziq.zzP(VERSION.SDK_INT);
        this.zzqC = new zzig(this.zzqz);
        this.zzqD = new zzno();
        this.zzqE = new zzcc();
        this.zzqF = new zzhj();
        this.zzqG = new zzbx();
        this.zzqH = new zzbw();
        this.zzqI = new zzby();
        this.zzqJ = new zzi();
        this.zzqK = new zzed();
        this.zzqL = new zziv();
        this.zzqM = new zzes();
        this.zzqN = new zzdv();
    }

    protected static void zza(zzp com_google_android_gms_ads_internal_zzp) {
        synchronized (zzqf) {
            zzqu = com_google_android_gms_ads_internal_zzp;
        }
    }

    public static zzig zzbA() {
        return zzbs().zzqC;
    }

    public static zznl zzbB() {
        return zzbs().zzqD;
    }

    public static zzcc zzbC() {
        return zzbs().zzqE;
    }

    public static zzhj zzbD() {
        return zzbs().zzqF;
    }

    public static zzbx zzbE() {
        return zzbs().zzqG;
    }

    public static zzbw zzbF() {
        return zzbs().zzqH;
    }

    public static zzby zzbG() {
        return zzbs().zzqI;
    }

    public static zzi zzbH() {
        return zzbs().zzqJ;
    }

    public static zzed zzbI() {
        return zzbs().zzqK;
    }

    public static zziv zzbJ() {
        return zzbs().zzqL;
    }

    public static zzes zzbK() {
        return zzbs().zzqM;
    }

    public static zzdv zzbL() {
        return zzbs().zzqN;
    }

    private static zzp zzbs() {
        zzp com_google_android_gms_ads_internal_zzp;
        synchronized (zzqf) {
            com_google_android_gms_ads_internal_zzp = zzqu;
        }
        return com_google_android_gms_ads_internal_zzp;
    }

    public static zza zzbt() {
        return zzbs().zzqv;
    }

    public static com.google.android.gms.ads.internal.overlay.zza zzbu() {
        return zzbs().zzqw;
    }

    public static zze zzbv() {
        return zzbs().zzqx;
    }

    public static zzgq zzbw() {
        return zzbs().zzqy;
    }

    public static zzip zzbx() {
        return zzbs().zzqz;
    }

    public static zzjp zzby() {
        return zzbs().zzqA;
    }

    public static zziq zzbz() {
        return zzbs().zzqB;
    }
}
