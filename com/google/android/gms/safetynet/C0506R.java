package com.google.android.gms.safetynet;

import com.softelite.testapp.C0577R;

/* renamed from: com.google.android.gms.safetynet.R */
public final class C0506R {

    /* renamed from: com.google.android.gms.safetynet.R.attr */
    public static final class attr {
        public static final int adSize = 2130772006;
        public static final int adSizes = 2130772007;
        public static final int adUnitId = 2130772008;
        public static final int ambientEnabled = 2130772078;
        public static final int appTheme = 2130772275;
        public static final int buttonSize = 2130772112;
        public static final int buyButtonAppearance = 2130772282;
        public static final int buyButtonHeight = 2130772279;
        public static final int buyButtonText = 2130772281;
        public static final int buyButtonWidth = 2130772280;
        public static final int cameraBearing = 2130772063;
        public static final int cameraTargetLat = 2130772064;
        public static final int cameraTargetLng = 2130772065;
        public static final int cameraTilt = 2130772066;
        public static final int cameraZoom = 2130772067;
        public static final int circleCrop = 2130772061;
        public static final int colorScheme = 2130772113;
        public static final int environment = 2130772276;
        public static final int fragmentMode = 2130772278;
        public static final int fragmentStyle = 2130772277;
        public static final int imageAspectRatio = 2130772060;
        public static final int imageAspectRatioAdjust = 2130772059;
        public static final int liteMode = 2130772068;
        public static final int mapType = 2130772062;
        public static final int maskedWalletDetailsBackground = 2130772285;
        public static final int maskedWalletDetailsButtonBackground = 2130772287;
        public static final int maskedWalletDetailsButtonTextAppearance = 2130772286;
        public static final int maskedWalletDetailsHeaderTextAppearance = 2130772284;
        public static final int maskedWalletDetailsLogoImageType = 2130772289;
        public static final int maskedWalletDetailsLogoTextColor = 2130772288;
        public static final int maskedWalletDetailsTextAppearance = 2130772283;
        public static final int scopeUris = 2130772114;
        public static final int uiCompass = 2130772069;
        public static final int uiMapToolbar = 2130772077;
        public static final int uiRotateGestures = 2130772070;
        public static final int uiScrollGestures = 2130772071;
        public static final int uiTiltGestures = 2130772072;
        public static final int uiZoomControls = 2130772073;
        public static final int uiZoomGestures = 2130772074;
        public static final int useViewLifecycle = 2130772075;
        public static final int windowTransitionStyle = 2130772042;
        public static final int zOrderOnTop = 2130772076;
    }

    /* renamed from: com.google.android.gms.safetynet.R.color */
    public static final class color {
        public static final int common_action_bar_splitter = 2131492885;
        public static final int common_google_signin_btn_text_dark = 2131492975;
        public static final int common_google_signin_btn_text_dark_default = 2131492886;
        public static final int common_google_signin_btn_text_dark_disabled = 2131492887;
        public static final int common_google_signin_btn_text_dark_focused = 2131492888;
        public static final int common_google_signin_btn_text_dark_pressed = 2131492889;
        public static final int common_google_signin_btn_text_light = 2131492976;
        public static final int common_google_signin_btn_text_light_default = 2131492890;
        public static final int common_google_signin_btn_text_light_disabled = 2131492891;
        public static final int common_google_signin_btn_text_light_focused = 2131492892;
        public static final int common_google_signin_btn_text_light_pressed = 2131492893;
        public static final int common_plus_signin_btn_text_dark = 2131492977;
        public static final int common_plus_signin_btn_text_dark_default = 2131492894;
        public static final int common_plus_signin_btn_text_dark_disabled = 2131492895;
        public static final int common_plus_signin_btn_text_dark_focused = 2131492896;
        public static final int common_plus_signin_btn_text_dark_pressed = 2131492897;
        public static final int common_plus_signin_btn_text_light = 2131492978;
        public static final int common_plus_signin_btn_text_light_default = 2131492898;
        public static final int common_plus_signin_btn_text_light_disabled = 2131492899;
        public static final int common_plus_signin_btn_text_light_focused = 2131492900;
        public static final int common_plus_signin_btn_text_light_pressed = 2131492901;
        public static final int wallet_bright_foreground_disabled_holo_light = 2131492952;
        public static final int wallet_bright_foreground_holo_dark = 2131492953;
        public static final int wallet_bright_foreground_holo_light = 2131492954;
        public static final int wallet_dim_foreground_disabled_holo_dark = 2131492955;
        public static final int wallet_dim_foreground_holo_dark = 2131492956;
        public static final int wallet_dim_foreground_inverse_disabled_holo_dark = 2131492957;
        public static final int wallet_dim_foreground_inverse_holo_dark = 2131492958;
        public static final int wallet_highlighted_text_holo_dark = 2131492959;
        public static final int wallet_highlighted_text_holo_light = 2131492960;
        public static final int wallet_hint_foreground_holo_dark = 2131492961;
        public static final int wallet_hint_foreground_holo_light = 2131492962;
        public static final int wallet_holo_blue_light = 2131492963;
        public static final int wallet_link_text_light = 2131492964;
        public static final int wallet_primary_text_holo_light = 2131492981;
        public static final int wallet_secondary_text_holo_dark = 2131492982;
    }

    /* renamed from: com.google.android.gms.safetynet.R.drawable */
    public static final class drawable {
        public static final int cast_ic_notification_0 = 2130837576;
        public static final int cast_ic_notification_1 = 2130837577;
        public static final int cast_ic_notification_2 = 2130837578;
        public static final int cast_ic_notification_connecting = 2130837579;
        public static final int cast_ic_notification_on = 2130837580;
        public static final int common_full_open_on_phone = 2130837581;
        public static final int common_google_signin_btn_icon_dark = 2130837582;
        public static final int common_google_signin_btn_icon_dark_disabled = 2130837583;
        public static final int common_google_signin_btn_icon_dark_focused = 2130837584;
        public static final int common_google_signin_btn_icon_dark_normal = 2130837585;
        public static final int common_google_signin_btn_icon_dark_pressed = 2130837586;
        public static final int common_google_signin_btn_icon_light = 2130837587;
        public static final int common_google_signin_btn_icon_light_disabled = 2130837588;
        public static final int common_google_signin_btn_icon_light_focused = 2130837589;
        public static final int common_google_signin_btn_icon_light_normal = 2130837590;
        public static final int common_google_signin_btn_icon_light_pressed = 2130837591;
        public static final int common_google_signin_btn_text_dark = 2130837592;
        public static final int common_google_signin_btn_text_dark_disabled = 2130837593;
        public static final int common_google_signin_btn_text_dark_focused = 2130837594;
        public static final int common_google_signin_btn_text_dark_normal = 2130837595;
        public static final int common_google_signin_btn_text_dark_pressed = 2130837596;
        public static final int common_google_signin_btn_text_light = 2130837597;
        public static final int common_google_signin_btn_text_light_disabled = 2130837598;
        public static final int common_google_signin_btn_text_light_focused = 2130837599;
        public static final int common_google_signin_btn_text_light_normal = 2130837600;
        public static final int common_google_signin_btn_text_light_pressed = 2130837601;
        public static final int common_ic_googleplayservices = 2130837602;
        public static final int common_plus_signin_btn_icon_dark = 2130837603;
        public static final int common_plus_signin_btn_icon_dark_disabled = 2130837604;
        public static final int common_plus_signin_btn_icon_dark_focused = 2130837605;
        public static final int common_plus_signin_btn_icon_dark_normal = 2130837606;
        public static final int common_plus_signin_btn_icon_dark_pressed = 2130837607;
        public static final int common_plus_signin_btn_icon_light = 2130837608;
        public static final int common_plus_signin_btn_icon_light_disabled = 2130837609;
        public static final int common_plus_signin_btn_icon_light_focused = 2130837610;
        public static final int common_plus_signin_btn_icon_light_normal = 2130837611;
        public static final int common_plus_signin_btn_icon_light_pressed = 2130837612;
        public static final int common_plus_signin_btn_text_dark = 2130837613;
        public static final int common_plus_signin_btn_text_dark_disabled = 2130837614;
        public static final int common_plus_signin_btn_text_dark_focused = 2130837615;
        public static final int common_plus_signin_btn_text_dark_normal = 2130837616;
        public static final int common_plus_signin_btn_text_dark_pressed = 2130837617;
        public static final int common_plus_signin_btn_text_light = 2130837618;
        public static final int common_plus_signin_btn_text_light_disabled = 2130837619;
        public static final int common_plus_signin_btn_text_light_focused = 2130837620;
        public static final int common_plus_signin_btn_text_light_normal = 2130837621;
        public static final int common_plus_signin_btn_text_light_pressed = 2130837622;
        public static final int ic_plusone_medium_off_client = 2130837649;
        public static final int ic_plusone_small_off_client = 2130837650;
        public static final int ic_plusone_standard_off_client = 2130837651;
        public static final int ic_plusone_tall_off_client = 2130837652;
        public static final int powered_by_google_dark = 2130837674;
        public static final int powered_by_google_light = 2130837675;
    }

    /* renamed from: com.google.android.gms.safetynet.R.id */
    public static final class id {
        public static final int adjust_height = 2131558452;
        public static final int adjust_width = 2131558453;
        public static final int android_pay = 2131558496;
        public static final int android_pay_dark = 2131558487;
        public static final int android_pay_light = 2131558488;
        public static final int android_pay_light_with_border = 2131558489;
        public static final int auto = 2131558465;
        public static final int book_now = 2131558480;
        public static final int buyButton = 2131558477;
        public static final int buy_now = 2131558481;
        public static final int buy_with = 2131558482;
        public static final int buy_with_google = 2131558483;
        public static final int cast_notification_id = 2131558404;
        public static final int classic = 2131558490;
        public static final int dark = 2131558466;
        public static final int donate_with = 2131558484;
        public static final int donate_with_google = 2131558485;
        public static final int google_wallet_classic = 2131558491;
        public static final int google_wallet_grayscale = 2131558492;
        public static final int google_wallet_monochrome = 2131558493;
        public static final int grayscale = 2131558494;
        public static final int holo_dark = 2131558471;
        public static final int holo_light = 2131558472;
        public static final int hybrid = 2131558454;
        public static final int icon_only = 2131558462;
        public static final int light = 2131558467;
        public static final int logo_only = 2131558486;
        public static final int match_parent = 2131558479;
        public static final int monochrome = 2131558495;
        public static final int none = 2131558417;
        public static final int normal = 2131558413;
        public static final int production = 2131558473;
        public static final int sandbox = 2131558474;
        public static final int satellite = 2131558455;
        public static final int selectionDetails = 2131558478;
        public static final int slide = 2131558448;
        public static final int standard = 2131558463;
        public static final int strict_sandbox = 2131558475;
        public static final int terrain = 2131558456;
        public static final int test = 2131558476;
        public static final int wide = 2131558464;
        public static final int wrap_content = 2131558470;
    }

    /* renamed from: com.google.android.gms.safetynet.R.integer */
    public static final class integer {
        public static final int google_play_services_version = 2131427333;
    }

    /* renamed from: com.google.android.gms.safetynet.R.raw */
    public static final class raw {
        public static final int gtm_analytics = 2131099648;
    }

    /* renamed from: com.google.android.gms.safetynet.R.string */
    public static final class string {
        public static final int accept = 2131165244;
        public static final int auth_google_play_services_client_facebook_display_name = 2131165249;
        public static final int auth_google_play_services_client_google_display_name = 2131165250;
        public static final int cast_notification_connected_message = 2131165251;
        public static final int cast_notification_connecting_message = 2131165252;
        public static final int cast_notification_disconnect = 2131165253;
        public static final int common_android_wear_notification_needs_update_text = 2131165203;
        public static final int common_android_wear_update_text = 2131165204;
        public static final int common_android_wear_update_title = 2131165205;
        public static final int common_google_play_services_api_unavailable_text = 2131165206;
        public static final int common_google_play_services_enable_button = 2131165207;
        public static final int common_google_play_services_enable_text = 2131165208;
        public static final int common_google_play_services_enable_title = 2131165209;
        public static final int common_google_play_services_error_notification_requested_by_msg = 2131165210;
        public static final int common_google_play_services_install_button = 2131165211;
        public static final int common_google_play_services_install_text_phone = 2131165212;
        public static final int common_google_play_services_install_text_tablet = 2131165213;
        public static final int common_google_play_services_install_title = 2131165214;
        public static final int common_google_play_services_invalid_account_text = 2131165215;
        public static final int common_google_play_services_invalid_account_title = 2131165216;
        public static final int common_google_play_services_needs_enabling_title = 2131165217;
        public static final int common_google_play_services_network_error_text = 2131165218;
        public static final int common_google_play_services_network_error_title = 2131165219;
        public static final int common_google_play_services_notification_needs_update_title = 2131165220;
        public static final int common_google_play_services_notification_ticker = 2131165221;
        public static final int common_google_play_services_sign_in_failed_text = 2131165222;
        public static final int common_google_play_services_sign_in_failed_title = 2131165223;
        public static final int common_google_play_services_unknown_issue = 2131165224;
        public static final int common_google_play_services_unsupported_text = 2131165225;
        public static final int common_google_play_services_unsupported_title = 2131165226;
        public static final int common_google_play_services_update_button = 2131165227;
        public static final int common_google_play_services_update_text = 2131165228;
        public static final int common_google_play_services_update_title = 2131165229;
        public static final int common_google_play_services_updating_text = 2131165230;
        public static final int common_google_play_services_updating_title = 2131165231;
        public static final int common_open_on_phone = 2131165232;
        public static final int common_signin_button_text = 2131165233;
        public static final int common_signin_button_text_long = 2131165234;
        public static final int create_calendar_message = 2131165255;
        public static final int create_calendar_title = 2131165256;
        public static final int decline = 2131165257;
        public static final int store_picture_message = 2131165272;
        public static final int store_picture_title = 2131165273;
        public static final int wallet_buy_button_place_holder = 2131165242;
    }

    /* renamed from: com.google.android.gms.safetynet.R.style */
    public static final class style {
        public static final int Theme_IAPTheme = 2131362039;
        public static final int WalletFragmentDefaultButtonTextAppearance = 2131362047;
        public static final int WalletFragmentDefaultDetailsHeaderTextAppearance = 2131362048;
        public static final int WalletFragmentDefaultDetailsTextAppearance = 2131362049;
        public static final int WalletFragmentDefaultStyle = 2131362050;
    }

    /* renamed from: com.google.android.gms.safetynet.R.styleable */
    public static final class styleable {
        public static final int[] AdsAttrs;
        public static final int AdsAttrs_adSize = 0;
        public static final int AdsAttrs_adSizes = 1;
        public static final int AdsAttrs_adUnitId = 2;
        public static final int[] CustomWalletTheme;
        public static final int CustomWalletTheme_windowTransitionStyle = 0;
        public static final int[] LoadingImageView;
        public static final int LoadingImageView_circleCrop = 2;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 0;
        public static final int[] MapAttrs;
        public static final int MapAttrs_ambientEnabled = 16;
        public static final int MapAttrs_cameraBearing = 1;
        public static final int MapAttrs_cameraTargetLat = 2;
        public static final int MapAttrs_cameraTargetLng = 3;
        public static final int MapAttrs_cameraTilt = 4;
        public static final int MapAttrs_cameraZoom = 5;
        public static final int MapAttrs_liteMode = 6;
        public static final int MapAttrs_mapType = 0;
        public static final int MapAttrs_uiCompass = 7;
        public static final int MapAttrs_uiMapToolbar = 15;
        public static final int MapAttrs_uiRotateGestures = 8;
        public static final int MapAttrs_uiScrollGestures = 9;
        public static final int MapAttrs_uiTiltGestures = 10;
        public static final int MapAttrs_uiZoomControls = 11;
        public static final int MapAttrs_uiZoomGestures = 12;
        public static final int MapAttrs_useViewLifecycle = 13;
        public static final int MapAttrs_zOrderOnTop = 14;
        public static final int[] SignInButton;
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;
        public static final int[] WalletFragmentOptions;
        public static final int WalletFragmentOptions_appTheme = 0;
        public static final int WalletFragmentOptions_environment = 1;
        public static final int WalletFragmentOptions_fragmentMode = 3;
        public static final int WalletFragmentOptions_fragmentStyle = 2;
        public static final int[] WalletFragmentStyle;
        public static final int WalletFragmentStyle_buyButtonAppearance = 3;
        public static final int WalletFragmentStyle_buyButtonHeight = 0;
        public static final int WalletFragmentStyle_buyButtonText = 2;
        public static final int WalletFragmentStyle_buyButtonWidth = 1;
        public static final int WalletFragmentStyle_maskedWalletDetailsBackground = 6;
        public static final int WalletFragmentStyle_maskedWalletDetailsButtonBackground = 8;
        public static final int WalletFragmentStyle_maskedWalletDetailsButtonTextAppearance = 7;
        public static final int WalletFragmentStyle_maskedWalletDetailsHeaderTextAppearance = 5;
        public static final int WalletFragmentStyle_maskedWalletDetailsLogoImageType = 10;
        public static final int WalletFragmentStyle_maskedWalletDetailsLogoTextColor = 9;
        public static final int WalletFragmentStyle_maskedWalletDetailsTextAppearance = 4;

        static {
            AdsAttrs = new int[]{C0577R.attr.adSize, C0577R.attr.adSizes, C0577R.attr.adUnitId};
            int[] iArr = new int[WalletFragmentStyle_buyButtonWidth];
            iArr[WalletFragmentStyle_buyButtonHeight] = C0577R.attr.windowTransitionStyle;
            CustomWalletTheme = iArr;
            LoadingImageView = new int[]{C0577R.attr.imageAspectRatioAdjust, C0577R.attr.imageAspectRatio, C0577R.attr.circleCrop};
            MapAttrs = new int[]{C0577R.attr.mapType, C0577R.attr.cameraBearing, C0577R.attr.cameraTargetLat, C0577R.attr.cameraTargetLng, C0577R.attr.cameraTilt, C0577R.attr.cameraZoom, C0577R.attr.liteMode, C0577R.attr.uiCompass, C0577R.attr.uiRotateGestures, C0577R.attr.uiScrollGestures, C0577R.attr.uiTiltGestures, C0577R.attr.uiZoomControls, C0577R.attr.uiZoomGestures, C0577R.attr.useViewLifecycle, C0577R.attr.zOrderOnTop, C0577R.attr.uiMapToolbar, C0577R.attr.ambientEnabled};
            SignInButton = new int[]{C0577R.attr.buttonSize, C0577R.attr.colorScheme, C0577R.attr.scopeUris};
            WalletFragmentOptions = new int[]{C0577R.attr.appTheme, C0577R.attr.environment, C0577R.attr.fragmentStyle, C0577R.attr.fragmentMode};
            WalletFragmentStyle = new int[]{C0577R.attr.buyButtonHeight, C0577R.attr.buyButtonWidth, C0577R.attr.buyButtonText, C0577R.attr.buyButtonAppearance, C0577R.attr.maskedWalletDetailsTextAppearance, C0577R.attr.maskedWalletDetailsHeaderTextAppearance, C0577R.attr.maskedWalletDetailsBackground, C0577R.attr.maskedWalletDetailsButtonTextAppearance, C0577R.attr.maskedWalletDetailsButtonBackground, C0577R.attr.maskedWalletDetailsLogoTextColor, C0577R.attr.maskedWalletDetailsLogoImageType};
        }
    }
}
