package com.google.android.gms.drive.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.drive.DriveApi.DriveContentsResult;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFile.DownloadProgressListener;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.internal.zzmn;
import com.google.android.gms.internal.zzmn.zzb;

public class zzw extends zzab implements DriveFile {

    private static class zza implements DownloadProgressListener {
        private final zzmn<DownloadProgressListener> zzape;

        /* renamed from: com.google.android.gms.drive.internal.zzw.zza.1 */
        class C07151 implements zzb<DownloadProgressListener> {
            final /* synthetic */ long zzapf;
            final /* synthetic */ long zzapg;
            final /* synthetic */ zza zzaph;

            C07151(zza com_google_android_gms_drive_internal_zzw_zza, long j, long j2) {
                this.zzaph = com_google_android_gms_drive_internal_zzw_zza;
                this.zzapf = j;
                this.zzapg = j2;
            }

            public void zza(DownloadProgressListener downloadProgressListener) {
                downloadProgressListener.onProgress(this.zzapf, this.zzapg);
            }

            public void zzpb() {
            }

            public /* synthetic */ void zzs(Object obj) {
                zza((DownloadProgressListener) obj);
            }
        }

        public zza(zzmn<DownloadProgressListener> com_google_android_gms_internal_zzmn_com_google_android_gms_drive_DriveFile_DownloadProgressListener) {
            this.zzape = com_google_android_gms_internal_zzmn_com_google_android_gms_drive_DriveFile_DownloadProgressListener;
        }

        public void onProgress(long bytesDownloaded, long bytesExpected) {
            this.zzape.zza(new C07151(this, bytesDownloaded, bytesExpected));
        }
    }

    /* renamed from: com.google.android.gms.drive.internal.zzw.1 */
    class C13081 extends zzc {
        final /* synthetic */ int zzaoB;
        final /* synthetic */ DownloadProgressListener zzapc;
        final /* synthetic */ zzw zzapd;

        C13081(zzw com_google_android_gms_drive_internal_zzw, GoogleApiClient googleApiClient, int i, DownloadProgressListener downloadProgressListener) {
            this.zzapd = com_google_android_gms_drive_internal_zzw;
            this.zzaoB = i;
            this.zzapc = downloadProgressListener;
            super(googleApiClient);
        }

        protected void zza(zzu com_google_android_gms_drive_internal_zzu) throws RemoteException {
            zza(com_google_android_gms_drive_internal_zzu.zzsF().zza(new OpenContentsRequest(this.zzapd.getDriveId(), this.zzaoB, 0), new zzbl(this, this.zzapc)).zzsK());
        }
    }

    public zzw(DriveId driveId) {
        super(driveId);
    }

    private static DownloadProgressListener zza(GoogleApiClient googleApiClient, DownloadProgressListener downloadProgressListener) {
        return downloadProgressListener == null ? null : new zza(googleApiClient.zzq(downloadProgressListener));
    }

    public PendingResult<DriveContentsResult> open(GoogleApiClient apiClient, int mode, DownloadProgressListener listener) {
        if (mode == DriveFile.MODE_READ_ONLY || mode == DriveFile.MODE_WRITE_ONLY || mode == DriveFile.MODE_READ_WRITE) {
            return apiClient.zza(new C13081(this, apiClient, mode, zza(apiClient, listener)));
        }
        throw new IllegalArgumentException("Invalid mode provided.");
    }
}
