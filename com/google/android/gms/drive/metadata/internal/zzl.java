package com.google.android.gms.drive.metadata.internal;

import android.os.Bundle;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.metadata.SearchableCollectionMetadataField;
import com.google.android.gms.drive.metadata.internal.zze.zza;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class zzl extends zzi<DriveId> implements SearchableCollectionMetadataField<DriveId> {
    public static final zza zzaqN;

    /* renamed from: com.google.android.gms.drive.metadata.internal.zzl.1 */
    static class C07161 implements zza {
        C07161() {
        }

        public void zzb(DataHolder dataHolder) {
            zzl.zzd(dataHolder);
        }

        public String zzte() {
            return "parentsExtraHolder";
        }
    }

    static {
        zzaqN = new C07161();
    }

    public zzl(int i) {
        super("parents", Collections.emptySet(), Arrays.asList(new String[]{"parentsExtra", "dbInstanceId", "parentsExtraHolder"}), i);
    }

    private void zzc(DataHolder dataHolder) {
        synchronized (dataHolder) {
            DataHolder dataHolder2 = (DataHolder) dataHolder.zzpH().getParcelable("parentsExtraHolder");
            if (dataHolder2 == null) {
                return;
            }
            try {
                int i;
                int count = dataHolder.getCount();
                ArrayList arrayList = new ArrayList(count);
                Map hashMap = new HashMap(count);
                for (i = 0; i < count; i++) {
                    int zzbI = dataHolder.zzbI(i);
                    ParentDriveIdSet parentDriveIdSet = new ParentDriveIdSet();
                    arrayList.add(parentDriveIdSet);
                    hashMap.put(Long.valueOf(dataHolder.zzb("sqlId", i, zzbI)), parentDriveIdSet);
                }
                Bundle zzpH = dataHolder2.zzpH();
                String string = zzpH.getString("childSqlIdColumn");
                String string2 = zzpH.getString("parentSqlIdColumn");
                String string3 = zzpH.getString("parentResIdColumn");
                int count2 = dataHolder2.getCount();
                for (i = 0; i < count2; i++) {
                    int zzbI2 = dataHolder2.zzbI(i);
                    ((ParentDriveIdSet) hashMap.get(Long.valueOf(dataHolder2.zzb(string, i, zzbI2)))).zza(new PartialDriveId(dataHolder2.zzd(string3, i, zzbI2), dataHolder2.zzb(string2, i, zzbI2), 1));
                }
                dataHolder.zzpH().putParcelableArrayList("parentsExtra", arrayList);
            } finally {
                dataHolder2.close();
                dataHolder.zzpH().remove("parentsExtraHolder");
            }
        }
    }

    private static void zzd(DataHolder dataHolder) {
        Bundle zzpH = dataHolder.zzpH();
        if (zzpH != null) {
            synchronized (dataHolder) {
                DataHolder dataHolder2 = (DataHolder) zzpH.getParcelable("parentsExtraHolder");
                if (dataHolder2 != null) {
                    dataHolder2.close();
                    zzpH.remove("parentsExtraHolder");
                }
            }
        }
    }

    protected /* synthetic */ Object zzc(DataHolder dataHolder, int i, int i2) {
        return zzd(dataHolder, i, i2);
    }

    protected Collection<DriveId> zzd(DataHolder dataHolder, int i, int i2) {
        Bundle zzpH = dataHolder.zzpH();
        List parcelableArrayList = zzpH.getParcelableArrayList("parentsExtra");
        if (parcelableArrayList == null) {
            if (zzpH.getParcelable("parentsExtraHolder") != null) {
                zzc(dataHolder);
                parcelableArrayList = zzpH.getParcelableArrayList("parentsExtra");
            }
            if (parcelableArrayList == null) {
                return null;
            }
        }
        return ((ParentDriveIdSet) parcelableArrayList.get(i)).zzD(zzpH.getLong("dbInstanceId"));
    }

    protected /* synthetic */ Object zzn(Bundle bundle) {
        return zzs(bundle);
    }

    protected Collection<DriveId> zzs(Bundle bundle) {
        Collection zzs = super.zzs(bundle);
        return zzs == null ? null : new HashSet(zzs);
    }
}
