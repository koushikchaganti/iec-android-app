package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public class Operator implements SafeParcelable {
    public static final Creator<Operator> CREATOR;
    public static final Operator zzasr;
    public static final Operator zzass;
    public static final Operator zzast;
    public static final Operator zzasu;
    public static final Operator zzasv;
    public static final Operator zzasw;
    public static final Operator zzasx;
    public static final Operator zzasy;
    public static final Operator zzasz;
    final String mTag;
    final int mVersionCode;

    static {
        CREATOR = new zzn();
        zzasr = new Operator("=");
        zzass = new Operator("<");
        zzast = new Operator("<=");
        zzasu = new Operator(">");
        zzasv = new Operator(">=");
        zzasw = new Operator("and");
        zzasx = new Operator("or");
        zzasy = new Operator("not");
        zzasz = new Operator("contains");
    }

    Operator(int versionCode, String tag) {
        this.mVersionCode = versionCode;
        this.mTag = tag;
    }

    private Operator(String tag) {
        this(1, tag);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Operator operator = (Operator) obj;
        return this.mTag == null ? operator.mTag == null : this.mTag.equals(operator.mTag);
    }

    public String getTag() {
        return this.mTag;
    }

    public int hashCode() {
        return (this.mTag == null ? 0 : this.mTag.hashCode()) + 31;
    }

    public void writeToParcel(Parcel out, int flags) {
        zzn.zza(this, out, flags);
    }
}
