package com.google.android.gms.measurement;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager.WakeLock;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.measurement.internal.zzae;
import com.google.android.gms.measurement.internal.zzo;
import com.google.android.gms.measurement.internal.zzt;
import com.google.android.gms.measurement.internal.zzu;

public final class AppMeasurementService extends Service {
    private static Boolean zzNu;
    private final Handler mHandler;

    /* renamed from: com.google.android.gms.measurement.AppMeasurementService.1 */
    class C04671 implements Runnable {
        final /* synthetic */ int zzNv;
        final /* synthetic */ zzt zzaQZ;
        final /* synthetic */ zzo zzaRa;
        final /* synthetic */ AppMeasurementService zzaRb;

        /* renamed from: com.google.android.gms.measurement.AppMeasurementService.1.1 */
        class C04661 implements Runnable {
            final /* synthetic */ C04671 zzaRc;

            C04661(C04671 c04671) {
                this.zzaRc = c04671;
            }

            public void run() {
                if (!this.zzaRc.zzaRb.stopSelfResult(this.zzaRc.zzNv)) {
                    return;
                }
                if (this.zzaRc.zzaQZ.zzAX().zzka()) {
                    this.zzaRc.zzaRa.zzBr().zzez("Device AppMeasurementService processed last upload request");
                } else {
                    this.zzaRc.zzaRa.zzBr().zzez("Local AppMeasurementService processed last upload request");
                }
            }
        }

        C04671(AppMeasurementService appMeasurementService, zzt com_google_android_gms_measurement_internal_zzt, int i, zzo com_google_android_gms_measurement_internal_zzo) {
            this.zzaRb = appMeasurementService;
            this.zzaQZ = com_google_android_gms_measurement_internal_zzt;
            this.zzNv = i;
            this.zzaRa = com_google_android_gms_measurement_internal_zzo;
        }

        public void run() {
            this.zzaQZ.zzBK();
            this.zzaRb.mHandler.post(new C04661(this));
        }
    }

    public AppMeasurementService() {
        this.mHandler = new Handler();
    }

    public static boolean zzY(Context context) {
        zzx.zzy(context);
        if (zzNu != null) {
            return zzNu.booleanValue();
        }
        boolean zza = zzae.zza(context, AppMeasurementService.class);
        zzNu = Boolean.valueOf(zza);
        return zza;
    }

    private void zzih() {
        try {
            synchronized (AppMeasurementReceiver.zzqf) {
                WakeLock wakeLock = AppMeasurementReceiver.zzaQY;
                if (wakeLock != null && wakeLock.isHeld()) {
                    wakeLock.release();
                }
            }
        } catch (SecurityException e) {
        }
    }

    private zzo zzzz() {
        return zzt.zzaU(this).zzzz();
    }

    public IBinder onBind(Intent intent) {
        if (intent == null) {
            zzzz().zzBl().zzez("onBind called with null intent");
            return null;
        }
        String action = intent.getAction();
        if ("com.google.android.gms.measurement.START".equals(action)) {
            return new zzu(zzt.zzaU(this));
        }
        zzzz().zzBm().zzj("onBind received unknown action", action);
        return null;
    }

    public void onCreate() {
        super.onCreate();
        zzt zzaU = zzt.zzaU(this);
        zzo zzzz = zzaU.zzzz();
        if (zzaU.zzAX().zzka()) {
            zzzz.zzBr().zzez("Device AppMeasurementService is starting up");
        } else {
            zzzz.zzBr().zzez("Local AppMeasurementService is starting up");
        }
    }

    public void onDestroy() {
        zzt zzaU = zzt.zzaU(this);
        zzo zzzz = zzaU.zzzz();
        if (zzaU.zzAX().zzka()) {
            zzzz.zzBr().zzez("Device AppMeasurementService is shutting down");
        } else {
            zzzz.zzBr().zzez("Local AppMeasurementService is shutting down");
        }
        super.onDestroy();
    }

    public void onRebind(Intent intent) {
        if (intent == null) {
            zzzz().zzBl().zzez("onRebind called with null intent");
            return;
        }
        zzzz().zzBr().zzj("onRebind called. action", intent.getAction());
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        zzih();
        zzt zzaU = zzt.zzaU(this);
        zzo zzzz = zzaU.zzzz();
        String action = intent.getAction();
        if (zzaU.zzAX().zzka()) {
            zzzz.zzBr().zze("Device AppMeasurementService called. startId, action", Integer.valueOf(startId), action);
        } else {
            zzzz.zzBr().zze("Local AppMeasurementService called. startId, action", Integer.valueOf(startId), action);
        }
        if ("com.google.android.gms.measurement.UPLOAD".equals(action)) {
            zzaU.zzAV().zzg(new C04671(this, zzaU, startId, zzzz));
        }
        return 2;
    }

    public boolean onUnbind(Intent intent) {
        if (intent == null) {
            zzzz().zzBl().zzez("onUnbind called with null intent");
        } else {
            zzzz().zzBr().zzj("onUnbind called for intent. action", intent.getAction());
        }
        return true;
    }
}
