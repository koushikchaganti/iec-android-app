package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzx;
import java.util.Iterator;

public class EventParams implements SafeParcelable, Iterable<String> {
    public static final zzi CREATOR;
    public final int versionCode;
    private final Bundle zzaSI;

    /* renamed from: com.google.android.gms.measurement.internal.EventParams.1 */
    class C04691 implements Iterator<String> {
        Iterator<String> zzaSJ;
        final /* synthetic */ EventParams zzaSK;

        C04691(EventParams eventParams) {
            this.zzaSK = eventParams;
            this.zzaSJ = this.zzaSK.zzaSI.keySet().iterator();
        }

        public boolean hasNext() {
            return this.zzaSJ.hasNext();
        }

        public String next() {
            return (String) this.zzaSJ.next();
        }

        public void remove() {
            throw new UnsupportedOperationException("Remove not supported");
        }
    }

    static {
        CREATOR = new zzi();
    }

    EventParams(int versionCode, Bundle bundle) {
        this.versionCode = versionCode;
        this.zzaSI = bundle;
    }

    EventParams(Bundle bundle) {
        zzx.zzy(bundle);
        this.zzaSI = bundle;
        this.versionCode = 1;
    }

    public int describeContents() {
        return 0;
    }

    Object get(String key) {
        return this.zzaSI.get(key);
    }

    public Iterator<String> iterator() {
        return new C04691(this);
    }

    public int size() {
        return this.zzaSI.size();
    }

    public String toString() {
        return this.zzaSI.toString();
    }

    public void writeToParcel(Parcel out, int flags) {
        zzi.zza(this, out, flags);
    }

    public Bundle zzBh() {
        return new Bundle(this.zzaSI);
    }
}
