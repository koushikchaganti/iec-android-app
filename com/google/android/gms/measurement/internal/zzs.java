package com.google.android.gms.measurement.internal;

import android.content.Context;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zznl;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;

public class zzs extends zzw {
    private zzc zzaTT;
    private zzc zzaTU;
    private final BlockingQueue<FutureTask<?>> zzaTV;
    private final BlockingQueue<FutureTask<?>> zzaTW;
    private final UncaughtExceptionHandler zzaTX;
    private final UncaughtExceptionHandler zzaTY;
    private final Object zzaTZ;
    private final Semaphore zzaUa;
    private volatile boolean zzaUb;

    private final class zza<V> extends FutureTask<V> {
        private final String zzaUc;
        final /* synthetic */ zzs zzaUd;

        zza(zzs com_google_android_gms_measurement_internal_zzs, Runnable runnable, String str) {
            this.zzaUd = com_google_android_gms_measurement_internal_zzs;
            super(runnable, null);
            zzx.zzy(str);
            this.zzaUc = str;
        }

        protected void setException(Throwable error) {
            this.zzaUd.zzzz().zzBl().zzj(this.zzaUc, error);
            super.setException(error);
        }
    }

    private final class zzb implements UncaughtExceptionHandler {
        private final String zzaUc;
        final /* synthetic */ zzs zzaUd;

        public zzb(zzs com_google_android_gms_measurement_internal_zzs, String str) {
            this.zzaUd = com_google_android_gms_measurement_internal_zzs;
            zzx.zzy(str);
            this.zzaUc = str;
        }

        public synchronized void uncaughtException(Thread thread, Throwable error) {
            this.zzaUd.zzzz().zzBl().zzj(this.zzaUc, error);
        }
    }

    private final class zzc extends Thread {
        final /* synthetic */ zzs zzaUd;
        private final Object zzaUe;
        private final BlockingQueue<FutureTask<?>> zzaUf;

        public zzc(zzs com_google_android_gms_measurement_internal_zzs, String str, BlockingQueue<FutureTask<?>> blockingQueue) {
            this.zzaUd = com_google_android_gms_measurement_internal_zzs;
            zzx.zzy(str);
            this.zzaUe = new Object();
            this.zzaUf = blockingQueue;
            setName(str);
        }

        private void zza(InterruptedException interruptedException) {
            this.zzaUd.zzzz().zzBm().zzj(getName() + " was interrupted", interruptedException);
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
            r4 = this;
        L_0x0000:
            r0 = r4.zzaUf;
            r0 = r0.poll();
            r0 = (java.util.concurrent.FutureTask) r0;
            if (r0 == 0) goto L_0x000e;
        L_0x000a:
            r0.run();
            goto L_0x0000;
        L_0x000e:
            r1 = r4.zzaUe;
            monitor-enter(r1);
            r0 = r4.zzaUf;	 Catch:{ all -> 0x005f }
            r0 = r0.peek();	 Catch:{ all -> 0x005f }
            if (r0 != 0) goto L_0x0028;
        L_0x0019:
            r0 = r4.zzaUd;	 Catch:{ all -> 0x005f }
            r0 = r0.zzaUb;	 Catch:{ all -> 0x005f }
            if (r0 != 0) goto L_0x0028;
        L_0x0021:
            r0 = r4.zzaUe;	 Catch:{ InterruptedException -> 0x005a }
            r2 = 30000; // 0x7530 float:4.2039E-41 double:1.4822E-319;
            r0.wait(r2);	 Catch:{ InterruptedException -> 0x005a }
        L_0x0028:
            monitor-exit(r1);	 Catch:{ all -> 0x005f }
            r0 = r4.zzaUd;
            r1 = r0.zzaTZ;
            monitor-enter(r1);
            r0 = r4.zzaUf;	 Catch:{ all -> 0x0071 }
            r0 = r0.peek();	 Catch:{ all -> 0x0071 }
            if (r0 != 0) goto L_0x0084;
        L_0x0038:
            r0 = r4.zzaUd;	 Catch:{ all -> 0x0071 }
            r0 = r0.zzaUa;	 Catch:{ all -> 0x0071 }
            r0.release();	 Catch:{ all -> 0x0071 }
            r0 = r4.zzaUd;	 Catch:{ all -> 0x0071 }
            r0 = r0.zzaTZ;	 Catch:{ all -> 0x0071 }
            r0.notifyAll();	 Catch:{ all -> 0x0071 }
            r0 = r4.zzaUd;	 Catch:{ all -> 0x0071 }
            r0 = r0.zzaTT;	 Catch:{ all -> 0x0071 }
            if (r4 != r0) goto L_0x0062;
        L_0x0052:
            r0 = r4.zzaUd;	 Catch:{ all -> 0x0071 }
            r2 = 0;
            r0.zzaTT = r2;	 Catch:{ all -> 0x0071 }
        L_0x0058:
            monitor-exit(r1);	 Catch:{ all -> 0x0071 }
            return;
        L_0x005a:
            r0 = move-exception;
            r4.zza(r0);	 Catch:{ all -> 0x005f }
            goto L_0x0028;
        L_0x005f:
            r0 = move-exception;
            monitor-exit(r1);	 Catch:{ all -> 0x005f }
            throw r0;
        L_0x0062:
            r0 = r4.zzaUd;	 Catch:{ all -> 0x0071 }
            r0 = r0.zzaTU;	 Catch:{ all -> 0x0071 }
            if (r4 != r0) goto L_0x0074;
        L_0x006a:
            r0 = r4.zzaUd;	 Catch:{ all -> 0x0071 }
            r2 = 0;
            r0.zzaTU = r2;	 Catch:{ all -> 0x0071 }
            goto L_0x0058;
        L_0x0071:
            r0 = move-exception;
            monitor-exit(r1);	 Catch:{ all -> 0x0071 }
            throw r0;
        L_0x0074:
            r0 = r4.zzaUd;	 Catch:{ all -> 0x0071 }
            r0 = r0.zzzz();	 Catch:{ all -> 0x0071 }
            r0 = r0.zzBl();	 Catch:{ all -> 0x0071 }
            r2 = "Current scheduler thread is neither worker nor network";
            r0.zzez(r2);	 Catch:{ all -> 0x0071 }
            goto L_0x0058;
        L_0x0084:
            monitor-exit(r1);	 Catch:{ all -> 0x0071 }
            goto L_0x0000;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzs.zzc.run():void");
        }

        public void zzeQ() {
            synchronized (this.zzaUe) {
                this.zzaUe.notifyAll();
            }
        }
    }

    zzs(zzt com_google_android_gms_measurement_internal_zzt) {
        super(com_google_android_gms_measurement_internal_zzt);
        this.zzaTZ = new Object();
        this.zzaUa = new Semaphore(2);
        this.zzaTV = new LinkedBlockingQueue();
        this.zzaTW = new LinkedBlockingQueue();
        this.zzaTX = new zzb(this, "Thread death: Uncaught exception on worker thread");
        this.zzaTY = new zzb(this, "Thread death: Uncaught exception on network thread");
    }

    private void zza(FutureTask<?> futureTask) {
        synchronized (this.zzaTZ) {
            this.zzaTV.add(futureTask);
            if (this.zzaTT == null) {
                this.zzaTT = new zzc(this, "Measurement Worker", this.zzaTV);
                this.zzaTT.setUncaughtExceptionHandler(this.zzaTX);
                this.zzaTT.start();
            } else {
                this.zzaTT.zzeQ();
            }
        }
    }

    private void zzb(FutureTask<?> futureTask) {
        synchronized (this.zzaTZ) {
            this.zzaTW.add(futureTask);
            if (this.zzaTU == null) {
                this.zzaTU = new zzc(this, "Measurement Network", this.zzaTW);
                this.zzaTU.setUncaughtExceptionHandler(this.zzaTY);
                this.zzaTU.start();
            } else {
                this.zzaTU.zzeQ();
            }
        }
    }

    public /* bridge */ /* synthetic */ Context getContext() {
        return super.getContext();
    }

    public void zzAR() {
        if (Thread.currentThread() != this.zzaTU) {
            throw new IllegalStateException("Call expected from network thread");
        }
    }

    public /* bridge */ /* synthetic */ zzm zzAS() {
        return super.zzAS();
    }

    public /* bridge */ /* synthetic */ zzz zzAT() {
        return super.zzAT();
    }

    public /* bridge */ /* synthetic */ zzae zzAU() {
        return super.zzAU();
    }

    public /* bridge */ /* synthetic */ zzs zzAV() {
        return super.zzAV();
    }

    public /* bridge */ /* synthetic */ zzr zzAW() {
        return super.zzAW();
    }

    public /* bridge */ /* synthetic */ zzc zzAX() {
        return super.zzAX();
    }

    public void zzg(Runnable runnable) throws IllegalStateException {
        zzje();
        zzx.zzy(runnable);
        zza(new zza(this, runnable, "Task exception on worker thread"));
    }

    public void zzh(Runnable runnable) throws IllegalStateException {
        zzje();
        zzx.zzy(runnable);
        zzb(new zza(this, runnable, "Task exception on network thread"));
    }

    public /* bridge */ /* synthetic */ void zziR() {
        super.zziR();
    }

    public void zziS() {
        if (Thread.currentThread() != this.zzaTT) {
            throw new IllegalStateException("Call expected from worker thread");
        }
    }

    public /* bridge */ /* synthetic */ zznl zziT() {
        return super.zziT();
    }

    protected void zzir() {
    }

    public /* bridge */ /* synthetic */ zzo zzzz() {
        return super.zzzz();
    }
}
