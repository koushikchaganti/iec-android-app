package com.google.android.gms.measurement.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.common.stats.zzb;
import com.google.android.gms.drive.ExecutionOptions;
import com.google.android.gms.internal.zznl;
import com.google.android.gms.measurement.AppMeasurementService;
import java.util.ArrayList;
import java.util.List;

public class zzz extends zzw {
    private final zza zzaUS;
    private zzl zzaUT;
    private Boolean zzaUU;
    private final zze zzaUV;
    private final zzaa zzaUW;
    private final List<Runnable> zzaUX;
    private final zze zzaUY;

    /* renamed from: com.google.android.gms.measurement.internal.zzz.3 */
    class C04863 implements Runnable {
        final /* synthetic */ zzz zzaUZ;

        C04863(zzz com_google_android_gms_measurement_internal_zzz) {
            this.zzaUZ = com_google_android_gms_measurement_internal_zzz;
        }

        public void run() {
            zzl zzc = this.zzaUZ.zzaUT;
            if (zzc == null) {
                this.zzaUZ.zzzz().zzBl().zzez("Failed to send measurementEnabled to service");
                return;
            }
            try {
                zzc.zzb(this.zzaUZ.zzAS().zzex(this.zzaUZ.zzzz().zzBs()));
                this.zzaUZ.zzjr();
            } catch (RemoteException e) {
                this.zzaUZ.zzzz().zzBl().zzj("Failed to send measurementEnabled to AppMeasurementService", e);
            }
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.zzz.4 */
    class C04874 implements Runnable {
        final /* synthetic */ String zzaFm;
        final /* synthetic */ EventParcel zzaUE;
        final /* synthetic */ zzz zzaUZ;

        C04874(zzz com_google_android_gms_measurement_internal_zzz, String str, EventParcel eventParcel) {
            this.zzaUZ = com_google_android_gms_measurement_internal_zzz;
            this.zzaFm = str;
            this.zzaUE = eventParcel;
        }

        public void run() {
            zzl zzc = this.zzaUZ.zzaUT;
            if (zzc == null) {
                this.zzaUZ.zzzz().zzBl().zzez("Discarding data. Failed to send event to service");
                return;
            }
            try {
                if (TextUtils.isEmpty(this.zzaFm)) {
                    zzc.zza(this.zzaUE, this.zzaUZ.zzAS().zzex(this.zzaUZ.zzzz().zzBs()));
                } else {
                    zzc.zza(this.zzaUE, this.zzaFm, this.zzaUZ.zzzz().zzBs());
                }
                this.zzaUZ.zzjr();
            } catch (RemoteException e) {
                this.zzaUZ.zzzz().zzBl().zzj("Failed to send event to AppMeasurementService", e);
            }
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.zzz.5 */
    class C04885 implements Runnable {
        final /* synthetic */ UserAttributeParcel zzaUG;
        final /* synthetic */ zzz zzaUZ;

        C04885(zzz com_google_android_gms_measurement_internal_zzz, UserAttributeParcel userAttributeParcel) {
            this.zzaUZ = com_google_android_gms_measurement_internal_zzz;
            this.zzaUG = userAttributeParcel;
        }

        public void run() {
            zzl zzc = this.zzaUZ.zzaUT;
            if (zzc == null) {
                this.zzaUZ.zzzz().zzBl().zzez("Discarding data. Failed to set user attribute");
                return;
            }
            try {
                zzc.zza(this.zzaUG, this.zzaUZ.zzAS().zzex(this.zzaUZ.zzzz().zzBs()));
                this.zzaUZ.zzjr();
            } catch (RemoteException e) {
                this.zzaUZ.zzzz().zzBl().zzj("Failed to send attribute to AppMeasurementService", e);
            }
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.zzz.6 */
    class C04896 implements Runnable {
        final /* synthetic */ zzz zzaUZ;

        C04896(zzz com_google_android_gms_measurement_internal_zzz) {
            this.zzaUZ = com_google_android_gms_measurement_internal_zzz;
        }

        public void run() {
            zzl zzc = this.zzaUZ.zzaUT;
            if (zzc == null) {
                this.zzaUZ.zzzz().zzBl().zzez("Discarding data. Failed to send app launch");
                return;
            }
            try {
                zzc.zza(this.zzaUZ.zzAS().zzex(this.zzaUZ.zzzz().zzBs()));
                this.zzaUZ.zzjr();
            } catch (RemoteException e) {
                this.zzaUZ.zzzz().zzBl().zzj("Failed to send app launch to AppMeasurementService", e);
            }
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.zzz.7 */
    class C04907 implements ServiceConnection {
        final /* synthetic */ zzz zzaUZ;

        C04907(zzz com_google_android_gms_measurement_internal_zzz) {
            this.zzaUZ = com_google_android_gms_measurement_internal_zzz;
        }

        public void onServiceConnected(ComponentName name, IBinder binder) {
        }

        public void onServiceDisconnected(ComponentName name) {
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.zzz.1 */
    class C08301 extends zze {
        final /* synthetic */ zzz zzaUZ;

        C08301(zzz com_google_android_gms_measurement_internal_zzz, zzt com_google_android_gms_measurement_internal_zzt) {
            this.zzaUZ = com_google_android_gms_measurement_internal_zzz;
            super(com_google_android_gms_measurement_internal_zzt);
        }

        public void run() {
            this.zzaUZ.zzjs();
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.zzz.2 */
    class C08312 extends zze {
        final /* synthetic */ zzz zzaUZ;

        C08312(zzz com_google_android_gms_measurement_internal_zzz, zzt com_google_android_gms_measurement_internal_zzt) {
            this.zzaUZ = com_google_android_gms_measurement_internal_zzz;
            super(com_google_android_gms_measurement_internal_zzt);
        }

        public void run() {
            this.zzaUZ.zzzz().zzBm().zzez("Tasks have been queued for a long time");
        }
    }

    protected class zza implements ServiceConnection, ConnectionCallbacks, OnConnectionFailedListener {
        final /* synthetic */ zzz zzaUZ;
        private volatile boolean zzaVa;
        private volatile zzn zzaVb;

        /* renamed from: com.google.android.gms.measurement.internal.zzz.zza.1 */
        class C04911 implements Runnable {
            final /* synthetic */ zzl zzaVc;
            final /* synthetic */ zza zzaVd;

            C04911(zza com_google_android_gms_measurement_internal_zzz_zza, zzl com_google_android_gms_measurement_internal_zzl) {
                this.zzaVd = com_google_android_gms_measurement_internal_zzz_zza;
                this.zzaVc = com_google_android_gms_measurement_internal_zzl;
            }

            public void run() {
                if (!this.zzaVd.zzaUZ.isConnected()) {
                    this.zzaVd.zzaUZ.zzzz().zzBq().zzez("Connected to service");
                    this.zzaVd.zzaUZ.zza(this.zzaVc);
                }
            }
        }

        /* renamed from: com.google.android.gms.measurement.internal.zzz.zza.2 */
        class C04922 implements Runnable {
            final /* synthetic */ ComponentName zzPw;
            final /* synthetic */ zza zzaVd;

            C04922(zza com_google_android_gms_measurement_internal_zzz_zza, ComponentName componentName) {
                this.zzaVd = com_google_android_gms_measurement_internal_zzz_zza;
                this.zzPw = componentName;
            }

            public void run() {
                this.zzaVd.zzaUZ.onServiceDisconnected(this.zzPw);
            }
        }

        /* renamed from: com.google.android.gms.measurement.internal.zzz.zza.3 */
        class C04933 implements Runnable {
            final /* synthetic */ zza zzaVd;
            final /* synthetic */ zzl zzaVe;

            C04933(zza com_google_android_gms_measurement_internal_zzz_zza, zzl com_google_android_gms_measurement_internal_zzl) {
                this.zzaVd = com_google_android_gms_measurement_internal_zzz_zza;
                this.zzaVe = com_google_android_gms_measurement_internal_zzl;
            }

            public void run() {
                if (!this.zzaVd.zzaUZ.isConnected()) {
                    this.zzaVd.zzaUZ.zzzz().zzBq().zzez("Connected to remote service");
                    this.zzaVd.zzaUZ.zza(this.zzaVe);
                }
            }
        }

        /* renamed from: com.google.android.gms.measurement.internal.zzz.zza.4 */
        class C04944 implements Runnable {
            final /* synthetic */ zza zzaVd;

            C04944(zza com_google_android_gms_measurement_internal_zzz_zza) {
                this.zzaVd = com_google_android_gms_measurement_internal_zzz_zza;
            }

            public void run() {
                this.zzaVd.zzaUZ.onServiceDisconnected(new ComponentName(this.zzaVd.zzaUZ.getContext(), AppMeasurementService.class));
            }
        }

        protected zza(zzz com_google_android_gms_measurement_internal_zzz) {
            this.zzaUZ = com_google_android_gms_measurement_internal_zzz;
        }

        public void onConnected(Bundle connectionHint) {
            zzx.zzcx("MeasurementServiceConnection.onConnected");
            synchronized (this) {
                this.zzaVa = false;
                try {
                    zzl com_google_android_gms_measurement_internal_zzl = (zzl) this.zzaVb.zzqs();
                    this.zzaVb = null;
                    this.zzaUZ.zzAV().zzg(new C04933(this, com_google_android_gms_measurement_internal_zzl));
                } catch (DeadObjectException e) {
                    this.zzaVb = null;
                } catch (IllegalStateException e2) {
                    this.zzaVb = null;
                }
            }
        }

        public void onConnectionFailed(ConnectionResult result) {
            zzx.zzcx("MeasurementServiceConnection.onConnectionFailed");
            this.zzaUZ.zzzz().zzBm().zzj("Service connection failed", result);
            synchronized (this) {
                this.zzaVa = false;
                this.zzaVb = null;
            }
        }

        public void onConnectionSuspended(int cause) {
            zzx.zzcx("MeasurementServiceConnection.onConnectionSuspended");
            this.zzaUZ.zzzz().zzBq().zzez("Service connection suspended");
            this.zzaUZ.zzAV().zzg(new C04944(this));
        }

        public void onServiceConnected(ComponentName name, IBinder binder) {
            zzx.zzcx("MeasurementServiceConnection.onServiceConnected");
            synchronized (this) {
                this.zzaVa = false;
                if (binder == null) {
                    this.zzaUZ.zzzz().zzBl().zzez("Service connected with null binder");
                    return;
                }
                zzl com_google_android_gms_measurement_internal_zzl = null;
                try {
                    String interfaceDescriptor = binder.getInterfaceDescriptor();
                    if ("com.google.android.gms.measurement.internal.IMeasurementService".equals(interfaceDescriptor)) {
                        com_google_android_gms_measurement_internal_zzl = com.google.android.gms.measurement.internal.zzl.zza.zzdi(binder);
                        this.zzaUZ.zzzz().zzBr().zzez("Bound to IMeasurementService interface");
                    } else {
                        this.zzaUZ.zzzz().zzBl().zzj("Got binder with a wrong descriptor", interfaceDescriptor);
                    }
                } catch (RemoteException e) {
                    this.zzaUZ.zzzz().zzBl().zzez("Service connect failed to get IMeasurementService");
                }
                if (com_google_android_gms_measurement_internal_zzl == null) {
                    try {
                        zzb.zzrz().zza(this.zzaUZ.getContext(), this.zzaUZ.zzaUS);
                    } catch (IllegalArgumentException e2) {
                    }
                } else {
                    this.zzaUZ.zzAV().zzg(new C04911(this, com_google_android_gms_measurement_internal_zzl));
                }
            }
        }

        public void onServiceDisconnected(ComponentName name) {
            zzx.zzcx("MeasurementServiceConnection.onServiceDisconnected");
            this.zzaUZ.zzzz().zzBq().zzez("Service disconnected");
            this.zzaUZ.zzAV().zzg(new C04922(this, name));
        }

        public void zzA(Intent intent) {
            this.zzaUZ.zziS();
            Context context = this.zzaUZ.getContext();
            zzb zzrz = zzb.zzrz();
            synchronized (this) {
                if (this.zzaVa) {
                    this.zzaUZ.zzzz().zzBr().zzez("Connection attempt already in progress");
                    return;
                }
                this.zzaVa = true;
                zzrz.zza(context, intent, this.zzaUZ.zzaUS, 129);
            }
        }

        public void zzCa() {
            this.zzaUZ.zziS();
            Context context = this.zzaUZ.getContext();
            synchronized (this) {
                if (this.zzaVa) {
                    this.zzaUZ.zzzz().zzBr().zzez("Connection attempt already in progress");
                } else if (this.zzaVb != null) {
                    this.zzaUZ.zzzz().zzBr().zzez("Already awaiting connection attempt");
                } else {
                    this.zzaVb = new zzn(context, Looper.getMainLooper(), zzf.zzas(context), this, this);
                    this.zzaUZ.zzzz().zzBr().zzez("Connecting to remote service");
                    this.zzaVa = true;
                    this.zzaVb.zzqp();
                }
            }
        }
    }

    protected zzz(zzt com_google_android_gms_measurement_internal_zzt) {
        super(com_google_android_gms_measurement_internal_zzt);
        this.zzaUX = new ArrayList();
        this.zzaUW = new zzaa(com_google_android_gms_measurement_internal_zzt.zziT());
        this.zzaUS = new zza(this);
        this.zzaUV = new C08301(this, com_google_android_gms_measurement_internal_zzt);
        this.zzaUY = new C08312(this, com_google_android_gms_measurement_internal_zzt);
    }

    private void onServiceDisconnected(ComponentName name) {
        zziS();
        if (this.zzaUT != null) {
            this.zzaUT = null;
            zzzz().zzBr().zzj("Disconnected from device MeasurementService", name);
            zzBY();
        }
    }

    private boolean zzBW() {
        List queryIntentServices = getContext().getPackageManager().queryIntentServices(new Intent(getContext(), AppMeasurementService.class), ExecutionOptions.MAX_TRACKING_TAG_STRING_LENGTH);
        return queryIntentServices != null && queryIntentServices.size() > 0;
    }

    private boolean zzBX() {
        zziS();
        zzje();
        if (zzAX().zzka()) {
            return true;
        }
        Intent intent = new Intent("com.google.android.gms.measurement.START");
        intent.setComponent(new ComponentName(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE, "com.google.android.gms.measurement.service.MeasurementBrokerService"));
        zzb zzrz = zzb.zzrz();
        zzzz().zzBr().zzez("Checking service availability");
        if (!zzrz.zza(getContext(), intent, new C04907(this), 0)) {
            return false;
        }
        zzzz().zzBr().zzez("Service available");
        return true;
    }

    private void zzBY() {
        zziS();
        zzjG();
    }

    private void zzBZ() {
        zziS();
        zzzz().zzBr().zzj("Processing queued up service tasks", Integer.valueOf(this.zzaUX.size()));
        for (Runnable zzg : this.zzaUX) {
            zzAV().zzg(zzg);
        }
        this.zzaUX.clear();
        this.zzaUY.cancel();
    }

    private void zza(zzl com_google_android_gms_measurement_internal_zzl) {
        zziS();
        zzx.zzy(com_google_android_gms_measurement_internal_zzl);
        this.zzaUT = com_google_android_gms_measurement_internal_zzl;
        zzjr();
        zzBZ();
    }

    private void zzi(Runnable runnable) throws IllegalStateException {
        zziS();
        if (isConnected()) {
            runnable.run();
        } else if (((long) this.zzaUX.size()) >= zzAX().zzAH()) {
            zzzz().zzBl().zzez("Discarding data. Max runnable queue size reached");
        } else {
            this.zzaUX.add(runnable);
            if (!this.zzaQX.zzBI()) {
                this.zzaUY.zzt(60000);
            }
            zzjG();
        }
    }

    private void zzjG() {
        zziS();
        zzje();
        if (!isConnected()) {
            if (this.zzaUU == null) {
                this.zzaUU = zzAW().zzBx();
                if (this.zzaUU == null) {
                    zzzz().zzBr().zzez("State of service unknown");
                    this.zzaUU = Boolean.valueOf(zzBX());
                    zzAW().zzap(this.zzaUU.booleanValue());
                }
            }
            if (this.zzaUU.booleanValue()) {
                zzzz().zzBr().zzez("Using measurement service");
                this.zzaUS.zzCa();
            } else if (zzBW() && !this.zzaQX.zzBI()) {
                zzzz().zzBr().zzez("Using local app measurement service");
                Intent intent = new Intent("com.google.android.gms.measurement.START");
                intent.setComponent(new ComponentName(getContext(), AppMeasurementService.class));
                this.zzaUS.zzA(intent);
            } else if (zzAX().zzkb()) {
                zzzz().zzBr().zzez("Using direct local measurement implementation");
                zza(new zzu(this.zzaQX, true));
            } else {
                zzzz().zzBl().zzez("Not in main process. Unable to use local measurement implementation. Please register the AppMeasurementService service in the app manifest");
            }
        }
    }

    private void zzjr() {
        zziS();
        this.zzaUW.start();
        if (!this.zzaQX.zzBI()) {
            this.zzaUV.zzt(zzAX().zzkv());
        }
    }

    private void zzjs() {
        zziS();
        if (isConnected()) {
            zzzz().zzBr().zzez("Inactivity, disconnecting from AppMeasurementService");
            disconnect();
        }
    }

    public void disconnect() {
        zziS();
        zzje();
        try {
            zzb.zzrz().zza(getContext(), this.zzaUS);
        } catch (IllegalStateException e) {
        } catch (IllegalArgumentException e2) {
        }
        this.zzaUT = null;
    }

    public /* bridge */ /* synthetic */ Context getContext() {
        return super.getContext();
    }

    public boolean isConnected() {
        zziS();
        zzje();
        return this.zzaUT != null;
    }

    public /* bridge */ /* synthetic */ void zzAR() {
        super.zzAR();
    }

    public /* bridge */ /* synthetic */ zzm zzAS() {
        return super.zzAS();
    }

    public /* bridge */ /* synthetic */ zzz zzAT() {
        return super.zzAT();
    }

    public /* bridge */ /* synthetic */ zzae zzAU() {
        return super.zzAU();
    }

    public /* bridge */ /* synthetic */ zzs zzAV() {
        return super.zzAV();
    }

    public /* bridge */ /* synthetic */ zzr zzAW() {
        return super.zzAW();
    }

    public /* bridge */ /* synthetic */ zzc zzAX() {
        return super.zzAX();
    }

    protected void zzBS() {
        zziS();
        zzje();
        zzi(new C04896(this));
    }

    protected void zzBV() {
        zziS();
        zzje();
        zzi(new C04863(this));
    }

    protected void zza(UserAttributeParcel userAttributeParcel) {
        zziS();
        zzje();
        zzi(new C04885(this, userAttributeParcel));
    }

    protected void zzb(EventParcel eventParcel, String str) {
        zzx.zzy(eventParcel);
        zziS();
        zzje();
        zzi(new C04874(this, str, eventParcel));
    }

    public /* bridge */ /* synthetic */ void zziR() {
        super.zziR();
    }

    public /* bridge */ /* synthetic */ void zziS() {
        super.zziS();
    }

    public /* bridge */ /* synthetic */ zznl zziT() {
        return super.zziT();
    }

    protected void zzir() {
    }

    public /* bridge */ /* synthetic */ zzo zzzz() {
        return super.zzzz();
    }
}
