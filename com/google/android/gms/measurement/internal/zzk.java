package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.zzd;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.drive.ExecutionOptions;
import com.google.android.gms.internal.zzmt;

public final class zzk {
    public static zza<Boolean> zzaSO;
    public static zza<Boolean> zzaSP;
    public static zza<String> zzaSQ;
    public static zza<Long> zzaSR;
    public static zza<Long> zzaSS;
    public static zza<Integer> zzaST;
    public static zza<Integer> zzaSU;
    public static zza<String> zzaSV;
    public static zza<Long> zzaSW;
    public static zza<Long> zzaSX;
    public static zza<Long> zzaSY;
    public static zza<Long> zzaSZ;
    public static zza<Long> zzaTa;
    public static zza<Long> zzaTb;
    public static zza<Integer> zzaTc;
    public static zza<Long> zzaTd;
    public static zza<Long> zzaTe;

    public static final class zza<V> {
        private final V zzRg;
        private final zzmt<V> zzRh;
        private V zzRi;

        private zza(zzmt<V> com_google_android_gms_internal_zzmt_V, V v) {
            zzx.zzy(com_google_android_gms_internal_zzmt_V);
            this.zzRh = com_google_android_gms_internal_zzmt_V;
            this.zzRg = v;
        }

        static zza<Integer> zzA(String str, int i) {
            return zzo(str, i, i);
        }

        static zza<String> zzN(String str, String str2) {
            return zzj(str, str2, str2);
        }

        static zza<Long> zzb(String str, long j, long j2) {
            return new zza(zzmt.zza(str, Long.valueOf(j2)), Long.valueOf(j));
        }

        static zza<Boolean> zzb(String str, boolean z, boolean z2) {
            return new zza(zzmt.zzg(str, z2), Boolean.valueOf(z));
        }

        static zza<Long> zzf(String str, long j) {
            return zzb(str, j, j);
        }

        static zza<Boolean> zzi(String str, boolean z) {
            return zzb(str, z, z);
        }

        static zza<String> zzj(String str, String str2, String str3) {
            return new zza(zzmt.zzw(str, str3), str2);
        }

        static zza<Integer> zzo(String str, int i, int i2) {
            return new zza(zzmt.zza(str, Integer.valueOf(i2)), Integer.valueOf(i));
        }

        public V get() {
            return this.zzRi != null ? this.zzRi : (zzd.zzaiU && zzmt.isInitialized()) ? this.zzRh.zzpF() : this.zzRg;
        }
    }

    static {
        zzaSO = zza.zzi("measurement.service_enabled", true);
        zzaSP = zza.zzi("measurement.service_client_enabled", true);
        zzaSQ = zza.zzj("measurement.log_tag", "GMPM", "GMPM-SVC");
        zzaSR = zza.zzf("measurement.ad_id_cache_time", 10000);
        zzaSS = zza.zzf("measurement.monitoring.sample_period_millis", 86400000);
        zzaST = zza.zzA("measurement.upload.max_bundles", 100);
        zzaSU = zza.zzA("measurement.upload.max_batch_size", ExecutionOptions.MAX_TRACKING_TAG_STRING_LENGTH);
        zzaSV = zza.zzN("measurement.upload.url", "https://app-measurement.com/a");
        zzaSW = zza.zzf("measurement.upload.backoff_period", 43200000);
        zzaSX = zza.zzf("measurement.upload.window_interval", 3600000);
        zzaSY = zza.zzf("measurement.upload.interval", 3600000);
        zzaSZ = zza.zzf("measurement.upload.stale_data_deletion_interval", 86400000);
        zzaTa = zza.zzf("measurement.upload.initial_upload_delay_time", 15000);
        zzaTb = zza.zzf("measurement.upload.retry_time", 1800000);
        zzaTc = zza.zzA("measurement.upload.retry_count", 6);
        zzaTd = zza.zzf("measurement.upload.max_queue_time", 2419200000L);
        zzaTe = zza.zzf("measurement.service_client.idle_disconnect_millis", 5000);
    }
}
