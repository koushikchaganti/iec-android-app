package com.google.android.gms.measurement.internal;

import android.os.Binder;
import android.os.Process;
import android.text.TextUtils;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.measurement.internal.zzl.zza;

public class zzu extends zza {
    private final zzt zzaQX;
    private final boolean zzaUB;

    /* renamed from: com.google.android.gms.measurement.internal.zzu.1 */
    class C04761 implements Runnable {
        final /* synthetic */ AppMetadata zzaUC;
        final /* synthetic */ zzu zzaUD;

        C04761(zzu com_google_android_gms_measurement_internal_zzu, AppMetadata appMetadata) {
            this.zzaUD = com_google_android_gms_measurement_internal_zzu;
            this.zzaUC = appMetadata;
        }

        public void run() {
            this.zzaUD.zzeA(this.zzaUC.zzaSr);
            this.zzaUD.zzaQX.zzc(this.zzaUC);
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.zzu.2 */
    class C04772 implements Runnable {
        final /* synthetic */ AppMetadata zzaUC;
        final /* synthetic */ zzu zzaUD;
        final /* synthetic */ EventParcel zzaUE;

        C04772(zzu com_google_android_gms_measurement_internal_zzu, AppMetadata appMetadata, EventParcel eventParcel) {
            this.zzaUD = com_google_android_gms_measurement_internal_zzu;
            this.zzaUC = appMetadata;
            this.zzaUE = eventParcel;
        }

        public void run() {
            this.zzaUD.zzeA(this.zzaUC.zzaSr);
            this.zzaUD.zzaQX.zzb(this.zzaUE, this.zzaUC);
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.zzu.3 */
    class C04783 implements Runnable {
        final /* synthetic */ String zzaFm;
        final /* synthetic */ zzu zzaUD;
        final /* synthetic */ EventParcel zzaUE;
        final /* synthetic */ String zzaUF;

        C04783(zzu com_google_android_gms_measurement_internal_zzu, String str, EventParcel eventParcel, String str2) {
            this.zzaUD = com_google_android_gms_measurement_internal_zzu;
            this.zzaUF = str;
            this.zzaUE = eventParcel;
            this.zzaFm = str2;
        }

        public void run() {
            this.zzaUD.zzeA(this.zzaUF);
            this.zzaUD.zzaQX.zza(this.zzaUE, this.zzaFm);
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.zzu.4 */
    class C04794 implements Runnable {
        final /* synthetic */ AppMetadata zzaUC;
        final /* synthetic */ zzu zzaUD;
        final /* synthetic */ UserAttributeParcel zzaUG;

        C04794(zzu com_google_android_gms_measurement_internal_zzu, AppMetadata appMetadata, UserAttributeParcel userAttributeParcel) {
            this.zzaUD = com_google_android_gms_measurement_internal_zzu;
            this.zzaUC = appMetadata;
            this.zzaUG = userAttributeParcel;
        }

        public void run() {
            this.zzaUD.zzeA(this.zzaUC.zzaSr);
            this.zzaUD.zzaQX.zzc(this.zzaUG, this.zzaUC);
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.zzu.5 */
    class C04805 implements Runnable {
        final /* synthetic */ AppMetadata zzaUC;
        final /* synthetic */ zzu zzaUD;
        final /* synthetic */ UserAttributeParcel zzaUG;

        C04805(zzu com_google_android_gms_measurement_internal_zzu, AppMetadata appMetadata, UserAttributeParcel userAttributeParcel) {
            this.zzaUD = com_google_android_gms_measurement_internal_zzu;
            this.zzaUC = appMetadata;
            this.zzaUG = userAttributeParcel;
        }

        public void run() {
            this.zzaUD.zzeA(this.zzaUC.zzaSr);
            this.zzaUD.zzaQX.zzb(this.zzaUG, this.zzaUC);
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.zzu.6 */
    class C04816 implements Runnable {
        final /* synthetic */ AppMetadata zzaUC;
        final /* synthetic */ zzu zzaUD;

        C04816(zzu com_google_android_gms_measurement_internal_zzu, AppMetadata appMetadata) {
            this.zzaUD = com_google_android_gms_measurement_internal_zzu;
            this.zzaUC = appMetadata;
        }

        public void run() {
            this.zzaUD.zzeA(this.zzaUC.zzaSr);
            this.zzaUD.zzaQX.zzd(this.zzaUC);
        }
    }

    public zzu(zzt com_google_android_gms_measurement_internal_zzt) {
        zzx.zzy(com_google_android_gms_measurement_internal_zzt);
        this.zzaQX = com_google_android_gms_measurement_internal_zzt;
        this.zzaUB = false;
    }

    public zzu(zzt com_google_android_gms_measurement_internal_zzt, boolean z) {
        zzx.zzy(com_google_android_gms_measurement_internal_zzt);
        this.zzaQX = com_google_android_gms_measurement_internal_zzt;
        this.zzaUB = z;
    }

    private void zzeB(String str) throws SecurityException {
        if (TextUtils.isEmpty(str)) {
            this.zzaQX.zzzz().zzBl().zzez("Measurement Service called without app package");
            throw new SecurityException("Measurement Service called without app package");
        }
        try {
            zzeC(str);
        } catch (SecurityException e) {
            this.zzaQX.zzzz().zzBl().zzj("Measurement Service called with invalid calling package", str);
            throw e;
        }
    }

    private void zzeC(String str) throws SecurityException {
        int myUid = this.zzaUB ? Process.myUid() : Binder.getCallingUid();
        if (!GooglePlayServicesUtil.zzb(this.zzaQX.getContext(), myUid, str)) {
            if (!GooglePlayServicesUtil.zze(this.zzaQX.getContext(), myUid) || this.zzaQX.zzBI()) {
                throw new SecurityException(String.format("Unknown calling package name '%s'.", new Object[]{str}));
            }
        }
    }

    public void zza(AppMetadata appMetadata) {
        zzx.zzy(appMetadata);
        zzeB(appMetadata.packageName);
        this.zzaQX.zzAV().zzg(new C04816(this, appMetadata));
    }

    public void zza(EventParcel eventParcel, AppMetadata appMetadata) {
        zzx.zzy(eventParcel);
        zzx.zzy(appMetadata);
        zzeB(appMetadata.packageName);
        this.zzaQX.zzAV().zzg(new C04772(this, appMetadata, eventParcel));
    }

    public void zza(EventParcel eventParcel, String str, String str2) {
        zzx.zzy(eventParcel);
        zzx.zzcG(str);
        zzeB(str);
        this.zzaQX.zzAV().zzg(new C04783(this, str2, eventParcel, str));
    }

    public void zza(UserAttributeParcel userAttributeParcel, AppMetadata appMetadata) {
        zzx.zzy(userAttributeParcel);
        zzx.zzy(appMetadata);
        zzeB(appMetadata.packageName);
        if (userAttributeParcel.getValue() == null) {
            this.zzaQX.zzAV().zzg(new C04794(this, appMetadata, userAttributeParcel));
        } else {
            this.zzaQX.zzAV().zzg(new C04805(this, appMetadata, userAttributeParcel));
        }
    }

    public void zzb(AppMetadata appMetadata) {
        zzx.zzy(appMetadata);
        zzeB(appMetadata.packageName);
        this.zzaQX.zzAV().zzg(new C04761(this, appMetadata));
    }

    void zzeA(String str) {
        if (!TextUtils.isEmpty(str)) {
            String[] split = str.split(":", 2);
            if (split.length == 2) {
                try {
                    long longValue = Long.valueOf(split[0]).longValue();
                    if (longValue > 0) {
                        this.zzaQX.zzAW().zzaTE.zzg(split[1], longValue);
                    } else {
                        this.zzaQX.zzzz().zzBm().zzj("Combining sample with a non-positive weight", Long.valueOf(longValue));
                    }
                } catch (NumberFormatException e) {
                    this.zzaQX.zzzz().zzBm().zzj("Combining sample with a non-number weight", split[0]);
                }
            }
        }
    }
}
