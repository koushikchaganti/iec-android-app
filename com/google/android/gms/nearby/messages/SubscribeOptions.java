package com.google.android.gms.nearby.messages;

import com.google.android.gms.common.internal.zzx;

public final class SubscribeOptions {
    public static final SubscribeOptions DEFAULT;
    private final Strategy zzaWL;
    private final MessageFilter zzaWY;
    private final SubscribeCallback zzaWZ;

    public static class Builder {
        private Strategy zzaWL;
        private MessageFilter zzaWY;
        private SubscribeCallback zzaWZ;

        public Builder() {
            this.zzaWL = Strategy.DEFAULT;
            this.zzaWY = MessageFilter.INCLUDE_ALL_MY_TYPES;
        }

        public SubscribeOptions build() {
            return new SubscribeOptions(this.zzaWY, this.zzaWZ, null);
        }

        public Builder setCallback(SubscribeCallback callback) {
            this.zzaWZ = (SubscribeCallback) zzx.zzy(callback);
            return this;
        }

        public Builder setFilter(MessageFilter filter) {
            this.zzaWY = filter;
            return this;
        }

        public Builder setStrategy(Strategy strategy) {
            this.zzaWL = strategy;
            return this;
        }
    }

    static {
        DEFAULT = new Builder().build();
    }

    private SubscribeOptions(Strategy strategy, MessageFilter filter, SubscribeCallback callback) {
        this.zzaWL = strategy;
        this.zzaWY = filter;
        this.zzaWZ = callback;
    }

    public SubscribeCallback getCallback() {
        return this.zzaWZ;
    }

    public MessageFilter getFilter() {
        return this.zzaWY;
    }

    public Strategy getStrategy() {
        return this.zzaWL;
    }
}
