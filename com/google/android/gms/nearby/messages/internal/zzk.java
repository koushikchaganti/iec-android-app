package com.google.android.gms.nearby.messages.internal;

import android.content.Context;
import android.os.Looper;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api.zzc;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzlx.zzb;
import com.google.android.gms.nearby.messages.Message;
import com.google.android.gms.nearby.messages.MessageFilter;
import com.google.android.gms.nearby.messages.MessageListener;
import com.google.android.gms.nearby.messages.Messages;
import com.google.android.gms.nearby.messages.MessagesOptions;
import com.google.android.gms.nearby.messages.PublishOptions;
import com.google.android.gms.nearby.messages.PublishOptions.Builder;
import com.google.android.gms.nearby.messages.StatusCallback;
import com.google.android.gms.nearby.messages.Strategy;
import com.google.android.gms.nearby.messages.SubscribeOptions;

public class zzk implements Messages {
    public static final zzc<zzj> zzTo;
    public static final com.google.android.gms.common.api.Api.zza<zzj, MessagesOptions> zzTp;

    /* renamed from: com.google.android.gms.nearby.messages.internal.zzk.1 */
    static class C08341 extends com.google.android.gms.common.api.Api.zza<zzj, MessagesOptions> {
        C08341() {
        }

        public int getPriority() {
            return Strategy.TTL_SECONDS_INFINITE;
        }

        public zzj zza(Context context, Looper looper, zzf com_google_android_gms_common_internal_zzf, MessagesOptions messagesOptions, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            return new zzj(context, looper, connectionCallbacks, onConnectionFailedListener, com_google_android_gms_common_internal_zzf, messagesOptions);
        }
    }

    static abstract class zza extends com.google.android.gms.internal.zzlx.zza<Status, zzj> {
        public zza(GoogleApiClient googleApiClient) {
            super(zzk.zzTo, googleApiClient);
        }

        public Status zzb(Status status) {
            return status;
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzb(status);
        }
    }

    /* renamed from: com.google.android.gms.nearby.messages.internal.zzk.2 */
    class C12412 extends zza {
        final /* synthetic */ StatusCallback zzaXy;
        final /* synthetic */ zzk zzaXz;

        C12412(zzk com_google_android_gms_nearby_messages_internal_zzk, GoogleApiClient googleApiClient, StatusCallback statusCallback) {
            this.zzaXz = com_google_android_gms_nearby_messages_internal_zzk;
            this.zzaXy = statusCallback;
            super(googleApiClient);
        }

        protected void zza(zzj com_google_android_gms_nearby_messages_internal_zzj) throws RemoteException {
            com_google_android_gms_nearby_messages_internal_zzj.zza((zzb) this, this.zzaXy);
        }
    }

    /* renamed from: com.google.android.gms.nearby.messages.internal.zzk.3 */
    class C12423 extends zza {
        final /* synthetic */ StatusCallback zzaXy;
        final /* synthetic */ zzk zzaXz;

        C12423(zzk com_google_android_gms_nearby_messages_internal_zzk, GoogleApiClient googleApiClient, StatusCallback statusCallback) {
            this.zzaXz = com_google_android_gms_nearby_messages_internal_zzk;
            this.zzaXy = statusCallback;
            super(googleApiClient);
        }

        protected void zza(zzj com_google_android_gms_nearby_messages_internal_zzj) throws RemoteException {
            com_google_android_gms_nearby_messages_internal_zzj.zzb(this, this.zzaXy);
        }
    }

    /* renamed from: com.google.android.gms.nearby.messages.internal.zzk.4 */
    class C12434 extends zza {
        final /* synthetic */ Message zzaXA;
        final /* synthetic */ PublishOptions zzaXB;
        final /* synthetic */ zzk zzaXz;

        C12434(zzk com_google_android_gms_nearby_messages_internal_zzk, GoogleApiClient googleApiClient, Message message, PublishOptions publishOptions) {
            this.zzaXz = com_google_android_gms_nearby_messages_internal_zzk;
            this.zzaXA = message;
            this.zzaXB = publishOptions;
            super(googleApiClient);
        }

        protected void zza(zzj com_google_android_gms_nearby_messages_internal_zzj) throws RemoteException {
            com_google_android_gms_nearby_messages_internal_zzj.zza(this, MessageWrapper.zza(this.zzaXA), this.zzaXB);
        }
    }

    /* renamed from: com.google.android.gms.nearby.messages.internal.zzk.5 */
    class C12445 extends zza {
        final /* synthetic */ Message zzaXA;
        final /* synthetic */ zzk zzaXz;

        C12445(zzk com_google_android_gms_nearby_messages_internal_zzk, GoogleApiClient googleApiClient, Message message) {
            this.zzaXz = com_google_android_gms_nearby_messages_internal_zzk;
            this.zzaXA = message;
            super(googleApiClient);
        }

        protected void zza(zzj com_google_android_gms_nearby_messages_internal_zzj) throws RemoteException {
            com_google_android_gms_nearby_messages_internal_zzj.zza((zzb) this, MessageWrapper.zza(this.zzaXA));
        }
    }

    /* renamed from: com.google.android.gms.nearby.messages.internal.zzk.6 */
    class C12456 extends zza {
        final /* synthetic */ MessageListener zzaXC;
        final /* synthetic */ SubscribeOptions zzaXD;
        final /* synthetic */ zzk zzaXz;

        C12456(zzk com_google_android_gms_nearby_messages_internal_zzk, GoogleApiClient googleApiClient, MessageListener messageListener, SubscribeOptions subscribeOptions) {
            this.zzaXz = com_google_android_gms_nearby_messages_internal_zzk;
            this.zzaXC = messageListener;
            this.zzaXD = subscribeOptions;
            super(googleApiClient);
        }

        protected void zza(zzj com_google_android_gms_nearby_messages_internal_zzj) throws RemoteException {
            com_google_android_gms_nearby_messages_internal_zzj.zza(this, this.zzaXC, this.zzaXD, null);
        }
    }

    /* renamed from: com.google.android.gms.nearby.messages.internal.zzk.7 */
    class C12467 extends zza {
        final /* synthetic */ MessageListener zzaXC;
        final /* synthetic */ zzk zzaXz;

        C12467(zzk com_google_android_gms_nearby_messages_internal_zzk, GoogleApiClient googleApiClient, MessageListener messageListener) {
            this.zzaXz = com_google_android_gms_nearby_messages_internal_zzk;
            this.zzaXC = messageListener;
            super(googleApiClient);
        }

        protected void zza(zzj com_google_android_gms_nearby_messages_internal_zzj) throws RemoteException {
            com_google_android_gms_nearby_messages_internal_zzj.zza((zzb) this, this.zzaXC);
        }
    }

    /* renamed from: com.google.android.gms.nearby.messages.internal.zzk.8 */
    class C12478 extends zza {
        final /* synthetic */ zzk zzaXz;

        C12478(zzk com_google_android_gms_nearby_messages_internal_zzk, GoogleApiClient googleApiClient) {
            this.zzaXz = com_google_android_gms_nearby_messages_internal_zzk;
            super(googleApiClient);
        }

        protected void zza(zzj com_google_android_gms_nearby_messages_internal_zzj) throws RemoteException {
            com_google_android_gms_nearby_messages_internal_zzj.zzj(this);
        }
    }

    static {
        zzTo = new zzc();
        zzTp = new C08341();
    }

    public PendingResult<Status> getPermissionStatus(GoogleApiClient client) {
        return client.zzb(new C12478(this, client));
    }

    public PendingResult<Status> publish(GoogleApiClient client, Message message) {
        return publish(client, message, PublishOptions.DEFAULT);
    }

    public PendingResult<Status> publish(GoogleApiClient client, Message message, PublishOptions options) {
        zzx.zzy(message);
        zzx.zzy(options);
        return client.zzb(new C12434(this, client, message, options));
    }

    @Deprecated
    public PendingResult<Status> publish(GoogleApiClient client, Message message, Strategy strategy) {
        return publish(client, message, new Builder().setStrategy(strategy).build());
    }

    public PendingResult<Status> registerStatusCallback(GoogleApiClient client, StatusCallback statusCallback) {
        zzx.zzy(statusCallback);
        return client.zzb(new C12412(this, client, statusCallback));
    }

    @Deprecated
    public PendingResult<Status> subscribe(GoogleApiClient client, MessageListener listener) {
        return subscribe(client, listener, SubscribeOptions.DEFAULT);
    }

    @Deprecated
    public PendingResult<Status> subscribe(GoogleApiClient client, MessageListener listener, Strategy strategy) {
        return subscribe(client, listener, new SubscribeOptions.Builder().setStrategy(strategy).build());
    }

    @Deprecated
    public PendingResult<Status> subscribe(GoogleApiClient client, MessageListener listener, Strategy strategy, MessageFilter filter) {
        return subscribe(client, listener, new SubscribeOptions.Builder().setStrategy(strategy).setFilter(filter).build());
    }

    public PendingResult<Status> subscribe(GoogleApiClient client, MessageListener listener, SubscribeOptions options) {
        zzx.zzy(listener);
        zzx.zzy(options);
        return client.zzb(new C12456(this, client, listener, options));
    }

    public PendingResult<Status> unpublish(GoogleApiClient client, Message message) {
        zzx.zzy(message);
        return client.zzb(new C12445(this, client, message));
    }

    public PendingResult<Status> unregisterStatusCallback(GoogleApiClient client, StatusCallback statusCallback) {
        return client.zzb(new C12423(this, client, statusCallback));
    }

    public PendingResult<Status> unsubscribe(GoogleApiClient client, MessageListener listener) {
        zzx.zzy(listener);
        return client.zzb(new C12467(this, client, listener));
    }
}
