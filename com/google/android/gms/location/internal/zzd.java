package com.google.android.gms.location.internal;

import android.app.PendingIntent;
import android.location.Location;
import android.os.Looper;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class zzd implements FusedLocationProviderApi {

    private static class zzb extends com.google.android.gms.location.internal.zzg.zza {
        private final com.google.android.gms.internal.zzlx.zzb<Status> zzakL;

        public zzb(com.google.android.gms.internal.zzlx.zzb<Status> com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_common_api_Status) {
            this.zzakL = com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_common_api_Status;
        }

        public void zza(FusedLocationProviderResult fusedLocationProviderResult) {
            this.zzakL.zzr(fusedLocationProviderResult.getStatus());
        }
    }

    private static abstract class zza extends com.google.android.gms.location.LocationServices.zza<Status> {
        public zza(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        public Status zzb(Status status) {
            return status;
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzb(status);
        }
    }

    /* renamed from: com.google.android.gms.location.internal.zzd.10 */
    class AnonymousClass10 extends zza {
        final /* synthetic */ PendingIntent zzaLC;
        final /* synthetic */ zzd zzaLJ;

        AnonymousClass10(zzd com_google_android_gms_location_internal_zzd, GoogleApiClient googleApiClient, PendingIntent pendingIntent) {
            this.zzaLJ = com_google_android_gms_location_internal_zzd;
            this.zzaLC = pendingIntent;
            super(googleApiClient);
        }

        protected void zza(zzl com_google_android_gms_location_internal_zzl) throws RemoteException {
            com_google_android_gms_location_internal_zzl.zza(this.zzaLC, new zzb(this));
        }
    }

    /* renamed from: com.google.android.gms.location.internal.zzd.1 */
    class C14321 extends zza {
        final /* synthetic */ LocationRequest zzaLH;
        final /* synthetic */ LocationListener zzaLI;
        final /* synthetic */ zzd zzaLJ;

        C14321(zzd com_google_android_gms_location_internal_zzd, GoogleApiClient googleApiClient, LocationRequest locationRequest, LocationListener locationListener) {
            this.zzaLJ = com_google_android_gms_location_internal_zzd;
            this.zzaLH = locationRequest;
            this.zzaLI = locationListener;
            super(googleApiClient);
        }

        protected void zza(zzl com_google_android_gms_location_internal_zzl) throws RemoteException {
            com_google_android_gms_location_internal_zzl.zza(this.zzaLH, this.zzaLI, null, new zzb(this));
        }
    }

    /* renamed from: com.google.android.gms.location.internal.zzd.2 */
    class C14332 extends zza {
        final /* synthetic */ zzd zzaLJ;
        final /* synthetic */ LocationCallback zzaLK;

        C14332(zzd com_google_android_gms_location_internal_zzd, GoogleApiClient googleApiClient, LocationCallback locationCallback) {
            this.zzaLJ = com_google_android_gms_location_internal_zzd;
            this.zzaLK = locationCallback;
            super(googleApiClient);
        }

        protected void zza(zzl com_google_android_gms_location_internal_zzl) throws RemoteException {
            com_google_android_gms_location_internal_zzl.zza(this.zzaLK, new zzb(this));
        }
    }

    /* renamed from: com.google.android.gms.location.internal.zzd.3 */
    class C14343 extends zza {
        final /* synthetic */ zzd zzaLJ;
        final /* synthetic */ boolean zzaLL;

        C14343(zzd com_google_android_gms_location_internal_zzd, GoogleApiClient googleApiClient, boolean z) {
            this.zzaLJ = com_google_android_gms_location_internal_zzd;
            this.zzaLL = z;
            super(googleApiClient);
        }

        protected void zza(zzl com_google_android_gms_location_internal_zzl) throws RemoteException {
            com_google_android_gms_location_internal_zzl.zzai(this.zzaLL);
            zzb(Status.zzaeX);
        }
    }

    /* renamed from: com.google.android.gms.location.internal.zzd.4 */
    class C14354 extends zza {
        final /* synthetic */ zzd zzaLJ;
        final /* synthetic */ Location zzaLM;

        C14354(zzd com_google_android_gms_location_internal_zzd, GoogleApiClient googleApiClient, Location location) {
            this.zzaLJ = com_google_android_gms_location_internal_zzd;
            this.zzaLM = location;
            super(googleApiClient);
        }

        protected void zza(zzl com_google_android_gms_location_internal_zzl) throws RemoteException {
            com_google_android_gms_location_internal_zzl.zzc(this.zzaLM);
            zzb(Status.zzaeX);
        }
    }

    /* renamed from: com.google.android.gms.location.internal.zzd.5 */
    class C14365 extends zza {
        final /* synthetic */ zzd zzaLJ;

        C14365(zzd com_google_android_gms_location_internal_zzd, GoogleApiClient googleApiClient) {
            this.zzaLJ = com_google_android_gms_location_internal_zzd;
            super(googleApiClient);
        }

        protected void zza(zzl com_google_android_gms_location_internal_zzl) throws RemoteException {
            com_google_android_gms_location_internal_zzl.zza(new zzb(this));
        }
    }

    /* renamed from: com.google.android.gms.location.internal.zzd.6 */
    class C14376 extends zza {
        final /* synthetic */ LocationRequest zzaLH;
        final /* synthetic */ LocationListener zzaLI;
        final /* synthetic */ zzd zzaLJ;
        final /* synthetic */ Looper zzaLN;

        C14376(zzd com_google_android_gms_location_internal_zzd, GoogleApiClient googleApiClient, LocationRequest locationRequest, LocationListener locationListener, Looper looper) {
            this.zzaLJ = com_google_android_gms_location_internal_zzd;
            this.zzaLH = locationRequest;
            this.zzaLI = locationListener;
            this.zzaLN = looper;
            super(googleApiClient);
        }

        protected void zza(zzl com_google_android_gms_location_internal_zzl) throws RemoteException {
            com_google_android_gms_location_internal_zzl.zza(this.zzaLH, this.zzaLI, this.zzaLN, new zzb(this));
        }
    }

    /* renamed from: com.google.android.gms.location.internal.zzd.7 */
    class C14387 extends zza {
        final /* synthetic */ LocationRequest zzaLH;
        final /* synthetic */ zzd zzaLJ;
        final /* synthetic */ LocationCallback zzaLK;
        final /* synthetic */ Looper zzaLN;

        C14387(zzd com_google_android_gms_location_internal_zzd, GoogleApiClient googleApiClient, LocationRequest locationRequest, LocationCallback locationCallback, Looper looper) {
            this.zzaLJ = com_google_android_gms_location_internal_zzd;
            this.zzaLH = locationRequest;
            this.zzaLK = locationCallback;
            this.zzaLN = looper;
            super(googleApiClient);
        }

        protected void zza(zzl com_google_android_gms_location_internal_zzl) throws RemoteException {
            com_google_android_gms_location_internal_zzl.zza(LocationRequestInternal.zzb(this.zzaLH), this.zzaLK, this.zzaLN, new zzb(this));
        }
    }

    /* renamed from: com.google.android.gms.location.internal.zzd.8 */
    class C14398 extends zza {
        final /* synthetic */ PendingIntent zzaLC;
        final /* synthetic */ LocationRequest zzaLH;
        final /* synthetic */ zzd zzaLJ;

        C14398(zzd com_google_android_gms_location_internal_zzd, GoogleApiClient googleApiClient, LocationRequest locationRequest, PendingIntent pendingIntent) {
            this.zzaLJ = com_google_android_gms_location_internal_zzd;
            this.zzaLH = locationRequest;
            this.zzaLC = pendingIntent;
            super(googleApiClient);
        }

        protected void zza(zzl com_google_android_gms_location_internal_zzl) throws RemoteException {
            com_google_android_gms_location_internal_zzl.zza(this.zzaLH, this.zzaLC, new zzb(this));
        }
    }

    /* renamed from: com.google.android.gms.location.internal.zzd.9 */
    class C14409 extends zza {
        final /* synthetic */ LocationListener zzaLI;
        final /* synthetic */ zzd zzaLJ;

        C14409(zzd com_google_android_gms_location_internal_zzd, GoogleApiClient googleApiClient, LocationListener locationListener) {
            this.zzaLJ = com_google_android_gms_location_internal_zzd;
            this.zzaLI = locationListener;
            super(googleApiClient);
        }

        protected void zza(zzl com_google_android_gms_location_internal_zzl) throws RemoteException {
            com_google_android_gms_location_internal_zzl.zza(this.zzaLI, new zzb(this));
        }
    }

    public PendingResult<Status> flushLocations(GoogleApiClient client) {
        return client.zzb(new C14365(this, client));
    }

    public Location getLastLocation(GoogleApiClient client) {
        try {
            return LocationServices.zzg(client).getLastLocation();
        } catch (Exception e) {
            return null;
        }
    }

    public LocationAvailability getLocationAvailability(GoogleApiClient client) {
        try {
            return LocationServices.zzg(client).zzyc();
        } catch (Exception e) {
            return null;
        }
    }

    public PendingResult<Status> removeLocationUpdates(GoogleApiClient client, PendingIntent callbackIntent) {
        return client.zzb(new AnonymousClass10(this, client, callbackIntent));
    }

    public PendingResult<Status> removeLocationUpdates(GoogleApiClient client, LocationCallback callback) {
        return client.zzb(new C14332(this, client, callback));
    }

    public PendingResult<Status> removeLocationUpdates(GoogleApiClient client, LocationListener listener) {
        return client.zzb(new C14409(this, client, listener));
    }

    public PendingResult<Status> requestLocationUpdates(GoogleApiClient client, LocationRequest request, PendingIntent callbackIntent) {
        return client.zzb(new C14398(this, client, request, callbackIntent));
    }

    public PendingResult<Status> requestLocationUpdates(GoogleApiClient client, LocationRequest request, LocationCallback callback, Looper looper) {
        return client.zzb(new C14387(this, client, request, callback, looper));
    }

    public PendingResult<Status> requestLocationUpdates(GoogleApiClient client, LocationRequest request, LocationListener listener) {
        return client.zzb(new C14321(this, client, request, listener));
    }

    public PendingResult<Status> requestLocationUpdates(GoogleApiClient client, LocationRequest request, LocationListener listener, Looper looper) {
        return client.zzb(new C14376(this, client, request, listener, looper));
    }

    public PendingResult<Status> setMockLocation(GoogleApiClient client, Location mockLocation) {
        return client.zzb(new C14354(this, client, mockLocation));
    }

    public PendingResult<Status> setMockMode(GoogleApiClient client, boolean isMockMode) {
        return client.zzb(new C14343(this, client, isMockMode));
    }
}
