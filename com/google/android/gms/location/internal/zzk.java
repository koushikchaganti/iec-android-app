package com.google.android.gms.location.internal;

import android.app.PendingIntent;
import android.content.ContentProviderClient;
import android.content.Context;
import android.location.Location;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.drive.events.CompletionEvent;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.zzd;
import com.google.android.gms.vision.barcode.Barcode.Phone;
import java.util.HashMap;
import java.util.Map;

public class zzk {
    private final Context mContext;
    private final zzp<zzi> zzaLF;
    private ContentProviderClient zzaLS;
    private boolean zzaLT;
    private Map<LocationCallback, zza> zzaLU;
    private Map<LocationListener, zzc> zzauU;

    private static class zzb extends Handler {
        private final LocationListener zzaLX;

        public zzb(LocationListener locationListener) {
            this.zzaLX = locationListener;
        }

        public zzb(LocationListener locationListener, Looper looper) {
            super(looper);
            this.zzaLX = locationListener;
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case CompletionEvent.STATUS_FAILURE /*1*/:
                    this.zzaLX.onLocationChanged(new Location((Location) msg.obj));
                default:
                    Log.e("LocationClientHelper", "unknown message in LocationHandler.handleMessage");
            }
        }
    }

    private static class zza extends com.google.android.gms.location.zzc.zza {
        private Handler zzaLV;

        /* renamed from: com.google.android.gms.location.internal.zzk.zza.1 */
        class C04611 extends Handler {
            final /* synthetic */ LocationCallback zzaLK;
            final /* synthetic */ zza zzaLW;

            C04611(zza com_google_android_gms_location_internal_zzk_zza, Looper looper, LocationCallback locationCallback) {
                this.zzaLW = com_google_android_gms_location_internal_zzk_zza;
                this.zzaLK = locationCallback;
                super(looper);
            }

            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case Phone.UNKNOWN /*0*/:
                        this.zzaLK.onLocationResult((LocationResult) msg.obj);
                    case CompletionEvent.STATUS_FAILURE /*1*/:
                        this.zzaLK.onLocationAvailability((LocationAvailability) msg.obj);
                    default:
                }
            }
        }

        zza(LocationCallback locationCallback, Looper looper) {
            if (looper == null) {
                looper = Looper.myLooper();
                zzx.zza(looper != null, (Object) "Can't create handler inside thread that has not called Looper.prepare()");
            }
            this.zzaLV = new C04611(this, looper, locationCallback);
        }

        private void zzb(int i, Object obj) {
            if (this.zzaLV == null) {
                Log.e("LocationClientHelper", "Received a data in client after calling removeLocationUpdates.");
                return;
            }
            Message obtain = Message.obtain();
            obtain.what = i;
            obtain.obj = obj;
            this.zzaLV.sendMessage(obtain);
        }

        public void onLocationAvailability(LocationAvailability state) {
            zzb(1, state);
        }

        public void onLocationResult(LocationResult locationResult) {
            zzb(0, locationResult);
        }

        public void release() {
            this.zzaLV = null;
        }
    }

    private static class zzc extends com.google.android.gms.location.zzd.zza {
        private Handler zzaLV;

        zzc(LocationListener locationListener, Looper looper) {
            if (looper == null) {
                zzx.zza(Looper.myLooper() != null, (Object) "Can't create handler inside thread that has not called Looper.prepare()");
            }
            this.zzaLV = looper == null ? new zzb(locationListener) : new zzb(locationListener, looper);
        }

        public void onLocationChanged(Location location) {
            if (this.zzaLV == null) {
                Log.e("LocationClientHelper", "Received a location in client after calling removeLocationUpdates.");
                return;
            }
            Message obtain = Message.obtain();
            obtain.what = 1;
            obtain.obj = location;
            this.zzaLV.sendMessage(obtain);
        }

        public void release() {
            this.zzaLV = null;
        }
    }

    public zzk(Context context, zzp<zzi> com_google_android_gms_location_internal_zzp_com_google_android_gms_location_internal_zzi) {
        this.zzaLS = null;
        this.zzaLT = false;
        this.zzauU = new HashMap();
        this.zzaLU = new HashMap();
        this.mContext = context;
        this.zzaLF = com_google_android_gms_location_internal_zzp_com_google_android_gms_location_internal_zzi;
    }

    private zza zza(LocationCallback locationCallback, Looper looper) {
        zza com_google_android_gms_location_internal_zzk_zza;
        synchronized (this.zzaLU) {
            com_google_android_gms_location_internal_zzk_zza = (zza) this.zzaLU.get(locationCallback);
            if (com_google_android_gms_location_internal_zzk_zza == null) {
                com_google_android_gms_location_internal_zzk_zza = new zza(locationCallback, looper);
            }
            this.zzaLU.put(locationCallback, com_google_android_gms_location_internal_zzk_zza);
        }
        return com_google_android_gms_location_internal_zzk_zza;
    }

    private zzc zza(LocationListener locationListener, Looper looper) {
        zzc com_google_android_gms_location_internal_zzk_zzc;
        synchronized (this.zzauU) {
            com_google_android_gms_location_internal_zzk_zzc = (zzc) this.zzauU.get(locationListener);
            if (com_google_android_gms_location_internal_zzk_zzc == null) {
                com_google_android_gms_location_internal_zzk_zzc = new zzc(locationListener, looper);
            }
            this.zzauU.put(locationListener, com_google_android_gms_location_internal_zzk_zzc);
        }
        return com_google_android_gms_location_internal_zzk_zzc;
    }

    public Location getLastLocation() {
        this.zzaLF.zzqr();
        try {
            return ((zzi) this.zzaLF.zzqs()).zzdS(this.mContext.getPackageName());
        } catch (Throwable e) {
            throw new IllegalStateException(e);
        }
    }

    public void removeAllListeners() {
        try {
            synchronized (this.zzauU) {
                for (zzd com_google_android_gms_location_zzd : this.zzauU.values()) {
                    if (com_google_android_gms_location_zzd != null) {
                        ((zzi) this.zzaLF.zzqs()).zza(LocationRequestUpdateData.zza(com_google_android_gms_location_zzd, null));
                    }
                }
                this.zzauU.clear();
            }
            synchronized (this.zzaLU) {
                for (com.google.android.gms.location.zzc com_google_android_gms_location_zzc : this.zzaLU.values()) {
                    if (com_google_android_gms_location_zzc != null) {
                        ((zzi) this.zzaLF.zzqs()).zza(LocationRequestUpdateData.zza(com_google_android_gms_location_zzc, null));
                    }
                }
                this.zzaLU.clear();
            }
        } catch (Throwable e) {
            throw new IllegalStateException(e);
        }
    }

    public void zza(PendingIntent pendingIntent, zzg com_google_android_gms_location_internal_zzg) throws RemoteException {
        this.zzaLF.zzqr();
        ((zzi) this.zzaLF.zzqs()).zza(LocationRequestUpdateData.zzb(pendingIntent, com_google_android_gms_location_internal_zzg));
    }

    public void zza(LocationCallback locationCallback, zzg com_google_android_gms_location_internal_zzg) throws RemoteException {
        this.zzaLF.zzqr();
        zzx.zzb((Object) locationCallback, (Object) "Invalid null callback");
        synchronized (this.zzaLU) {
            com.google.android.gms.location.zzc com_google_android_gms_location_zzc = (zza) this.zzaLU.remove(locationCallback);
            if (com_google_android_gms_location_zzc != null) {
                com_google_android_gms_location_zzc.release();
                ((zzi) this.zzaLF.zzqs()).zza(LocationRequestUpdateData.zza(com_google_android_gms_location_zzc, com_google_android_gms_location_internal_zzg));
            }
        }
    }

    public void zza(LocationListener locationListener, zzg com_google_android_gms_location_internal_zzg) throws RemoteException {
        this.zzaLF.zzqr();
        zzx.zzb((Object) locationListener, (Object) "Invalid null listener");
        synchronized (this.zzauU) {
            zzd com_google_android_gms_location_zzd = (zzc) this.zzauU.remove(locationListener);
            if (this.zzaLS != null && this.zzauU.isEmpty()) {
                this.zzaLS.release();
                this.zzaLS = null;
            }
            if (com_google_android_gms_location_zzd != null) {
                com_google_android_gms_location_zzd.release();
                ((zzi) this.zzaLF.zzqs()).zza(LocationRequestUpdateData.zza(com_google_android_gms_location_zzd, com_google_android_gms_location_internal_zzg));
            }
        }
    }

    public void zza(LocationRequest locationRequest, PendingIntent pendingIntent, zzg com_google_android_gms_location_internal_zzg) throws RemoteException {
        this.zzaLF.zzqr();
        ((zzi) this.zzaLF.zzqs()).zza(LocationRequestUpdateData.zza(LocationRequestInternal.zzb(locationRequest), pendingIntent, com_google_android_gms_location_internal_zzg));
    }

    public void zza(LocationRequest locationRequest, LocationListener locationListener, Looper looper, zzg com_google_android_gms_location_internal_zzg) throws RemoteException {
        this.zzaLF.zzqr();
        ((zzi) this.zzaLF.zzqs()).zza(LocationRequestUpdateData.zza(LocationRequestInternal.zzb(locationRequest), zza(locationListener, looper), com_google_android_gms_location_internal_zzg));
    }

    public void zza(LocationRequestInternal locationRequestInternal, LocationCallback locationCallback, Looper looper, zzg com_google_android_gms_location_internal_zzg) throws RemoteException {
        this.zzaLF.zzqr();
        ((zzi) this.zzaLF.zzqs()).zza(LocationRequestUpdateData.zza(locationRequestInternal, zza(locationCallback, looper), com_google_android_gms_location_internal_zzg));
    }

    public void zza(zzg com_google_android_gms_location_internal_zzg) throws RemoteException {
        this.zzaLF.zzqr();
        ((zzi) this.zzaLF.zzqs()).zza(com_google_android_gms_location_internal_zzg);
    }

    public void zzai(boolean z) throws RemoteException {
        this.zzaLF.zzqr();
        ((zzi) this.zzaLF.zzqs()).zzai(z);
        this.zzaLT = z;
    }

    public void zzc(Location location) throws RemoteException {
        this.zzaLF.zzqr();
        ((zzi) this.zzaLF.zzqs()).zzc(location);
    }

    public LocationAvailability zzyc() {
        this.zzaLF.zzqr();
        try {
            return ((zzi) this.zzaLF.zzqs()).zzdT(this.mContext.getPackageName());
        } catch (Throwable e) {
            throw new IllegalStateException(e);
        }
    }

    public void zzyd() {
        if (this.zzaLT) {
            try {
                zzai(false);
            } catch (Throwable e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
