package com.google.android.gms.location.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.zzlx.zzb;
import com.google.android.gms.location.LocationServices.zza;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.SettingsApi;

public class zzq implements SettingsApi {

    /* renamed from: com.google.android.gms.location.internal.zzq.1 */
    class C12401 extends zza<LocationSettingsResult> {
        final /* synthetic */ String zzaFm;
        final /* synthetic */ LocationSettingsRequest zzaMm;
        final /* synthetic */ zzq zzaMn;

        C12401(zzq com_google_android_gms_location_internal_zzq, GoogleApiClient googleApiClient, LocationSettingsRequest locationSettingsRequest, String str) {
            this.zzaMn = com_google_android_gms_location_internal_zzq;
            this.zzaMm = locationSettingsRequest;
            this.zzaFm = str;
            super(googleApiClient);
        }

        protected void zza(zzl com_google_android_gms_location_internal_zzl) throws RemoteException {
            com_google_android_gms_location_internal_zzl.zza(this.zzaMm, (zzb) this, this.zzaFm);
        }

        public LocationSettingsResult zzaO(Status status) {
            return new LocationSettingsResult(status);
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzaO(status);
        }
    }

    public PendingResult<LocationSettingsResult> checkLocationSettings(GoogleApiClient client, LocationSettingsRequest request) {
        return zza(client, request, null);
    }

    public PendingResult<LocationSettingsResult> zza(GoogleApiClient googleApiClient, LocationSettingsRequest locationSettingsRequest, String str) {
        return googleApiClient.zza(new C12401(this, googleApiClient, locationSettingsRequest, str));
    }
}
