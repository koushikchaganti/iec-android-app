package com.google.android.gms.location.places.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.location.places.AddPlaceRequest;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.GeoDataApi;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataResult;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.zzf;
import com.google.android.gms.location.places.zzf.zzb;
import com.google.android.gms.location.places.zzl;
import com.google.android.gms.location.places.zzl.zza;
import com.google.android.gms.location.places.zzl.zzc;
import com.google.android.gms.maps.model.LatLngBounds;
import java.util.Arrays;

public class zzd implements GeoDataApi {

    /* renamed from: com.google.android.gms.location.places.internal.zzd.1 */
    class C14441 extends zzc<zze> {
        final /* synthetic */ AddPlaceRequest zzaNr;
        final /* synthetic */ zzd zzaNs;

        C14441(zzd com_google_android_gms_location_places_internal_zzd, Api.zzc com_google_android_gms_common_api_Api_zzc, GoogleApiClient googleApiClient, AddPlaceRequest addPlaceRequest) {
            this.zzaNs = com_google_android_gms_location_places_internal_zzd;
            this.zzaNr = addPlaceRequest;
            super(com_google_android_gms_common_api_Api_zzc, googleApiClient);
        }

        protected void zza(zze com_google_android_gms_location_places_internal_zze) throws RemoteException {
            com_google_android_gms_location_places_internal_zze.zza(new zzl((zzc) this, com_google_android_gms_location_places_internal_zze.getContext()), this.zzaNr);
        }
    }

    /* renamed from: com.google.android.gms.location.places.internal.zzd.2 */
    class C14452 extends zzc<zze> {
        final /* synthetic */ zzd zzaNs;
        final /* synthetic */ String[] zzaNt;

        C14452(zzd com_google_android_gms_location_places_internal_zzd, Api.zzc com_google_android_gms_common_api_Api_zzc, GoogleApiClient googleApiClient, String[] strArr) {
            this.zzaNs = com_google_android_gms_location_places_internal_zzd;
            this.zzaNt = strArr;
            super(com_google_android_gms_common_api_Api_zzc, googleApiClient);
        }

        protected void zza(zze com_google_android_gms_location_places_internal_zze) throws RemoteException {
            com_google_android_gms_location_places_internal_zze.zza(new zzl((zzc) this, com_google_android_gms_location_places_internal_zze.getContext()), Arrays.asList(this.zzaNt));
        }
    }

    /* renamed from: com.google.android.gms.location.places.internal.zzd.3 */
    class C14463 extends zza<zze> {
        final /* synthetic */ String zzaDz;
        final /* synthetic */ zzd zzaNs;
        final /* synthetic */ LatLngBounds zzaNu;
        final /* synthetic */ AutocompleteFilter zzaNv;

        C14463(zzd com_google_android_gms_location_places_internal_zzd, Api.zzc com_google_android_gms_common_api_Api_zzc, GoogleApiClient googleApiClient, String str, LatLngBounds latLngBounds, AutocompleteFilter autocompleteFilter) {
            this.zzaNs = com_google_android_gms_location_places_internal_zzd;
            this.zzaDz = str;
            this.zzaNu = latLngBounds;
            this.zzaNv = autocompleteFilter;
            super(com_google_android_gms_common_api_Api_zzc, googleApiClient);
        }

        protected void zza(zze com_google_android_gms_location_places_internal_zze) throws RemoteException {
            com_google_android_gms_location_places_internal_zze.zza(new zzl((zza) this), this.zzaDz, this.zzaNu, this.zzaNv);
        }
    }

    /* renamed from: com.google.android.gms.location.places.internal.zzd.4 */
    class C14474 extends zzb<zze> {
        final /* synthetic */ zzd zzaNs;
        final /* synthetic */ String zzaNw;

        C14474(zzd com_google_android_gms_location_places_internal_zzd, Api.zzc com_google_android_gms_common_api_Api_zzc, GoogleApiClient googleApiClient, String str) {
            this.zzaNs = com_google_android_gms_location_places_internal_zzd;
            this.zzaNw = str;
            super(com_google_android_gms_common_api_Api_zzc, googleApiClient);
        }

        protected void zza(zze com_google_android_gms_location_places_internal_zze) throws RemoteException {
            com_google_android_gms_location_places_internal_zze.zza(new zzf((zzb) this), this.zzaNw);
        }
    }

    public PendingResult<PlaceBuffer> addPlace(GoogleApiClient client, AddPlaceRequest addPlaceRequest) {
        return client.zzb(new C14441(this, Places.zzaMU, client, addPlaceRequest));
    }

    public PendingResult<AutocompletePredictionBuffer> getAutocompletePredictions(GoogleApiClient client, String query, LatLngBounds bounds, AutocompleteFilter filter) {
        return client.zza(new C14463(this, Places.zzaMU, client, query, bounds, filter));
    }

    public PendingResult<PlaceBuffer> getPlaceById(GoogleApiClient client, String... placeIds) {
        boolean z = true;
        if (placeIds == null || placeIds.length < 1) {
            z = false;
        }
        zzx.zzab(z);
        return client.zza(new C14452(this, Places.zzaMU, client, placeIds));
    }

    public PendingResult<PlacePhotoMetadataResult> getPlacePhotos(GoogleApiClient client, String placeId) {
        return client.zza(new C14474(this, Places.zzaMU, client, placeId));
    }
}
