package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.ConfigApi;
import com.google.android.gms.fitness.request.DataTypeCreateRequest;
import com.google.android.gms.fitness.request.DataTypeReadRequest;
import com.google.android.gms.fitness.request.DisableFitRequest;
import com.google.android.gms.fitness.result.DataTypeResult;
import com.google.android.gms.internal.zzlx.zzb;

public class zzpu implements ConfigApi {

    private static class zza extends com.google.android.gms.internal.zzpc.zza {
        private final zzb<DataTypeResult> zzakL;

        private zza(zzb<DataTypeResult> com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_fitness_result_DataTypeResult) {
            this.zzakL = com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_fitness_result_DataTypeResult;
        }

        public void zza(DataTypeResult dataTypeResult) {
            this.zzakL.zzr(dataTypeResult);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzpu.1 */
    class C12311 extends zza<DataTypeResult> {
        final /* synthetic */ DataTypeCreateRequest zzaxy;
        final /* synthetic */ zzpu zzaxz;

        C12311(zzpu com_google_android_gms_internal_zzpu, GoogleApiClient googleApiClient, DataTypeCreateRequest dataTypeCreateRequest) {
            this.zzaxz = com_google_android_gms_internal_zzpu;
            this.zzaxy = dataTypeCreateRequest;
            super(googleApiClient);
        }

        protected DataTypeResult zzK(Status status) {
            return DataTypeResult.zzT(status);
        }

        protected void zza(zzot com_google_android_gms_internal_zzot) throws RemoteException {
            ((zzpe) com_google_android_gms_internal_zzot.zzqs()).zza(new DataTypeCreateRequest(this.zzaxy, new zza(null)));
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzK(status);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzpu.2 */
    class C12322 extends zza<DataTypeResult> {
        final /* synthetic */ String zzaxA;
        final /* synthetic */ zzpu zzaxz;

        C12322(zzpu com_google_android_gms_internal_zzpu, GoogleApiClient googleApiClient, String str) {
            this.zzaxz = com_google_android_gms_internal_zzpu;
            this.zzaxA = str;
            super(googleApiClient);
        }

        protected DataTypeResult zzK(Status status) {
            return DataTypeResult.zzT(status);
        }

        protected void zza(zzot com_google_android_gms_internal_zzot) throws RemoteException {
            ((zzpe) com_google_android_gms_internal_zzot.zzqs()).zza(new DataTypeReadRequest(this.zzaxA, new zza(null)));
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzK(status);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzpu.3 */
    class C13933 extends zzc {
        final /* synthetic */ zzpu zzaxz;

        C13933(zzpu com_google_android_gms_internal_zzpu, GoogleApiClient googleApiClient) {
            this.zzaxz = com_google_android_gms_internal_zzpu;
            super(googleApiClient);
        }

        protected void zza(zzot com_google_android_gms_internal_zzot) throws RemoteException {
            ((zzpe) com_google_android_gms_internal_zzot.zzqs()).zza(new DisableFitRequest(new zzqa(this)));
        }
    }

    public PendingResult<DataTypeResult> createCustomDataType(GoogleApiClient client, DataTypeCreateRequest request) {
        return client.zzb(new C12311(this, client, request));
    }

    public PendingResult<Status> disableFit(GoogleApiClient client) {
        return client.zzb(new C13933(this, client));
    }

    public PendingResult<DataTypeResult> readDataType(GoogleApiClient client, String dataTypeName) {
        return client.zza(new C12322(this, client, dataTypeName));
    }
}
