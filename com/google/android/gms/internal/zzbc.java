package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import java.util.concurrent.Future;

@zzha
public class zzbc {

    /* renamed from: com.google.android.gms.internal.zzbc.1 */
    class C03531 implements Runnable {
        final /* synthetic */ Context zzsm;
        final /* synthetic */ VersionInfoParcel zzsn;
        final /* synthetic */ zza zzso;
        final /* synthetic */ zzan zzsp;
        final /* synthetic */ String zzsq;
        final /* synthetic */ zzbc zzsr;

        C03531(zzbc com_google_android_gms_internal_zzbc, Context context, VersionInfoParcel versionInfoParcel, zza com_google_android_gms_internal_zzbc_zza, zzan com_google_android_gms_internal_zzan, String str) {
            this.zzsr = com_google_android_gms_internal_zzbc;
            this.zzsm = context;
            this.zzsn = versionInfoParcel;
            this.zzso = com_google_android_gms_internal_zzbc_zza;
            this.zzsp = com_google_android_gms_internal_zzan;
            this.zzsq = str;
        }

        public void run() {
            this.zzsr.zza(this.zzsm, this.zzsn, this.zzso, this.zzsp).zzt(this.zzsq);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzbc.2 */
    class C07382 implements com.google.android.gms.internal.zzbb.zza {
        final /* synthetic */ zza zzso;
        final /* synthetic */ zzbc zzsr;
        final /* synthetic */ zzbb zzss;

        C07382(zzbc com_google_android_gms_internal_zzbc, zza com_google_android_gms_internal_zzbc_zza, zzbb com_google_android_gms_internal_zzbb) {
            this.zzsr = com_google_android_gms_internal_zzbc;
            this.zzso = com_google_android_gms_internal_zzbc_zza;
            this.zzss = com_google_android_gms_internal_zzbb;
        }

        public void zzcr() {
            this.zzso.zzf(this.zzss);
        }
    }

    private static class zza<JavascriptEngine> extends zzjb<JavascriptEngine> {
        JavascriptEngine zzst;

        private zza() {
        }
    }

    private zzbb zza(Context context, VersionInfoParcel versionInfoParcel, zza<zzbb> com_google_android_gms_internal_zzbc_zza_com_google_android_gms_internal_zzbb, zzan com_google_android_gms_internal_zzan) {
        zzbb com_google_android_gms_internal_zzbd = new zzbd(context, versionInfoParcel, com_google_android_gms_internal_zzan);
        com_google_android_gms_internal_zzbc_zza_com_google_android_gms_internal_zzbb.zzst = com_google_android_gms_internal_zzbd;
        com_google_android_gms_internal_zzbd.zza(new C07382(this, com_google_android_gms_internal_zzbc_zza_com_google_android_gms_internal_zzbb, com_google_android_gms_internal_zzbd));
        return com_google_android_gms_internal_zzbd;
    }

    public Future<zzbb> zza(Context context, VersionInfoParcel versionInfoParcel, String str, zzan com_google_android_gms_internal_zzan) {
        Future com_google_android_gms_internal_zzbc_zza = new zza();
        zzip.zzKO.post(new C03531(this, context, versionInfoParcel, com_google_android_gms_internal_zzbc_zza, com_google_android_gms_internal_zzan, str));
        return com_google_android_gms_internal_zzbc_zza;
    }
}
