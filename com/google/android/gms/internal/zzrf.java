package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.panorama.Panorama;
import com.google.android.gms.panorama.PanoramaApi;
import com.google.android.gms.panorama.PanoramaApi.PanoramaResult;

public class zzrf implements PanoramaApi {

    /* renamed from: com.google.android.gms.internal.zzrf.3 */
    static class C11693 extends com.google.android.gms.internal.zzrd.zza {
        final /* synthetic */ Uri zzaYf;
        final /* synthetic */ zzrd zzaYh;
        final /* synthetic */ Context zzsm;

        C11693(Context context, Uri uri, zzrd com_google_android_gms_internal_zzrd) {
            this.zzsm = context;
            this.zzaYf = uri;
            this.zzaYh = com_google_android_gms_internal_zzrd;
        }

        public void zza(int i, Bundle bundle, int i2, Intent intent) throws RemoteException {
            zzrf.zza(this.zzsm, this.zzaYf);
            this.zzaYh.zza(i, bundle, i2, intent);
        }
    }

    private static final class zzb extends com.google.android.gms.internal.zzrd.zza {
        private final com.google.android.gms.internal.zzlx.zzb<PanoramaResult> zzakL;

        public zzb(com.google.android.gms.internal.zzlx.zzb<PanoramaResult> com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_panorama_PanoramaApi_PanoramaResult) {
            this.zzakL = com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_panorama_PanoramaApi_PanoramaResult;
        }

        public void zza(int i, Bundle bundle, int i2, Intent intent) {
            this.zzakL.zzr(new zzrh(new Status(i, null, bundle != null ? (PendingIntent) bundle.getParcelable("pendingIntent") : null), intent));
        }
    }

    private static abstract class zzc<R extends Result> extends com.google.android.gms.internal.zzlx.zza<R, zzrg> {
        protected zzc(GoogleApiClient googleApiClient) {
            super(Panorama.zzTo, googleApiClient);
        }

        protected abstract void zza(Context context, zzre com_google_android_gms_internal_zzre) throws RemoteException;

        protected final void zza(zzrg com_google_android_gms_internal_zzrg) throws RemoteException {
            zza(com_google_android_gms_internal_zzrg.getContext(), (zzre) com_google_android_gms_internal_zzrg.zzqs());
        }
    }

    private static abstract class zza extends zzc<PanoramaResult> {
        public zza(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        protected PanoramaResult zzaZ(Status status) {
            return new zzrh(status, null);
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzaZ(status);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzrf.1 */
    class C14101 extends zza {
        final /* synthetic */ Uri zzaYf;
        final /* synthetic */ zzrf zzaYg;

        C14101(zzrf com_google_android_gms_internal_zzrf, GoogleApiClient googleApiClient, Uri uri) {
            this.zzaYg = com_google_android_gms_internal_zzrf;
            this.zzaYf = uri;
            super(googleApiClient);
        }

        protected void zza(Context context, zzre com_google_android_gms_internal_zzre) throws RemoteException {
            com_google_android_gms_internal_zzre.zza(new zzb(this), this.zzaYf, null, false);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzrf.2 */
    class C14112 extends zza {
        final /* synthetic */ Uri zzaYf;
        final /* synthetic */ zzrf zzaYg;

        C14112(zzrf com_google_android_gms_internal_zzrf, GoogleApiClient googleApiClient, Uri uri) {
            this.zzaYg = com_google_android_gms_internal_zzrf;
            this.zzaYf = uri;
            super(googleApiClient);
        }

        protected void zza(Context context, zzre com_google_android_gms_internal_zzre) throws RemoteException {
            zzrf.zza(context, com_google_android_gms_internal_zzre, new zzb(this), this.zzaYf, null);
        }
    }

    private static void zza(Context context, Uri uri) {
        context.revokeUriPermission(uri, 1);
    }

    private static void zza(Context context, zzre com_google_android_gms_internal_zzre, zzrd com_google_android_gms_internal_zzrd, Uri uri, Bundle bundle) throws RemoteException {
        context.grantUriPermission(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE, uri, 1);
        try {
            com_google_android_gms_internal_zzre.zza(new C11693(context, uri, com_google_android_gms_internal_zzrd), uri, bundle, true);
        } catch (RemoteException e) {
            zza(context, uri);
            throw e;
        } catch (RuntimeException e2) {
            zza(context, uri);
            throw e2;
        }
    }

    public PendingResult<PanoramaResult> loadPanoramaInfo(GoogleApiClient client, Uri uri) {
        return client.zza(new C14101(this, client, uri));
    }

    public PendingResult<PanoramaResult> loadPanoramaInfoAndGrantAccess(GoogleApiClient client, Uri uri) {
        return client.zza(new C14112(this, client, uri));
    }
}
