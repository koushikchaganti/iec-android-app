package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.search.SearchAuth.StatusCodes;
import java.util.Map;

@zzha
public class zzei {
    private final Context mContext;
    private final String zzAg;
    private zzb<zzbb> zzAh;
    private zzb<zzbb> zzAi;
    private zze zzAj;
    private int zzAk;
    private final VersionInfoParcel zzpI;
    private final Object zzpK;

    /* renamed from: com.google.android.gms.internal.zzei.1 */
    class C03801 implements Runnable {
        final /* synthetic */ zze zzAl;
        final /* synthetic */ zzei zzAm;

        /* renamed from: com.google.android.gms.internal.zzei.1.4 */
        class C03794 implements Runnable {
            final /* synthetic */ zzbb zzAn;
            final /* synthetic */ C03801 zzAo;

            /* renamed from: com.google.android.gms.internal.zzei.1.4.1 */
            class C03781 implements Runnable {
                final /* synthetic */ C03794 zzAs;

                C03781(C03794 c03794) {
                    this.zzAs = c03794;
                }

                public void run() {
                    this.zzAs.zzAn.destroy();
                }
            }

            C03794(C03801 c03801, zzbb com_google_android_gms_internal_zzbb) {
                this.zzAo = c03801;
                this.zzAn = com_google_android_gms_internal_zzbb;
            }

            public void run() {
                synchronized (this.zzAo.zzAm.zzpK) {
                    if (this.zzAo.zzAl.getStatus() == -1 || this.zzAo.zzAl.getStatus() == 1) {
                        return;
                    }
                    this.zzAo.zzAl.reject();
                    zzip.runOnUiThread(new C03781(this));
                    com.google.android.gms.ads.internal.util.client.zzb.m10v("Could not receive loaded message in a timely manner. Rejecting.");
                }
            }
        }

        /* renamed from: com.google.android.gms.internal.zzei.1.1 */
        class C07651 implements com.google.android.gms.internal.zzbb.zza {
            final /* synthetic */ zzbb zzAn;
            final /* synthetic */ C03801 zzAo;

            /* renamed from: com.google.android.gms.internal.zzei.1.1.1 */
            class C03771 implements Runnable {
                final /* synthetic */ C07651 zzAp;

                /* renamed from: com.google.android.gms.internal.zzei.1.1.1.1 */
                class C03761 implements Runnable {
                    final /* synthetic */ C03771 zzAq;

                    C03761(C03771 c03771) {
                        this.zzAq = c03771;
                    }

                    public void run() {
                        this.zzAq.zzAp.zzAn.destroy();
                    }
                }

                C03771(C07651 c07651) {
                    this.zzAp = c07651;
                }

                public void run() {
                    synchronized (this.zzAp.zzAo.zzAm.zzpK) {
                        if (this.zzAp.zzAo.zzAl.getStatus() == -1 || this.zzAp.zzAo.zzAl.getStatus() == 1) {
                            return;
                        }
                        this.zzAp.zzAo.zzAl.reject();
                        zzip.runOnUiThread(new C03761(this));
                        com.google.android.gms.ads.internal.util.client.zzb.m10v("Could not receive loaded message in a timely manner. Rejecting.");
                    }
                }
            }

            C07651(C03801 c03801, zzbb com_google_android_gms_internal_zzbb) {
                this.zzAo = c03801;
                this.zzAn = com_google_android_gms_internal_zzbb;
            }

            public void zzcr() {
                zzip.zzKO.postDelayed(new C03771(this), (long) zza.zzAv);
            }
        }

        /* renamed from: com.google.android.gms.internal.zzei.1.2 */
        class C07662 implements zzdl {
            final /* synthetic */ zzbb zzAn;
            final /* synthetic */ C03801 zzAo;

            C07662(C03801 c03801, zzbb com_google_android_gms_internal_zzbb) {
                this.zzAo = c03801;
                this.zzAn = com_google_android_gms_internal_zzbb;
            }

            public void zza(zzjn com_google_android_gms_internal_zzjn, Map<String, String> map) {
                synchronized (this.zzAo.zzAm.zzpK) {
                    if (this.zzAo.zzAl.getStatus() == -1 || this.zzAo.zzAl.getStatus() == 1) {
                        return;
                    }
                    this.zzAo.zzAm.zzAk = 0;
                    this.zzAo.zzAm.zzAh.zzc(this.zzAn);
                    this.zzAo.zzAl.zzg(this.zzAn);
                    this.zzAo.zzAm.zzAj = this.zzAo.zzAl;
                    com.google.android.gms.ads.internal.util.client.zzb.m10v("Successfully loaded JS Engine.");
                }
            }
        }

        /* renamed from: com.google.android.gms.internal.zzei.1.3 */
        class C07673 implements zzdl {
            final /* synthetic */ zzbb zzAn;
            final /* synthetic */ C03801 zzAo;
            final /* synthetic */ zziy zzAr;

            C07673(C03801 c03801, zzbb com_google_android_gms_internal_zzbb, zziy com_google_android_gms_internal_zziy) {
                this.zzAo = c03801;
                this.zzAn = com_google_android_gms_internal_zzbb;
                this.zzAr = com_google_android_gms_internal_zziy;
            }

            public void zza(zzjn com_google_android_gms_internal_zzjn, Map<String, String> map) {
                synchronized (this.zzAo.zzAm.zzpK) {
                    com.google.android.gms.ads.internal.util.client.zzb.zzaG("JS Engine is requesting an update");
                    if (this.zzAo.zzAm.zzAk == 0) {
                        com.google.android.gms.ads.internal.util.client.zzb.zzaG("Starting reload.");
                        this.zzAo.zzAm.zzAk = 2;
                        this.zzAo.zzAm.zzeh();
                    }
                    this.zzAn.zzb("/requestReload", (zzdl) this.zzAr.get());
                }
            }
        }

        C03801(zzei com_google_android_gms_internal_zzei, zze com_google_android_gms_internal_zzei_zze) {
            this.zzAm = com_google_android_gms_internal_zzei;
            this.zzAl = com_google_android_gms_internal_zzei_zze;
        }

        public void run() {
            zzbb zza = this.zzAm.zza(this.zzAm.mContext, this.zzAm.zzpI);
            zza.zza(new C07651(this, zza));
            zza.zza("/jsLoaded", new C07662(this, zza));
            zziy com_google_android_gms_internal_zziy = new zziy();
            zzdl c07673 = new C07673(this, zza, com_google_android_gms_internal_zziy);
            com_google_android_gms_internal_zziy.set(c07673);
            zza.zza("/requestReload", c07673);
            if (this.zzAm.zzAg.endsWith(".js")) {
                zza.zzs(this.zzAm.zzAg);
            } else if (this.zzAm.zzAg.startsWith("<html>")) {
                zza.zzu(this.zzAm.zzAg);
            } else {
                zza.zzt(this.zzAm.zzAg);
            }
            zzip.zzKO.postDelayed(new C03794(this, zza), (long) zza.zzAu);
        }
    }

    static class zza {
        static int zzAu;
        static int zzAv;

        static {
            zzAu = 60000;
            zzAv = StatusCodes.AUTH_DISABLED;
        }
    }

    public interface zzb<T> {
        void zzc(T t);
    }

    /* renamed from: com.google.android.gms.internal.zzei.2 */
    class C07682 implements com.google.android.gms.internal.zzjg.zzc<zzbb> {
        final /* synthetic */ zzei zzAm;
        final /* synthetic */ zze zzAt;

        C07682(zzei com_google_android_gms_internal_zzei, zze com_google_android_gms_internal_zzei_zze) {
            this.zzAm = com_google_android_gms_internal_zzei;
            this.zzAt = com_google_android_gms_internal_zzei_zze;
        }

        public void zza(zzbb com_google_android_gms_internal_zzbb) {
            synchronized (this.zzAm.zzpK) {
                this.zzAm.zzAk = 0;
                if (!(this.zzAm.zzAj == null || this.zzAt == this.zzAm.zzAj)) {
                    com.google.android.gms.ads.internal.util.client.zzb.m10v("New JS engine is loaded, marking previous one as destroyable.");
                    this.zzAm.zzAj.zzel();
                }
                this.zzAm.zzAj = this.zzAt;
            }
        }

        public /* synthetic */ void zzc(Object obj) {
            zza((zzbb) obj);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzei.3 */
    class C07693 implements com.google.android.gms.internal.zzjg.zza {
        final /* synthetic */ zzei zzAm;
        final /* synthetic */ zze zzAt;

        C07693(zzei com_google_android_gms_internal_zzei, zze com_google_android_gms_internal_zzei_zze) {
            this.zzAm = com_google_android_gms_internal_zzei;
            this.zzAt = com_google_android_gms_internal_zzei_zze;
        }

        public void run() {
            synchronized (this.zzAm.zzpK) {
                this.zzAm.zzAk = 1;
                com.google.android.gms.ads.internal.util.client.zzb.m10v("Failed loading new engine. Marking new engine destroyable.");
                this.zzAt.zzel();
            }
        }
    }

    public static class zzc<T> implements zzb<T> {
        public void zzc(T t) {
        }
    }

    public static class zzd extends zzjh<zzbe> {
        private final zze zzAw;
        private boolean zzAx;
        private final Object zzpK;

        /* renamed from: com.google.android.gms.internal.zzei.zzd.1 */
        class C07701 implements com.google.android.gms.internal.zzjg.zzc<zzbe> {
            final /* synthetic */ zzd zzAy;

            C07701(zzd com_google_android_gms_internal_zzei_zzd) {
                this.zzAy = com_google_android_gms_internal_zzei_zzd;
            }

            public void zzb(zzbe com_google_android_gms_internal_zzbe) {
                com.google.android.gms.ads.internal.util.client.zzb.m10v("Ending javascript session.");
                ((zzbf) com_google_android_gms_internal_zzbe).zzcs();
            }

            public /* synthetic */ void zzc(Object obj) {
                zzb((zzbe) obj);
            }
        }

        /* renamed from: com.google.android.gms.internal.zzei.zzd.2 */
        class C07712 implements com.google.android.gms.internal.zzjg.zzc<zzbe> {
            final /* synthetic */ zzd zzAy;

            C07712(zzd com_google_android_gms_internal_zzei_zzd) {
                this.zzAy = com_google_android_gms_internal_zzei_zzd;
            }

            public void zzb(zzbe com_google_android_gms_internal_zzbe) {
                com.google.android.gms.ads.internal.util.client.zzb.m10v("Releasing engine reference.");
                this.zzAy.zzAw.zzek();
            }

            public /* synthetic */ void zzc(Object obj) {
                zzb((zzbe) obj);
            }
        }

        /* renamed from: com.google.android.gms.internal.zzei.zzd.3 */
        class C07723 implements com.google.android.gms.internal.zzjg.zza {
            final /* synthetic */ zzd zzAy;

            C07723(zzd com_google_android_gms_internal_zzei_zzd) {
                this.zzAy = com_google_android_gms_internal_zzei_zzd;
            }

            public void run() {
                this.zzAy.zzAw.zzek();
            }
        }

        public zzd(zze com_google_android_gms_internal_zzei_zze) {
            this.zzpK = new Object();
            this.zzAw = com_google_android_gms_internal_zzei_zze;
        }

        public void release() {
            synchronized (this.zzpK) {
                if (this.zzAx) {
                    return;
                }
                this.zzAx = true;
                zza(new C07701(this), new com.google.android.gms.internal.zzjg.zzb());
                zza(new C07712(this), new C07723(this));
            }
        }
    }

    public static class zze extends zzjh<zzbb> {
        private int zzAA;
        private zzb<zzbb> zzAi;
        private boolean zzAz;
        private final Object zzpK;

        /* renamed from: com.google.android.gms.internal.zzei.zze.1 */
        class C07731 implements com.google.android.gms.internal.zzjg.zzc<zzbb> {
            final /* synthetic */ zzd zzAB;
            final /* synthetic */ zze zzAC;

            C07731(zze com_google_android_gms_internal_zzei_zze, zzd com_google_android_gms_internal_zzei_zzd) {
                this.zzAC = com_google_android_gms_internal_zzei_zze;
                this.zzAB = com_google_android_gms_internal_zzei_zzd;
            }

            public void zza(zzbb com_google_android_gms_internal_zzbb) {
                com.google.android.gms.ads.internal.util.client.zzb.m10v("Getting a new session for JS Engine.");
                this.zzAB.zzg(com_google_android_gms_internal_zzbb.zzcq());
            }

            public /* synthetic */ void zzc(Object obj) {
                zza((zzbb) obj);
            }
        }

        /* renamed from: com.google.android.gms.internal.zzei.zze.2 */
        class C07742 implements com.google.android.gms.internal.zzjg.zza {
            final /* synthetic */ zzd zzAB;
            final /* synthetic */ zze zzAC;

            C07742(zze com_google_android_gms_internal_zzei_zze, zzd com_google_android_gms_internal_zzei_zzd) {
                this.zzAC = com_google_android_gms_internal_zzei_zze;
                this.zzAB = com_google_android_gms_internal_zzei_zzd;
            }

            public void run() {
                com.google.android.gms.ads.internal.util.client.zzb.m10v("Rejecting reference for JS Engine.");
                this.zzAB.reject();
            }
        }

        /* renamed from: com.google.android.gms.internal.zzei.zze.3 */
        class C07753 implements com.google.android.gms.internal.zzjg.zzc<zzbb> {
            final /* synthetic */ zze zzAC;

            /* renamed from: com.google.android.gms.internal.zzei.zze.3.1 */
            class C03811 implements Runnable {
                final /* synthetic */ C07753 zzAD;
                final /* synthetic */ zzbb zzss;

                C03811(C07753 c07753, zzbb com_google_android_gms_internal_zzbb) {
                    this.zzAD = c07753;
                    this.zzss = com_google_android_gms_internal_zzbb;
                }

                public void run() {
                    this.zzAD.zzAC.zzAi.zzc(this.zzss);
                    this.zzss.destroy();
                }
            }

            C07753(zze com_google_android_gms_internal_zzei_zze) {
                this.zzAC = com_google_android_gms_internal_zzei_zze;
            }

            public void zza(zzbb com_google_android_gms_internal_zzbb) {
                zzip.runOnUiThread(new C03811(this, com_google_android_gms_internal_zzbb));
            }

            public /* synthetic */ void zzc(Object obj) {
                zza((zzbb) obj);
            }
        }

        public zze(zzb<zzbb> com_google_android_gms_internal_zzei_zzb_com_google_android_gms_internal_zzbb) {
            this.zzpK = new Object();
            this.zzAi = com_google_android_gms_internal_zzei_zzb_com_google_android_gms_internal_zzbb;
            this.zzAz = false;
            this.zzAA = 0;
        }

        public zzd zzej() {
            zzd com_google_android_gms_internal_zzei_zzd = new zzd(this);
            synchronized (this.zzpK) {
                zza(new C07731(this, com_google_android_gms_internal_zzei_zzd), new C07742(this, com_google_android_gms_internal_zzei_zzd));
                zzx.zzaa(this.zzAA >= 0);
                this.zzAA++;
            }
            return com_google_android_gms_internal_zzei_zzd;
        }

        protected void zzek() {
            boolean z = true;
            synchronized (this.zzpK) {
                if (this.zzAA < 1) {
                    z = false;
                }
                zzx.zzaa(z);
                com.google.android.gms.ads.internal.util.client.zzb.m10v("Releasing 1 reference for JS Engine");
                this.zzAA--;
                zzem();
            }
        }

        public void zzel() {
            boolean z = true;
            synchronized (this.zzpK) {
                if (this.zzAA < 0) {
                    z = false;
                }
                zzx.zzaa(z);
                com.google.android.gms.ads.internal.util.client.zzb.m10v("Releasing root reference. JS Engine will be destroyed once other references are released.");
                this.zzAz = true;
                zzem();
            }
        }

        protected void zzem() {
            synchronized (this.zzpK) {
                zzx.zzaa(this.zzAA >= 0);
                if (this.zzAz && this.zzAA == 0) {
                    com.google.android.gms.ads.internal.util.client.zzb.m10v("No reference is left (including root). Cleaning up engine.");
                    zza(new C07753(this), new com.google.android.gms.internal.zzjg.zzb());
                } else {
                    com.google.android.gms.ads.internal.util.client.zzb.m10v("There are still references to the engine. Not destroying.");
                }
            }
        }
    }

    public zzei(Context context, VersionInfoParcel versionInfoParcel, String str) {
        this.zzpK = new Object();
        this.zzAk = 1;
        this.zzAg = str;
        this.mContext = context.getApplicationContext();
        this.zzpI = versionInfoParcel;
        this.zzAh = new zzc();
        this.zzAi = new zzc();
    }

    public zzei(Context context, VersionInfoParcel versionInfoParcel, String str, zzb<zzbb> com_google_android_gms_internal_zzei_zzb_com_google_android_gms_internal_zzbb, zzb<zzbb> com_google_android_gms_internal_zzei_zzb_com_google_android_gms_internal_zzbb2) {
        this(context, versionInfoParcel, str);
        this.zzAh = com_google_android_gms_internal_zzei_zzb_com_google_android_gms_internal_zzbb;
        this.zzAi = com_google_android_gms_internal_zzei_zzb_com_google_android_gms_internal_zzbb2;
    }

    private zze zzeg() {
        zze com_google_android_gms_internal_zzei_zze = new zze(this.zzAi);
        zzip.runOnUiThread(new C03801(this, com_google_android_gms_internal_zzei_zze));
        return com_google_android_gms_internal_zzei_zze;
    }

    protected zzbb zza(Context context, VersionInfoParcel versionInfoParcel) {
        return new zzbd(context, versionInfoParcel, null);
    }

    protected zze zzeh() {
        zze zzeg = zzeg();
        zzeg.zza(new C07682(this, zzeg), new C07693(this, zzeg));
        return zzeg;
    }

    public zzd zzei() {
        zzd zzej;
        synchronized (this.zzpK) {
            if (this.zzAj == null || this.zzAj.getStatus() == -1) {
                this.zzAk = 2;
                this.zzAj = zzeh();
                zzej = this.zzAj.zzej();
            } else if (this.zzAk == 0) {
                zzej = this.zzAj.zzej();
            } else if (this.zzAk == 1) {
                this.zzAk = 2;
                zzeh();
                zzej = this.zzAj.zzej();
            } else if (this.zzAk == 2) {
                zzej = this.zzAj.zzej();
            } else {
                zzej = this.zzAj.zzej();
            }
        }
        return zzej;
    }
}
