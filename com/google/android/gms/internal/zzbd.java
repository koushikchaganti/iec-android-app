package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzl;
import com.google.android.gms.ads.internal.overlay.zzg;
import com.google.android.gms.ads.internal.overlay.zzn;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zze;
import com.google.android.gms.ads.internal.zzp;
import com.google.android.gms.internal.zzjo.zza;
import org.json.JSONObject;

@zzha
public class zzbd implements zzbb {
    private final zzjn zzps;

    /* renamed from: com.google.android.gms.internal.zzbd.1 */
    class C03541 implements Runnable {
        final /* synthetic */ String zzsu;
        final /* synthetic */ JSONObject zzsv;
        final /* synthetic */ zzbd zzsw;

        C03541(zzbd com_google_android_gms_internal_zzbd, String str, JSONObject jSONObject) {
            this.zzsw = com_google_android_gms_internal_zzbd;
            this.zzsu = str;
            this.zzsv = jSONObject;
        }

        public void run() {
            this.zzsw.zzps.zza(this.zzsu, this.zzsv);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzbd.2 */
    class C03552 implements Runnable {
        final /* synthetic */ String zzsu;
        final /* synthetic */ zzbd zzsw;
        final /* synthetic */ String zzsx;

        C03552(zzbd com_google_android_gms_internal_zzbd, String str, String str2) {
            this.zzsw = com_google_android_gms_internal_zzbd;
            this.zzsu = str;
            this.zzsx = str2;
        }

        public void run() {
            this.zzsw.zzps.zza(this.zzsu, this.zzsx);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzbd.3 */
    class C03563 implements Runnable {
        final /* synthetic */ zzbd zzsw;
        final /* synthetic */ String zzsy;

        C03563(zzbd com_google_android_gms_internal_zzbd, String str) {
            this.zzsw = com_google_android_gms_internal_zzbd;
            this.zzsy = str;
        }

        public void run() {
            this.zzsw.zzps.loadData(this.zzsy, "text/html", "UTF-8");
        }
    }

    /* renamed from: com.google.android.gms.internal.zzbd.4 */
    class C03574 implements Runnable {
        final /* synthetic */ zzbd zzsw;
        final /* synthetic */ String zzsy;

        C03574(zzbd com_google_android_gms_internal_zzbd, String str) {
            this.zzsw = com_google_android_gms_internal_zzbd;
            this.zzsy = str;
        }

        public void run() {
            this.zzsw.zzps.loadData(this.zzsy, "text/html", "UTF-8");
        }
    }

    /* renamed from: com.google.android.gms.internal.zzbd.5 */
    class C03585 implements Runnable {
        final /* synthetic */ String zzsq;
        final /* synthetic */ zzbd zzsw;

        C03585(zzbd com_google_android_gms_internal_zzbd, String str) {
            this.zzsw = com_google_android_gms_internal_zzbd;
            this.zzsq = str;
        }

        public void run() {
            this.zzsw.zzps.loadUrl(this.zzsq);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzbd.6 */
    class C07396 implements zza {
        final /* synthetic */ zzbd zzsw;
        final /* synthetic */ zzbb.zza zzsz;

        C07396(zzbd com_google_android_gms_internal_zzbd, zzbb.zza com_google_android_gms_internal_zzbb_zza) {
            this.zzsw = com_google_android_gms_internal_zzbd;
            this.zzsz = com_google_android_gms_internal_zzbb_zza;
        }

        public void zza(zzjn com_google_android_gms_internal_zzjn, boolean z) {
            this.zzsz.zzcr();
        }
    }

    public zzbd(Context context, VersionInfoParcel versionInfoParcel, zzan com_google_android_gms_internal_zzan) {
        this.zzps = zzp.zzby().zza(context, new AdSizeParcel(), false, false, com_google_android_gms_internal_zzan, versionInfoParcel);
        this.zzps.getWebView().setWillNotDraw(true);
    }

    private void runOnUiThread(Runnable runnable) {
        if (zzl.zzcN().zzhr()) {
            runnable.run();
        } else {
            zzip.zzKO.post(runnable);
        }
    }

    public void destroy() {
        this.zzps.destroy();
    }

    public void zza(com.google.android.gms.ads.internal.client.zza com_google_android_gms_ads_internal_client_zza, zzg com_google_android_gms_ads_internal_overlay_zzg, zzdh com_google_android_gms_internal_zzdh, zzn com_google_android_gms_ads_internal_overlay_zzn, boolean z, zzdn com_google_android_gms_internal_zzdn, zzdp com_google_android_gms_internal_zzdp, zze com_google_android_gms_ads_internal_zze, zzfs com_google_android_gms_internal_zzfs) {
        this.zzps.zzhC().zzb(com_google_android_gms_ads_internal_client_zza, com_google_android_gms_ads_internal_overlay_zzg, com_google_android_gms_internal_zzdh, com_google_android_gms_ads_internal_overlay_zzn, z, com_google_android_gms_internal_zzdn, com_google_android_gms_internal_zzdp, new zze(false), com_google_android_gms_internal_zzfs);
    }

    public void zza(zzbb.zza com_google_android_gms_internal_zzbb_zza) {
        this.zzps.zzhC().zza(new C07396(this, com_google_android_gms_internal_zzbb_zza));
    }

    public void zza(String str, zzdl com_google_android_gms_internal_zzdl) {
        this.zzps.zzhC().zza(str, com_google_android_gms_internal_zzdl);
    }

    public void zza(String str, String str2) {
        runOnUiThread(new C03552(this, str, str2));
    }

    public void zza(String str, JSONObject jSONObject) {
        runOnUiThread(new C03541(this, str, jSONObject));
    }

    public void zzb(String str, zzdl com_google_android_gms_internal_zzdl) {
        this.zzps.zzhC().zzb(str, com_google_android_gms_internal_zzdl);
    }

    public void zzb(String str, JSONObject jSONObject) {
        this.zzps.zzb(str, jSONObject);
    }

    public zzbf zzcq() {
        return new zzbg(this);
    }

    public void zzs(String str) {
        runOnUiThread(new C03563(this, String.format("<!DOCTYPE html><html><head><script src=\"%s\"></script></head><body></body></html>", new Object[]{str})));
    }

    public void zzt(String str) {
        runOnUiThread(new C03585(this, str));
    }

    public void zzu(String str) {
        runOnUiThread(new C03574(this, str));
    }
}
