package com.google.android.gms.internal;

import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.AdRequest.Gender;
import com.google.ads.AdSize;
import com.google.ads.mediation.MediationAdRequest;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.zza;
import com.google.android.gms.drive.events.CompletionEvent;
import com.google.android.gms.vision.barcode.Barcode;
import java.util.Date;
import java.util.HashSet;

@zzha
public final class zzfk {

    /* renamed from: com.google.android.gms.internal.zzfk.1 */
    static /* synthetic */ class C03951 {
        static final /* synthetic */ int[] zzBS;
        static final /* synthetic */ int[] zzBT;

        static {
            zzBT = new int[ErrorCode.values().length];
            try {
                zzBT[ErrorCode.INTERNAL_ERROR.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                zzBT[ErrorCode.INVALID_REQUEST.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                zzBT[ErrorCode.NETWORK_ERROR.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                zzBT[ErrorCode.NO_FILL.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            zzBS = new int[Gender.values().length];
            try {
                zzBS[Gender.FEMALE.ordinal()] = 1;
            } catch (NoSuchFieldError e5) {
            }
            try {
                zzBS[Gender.MALE.ordinal()] = 2;
            } catch (NoSuchFieldError e6) {
            }
            try {
                zzBS[Gender.UNKNOWN.ordinal()] = 3;
            } catch (NoSuchFieldError e7) {
            }
        }
    }

    public static int zza(ErrorCode errorCode) {
        switch (C03951.zzBT[errorCode.ordinal()]) {
            case CompletionEvent.STATUS_CONFLICT /*2*/:
                return 1;
            case CompletionEvent.STATUS_CANCELED /*3*/:
                return 2;
            case Barcode.PHONE /*4*/:
                return 3;
            default:
                return 0;
        }
    }

    public static AdSize zzb(AdSizeParcel adSizeParcel) {
        int i = 0;
        AdSize[] adSizeArr = new AdSize[]{AdSize.SMART_BANNER, AdSize.BANNER, AdSize.IAB_MRECT, AdSize.IAB_BANNER, AdSize.IAB_LEADERBOARD, AdSize.IAB_WIDE_SKYSCRAPER};
        while (i < adSizeArr.length) {
            if (adSizeArr[i].getWidth() == adSizeParcel.width && adSizeArr[i].getHeight() == adSizeParcel.height) {
                return adSizeArr[i];
            }
            i++;
        }
        return new AdSize(zza.zza(adSizeParcel.width, adSizeParcel.height, adSizeParcel.zztV));
    }

    public static MediationAdRequest zzi(AdRequestParcel adRequestParcel) {
        return new MediationAdRequest(new Date(adRequestParcel.zztq), zzu(adRequestParcel.zztr), adRequestParcel.zzts != null ? new HashSet(adRequestParcel.zzts) : null, adRequestParcel.zztt, adRequestParcel.zzty);
    }

    public static Gender zzu(int i) {
        switch (i) {
            case CompletionEvent.STATUS_FAILURE /*1*/:
                return Gender.MALE;
            case CompletionEvent.STATUS_CONFLICT /*2*/:
                return Gender.FEMALE;
            default:
                return Gender.UNKNOWN;
        }
    }
}
