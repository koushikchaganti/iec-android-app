package com.google.android.gms.internal;

import android.content.Context;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.android.gms.ads.internal.util.client.zzb;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@zzha
public class zzga implements zzfy {
    private final Context mContext;
    final Set<WebView> zzEq;

    /* renamed from: com.google.android.gms.internal.zzga.1 */
    class C04031 implements Runnable {
        final /* synthetic */ String zzEr;
        final /* synthetic */ String zzEs;
        final /* synthetic */ zzga zzEt;

        /* renamed from: com.google.android.gms.internal.zzga.1.1 */
        class C04021 extends WebViewClient {
            final /* synthetic */ C04031 zzEu;
            final /* synthetic */ WebView zzsZ;

            C04021(C04031 c04031, WebView webView) {
                this.zzEu = c04031;
                this.zzsZ = webView;
            }

            public void onPageFinished(WebView view, String url) {
                zzb.zzaF("Loading assets have finished");
                this.zzEu.zzEt.zzEq.remove(this.zzsZ);
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                zzb.zzaH("Loading assets have failed.");
                this.zzEu.zzEt.zzEq.remove(this.zzsZ);
            }
        }

        C04031(zzga com_google_android_gms_internal_zzga, String str, String str2) {
            this.zzEt = com_google_android_gms_internal_zzga;
            this.zzEr = str;
            this.zzEs = str2;
        }

        public void run() {
            WebView zzfE = this.zzEt.zzfE();
            zzfE.setWebViewClient(new C04021(this, zzfE));
            this.zzEt.zzEq.add(zzfE);
            zzfE.loadDataWithBaseURL(this.zzEr, this.zzEs, "text/html", "UTF-8", null);
            zzb.zzaF("Fetching assets finished.");
        }
    }

    public zzga(Context context) {
        this.zzEq = Collections.synchronizedSet(new HashSet());
        this.mContext = context;
    }

    public void zza(String str, String str2, String str3) {
        zzb.zzaF("Fetching assets for the given html");
        zzip.zzKO.post(new C04031(this, str2, str3));
    }

    public WebView zzfE() {
        WebView webView = new WebView(this.mContext);
        webView.getSettings().setJavaScriptEnabled(true);
        return webView;
    }
}
