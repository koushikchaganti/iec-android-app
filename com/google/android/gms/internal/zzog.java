package com.google.android.gms.internal;

import android.os.Bundle;
import android.support.v4.util.LongSparseArray;
import android.util.SparseArray;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.metadata.CustomPropertyKey;
import com.google.android.gms.drive.metadata.internal.AppVisibleCustomProperties;
import com.google.android.gms.drive.metadata.internal.CustomProperty;
import com.google.android.gms.drive.metadata.internal.zze.zza;
import com.google.android.gms.drive.metadata.internal.zzj;
import java.util.Arrays;

public class zzog extends zzj<AppVisibleCustomProperties> {
    public static final zza zzarF;

    /* renamed from: com.google.android.gms.internal.zzog.1 */
    static class C08121 implements zza {
        C08121() {
        }

        public void zzb(DataHolder dataHolder) {
            zzog.zzd(dataHolder);
        }

        public String zzte() {
            return "customPropertiesExtraHolder";
        }
    }

    static {
        zzarF = new C08121();
    }

    public zzog(int i) {
        super("customProperties", Arrays.asList(new String[]{"hasCustomProperties", "sqlId"}), Arrays.asList(new String[]{"customPropertiesExtra", "customPropertiesExtraHolder"}), i);
    }

    private static void zzc(DataHolder dataHolder) {
        synchronized (dataHolder) {
            DataHolder dataHolder2 = (DataHolder) dataHolder.zzpH().getParcelable("customPropertiesExtraHolder");
            if (dataHolder2 == null) {
                return;
            }
            try {
                LongSparseArray zzf = zzf(dataHolder2);
                SparseArray sparseArray = new SparseArray();
                for (int i = 0; i < dataHolder.getCount(); i++) {
                    AppVisibleCustomProperties.zza com_google_android_gms_drive_metadata_internal_AppVisibleCustomProperties_zza = (AppVisibleCustomProperties.zza) zzf.get(dataHolder.zzb("sqlId", i, dataHolder.zzbI(i)));
                    if (com_google_android_gms_drive_metadata_internal_AppVisibleCustomProperties_zza != null) {
                        sparseArray.append(i, com_google_android_gms_drive_metadata_internal_AppVisibleCustomProperties_zza.zztb());
                    }
                }
                dataHolder.zzpH().putSparseParcelableArray("customPropertiesExtra", sparseArray);
            } finally {
                dataHolder2.close();
                dataHolder.zzpH().remove("customPropertiesExtraHolder");
            }
        }
    }

    private static void zzd(DataHolder dataHolder) {
        Bundle zzpH = dataHolder.zzpH();
        if (zzpH != null) {
            synchronized (dataHolder) {
                DataHolder dataHolder2 = (DataHolder) zzpH.getParcelable("customPropertiesExtraHolder");
                if (dataHolder2 != null) {
                    dataHolder2.close();
                    zzpH.remove("customPropertiesExtraHolder");
                }
            }
        }
    }

    private static LongSparseArray<AppVisibleCustomProperties.zza> zzf(DataHolder dataHolder) {
        Bundle zzpH = dataHolder.zzpH();
        String string = zzpH.getString("entryIdColumn");
        String string2 = zzpH.getString("keyColumn");
        String string3 = zzpH.getString("visibilityColumn");
        String string4 = zzpH.getString("valueColumn");
        LongSparseArray<AppVisibleCustomProperties.zza> longSparseArray = new LongSparseArray();
        for (int i = 0; i < dataHolder.getCount(); i++) {
            int zzbI = dataHolder.zzbI(i);
            long zzb = dataHolder.zzb(string, i, zzbI);
            String zzd = dataHolder.zzd(string2, i, zzbI);
            int zzc = dataHolder.zzc(string3, i, zzbI);
            CustomProperty customProperty = new CustomProperty(new CustomPropertyKey(zzd, zzc), dataHolder.zzd(string4, i, zzbI));
            AppVisibleCustomProperties.zza com_google_android_gms_drive_metadata_internal_AppVisibleCustomProperties_zza = (AppVisibleCustomProperties.zza) longSparseArray.get(zzb);
            if (com_google_android_gms_drive_metadata_internal_AppVisibleCustomProperties_zza == null) {
                com_google_android_gms_drive_metadata_internal_AppVisibleCustomProperties_zza = new AppVisibleCustomProperties.zza();
                longSparseArray.put(zzb, com_google_android_gms_drive_metadata_internal_AppVisibleCustomProperties_zza);
            }
            com_google_android_gms_drive_metadata_internal_AppVisibleCustomProperties_zza.zza(customProperty);
        }
        return longSparseArray;
    }

    protected /* synthetic */ Object zzc(DataHolder dataHolder, int i, int i2) {
        return zzl(dataHolder, i, i2);
    }

    protected AppVisibleCustomProperties zzl(DataHolder dataHolder, int i, int i2) {
        Bundle zzpH = dataHolder.zzpH();
        SparseArray sparseParcelableArray = zzpH.getSparseParcelableArray("customPropertiesExtra");
        if (sparseParcelableArray == null) {
            if (zzpH.getParcelable("customPropertiesExtraHolder") != null) {
                zzc(dataHolder);
                sparseParcelableArray = zzpH.getSparseParcelableArray("customPropertiesExtra");
            }
            if (sparseParcelableArray == null) {
                return AppVisibleCustomProperties.zzaqF;
            }
        }
        return (AppVisibleCustomProperties) sparseParcelableArray.get(i, AppVisibleCustomProperties.zzaqF);
    }
}
