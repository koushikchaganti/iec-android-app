package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzp;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzie.zza;

@zzha
public class zzhu extends zzil implements zzhv, zzhy {
    private final Context mContext;
    private final String zzBd;
    private final zza zzFc;
    private int zzFt;
    private final String zzJg;
    private final zzht zzJo;
    private final zzhy zzJp;
    private final String zzJq;
    private int zzJr;
    private final Object zzpK;

    /* renamed from: com.google.android.gms.internal.zzhu.1 */
    class C04151 implements Runnable {
        final /* synthetic */ zzex zzJs;
        final /* synthetic */ zzhu zzJt;
        final /* synthetic */ AdRequestParcel zzpL;

        C04151(zzhu com_google_android_gms_internal_zzhu, zzex com_google_android_gms_internal_zzex, AdRequestParcel adRequestParcel) {
            this.zzJt = com_google_android_gms_internal_zzhu;
            this.zzJs = com_google_android_gms_internal_zzex;
            this.zzpL = adRequestParcel;
        }

        public void run() {
            try {
                this.zzJs.zzc(this.zzpL, this.zzJt.zzJq);
            } catch (Throwable e) {
                zzb.zzd("Fail to load ad from adapter.", e);
                this.zzJt.zza(this.zzJt.zzBd, 0);
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.zzhu.2 */
    class C04162 implements Runnable {
        final /* synthetic */ zzex zzJs;
        final /* synthetic */ zzhu zzJt;
        final /* synthetic */ zzhx zzJu;
        final /* synthetic */ AdRequestParcel zzpL;

        C04162(zzhu com_google_android_gms_internal_zzhu, zzex com_google_android_gms_internal_zzex, AdRequestParcel adRequestParcel, zzhx com_google_android_gms_internal_zzhx) {
            this.zzJt = com_google_android_gms_internal_zzhu;
            this.zzJs = com_google_android_gms_internal_zzex;
            this.zzpL = adRequestParcel;
            this.zzJu = com_google_android_gms_internal_zzhx;
        }

        public void run() {
            try {
                this.zzJs.zza(zze.zzB(this.zzJt.mContext), this.zzpL, this.zzJt.zzJg, this.zzJu, this.zzJt.zzJq);
            } catch (Throwable e) {
                zzb.zzd("Fail to initialize adapter " + this.zzJt.zzBd, e);
                this.zzJt.zza(this.zzJt.zzBd, 0);
            }
        }
    }

    public zzhu(Context context, String str, String str2, String str3, zza com_google_android_gms_internal_zzie_zza, zzht com_google_android_gms_internal_zzht, zzhy com_google_android_gms_internal_zzhy) {
        this.zzJr = 0;
        this.zzFt = 3;
        this.mContext = context;
        this.zzBd = str;
        this.zzJg = str2;
        this.zzJq = str3;
        this.zzFc = com_google_android_gms_internal_zzie_zza;
        this.zzJo = com_google_android_gms_internal_zzht;
        this.zzpK = new Object();
        this.zzJp = com_google_android_gms_internal_zzhy;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void zzk(long r4) {
        /*
        r3 = this;
    L_0x0000:
        r1 = r3.zzpK;
        monitor-enter(r1);
        r0 = r3.zzJr;	 Catch:{ all -> 0x0011 }
        if (r0 == 0) goto L_0x0009;
    L_0x0007:
        monitor-exit(r1);	 Catch:{ all -> 0x0011 }
    L_0x0008:
        return;
    L_0x0009:
        r0 = r3.zzf(r4);	 Catch:{ all -> 0x0011 }
        if (r0 != 0) goto L_0x0014;
    L_0x000f:
        monitor-exit(r1);	 Catch:{ all -> 0x0011 }
        goto L_0x0008;
    L_0x0011:
        r0 = move-exception;
        monitor-exit(r1);	 Catch:{ all -> 0x0011 }
        throw r0;
    L_0x0014:
        monitor-exit(r1);	 Catch:{ all -> 0x0011 }
        goto L_0x0000;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzhu.zzk(long):void");
    }

    public void onStop() {
    }

    public void zzN(int i) {
        zza(this.zzBd, 0);
    }

    public void zza(String str, int i) {
        synchronized (this.zzpK) {
            this.zzJr = 2;
            this.zzFt = i;
            this.zzpK.notify();
        }
    }

    public void zzaw(String str) {
        synchronized (this.zzpK) {
            this.zzJr = 1;
            this.zzpK.notify();
        }
    }

    public void zzbp() {
        if (this.zzJo != null && this.zzJo.zzgB() != null && this.zzJo.zzgA() != null) {
            zzhx zzgB = this.zzJo.zzgB();
            zzgB.zza((zzhy) this);
            zzgB.zza((zzhv) this);
            AdRequestParcel adRequestParcel = this.zzFc.zzJK.zzGq;
            zzex zzgA = this.zzJo.zzgA();
            try {
                if (zzgA.isInitialized()) {
                    com.google.android.gms.ads.internal.util.client.zza.zzLE.post(new C04151(this, zzgA, adRequestParcel));
                } else {
                    com.google.android.gms.ads.internal.util.client.zza.zzLE.post(new C04162(this, zzgA, adRequestParcel, zzgB));
                }
            } catch (Throwable e) {
                zzb.zzd("Fail to check if adapter is initialized.", e);
                zza(this.zzBd, 0);
            }
            zzk(zzp.zzbB().elapsedRealtime());
            zzgB.zza(null);
            zzgB.zza(null);
            if (this.zzJr == 1) {
                this.zzJp.zzaw(this.zzBd);
            } else {
                this.zzJp.zza(this.zzBd, this.zzFt);
            }
        }
    }

    protected boolean zzf(long j) {
        long elapsedRealtime = 20000 - (zzp.zzbB().elapsedRealtime() - j);
        if (elapsedRealtime <= 0) {
            return false;
        }
        try {
            this.zzpK.wait(elapsedRealtime);
            return true;
        } catch (InterruptedException e) {
            return false;
        }
    }

    public void zzgC() {
        this.zzJo.zzgB();
        AdRequestParcel adRequestParcel = this.zzFc.zzJK.zzGq;
        try {
            this.zzJo.zzgA().zzc(adRequestParcel, this.zzJq);
        } catch (Throwable e) {
            zzb.zzd("Fail to load ad from adapter.", e);
            zza(this.zzBd, 0);
        }
    }
}
