package com.google.android.gms.internal;

import android.content.Context;
import android.os.Looper;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api.ApiOptions.NoOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.connection.AppMetadata;
import com.google.android.gms.nearby.connection.Connections;
import com.google.android.gms.nearby.connection.Connections.ConnectionRequestListener;
import com.google.android.gms.nearby.connection.Connections.ConnectionResponseCallback;
import com.google.android.gms.nearby.connection.Connections.EndpointDiscoveryListener;
import com.google.android.gms.nearby.connection.Connections.MessageListener;
import com.google.android.gms.nearby.connection.Connections.StartAdvertisingResult;
import java.util.List;

public final class zzra implements Connections {
    public static final com.google.android.gms.common.api.Api.zzc<zzqz> zzTo;
    public static final com.google.android.gms.common.api.Api.zza<zzqz, NoOptions> zzTp;

    /* renamed from: com.google.android.gms.internal.zzra.1 */
    static class C08211 extends com.google.android.gms.common.api.Api.zza<zzqz, NoOptions> {
        C08211() {
        }

        public /* synthetic */ com.google.android.gms.common.api.Api.zzb zza(Context context, Looper looper, zzf com_google_android_gms_common_internal_zzf, Object obj, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            return zzp(context, looper, com_google_android_gms_common_internal_zzf, (NoOptions) obj, connectionCallbacks, onConnectionFailedListener);
        }

        public zzqz zzp(Context context, Looper looper, zzf com_google_android_gms_common_internal_zzf, NoOptions noOptions, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            return new zzqz(context, looper, com_google_android_gms_common_internal_zzf, connectionCallbacks, onConnectionFailedListener);
        }
    }

    private static abstract class zza<R extends Result> extends com.google.android.gms.internal.zzlx.zza<R, zzqz> {
        public zza(GoogleApiClient googleApiClient) {
            super(zzra.zzTo, googleApiClient);
        }
    }

    private static abstract class zzb extends zza<StartAdvertisingResult> {

        /* renamed from: com.google.android.gms.internal.zzra.zzb.1 */
        class C11681 implements StartAdvertisingResult {
            final /* synthetic */ Status zzYl;
            final /* synthetic */ zzb zzaWB;

            C11681(zzb com_google_android_gms_internal_zzra_zzb, Status status) {
                this.zzaWB = com_google_android_gms_internal_zzra_zzb;
                this.zzYl = status;
            }

            public String getLocalEndpointName() {
                return null;
            }

            public Status getStatus() {
                return this.zzYl;
            }
        }

        private zzb(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        public StartAdvertisingResult zzaY(Status status) {
            return new C11681(this, status);
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzaY(status);
        }
    }

    private static abstract class zzc extends zza<Status> {
        private zzc(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        public Status zzb(Status status) {
            return status;
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzb(status);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzra.2 */
    class C14052 extends zzb {
        final /* synthetic */ String val$name;
        final /* synthetic */ AppMetadata zzaWu;
        final /* synthetic */ long zzaWv;
        final /* synthetic */ zzmn zzaWw;
        final /* synthetic */ zzra zzaWx;

        C14052(zzra com_google_android_gms_internal_zzra, GoogleApiClient googleApiClient, String str, AppMetadata appMetadata, long j, zzmn com_google_android_gms_internal_zzmn) {
            this.zzaWx = com_google_android_gms_internal_zzra;
            this.val$name = str;
            this.zzaWu = appMetadata;
            this.zzaWv = j;
            this.zzaWw = com_google_android_gms_internal_zzmn;
            super(null);
        }

        protected void zza(zzqz com_google_android_gms_internal_zzqz) throws RemoteException {
            com_google_android_gms_internal_zzqz.zza(this, this.val$name, this.zzaWu, this.zzaWv, this.zzaWw);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzra.3 */
    class C14063 extends zzc {
        final /* synthetic */ String zzaWs;
        final /* synthetic */ long zzaWv;
        final /* synthetic */ zzra zzaWx;
        final /* synthetic */ zzmn zzaWy;

        C14063(zzra com_google_android_gms_internal_zzra, GoogleApiClient googleApiClient, String str, long j, zzmn com_google_android_gms_internal_zzmn) {
            this.zzaWx = com_google_android_gms_internal_zzra;
            this.zzaWs = str;
            this.zzaWv = j;
            this.zzaWy = com_google_android_gms_internal_zzmn;
            super(null);
        }

        protected void zza(zzqz com_google_android_gms_internal_zzqz) throws RemoteException {
            com_google_android_gms_internal_zzqz.zza((com.google.android.gms.internal.zzlx.zzb) this, this.zzaWs, this.zzaWv, this.zzaWy);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzra.4 */
    class C14074 extends zzc {
        final /* synthetic */ String val$name;
        final /* synthetic */ byte[] zzaEC;
        final /* synthetic */ zzmn zzaWA;
        final /* synthetic */ String zzaWf;
        final /* synthetic */ zzra zzaWx;
        final /* synthetic */ zzmn zzaWz;

        C14074(zzra com_google_android_gms_internal_zzra, GoogleApiClient googleApiClient, String str, String str2, byte[] bArr, zzmn com_google_android_gms_internal_zzmn, zzmn com_google_android_gms_internal_zzmn2) {
            this.zzaWx = com_google_android_gms_internal_zzra;
            this.val$name = str;
            this.zzaWf = str2;
            this.zzaEC = bArr;
            this.zzaWz = com_google_android_gms_internal_zzmn;
            this.zzaWA = com_google_android_gms_internal_zzmn2;
            super(null);
        }

        protected void zza(zzqz com_google_android_gms_internal_zzqz) throws RemoteException {
            com_google_android_gms_internal_zzqz.zza(this, this.val$name, this.zzaWf, this.zzaEC, this.zzaWz, this.zzaWA);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzra.5 */
    class C14085 extends zzc {
        final /* synthetic */ byte[] zzaEC;
        final /* synthetic */ zzmn zzaWA;
        final /* synthetic */ String zzaWf;
        final /* synthetic */ zzra zzaWx;

        C14085(zzra com_google_android_gms_internal_zzra, GoogleApiClient googleApiClient, String str, byte[] bArr, zzmn com_google_android_gms_internal_zzmn) {
            this.zzaWx = com_google_android_gms_internal_zzra;
            this.zzaWf = str;
            this.zzaEC = bArr;
            this.zzaWA = com_google_android_gms_internal_zzmn;
            super(null);
        }

        protected void zza(zzqz com_google_android_gms_internal_zzqz) throws RemoteException {
            com_google_android_gms_internal_zzqz.zza((com.google.android.gms.internal.zzlx.zzb) this, this.zzaWf, this.zzaEC, this.zzaWA);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzra.6 */
    class C14096 extends zzc {
        final /* synthetic */ String zzaWf;
        final /* synthetic */ zzra zzaWx;

        C14096(zzra com_google_android_gms_internal_zzra, GoogleApiClient googleApiClient, String str) {
            this.zzaWx = com_google_android_gms_internal_zzra;
            this.zzaWf = str;
            super(null);
        }

        protected void zza(zzqz com_google_android_gms_internal_zzqz) throws RemoteException {
            com_google_android_gms_internal_zzqz.zzp(this, this.zzaWf);
        }
    }

    static {
        zzTo = new com.google.android.gms.common.api.Api.zzc();
        zzTp = new C08211();
    }

    public static zzqz zzd(GoogleApiClient googleApiClient, boolean z) {
        zzx.zzb(googleApiClient != null, (Object) "GoogleApiClient parameter is required.");
        zzx.zza(googleApiClient.isConnected(), (Object) "GoogleApiClient must be connected.");
        return zze(googleApiClient, z);
    }

    public static zzqz zze(GoogleApiClient googleApiClient, boolean z) {
        zzx.zza(googleApiClient.zza(Nearby.CONNECTIONS_API), (Object) "GoogleApiClient is not configured to use the Nearby Connections Api. Pass Nearby.CONNECTIONS_API into GoogleApiClient.Builder#addApi() to use this feature.");
        boolean hasConnectedApi = googleApiClient.hasConnectedApi(Nearby.CONNECTIONS_API);
        if (!z || hasConnectedApi) {
            return hasConnectedApi ? (zzqz) googleApiClient.zza(zzTo) : null;
        } else {
            throw new IllegalStateException("GoogleApiClient has an optional Nearby.CONNECTIONS_API and is not connected to Nearby Connections. Use GoogleApiClient.hasConnectedApi(Nearby.CONNECTIONS_API) to guard this call.");
        }
    }

    public PendingResult<Status> acceptConnectionRequest(GoogleApiClient apiClient, String remoteEndpointId, byte[] payload, MessageListener messageListener) {
        return apiClient.zzb(new C14085(this, apiClient, remoteEndpointId, payload, apiClient.zzq(messageListener)));
    }

    public void disconnectFromEndpoint(GoogleApiClient apiClient, String remoteEndpointId) {
        zzd(apiClient, false).zzeM(remoteEndpointId);
    }

    public String getLocalDeviceId(GoogleApiClient apiClient) {
        return zzd(apiClient, true).zzCt();
    }

    public String getLocalEndpointId(GoogleApiClient apiClient) {
        return zzd(apiClient, true).zzCs();
    }

    public PendingResult<Status> rejectConnectionRequest(GoogleApiClient apiClient, String remoteEndpointId) {
        return apiClient.zzb(new C14096(this, apiClient, remoteEndpointId));
    }

    public PendingResult<Status> sendConnectionRequest(GoogleApiClient apiClient, String name, String remoteEndpointId, byte[] payload, ConnectionResponseCallback connectionResponseCallback, MessageListener messageListener) {
        return apiClient.zzb(new C14074(this, apiClient, name, remoteEndpointId, payload, apiClient.zzq(connectionResponseCallback), apiClient.zzq(messageListener)));
    }

    public void sendReliableMessage(GoogleApiClient apiClient, String remoteEndpointId, byte[] payload) {
        zzd(apiClient, false).zza(new String[]{remoteEndpointId}, payload);
    }

    public void sendReliableMessage(GoogleApiClient apiClient, List<String> remoteEndpointIds, byte[] payload) {
        zzd(apiClient, false).zza((String[]) remoteEndpointIds.toArray(new String[remoteEndpointIds.size()]), payload);
    }

    public void sendUnreliableMessage(GoogleApiClient apiClient, String remoteEndpointId, byte[] payload) {
        zzd(apiClient, false).zzb(new String[]{remoteEndpointId}, payload);
    }

    public void sendUnreliableMessage(GoogleApiClient apiClient, List<String> remoteEndpointIds, byte[] payload) {
        zzd(apiClient, false).zzb((String[]) remoteEndpointIds.toArray(new String[remoteEndpointIds.size()]), payload);
    }

    public PendingResult<StartAdvertisingResult> startAdvertising(GoogleApiClient apiClient, String name, AppMetadata appMetadata, long durationMillis, ConnectionRequestListener connectionRequestListener) {
        return apiClient.zzb(new C14052(this, apiClient, name, appMetadata, durationMillis, apiClient.zzq(connectionRequestListener)));
    }

    public PendingResult<Status> startDiscovery(GoogleApiClient apiClient, String serviceId, long durationMillis, EndpointDiscoveryListener listener) {
        return apiClient.zzb(new C14063(this, apiClient, serviceId, durationMillis, apiClient.zzq(listener)));
    }

    public void stopAdvertising(GoogleApiClient apiClient) {
        zzd(apiClient, false).zzCu();
    }

    public void stopAllEndpoints(GoogleApiClient apiClient) {
        zzd(apiClient, false).zzCv();
    }

    public void stopDiscovery(GoogleApiClient apiClient, String serviceId) {
        zzd(apiClient, false).zzeL(serviceId);
    }
}
