package com.google.android.gms.internal;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.auth.api.proxy.ProxyApi;
import com.google.android.gms.auth.api.proxy.ProxyApi.ProxyResult;
import com.google.android.gms.auth.api.proxy.ProxyRequest;
import com.google.android.gms.auth.api.proxy.ProxyResponse;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.internal.zzx;

public class zzlb implements ProxyApi {

    /* renamed from: com.google.android.gms.internal.zzlb.1 */
    class C12271 extends zzla {
        final /* synthetic */ ProxyRequest zzVk;
        final /* synthetic */ zzlb zzVl;

        /* renamed from: com.google.android.gms.internal.zzlb.1.1 */
        class C12071 extends zzkw {
            final /* synthetic */ C12271 zzVm;

            C12071(C12271 c12271) {
                this.zzVm = c12271;
            }

            public void zza(ProxyResponse proxyResponse) {
                this.zzVm.zzb(new zzlc(proxyResponse));
            }
        }

        C12271(zzlb com_google_android_gms_internal_zzlb, GoogleApiClient googleApiClient, ProxyRequest proxyRequest) {
            this.zzVl = com_google_android_gms_internal_zzlb;
            this.zzVk = proxyRequest;
            super(googleApiClient);
        }

        protected void zza(Context context, zzkz com_google_android_gms_internal_zzkz) throws RemoteException {
            com_google_android_gms_internal_zzkz.zza(new C12071(this), this.zzVk);
        }
    }

    public PendingResult<ProxyResult> performProxyRequest(GoogleApiClient client, ProxyRequest request) {
        zzx.zzy(client);
        zzx.zzy(request);
        return client.zzb(new C12271(this, client, request));
    }
}
