package com.google.android.gms.internal;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.util.Pair;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzq;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.drive.events.CompletionEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public abstract class zzly<R extends Result> extends PendingResult<R> {
    private boolean zzL;
    private volatile R zzaeT;
    private final Object zzafd;
    protected final zza<R> zzafe;
    private final ArrayList<com.google.android.gms.common.api.PendingResult.zza> zzaff;
    private ResultCallback<? super R> zzafg;
    private volatile boolean zzafh;
    private boolean zzafi;
    private zzq zzafj;
    private Integer zzafk;
    private volatile zzms<R> zzafl;
    private final CountDownLatch zzpy;

    public static class zza<R extends Result> extends Handler {
        public zza() {
            this(Looper.getMainLooper());
        }

        public zza(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case CompletionEvent.STATUS_FAILURE /*1*/:
                    Pair pair = (Pair) msg.obj;
                    zzb((ResultCallback) pair.first, (Result) pair.second);
                case CompletionEvent.STATUS_CONFLICT /*2*/:
                    ((zzly) msg.obj).zzy(Status.zzafa);
                default:
                    Log.wtf("BasePendingResult", "Don't know how to handle message: " + msg.what, new Exception());
            }
        }

        public void zza(ResultCallback<? super R> resultCallback, R r) {
            sendMessage(obtainMessage(1, new Pair(resultCallback, r)));
        }

        public void zza(zzly<R> com_google_android_gms_internal_zzly_R, long j) {
            sendMessageDelayed(obtainMessage(2, com_google_android_gms_internal_zzly_R), j);
        }

        protected void zzb(ResultCallback<? super R> resultCallback, R r) {
            try {
                resultCallback.onResult(r);
            } catch (RuntimeException e) {
                zzly.zzd(r);
                throw e;
            }
        }

        public void zzoS() {
            removeMessages(2);
        }
    }

    @Deprecated
    protected zzly(Looper looper) {
        this.zzafd = new Object();
        this.zzpy = new CountDownLatch(1);
        this.zzaff = new ArrayList();
        this.zzafe = new zza(looper);
    }

    protected zzly(GoogleApiClient googleApiClient) {
        this.zzafd = new Object();
        this.zzpy = new CountDownLatch(1);
        this.zzaff = new ArrayList();
        this.zzafe = new zza(googleApiClient != null ? googleApiClient.getLooper() : Looper.getMainLooper());
    }

    private R get() {
        R r;
        boolean z = true;
        synchronized (this.zzafd) {
            if (this.zzafh) {
                z = false;
            }
            zzx.zza(z, (Object) "Result has already been consumed.");
            zzx.zza(isReady(), (Object) "Result is not ready.");
            r = this.zzaeT;
            this.zzaeT = null;
            this.zzafg = null;
            this.zzafh = true;
        }
        zzoR();
        return r;
    }

    private void zzc(R r) {
        this.zzaeT = r;
        this.zzafj = null;
        this.zzpy.countDown();
        Status status = this.zzaeT.getStatus();
        if (this.zzafg != null) {
            this.zzafe.zzoS();
            if (!this.zzL) {
                this.zzafe.zza(this.zzafg, get());
            }
        }
        Iterator it = this.zzaff.iterator();
        while (it.hasNext()) {
            ((com.google.android.gms.common.api.PendingResult.zza) it.next()).zzu(status);
        }
        this.zzaff.clear();
    }

    public static void zzd(Result result) {
        if (result instanceof Releasable) {
            try {
                ((Releasable) result).release();
            } catch (Throwable e) {
                Log.w("BasePendingResult", "Unable to release " + result, e);
            }
        }
    }

    public final R await() {
        boolean z = true;
        zzx.zza(Looper.myLooper() != Looper.getMainLooper(), (Object) "await must not be called on the UI thread");
        zzx.zza(!this.zzafh, (Object) "Result has already been consumed");
        if (this.zzafl != null) {
            z = false;
        }
        zzx.zza(z, (Object) "Cannot await if then() has been called.");
        try {
            this.zzpy.await();
        } catch (InterruptedException e) {
            zzy(Status.zzaeY);
        }
        zzx.zza(isReady(), (Object) "Result is not ready.");
        return get();
    }

    public final R await(long time, TimeUnit units) {
        boolean z = true;
        boolean z2 = time <= 0 || Looper.myLooper() != Looper.getMainLooper();
        zzx.zza(z2, (Object) "await must not be called on the UI thread when time is greater than zero.");
        zzx.zza(!this.zzafh, (Object) "Result has already been consumed.");
        if (this.zzafl != null) {
            z = false;
        }
        zzx.zza(z, (Object) "Cannot await if then() has been called.");
        try {
            if (!this.zzpy.await(time, units)) {
                zzy(Status.zzafa);
            }
        } catch (InterruptedException e) {
            zzy(Status.zzaeY);
        }
        zzx.zza(isReady(), (Object) "Result is not ready.");
        return get();
    }

    public void cancel() {
        synchronized (this.zzafd) {
            if (this.zzL || this.zzafh) {
                return;
            }
            if (this.zzafj != null) {
                try {
                    this.zzafj.cancel();
                } catch (RemoteException e) {
                }
            }
            zzd(this.zzaeT);
            this.zzafg = null;
            this.zzL = true;
            zzc(zzc(Status.zzafb));
        }
    }

    public boolean isCanceled() {
        boolean z;
        synchronized (this.zzafd) {
            z = this.zzL;
        }
        return z;
    }

    public final boolean isReady() {
        return this.zzpy.getCount() == 0;
    }

    public final void setResultCallback(ResultCallback<? super R> callback) {
        boolean z = true;
        zzx.zza(!this.zzafh, (Object) "Result has already been consumed.");
        synchronized (this.zzafd) {
            if (this.zzafl != null) {
                z = false;
            }
            zzx.zza(z, (Object) "Cannot set callbacks if then() has been called.");
            if (isCanceled()) {
                return;
            }
            if (isReady()) {
                this.zzafe.zza((ResultCallback) callback, get());
            } else {
                this.zzafg = callback;
            }
        }
    }

    public final void setResultCallback(ResultCallback<? super R> callback, long time, TimeUnit units) {
        boolean z = true;
        zzx.zza(!this.zzafh, (Object) "Result has already been consumed.");
        synchronized (this.zzafd) {
            if (this.zzafl != null) {
                z = false;
            }
            zzx.zza(z, (Object) "Cannot set callbacks if then() has been called.");
            if (isCanceled()) {
                return;
            }
            if (isReady()) {
                this.zzafe.zza((ResultCallback) callback, get());
            } else {
                this.zzafg = callback;
                this.zzafe.zza(this, units.toMillis(time));
            }
        }
    }

    public final void zza(com.google.android.gms.common.api.PendingResult.zza com_google_android_gms_common_api_PendingResult_zza) {
        boolean z = true;
        zzx.zza(!this.zzafh, (Object) "Result has already been consumed.");
        if (com_google_android_gms_common_api_PendingResult_zza == null) {
            z = false;
        }
        zzx.zzb(z, (Object) "Callback cannot be null.");
        synchronized (this.zzafd) {
            if (isReady()) {
                com_google_android_gms_common_api_PendingResult_zza.zzu(this.zzaeT.getStatus());
            } else {
                this.zzaff.add(com_google_android_gms_common_api_PendingResult_zza);
            }
        }
    }

    protected final void zza(zzq com_google_android_gms_common_internal_zzq) {
        synchronized (this.zzafd) {
            this.zzafj = com_google_android_gms_common_internal_zzq;
        }
    }

    public final void zzb(R r) {
        boolean z = true;
        synchronized (this.zzafd) {
            if (this.zzafi || this.zzL) {
                zzd(r);
                return;
            }
            zzx.zza(!isReady(), (Object) "Results have already been set");
            if (this.zzafh) {
                z = false;
            }
            zzx.zza(z, (Object) "Result has already been consumed");
            zzc((Result) r);
        }
    }

    protected abstract R zzc(Status status);

    public Integer zzoL() {
        return this.zzafk;
    }

    protected void zzoR() {
    }

    public final void zzy(Status status) {
        synchronized (this.zzafd) {
            if (!isReady()) {
                zzb(zzc(status));
                this.zzafi = true;
            }
        }
    }
}
