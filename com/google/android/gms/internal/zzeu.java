package com.google.android.gms.internal;

import android.content.Context;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.util.client.zzb;
import java.util.ArrayList;
import java.util.List;

@zzha
public class zzeu implements zzel {
    private final Context mContext;
    private zzeq zzBD;
    private final zzen zzBf;
    private final AdRequestInfoParcel zzBu;
    private final long zzBv;
    private final long zzBw;
    private boolean zzBy;
    private final zzch zzoU;
    private final Object zzpK;
    private final zzew zzpd;
    private final boolean zzrF;

    /* renamed from: com.google.android.gms.internal.zzeu.1 */
    class C03851 implements Runnable {
        final /* synthetic */ zzer zzBE;
        final /* synthetic */ zzeu zzBF;

        C03851(zzeu com_google_android_gms_internal_zzeu, zzer com_google_android_gms_internal_zzer) {
            this.zzBF = com_google_android_gms_internal_zzeu;
            this.zzBE = com_google_android_gms_internal_zzer;
        }

        public void run() {
            try {
                this.zzBE.zzBq.destroy();
            } catch (Throwable e) {
                zzb.zzd("Could not destroy mediation adapter.", e);
            }
        }
    }

    public zzeu(Context context, AdRequestInfoParcel adRequestInfoParcel, zzew com_google_android_gms_internal_zzew, zzen com_google_android_gms_internal_zzen, boolean z, long j, long j2, zzch com_google_android_gms_internal_zzch) {
        this.zzpK = new Object();
        this.zzBy = false;
        this.mContext = context;
        this.zzBu = adRequestInfoParcel;
        this.zzpd = com_google_android_gms_internal_zzew;
        this.zzBf = com_google_android_gms_internal_zzen;
        this.zzrF = z;
        this.zzBv = j;
        this.zzBw = j2;
        this.zzoU = com_google_android_gms_internal_zzch;
    }

    public void cancel() {
        synchronized (this.zzpK) {
            this.zzBy = true;
            if (this.zzBD != null) {
                this.zzBD.cancel();
            }
        }
    }

    public zzer zzc(List<zzem> list) {
        zzb.zzaF("Starting mediation.");
        Iterable arrayList = new ArrayList();
        zzcf zzdu = this.zzoU.zzdu();
        for (zzem com_google_android_gms_internal_zzem : list) {
            zzb.zzaG("Trying mediation network: " + com_google_android_gms_internal_zzem.zzAF);
            for (String str : com_google_android_gms_internal_zzem.zzAG) {
                zzcf zzdu2 = this.zzoU.zzdu();
                synchronized (this.zzpK) {
                    if (this.zzBy) {
                        zzer com_google_android_gms_internal_zzer = new zzer(-1);
                        return com_google_android_gms_internal_zzer;
                    }
                    this.zzBD = new zzeq(this.mContext, str, this.zzpd, this.zzBf, com_google_android_gms_internal_zzem, this.zzBu.zzGq, this.zzBu.zzqV, this.zzBu.zzqR, this.zzrF, this.zzBu.zzrj, this.zzBu.zzrl);
                    com_google_android_gms_internal_zzer = this.zzBD.zza(this.zzBv, this.zzBw);
                    if (com_google_android_gms_internal_zzer.zzBo == 0) {
                        zzb.zzaF("Adapter succeeded.");
                        this.zzoU.zzd("mediation_network_succeed", str);
                        if (!arrayList.isEmpty()) {
                            this.zzoU.zzd("mediation_networks_fail", TextUtils.join(",", arrayList));
                        }
                        this.zzoU.zza(zzdu2, "mls");
                        this.zzoU.zza(zzdu, "ttm");
                        return com_google_android_gms_internal_zzer;
                    }
                    arrayList.add(str);
                    this.zzoU.zza(zzdu2, "mlf");
                    if (com_google_android_gms_internal_zzer.zzBq != null) {
                        zzip.zzKO.post(new C03851(this, com_google_android_gms_internal_zzer));
                    }
                }
            }
        }
        if (!arrayList.isEmpty()) {
            this.zzoU.zzd("mediation_networks_fail", TextUtils.join(",", arrayList));
        }
        return new zzer(1);
    }
}
