package com.google.android.gms.internal;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzp;
import com.google.android.gms.drive.events.CompletionEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@zzha
public class zzet implements zzel {
    private final Context mContext;
    private final zzen zzBf;
    private final AdRequestInfoParcel zzBu;
    private final long zzBv;
    private final long zzBw;
    private final int zzBx;
    private boolean zzBy;
    private final Map<zzje<zzer>, zzeq> zzBz;
    private final Object zzpK;
    private final zzew zzpd;
    private final boolean zzrF;

    /* renamed from: com.google.android.gms.internal.zzet.1 */
    class C03831 implements Callable<zzer> {
        final /* synthetic */ zzeq zzBA;
        final /* synthetic */ zzet zzBB;

        C03831(zzet com_google_android_gms_internal_zzet, zzeq com_google_android_gms_internal_zzeq) {
            this.zzBB = com_google_android_gms_internal_zzet;
            this.zzBA = com_google_android_gms_internal_zzeq;
        }

        public /* synthetic */ Object call() throws Exception {
            return zzet();
        }

        public zzer zzet() throws Exception {
            synchronized (this.zzBB.zzpK) {
                if (this.zzBB.zzBy) {
                    return null;
                }
                return this.zzBA.zza(this.zzBB.zzBv, this.zzBB.zzBw);
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.zzet.2 */
    class C03842 implements Runnable {
        final /* synthetic */ zzet zzBB;
        final /* synthetic */ zzje zzBC;

        C03842(zzet com_google_android_gms_internal_zzet, zzje com_google_android_gms_internal_zzje) {
            this.zzBB = com_google_android_gms_internal_zzet;
            this.zzBC = com_google_android_gms_internal_zzje;
        }

        public void run() {
            for (zzje com_google_android_gms_internal_zzje : this.zzBB.zzBz.keySet()) {
                if (com_google_android_gms_internal_zzje != this.zzBC) {
                    ((zzeq) this.zzBB.zzBz.get(com_google_android_gms_internal_zzje)).cancel();
                }
            }
        }
    }

    public zzet(Context context, AdRequestInfoParcel adRequestInfoParcel, zzew com_google_android_gms_internal_zzew, zzen com_google_android_gms_internal_zzen, boolean z, long j, long j2, int i) {
        this.zzpK = new Object();
        this.zzBy = false;
        this.zzBz = new HashMap();
        this.mContext = context;
        this.zzBu = adRequestInfoParcel;
        this.zzpd = com_google_android_gms_internal_zzew;
        this.zzBf = com_google_android_gms_internal_zzen;
        this.zzrF = z;
        this.zzBv = j;
        this.zzBw = j2;
        this.zzBx = i;
    }

    private void zza(zzje<zzer> com_google_android_gms_internal_zzje_com_google_android_gms_internal_zzer) {
        zzip.zzKO.post(new C03842(this, com_google_android_gms_internal_zzje_com_google_android_gms_internal_zzer));
    }

    private zzer zzd(List<zzje<zzer>> list) {
        Throwable e;
        synchronized (this.zzpK) {
            if (this.zzBy) {
                zzer com_google_android_gms_internal_zzer = new zzer(-1);
                return com_google_android_gms_internal_zzer;
            }
            for (zzje com_google_android_gms_internal_zzje : list) {
                try {
                    com_google_android_gms_internal_zzer = (zzer) com_google_android_gms_internal_zzje.get();
                    if (com_google_android_gms_internal_zzer != null && com_google_android_gms_internal_zzer.zzBo == 0) {
                        zza(com_google_android_gms_internal_zzje);
                        return com_google_android_gms_internal_zzer;
                    }
                } catch (InterruptedException e2) {
                    e = e2;
                    zzb.zzd("Exception while processing an adapter; continuing with other adapters", e);
                } catch (ExecutionException e3) {
                    e = e3;
                    zzb.zzd("Exception while processing an adapter; continuing with other adapters", e);
                }
            }
            zza(null);
            return new zzer(1);
        }
    }

    private zzer zze(List<zzje<zzer>> list) {
        RemoteException max;
        synchronized (this.zzpK) {
            if (this.zzBy) {
                zzer com_google_android_gms_internal_zzer = new zzer(-1);
                return com_google_android_gms_internal_zzer;
            }
            long j = -1;
            zzje com_google_android_gms_internal_zzje = null;
            com_google_android_gms_internal_zzer = null;
            long j2 = this.zzBf.zzAY != -1 ? this.zzBf.zzAY : 10000;
            long j3 = j2;
            for (zzje com_google_android_gms_internal_zzje2 : list) {
                zzer com_google_android_gms_internal_zzer2;
                zzez com_google_android_gms_internal_zzez;
                int zzes;
                zzer com_google_android_gms_internal_zzer3;
                zzje com_google_android_gms_internal_zzje3;
                zzer com_google_android_gms_internal_zzer4;
                long currentTimeMillis = zzp.zzbB().currentTimeMillis();
                if (j3 == 0) {
                    try {
                        if (com_google_android_gms_internal_zzje2.isDone()) {
                            com_google_android_gms_internal_zzer2 = (zzer) com_google_android_gms_internal_zzje2.get();
                            if (com_google_android_gms_internal_zzer2 != null && com_google_android_gms_internal_zzer2.zzBo == 0) {
                                com_google_android_gms_internal_zzez = com_google_android_gms_internal_zzer2.zzBt;
                                if (com_google_android_gms_internal_zzez != null && com_google_android_gms_internal_zzez.zzes() > j) {
                                    zzes = com_google_android_gms_internal_zzez.zzes();
                                    com_google_android_gms_internal_zzer3 = com_google_android_gms_internal_zzer2;
                                    com_google_android_gms_internal_zzje3 = com_google_android_gms_internal_zzje2;
                                    com_google_android_gms_internal_zzer4 = com_google_android_gms_internal_zzer3;
                                    com_google_android_gms_internal_zzje = com_google_android_gms_internal_zzje3;
                                    com_google_android_gms_internal_zzer3 = com_google_android_gms_internal_zzer4;
                                    max = Math.max(j3 - (zzp.zzbB().currentTimeMillis() - currentTimeMillis), 0);
                                    j = zzes;
                                    com_google_android_gms_internal_zzer = com_google_android_gms_internal_zzer3;
                                    j3 = max;
                                }
                            }
                            com_google_android_gms_internal_zzer4 = com_google_android_gms_internal_zzer;
                            com_google_android_gms_internal_zzje3 = com_google_android_gms_internal_zzje;
                            zzes = j;
                            com_google_android_gms_internal_zzje = com_google_android_gms_internal_zzje3;
                            com_google_android_gms_internal_zzer3 = com_google_android_gms_internal_zzer4;
                            max = Math.max(j3 - (zzp.zzbB().currentTimeMillis() - currentTimeMillis), 0);
                            j = zzes;
                            com_google_android_gms_internal_zzer = com_google_android_gms_internal_zzer3;
                            j3 = max;
                        }
                    } catch (InterruptedException e) {
                        max = e;
                        try {
                            zzb.zzd("Exception while processing an adapter; continuing with other adapters", max);
                            j3 = max;
                        } finally {
                            com_google_android_gms_internal_zzer = j3 - (zzp.zzbB().currentTimeMillis() - currentTimeMillis);
                            j = 0;
                            Math.max(com_google_android_gms_internal_zzer, j);
                            j = j3;
                        }
                    } catch (ExecutionException e2) {
                        max = e2;
                        zzb.zzd("Exception while processing an adapter; continuing with other adapters", max);
                        j3 = max;
                    } catch (RemoteException e3) {
                        max = e3;
                        zzb.zzd("Exception while processing an adapter; continuing with other adapters", max);
                        j3 = max;
                    } catch (TimeoutException e4) {
                        max = e4;
                        zzb.zzd("Exception while processing an adapter; continuing with other adapters", max);
                        j3 = max;
                    }
                }
                com_google_android_gms_internal_zzer2 = (zzer) com_google_android_gms_internal_zzje2.get(j3, TimeUnit.MILLISECONDS);
                com_google_android_gms_internal_zzez = com_google_android_gms_internal_zzer2.zzBt;
                zzes = com_google_android_gms_internal_zzez.zzes();
                com_google_android_gms_internal_zzer3 = com_google_android_gms_internal_zzer2;
                com_google_android_gms_internal_zzje3 = com_google_android_gms_internal_zzje2;
                com_google_android_gms_internal_zzer4 = com_google_android_gms_internal_zzer3;
                com_google_android_gms_internal_zzje = com_google_android_gms_internal_zzje3;
                com_google_android_gms_internal_zzer3 = com_google_android_gms_internal_zzer4;
                max = Math.max(j3 - (zzp.zzbB().currentTimeMillis() - currentTimeMillis), 0);
                j = zzes;
                com_google_android_gms_internal_zzer = com_google_android_gms_internal_zzer3;
                j3 = max;
            }
            zza(com_google_android_gms_internal_zzje);
            return com_google_android_gms_internal_zzer == null ? new zzer(1) : com_google_android_gms_internal_zzer;
        }
    }

    public void cancel() {
        synchronized (this.zzpK) {
            this.zzBy = true;
            for (zzeq cancel : this.zzBz.values()) {
                cancel.cancel();
            }
        }
    }

    public zzer zzc(List<zzem> list) {
        zzb.zzaF("Starting mediation.");
        ExecutorService newCachedThreadPool = Executors.newCachedThreadPool();
        List arrayList = new ArrayList();
        for (zzem com_google_android_gms_internal_zzem : list) {
            zzb.zzaG("Trying mediation network: " + com_google_android_gms_internal_zzem.zzAF);
            for (String com_google_android_gms_internal_zzeq : com_google_android_gms_internal_zzem.zzAG) {
                zzeq com_google_android_gms_internal_zzeq2 = new zzeq(this.mContext, com_google_android_gms_internal_zzeq, this.zzpd, this.zzBf, com_google_android_gms_internal_zzem, this.zzBu.zzGq, this.zzBu.zzqV, this.zzBu.zzqR, this.zzrF, this.zzBu.zzrj, this.zzBu.zzrl);
                zzje zza = zzio.zza(newCachedThreadPool, new C03831(this, com_google_android_gms_internal_zzeq2));
                this.zzBz.put(zza, com_google_android_gms_internal_zzeq2);
                arrayList.add(zza);
            }
        }
        switch (this.zzBx) {
            case CompletionEvent.STATUS_CONFLICT /*2*/:
                return zze(arrayList);
            default:
                return zzd(arrayList);
        }
    }
}
