package com.google.android.gms.internal;

import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.fitness.HistoryApi;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.request.DailyTotalRequest;
import com.google.android.gms.fitness.request.DataDeleteRequest;
import com.google.android.gms.fitness.request.DataInsertRequest;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DailyTotalResult;
import com.google.android.gms.fitness.result.DataReadResult;
import com.google.android.gms.internal.zzlx.zzb;

public class zzpv implements HistoryApi {

    private static class zza extends com.google.android.gms.internal.zzpa.zza {
        private final zzb<DataReadResult> zzakL;
        private int zzaxI;
        private DataReadResult zzaxJ;

        private zza(zzb<DataReadResult> com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_fitness_result_DataReadResult) {
            this.zzaxI = 0;
            this.zzaxJ = null;
            this.zzakL = com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_fitness_result_DataReadResult;
        }

        public void zza(DataReadResult dataReadResult) {
            synchronized (this) {
                if (Log.isLoggable("Fitness", 2)) {
                    Log.v("Fitness", "Received batch result " + this.zzaxI);
                }
                if (this.zzaxJ == null) {
                    this.zzaxJ = dataReadResult;
                } else {
                    this.zzaxJ.zzb(dataReadResult);
                }
                this.zzaxI++;
                if (this.zzaxI == this.zzaxJ.zzuH()) {
                    this.zzakL.zzr(this.zzaxJ);
                }
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.zzpv.3 */
    class C12333 extends zza<DataReadResult> {
        final /* synthetic */ zzpv zzaxD;
        final /* synthetic */ DataReadRequest zzaxF;

        C12333(zzpv com_google_android_gms_internal_zzpv, GoogleApiClient googleApiClient, DataReadRequest dataReadRequest) {
            this.zzaxD = com_google_android_gms_internal_zzpv;
            this.zzaxF = dataReadRequest;
            super(googleApiClient);
        }

        protected DataReadResult zzL(Status status) {
            return DataReadResult.zza(status, this.zzaxF);
        }

        protected void zza(zzou com_google_android_gms_internal_zzou) throws RemoteException {
            ((zzpf) com_google_android_gms_internal_zzou.zzqs()).zza(new DataReadRequest(this.zzaxF, new zza(null)));
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzL(status);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzpv.4 */
    class C12344 extends zza<DailyTotalResult> {
        final /* synthetic */ zzpv zzaxD;
        final /* synthetic */ DataType zzaxG;

        /* renamed from: com.google.android.gms.internal.zzpv.4.1 */
        class C11671 extends com.google.android.gms.internal.zzoz.zza {
            final /* synthetic */ C12344 zzaxH;

            C11671(C12344 c12344) {
                this.zzaxH = c12344;
            }

            public void zza(DailyTotalResult dailyTotalResult) throws RemoteException {
                this.zzaxH.zzb(dailyTotalResult);
            }
        }

        C12344(zzpv com_google_android_gms_internal_zzpv, GoogleApiClient googleApiClient, DataType dataType) {
            this.zzaxD = com_google_android_gms_internal_zzpv;
            this.zzaxG = dataType;
            super(googleApiClient);
        }

        protected DailyTotalResult zzM(Status status) {
            return DailyTotalResult.zza(status, this.zzaxG);
        }

        protected void zza(zzou com_google_android_gms_internal_zzou) throws RemoteException {
            ((zzpf) com_google_android_gms_internal_zzou.zzqs()).zza(new DailyTotalRequest(new C11671(this), this.zzaxG));
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzM(status);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzpv.1 */
    class C13941 extends zzc {
        final /* synthetic */ DataSet zzaxB;
        final /* synthetic */ boolean zzaxC;
        final /* synthetic */ zzpv zzaxD;

        C13941(zzpv com_google_android_gms_internal_zzpv, GoogleApiClient googleApiClient, DataSet dataSet, boolean z) {
            this.zzaxD = com_google_android_gms_internal_zzpv;
            this.zzaxB = dataSet;
            this.zzaxC = z;
            super(googleApiClient);
        }

        protected void zza(zzou com_google_android_gms_internal_zzou) throws RemoteException {
            ((zzpf) com_google_android_gms_internal_zzou.zzqs()).zza(new DataInsertRequest(this.zzaxB, new zzqa(this), this.zzaxC));
        }
    }

    /* renamed from: com.google.android.gms.internal.zzpv.2 */
    class C13952 extends zzc {
        final /* synthetic */ zzpv zzaxD;
        final /* synthetic */ DataDeleteRequest zzaxE;

        C13952(zzpv com_google_android_gms_internal_zzpv, GoogleApiClient googleApiClient, DataDeleteRequest dataDeleteRequest) {
            this.zzaxD = com_google_android_gms_internal_zzpv;
            this.zzaxE = dataDeleteRequest;
            super(googleApiClient);
        }

        protected void zza(zzou com_google_android_gms_internal_zzou) throws RemoteException {
            ((zzpf) com_google_android_gms_internal_zzou.zzqs()).zza(new DataDeleteRequest(this.zzaxE, new zzqa(this)));
        }
    }

    private PendingResult<Status> zza(GoogleApiClient googleApiClient, DataSet dataSet, boolean z) {
        zzx.zzb((Object) dataSet, (Object) "Must set the data set");
        zzx.zza(!dataSet.getDataPoints().isEmpty(), (Object) "Cannot use an empty data set");
        zzx.zzb(dataSet.getDataSource().zztK(), (Object) "Must set the app package name for the data source");
        return googleApiClient.zza(new C13941(this, googleApiClient, dataSet, z));
    }

    public PendingResult<Status> deleteData(GoogleApiClient client, DataDeleteRequest request) {
        return client.zza(new C13952(this, client, request));
    }

    public PendingResult<Status> insertData(GoogleApiClient client, DataSet dataSet) {
        return zza(client, dataSet, false);
    }

    public PendingResult<DailyTotalResult> readDailyTotal(GoogleApiClient client, DataType dataType) {
        return client.zza(new C12344(this, client, dataType));
    }

    public PendingResult<DataReadResult> readData(GoogleApiClient client, DataReadRequest request) {
        return client.zza(new C12333(this, client, request));
    }
}
