package com.google.android.gms.internal;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.overlay.zzd;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.drive.ExecutionOptions;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzha
public final class zzdk {
    public static final zzdl zzyA;
    public static final zzdl zzyB;
    public static final zzdl zzyC;
    public static final zzdl zzyD;
    public static final zzdl zzyE;
    public static final zzdl zzyF;
    public static final zzdl zzyG;
    public static final zzdl zzyH;
    public static final zzdl zzyI;
    public static final zzdl zzyJ;
    public static final zzdl zzyK;
    public static final zzdl zzyL;
    public static final zzdl zzyz;

    /* renamed from: com.google.android.gms.internal.zzdk.1 */
    static class C07471 implements zzdl {
        C07471() {
        }

        public void zza(zzjn com_google_android_gms_internal_zzjn, Map<String, String> map) {
        }
    }

    /* renamed from: com.google.android.gms.internal.zzdk.2 */
    static class C07482 implements zzdl {
        C07482() {
        }

        public void zza(zzjn com_google_android_gms_internal_zzjn, Map<String, String> map) {
            if (((Boolean) zzbz.zzwy.get()).booleanValue()) {
                com_google_android_gms_internal_zzjn.zzF(!Boolean.parseBoolean((String) map.get("disabled")));
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.zzdk.3 */
    static class C07493 implements zzdl {
        C07493() {
        }

        public void zza(zzjn com_google_android_gms_internal_zzjn, Map<String, String> map) {
            String str = (String) map.get("urls");
            if (TextUtils.isEmpty(str)) {
                zzb.zzaH("URLs missing in canOpenURLs GMSG.");
                return;
            }
            String[] split = str.split(",");
            Map hashMap = new HashMap();
            PackageManager packageManager = com_google_android_gms_internal_zzjn.getContext().getPackageManager();
            for (String str2 : split) {
                String[] split2 = str2.split(";", 2);
                hashMap.put(str2, Boolean.valueOf(packageManager.resolveActivity(new Intent(split2.length > 1 ? split2[1].trim() : "android.intent.action.VIEW", Uri.parse(split2[0].trim())), ExecutionOptions.MAX_TRACKING_TAG_STRING_LENGTH) != null));
            }
            com_google_android_gms_internal_zzjn.zzb("openableURLs", hashMap);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzdk.4 */
    static class C07504 implements zzdl {
        C07504() {
        }

        public void zza(zzjn com_google_android_gms_internal_zzjn, Map<String, String> map) {
            PackageManager packageManager = com_google_android_gms_internal_zzjn.getContext().getPackageManager();
            try {
                try {
                    JSONArray jSONArray = new JSONObject((String) map.get("data")).getJSONArray("intents");
                    JSONObject jSONObject = new JSONObject();
                    for (int i = 0; i < jSONArray.length(); i++) {
                        try {
                            JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                            String optString = jSONObject2.optString("id");
                            Object optString2 = jSONObject2.optString("u");
                            Object optString3 = jSONObject2.optString("i");
                            Object optString4 = jSONObject2.optString("m");
                            Object optString5 = jSONObject2.optString("p");
                            Object optString6 = jSONObject2.optString("c");
                            jSONObject2.optString("f");
                            jSONObject2.optString("e");
                            Intent intent = new Intent();
                            if (!TextUtils.isEmpty(optString2)) {
                                intent.setData(Uri.parse(optString2));
                            }
                            if (!TextUtils.isEmpty(optString3)) {
                                intent.setAction(optString3);
                            }
                            if (!TextUtils.isEmpty(optString4)) {
                                intent.setType(optString4);
                            }
                            if (!TextUtils.isEmpty(optString5)) {
                                intent.setPackage(optString5);
                            }
                            if (!TextUtils.isEmpty(optString6)) {
                                String[] split = optString6.split("/", 2);
                                if (split.length == 2) {
                                    intent.setComponent(new ComponentName(split[0], split[1]));
                                }
                            }
                            try {
                                jSONObject.put(optString, packageManager.resolveActivity(intent, ExecutionOptions.MAX_TRACKING_TAG_STRING_LENGTH) != null);
                            } catch (Throwable e) {
                                zzb.zzb("Error constructing openable urls response.", e);
                            }
                        } catch (Throwable e2) {
                            zzb.zzb("Error parsing the intent data.", e2);
                        }
                    }
                    com_google_android_gms_internal_zzjn.zzb("openableIntents", jSONObject);
                } catch (JSONException e3) {
                    com_google_android_gms_internal_zzjn.zzb("openableIntents", new JSONObject());
                }
            } catch (JSONException e4) {
                com_google_android_gms_internal_zzjn.zzb("openableIntents", new JSONObject());
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.zzdk.5 */
    static class C07515 implements zzdl {
        C07515() {
        }

        public void zza(zzjn com_google_android_gms_internal_zzjn, Map<String, String> map) {
            String str = (String) map.get("u");
            if (str == null) {
                zzb.zzaH("URL missing from click GMSG.");
                return;
            }
            Uri zza;
            Uri parse = Uri.parse(str);
            try {
                zzan zzhE = com_google_android_gms_internal_zzjn.zzhE();
                if (zzhE != null && zzhE.zzb(parse)) {
                    zza = zzhE.zza(parse, com_google_android_gms_internal_zzjn.getContext());
                    new zziw(com_google_android_gms_internal_zzjn.getContext(), com_google_android_gms_internal_zzjn.zzhF().afmaVersion, zza.toString()).zzgX();
                }
            } catch (zzao e) {
                zzb.zzaH("Unable to append parameter to URL: " + str);
            }
            zza = parse;
            new zziw(com_google_android_gms_internal_zzjn.getContext(), com_google_android_gms_internal_zzjn.zzhF().afmaVersion, zza.toString()).zzgX();
        }
    }

    /* renamed from: com.google.android.gms.internal.zzdk.6 */
    static class C07526 implements zzdl {
        C07526() {
        }

        public void zza(zzjn com_google_android_gms_internal_zzjn, Map<String, String> map) {
            zzd zzhA = com_google_android_gms_internal_zzjn.zzhA();
            if (zzhA != null) {
                zzhA.close();
                return;
            }
            zzhA = com_google_android_gms_internal_zzjn.zzhB();
            if (zzhA != null) {
                zzhA.close();
            } else {
                zzb.zzaH("A GMSG tried to close something that wasn't an overlay.");
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.zzdk.7 */
    static class C07537 implements zzdl {
        C07537() {
        }

        public void zza(zzjn com_google_android_gms_internal_zzjn, Map<String, String> map) {
            com_google_android_gms_internal_zzjn.zzE("1".equals(map.get("custom_close")));
        }
    }

    /* renamed from: com.google.android.gms.internal.zzdk.8 */
    static class C07548 implements zzdl {
        C07548() {
        }

        public void zza(zzjn com_google_android_gms_internal_zzjn, Map<String, String> map) {
            String str = (String) map.get("u");
            if (str == null) {
                zzb.zzaH("URL missing from httpTrack GMSG.");
            } else {
                new zziw(com_google_android_gms_internal_zzjn.getContext(), com_google_android_gms_internal_zzjn.zzhF().afmaVersion, str).zzgX();
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.zzdk.9 */
    static class C07559 implements zzdl {
        C07559() {
        }

        public void zza(zzjn com_google_android_gms_internal_zzjn, Map<String, String> map) {
            zzb.zzaG("Received log message: " + ((String) map.get("string")));
        }
    }

    static {
        zzyz = new C07471();
        zzyA = new C07493();
        zzyB = new C07504();
        zzyC = new C07515();
        zzyD = new C07526();
        zzyE = new C07537();
        zzyF = new C07548();
        zzyG = new C07559();
        zzyH = new zzdl() {
            public void zza(zzjn com_google_android_gms_internal_zzjn, Map<String, String> map) {
                String str = (String) map.get("ty");
                String str2 = (String) map.get("td");
                try {
                    int parseInt = Integer.parseInt((String) map.get("tx"));
                    int parseInt2 = Integer.parseInt(str);
                    int parseInt3 = Integer.parseInt(str2);
                    zzan zzhE = com_google_android_gms_internal_zzjn.zzhE();
                    if (zzhE != null) {
                        zzhE.zzac().zza(parseInt, parseInt2, parseInt3);
                    }
                } catch (NumberFormatException e) {
                    zzb.zzaH("Could not parse touch parameters from gmsg.");
                }
            }
        };
        zzyI = new C07482();
        zzyJ = new zzdt();
        zzyK = new zzdx();
        zzyL = new zzdj();
    }
}
