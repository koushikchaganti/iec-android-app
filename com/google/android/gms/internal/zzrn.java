package com.google.android.gms.internal;

import android.net.Uri;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Moments;
import com.google.android.gms.plus.Moments.LoadMomentsResult;
import com.google.android.gms.plus.internal.zze;
import com.google.android.gms.plus.model.moments.Moment;
import com.google.android.gms.plus.model.moments.MomentBuffer;

public final class zzrn implements Moments {

    private static abstract class zza extends com.google.android.gms.plus.Plus.zza<LoadMomentsResult> {

        /* renamed from: com.google.android.gms.internal.zzrn.zza.1 */
        class C11701 implements LoadMomentsResult {
            final /* synthetic */ Status zzYl;
            final /* synthetic */ zza zzaZt;

            C11701(zza com_google_android_gms_internal_zzrn_zza, Status status) {
                this.zzaZt = com_google_android_gms_internal_zzrn_zza;
                this.zzYl = status;
            }

            public MomentBuffer getMomentBuffer() {
                return null;
            }

            public String getNextPageToken() {
                return null;
            }

            public Status getStatus() {
                return this.zzYl;
            }

            public String getUpdated() {
                return null;
            }

            public void release() {
            }
        }

        private zza(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        public LoadMomentsResult zzbb(Status status) {
            return new C11701(this, status);
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzbb(status);
        }
    }

    private static abstract class zzb extends com.google.android.gms.plus.Plus.zza<Status> {
        private zzb(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        public Status zzb(Status status) {
            return status;
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzb(status);
        }
    }

    private static abstract class zzc extends com.google.android.gms.plus.Plus.zza<Status> {
        private zzc(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        public Status zzb(Status status) {
            return status;
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzb(status);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzrn.1 */
    class C14131 extends zza {
        final /* synthetic */ zzrn zzaZm;

        C14131(zzrn com_google_android_gms_internal_zzrn, GoogleApiClient googleApiClient) {
            this.zzaZm = com_google_android_gms_internal_zzrn;
            super(null);
        }

        protected void zza(zze com_google_android_gms_plus_internal_zze) {
            com_google_android_gms_plus_internal_zze.zzl(this);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzrn.2 */
    class C14142 extends zza {
        final /* synthetic */ int zzaDL;
        final /* synthetic */ zzrn zzaZm;
        final /* synthetic */ String zzaZn;
        final /* synthetic */ Uri zzaZo;
        final /* synthetic */ String zzaZp;
        final /* synthetic */ String zzaZq;

        C14142(zzrn com_google_android_gms_internal_zzrn, GoogleApiClient googleApiClient, int i, String str, Uri uri, String str2, String str3) {
            this.zzaZm = com_google_android_gms_internal_zzrn;
            this.zzaDL = i;
            this.zzaZn = str;
            this.zzaZo = uri;
            this.zzaZp = str2;
            this.zzaZq = str3;
            super(null);
        }

        protected void zza(zze com_google_android_gms_plus_internal_zze) {
            com_google_android_gms_plus_internal_zze.zza(this, this.zzaDL, this.zzaZn, this.zzaZo, this.zzaZp, this.zzaZq);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzrn.3 */
    class C14153 extends zzc {
        final /* synthetic */ zzrn zzaZm;
        final /* synthetic */ Moment zzaZr;

        C14153(zzrn com_google_android_gms_internal_zzrn, GoogleApiClient googleApiClient, Moment moment) {
            this.zzaZm = com_google_android_gms_internal_zzrn;
            this.zzaZr = moment;
            super(null);
        }

        protected void zza(zze com_google_android_gms_plus_internal_zze) {
            com_google_android_gms_plus_internal_zze.zza((com.google.android.gms.internal.zzlx.zzb) this, this.zzaZr);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzrn.4 */
    class C14164 extends zzb {
        final /* synthetic */ zzrn zzaZm;
        final /* synthetic */ String zzaZs;

        C14164(zzrn com_google_android_gms_internal_zzrn, GoogleApiClient googleApiClient, String str) {
            this.zzaZm = com_google_android_gms_internal_zzrn;
            this.zzaZs = str;
            super(null);
        }

        protected void zza(zze com_google_android_gms_plus_internal_zze) {
            com_google_android_gms_plus_internal_zze.zzeS(this.zzaZs);
            zzb(Status.zzaeX);
        }
    }

    public PendingResult<LoadMomentsResult> load(GoogleApiClient googleApiClient) {
        return googleApiClient.zza(new C14131(this, googleApiClient));
    }

    public PendingResult<LoadMomentsResult> load(GoogleApiClient googleApiClient, int maxResults, String pageToken, Uri targetUrl, String type, String userId) {
        return googleApiClient.zza(new C14142(this, googleApiClient, maxResults, pageToken, targetUrl, type, userId));
    }

    public PendingResult<Status> remove(GoogleApiClient googleApiClient, String momentId) {
        return googleApiClient.zzb(new C14164(this, googleApiClient, momentId));
    }

    public PendingResult<Status> write(GoogleApiClient googleApiClient, Moment moment) {
        return googleApiClient.zzb(new C14153(this, googleApiClient, moment));
    }
}
