package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.google.android.gms.auth.api.signin.internal.IdpTokenType;
import com.google.android.gms.auth.api.signin.zze;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzld.zza;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class zzlf extends zzle {
    public static final List<String> zzWA;
    private Class<?> zzWB;
    private Class<?> zzWC;
    private Object zzWD;
    private Object zzWE;

    /* renamed from: com.google.android.gms.internal.zzlf.1 */
    class C04441 implements InvocationHandler {
        final /* synthetic */ zzlf zzWF;

        C04441(zzlf com_google_android_gms_internal_zzlf) {
            this.zzWF = com_google_android_gms_internal_zzlf;
        }

        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            Class cls = Class.forName("com.facebook.login.LoginResult");
            Class cls2 = Class.forName("com.facebook.FacebookException");
            Class[] parameterTypes = method.getParameterTypes();
            if (method.getName().equals("onSuccess") && parameterTypes.length == 1 && args[0].getClass() == cls) {
                this.zzWF.zznc().zzk(this.zzWF.zza(IdpTokenType.zzWc, (String) Class.forName("com.facebook.AccessToken").getMethod("getToken", new Class[0]).invoke(cls.getDeclaredMethod("getAccessToken", new Class[0]).invoke(args[0], new Object[0]), new Object[0]), this.zzWF.zznd()));
            } else if (method.getName().equals("onCancel") && parameterTypes.length == 0) {
                this.zzWF.zznc().zzmV();
            } else if (method.getName().equals("onError") && parameterTypes.length == 1 && parameterTypes[0] == cls2) {
                Log.e("AuthSignInClient", "facebook login error!", (Exception) args[0]);
                this.zzWF.zznc().zza((Exception) args[0]);
            } else {
                throw new ExceptionInInitializerError("Method not supported!");
            }
            return null;
        }
    }

    /* renamed from: com.google.android.gms.internal.zzlf.2 */
    class C04452 implements InvocationHandler {
        final /* synthetic */ zzlf zzWF;
        final /* synthetic */ Class zzWG;

        C04452(zzlf com_google_android_gms_internal_zzlf, Class cls) {
            this.zzWF = com_google_android_gms_internal_zzlf;
            this.zzWG = cls;
        }

        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            Class cls = Class.forName("com.facebook.SessionState");
            Class[] parameterTypes = method.getParameterTypes();
            if (method.getName().equals(NotificationCompatApi21.CATEGORY_CALL) && parameterTypes.length == 3 && parameterTypes[0] == this.zzWG && parameterTypes[1] == cls && parameterTypes[2] == Exception.class) {
                if (((Boolean) this.zzWG.getDeclaredMethod("isOpened", new Class[0]).invoke(args[0], new Object[0])).booleanValue()) {
                    this.zzWF.zznc().zzk(this.zzWF.zza(IdpTokenType.zzWc, (String) this.zzWG.getDeclaredMethod("getAccessToken", new Class[0]).invoke(args[0], new Object[0]), this.zzWF.zznd()));
                }
                return null;
            }
            throw new ExceptionInInitializerError("Method not supported!");
        }
    }

    static {
        zzWA = Collections.singletonList(Scopes.EMAIL);
    }

    public zzlf(Activity activity, List<String> list) {
        super(activity, zzWA, list);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void zzag(android.content.Context r5) throws java.lang.IllegalStateException {
        /*
        com.google.android.gms.common.internal.zzx.zzy(r5);
        r0 = "com.facebook.FacebookSdk";
        r0 = java.lang.Class.forName(r0);	 Catch:{ ClassNotFoundException -> 0x006b }
        r1 = "sdkInitialize";
        r2 = 2;
        r2 = new java.lang.Class[r2];	 Catch:{ NoSuchMethodException -> 0x00ab, IllegalAccessException -> 0x005b, InvocationTargetException -> 0x00ad }
        r3 = 0;
        r4 = android.content.Context.class;
        r2[r3] = r4;	 Catch:{ NoSuchMethodException -> 0x00ab, IllegalAccessException -> 0x005b, InvocationTargetException -> 0x00ad }
        r3 = 1;
        r4 = java.lang.Integer.TYPE;	 Catch:{ NoSuchMethodException -> 0x00ab, IllegalAccessException -> 0x005b, InvocationTargetException -> 0x00ad }
        r2[r3] = r4;	 Catch:{ NoSuchMethodException -> 0x00ab, IllegalAccessException -> 0x005b, InvocationTargetException -> 0x00ad }
        r0 = r0.getDeclaredMethod(r1, r2);	 Catch:{ NoSuchMethodException -> 0x00ab, IllegalAccessException -> 0x005b, InvocationTargetException -> 0x00ad }
        r1 = 0;
        r2 = 2;
        r2 = new java.lang.Object[r2];	 Catch:{ NoSuchMethodException -> 0x00ab, IllegalAccessException -> 0x005b, InvocationTargetException -> 0x00ad }
        r3 = 0;
        r4 = r5.getApplicationContext();	 Catch:{ NoSuchMethodException -> 0x00ab, IllegalAccessException -> 0x005b, InvocationTargetException -> 0x00ad }
        r2[r3] = r4;	 Catch:{ NoSuchMethodException -> 0x00ab, IllegalAccessException -> 0x005b, InvocationTargetException -> 0x00ad }
        r3 = 1;
        r4 = 64206; // 0xface float:8.9972E-41 double:3.1722E-319;
        r4 = java.lang.Integer.valueOf(r4);	 Catch:{ NoSuchMethodException -> 0x00ab, IllegalAccessException -> 0x005b, InvocationTargetException -> 0x00ad }
        r2[r3] = r4;	 Catch:{ NoSuchMethodException -> 0x00ab, IllegalAccessException -> 0x005b, InvocationTargetException -> 0x00ad }
        r0.invoke(r1, r2);	 Catch:{ NoSuchMethodException -> 0x00ab, IllegalAccessException -> 0x005b, InvocationTargetException -> 0x00ad }
        r0 = "com.facebook.login.LoginManager";
        r0 = java.lang.Class.forName(r0);	 Catch:{ NoSuchMethodException -> 0x00ab, IllegalAccessException -> 0x005b, InvocationTargetException -> 0x00ad }
        r1 = "getInstance";
        r2 = 0;
        r2 = new java.lang.Class[r2];	 Catch:{ NoSuchMethodException -> 0x00ab, IllegalAccessException -> 0x005b, InvocationTargetException -> 0x00ad }
        r1 = r0.getDeclaredMethod(r1, r2);	 Catch:{ NoSuchMethodException -> 0x00ab, IllegalAccessException -> 0x005b, InvocationTargetException -> 0x00ad }
        r2 = 0;
        r3 = 0;
        r3 = new java.lang.Object[r3];	 Catch:{ NoSuchMethodException -> 0x00ab, IllegalAccessException -> 0x005b, InvocationTargetException -> 0x00ad }
        r1 = r1.invoke(r2, r3);	 Catch:{ NoSuchMethodException -> 0x00ab, IllegalAccessException -> 0x005b, InvocationTargetException -> 0x00ad }
        r2 = "logOut";
        r3 = 0;
        r3 = new java.lang.Class[r3];	 Catch:{ NoSuchMethodException -> 0x00ab, IllegalAccessException -> 0x005b, InvocationTargetException -> 0x00ad }
        r0 = r0.getDeclaredMethod(r2, r3);	 Catch:{ NoSuchMethodException -> 0x00ab, IllegalAccessException -> 0x005b, InvocationTargetException -> 0x00ad }
        r2 = 0;
        r2 = new java.lang.Object[r2];	 Catch:{ NoSuchMethodException -> 0x00ab, IllegalAccessException -> 0x005b, InvocationTargetException -> 0x00ad }
        r0.invoke(r1, r2);	 Catch:{ NoSuchMethodException -> 0x00ab, IllegalAccessException -> 0x005b, InvocationTargetException -> 0x00ad }
    L_0x005a:
        return;
    L_0x005b:
        r0 = move-exception;
    L_0x005c:
        r1 = "AuthSignInClient";
        r2 = "Facebook logout error.";
        android.util.Log.e(r1, r2, r0);	 Catch:{ ClassNotFoundException -> 0x006b }
        r0 = new java.lang.IllegalStateException;	 Catch:{ ClassNotFoundException -> 0x006b }
        r1 = "No supported Facebook SDK version found to use Facebook logout.";
        r0.<init>(r1);	 Catch:{ ClassNotFoundException -> 0x006b }
        throw r0;	 Catch:{ ClassNotFoundException -> 0x006b }
    L_0x006b:
        r0 = move-exception;
        r0 = "com.facebook.Session";
        r0 = java.lang.Class.forName(r0);	 Catch:{ ClassNotFoundException -> 0x0095, NoSuchMethodException -> 0x00a7, IllegalAccessException -> 0x00a5, InvocationTargetException -> 0x00a9 }
        r1 = "getActiveSession";
        r2 = 0;
        r2 = new java.lang.Class[r2];	 Catch:{ ClassNotFoundException -> 0x0095, NoSuchMethodException -> 0x00a7, IllegalAccessException -> 0x00a5, InvocationTargetException -> 0x00a9 }
        r1 = r0.getDeclaredMethod(r1, r2);	 Catch:{ ClassNotFoundException -> 0x0095, NoSuchMethodException -> 0x00a7, IllegalAccessException -> 0x00a5, InvocationTargetException -> 0x00a9 }
        r2 = 0;
        r3 = 0;
        r3 = new java.lang.Object[r3];	 Catch:{ ClassNotFoundException -> 0x0095, NoSuchMethodException -> 0x00a7, IllegalAccessException -> 0x00a5, InvocationTargetException -> 0x00a9 }
        r1 = r1.invoke(r2, r3);	 Catch:{ ClassNotFoundException -> 0x0095, NoSuchMethodException -> 0x00a7, IllegalAccessException -> 0x00a5, InvocationTargetException -> 0x00a9 }
        if (r1 == 0) goto L_0x005a;
    L_0x0085:
        r2 = "closeAndClearTokenInformation";
        r3 = 0;
        r3 = new java.lang.Class[r3];	 Catch:{ ClassNotFoundException -> 0x0095, NoSuchMethodException -> 0x00a7, IllegalAccessException -> 0x00a5, InvocationTargetException -> 0x00a9 }
        r0 = r0.getDeclaredMethod(r2, r3);	 Catch:{ ClassNotFoundException -> 0x0095, NoSuchMethodException -> 0x00a7, IllegalAccessException -> 0x00a5, InvocationTargetException -> 0x00a9 }
        r2 = 0;
        r2 = new java.lang.Object[r2];	 Catch:{ ClassNotFoundException -> 0x0095, NoSuchMethodException -> 0x00a7, IllegalAccessException -> 0x00a5, InvocationTargetException -> 0x00a9 }
        r0.invoke(r1, r2);	 Catch:{ ClassNotFoundException -> 0x0095, NoSuchMethodException -> 0x00a7, IllegalAccessException -> 0x00a5, InvocationTargetException -> 0x00a9 }
        goto L_0x005a;
    L_0x0095:
        r0 = move-exception;
    L_0x0096:
        r1 = "AuthSignInClient";
        r2 = "Facebook logout error.";
        android.util.Log.e(r1, r2, r0);
        r0 = new java.lang.IllegalStateException;
        r1 = "No supported Facebook SDK version found to use Facebook logout.";
        r0.<init>(r1);
        throw r0;
    L_0x00a5:
        r0 = move-exception;
        goto L_0x0096;
    L_0x00a7:
        r0 = move-exception;
        goto L_0x0096;
    L_0x00a9:
        r0 = move-exception;
        goto L_0x0096;
    L_0x00ab:
        r0 = move-exception;
        goto L_0x005c;
    L_0x00ad:
        r0 = move-exception;
        goto L_0x005c;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzlf.zzag(android.content.Context):void");
    }

    private void zzne() throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        if (this.zzWE != null) {
            Class.forName("com.facebook.login.LoginManager").getDeclaredMethod("logInWithReadPermissions", new Class[]{Activity.class, Collection.class}).invoke(this.zzWE, new Object[]{this.mActivity, new ArrayList(zznb())});
            return;
        }
        Class cls = Class.forName("com.facebook.Session$OpenRequest");
        Object newInstance = cls.getConstructor(new Class[]{Activity.class}).newInstance(new Object[]{this.mActivity});
        cls.getDeclaredMethod("setPermissions", new Class[]{List.class}).invoke(newInstance, new Object[]{new ArrayList(zznb())});
        cls.getDeclaredMethod("setRequestCode", new Class[]{Integer.TYPE}).invoke(newInstance, new Object[]{Integer.valueOf(64206)});
        Class cls2 = Class.forName("com.facebook.Session");
        cls.getDeclaredMethod("setCallback", new Class[]{Class.forName("com.facebook.Session$StatusCallback")}).invoke(newInstance, new Object[]{zzng()});
        Object newInstance2 = cls2.getConstructor(new Class[]{Context.class}).newInstance(new Object[]{this.mActivity});
        cls2.getDeclaredMethod("setActiveSession", new Class[]{cls2}).invoke(null, new Object[]{newInstance2});
        cls2.getDeclaredMethod("openForRead", new Class[]{cls}).invoke(newInstance2, new Object[]{newInstance});
    }

    private Object zznf() throws ClassNotFoundException {
        return Proxy.newProxyInstance(Class.forName("com.facebook.FacebookCallback").getClassLoader(), new Class[]{r0}, new C04441(this));
    }

    private Object zzng() throws ClassNotFoundException {
        Class cls = Class.forName("com.facebook.Session");
        return Proxy.newProxyInstance(Class.forName("com.facebook.Session$StatusCallback").getClassLoader(), new Class[]{r1}, new C04452(this, cls));
    }

    public void zza(zza com_google_android_gms_internal_zzld_zza) {
        Throwable e;
        zzb(null, null, (zza) zzx.zzy(com_google_android_gms_internal_zzld_zza));
        try {
            zzne();
        } catch (ClassNotFoundException e2) {
            e = e2;
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e3) {
            e = e3;
            throw new RuntimeException(e);
        } catch (IllegalAccessException e4) {
            e = e4;
            throw new RuntimeException(e);
        } catch (InvocationTargetException e5) {
            e = e5;
            throw new RuntimeException(e);
        } catch (InstantiationException e6) {
            e = e6;
            throw new RuntimeException(e);
        }
    }

    public void zza(String str, zza com_google_android_gms_internal_zzld_zza) {
        Throwable e;
        zzb((String) zzx.zzy(str), null, (zza) zzx.zzy(com_google_android_gms_internal_zzld_zza));
        try {
            zzne();
        } catch (ClassNotFoundException e2) {
            e = e2;
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e3) {
            e = e3;
            throw new RuntimeException(e);
        } catch (IllegalAccessException e4) {
            e = e4;
            throw new RuntimeException(e);
        } catch (InvocationTargetException e5) {
            e = e5;
            throw new RuntimeException(e);
        } catch (InstantiationException e6) {
            e = e6;
            throw new RuntimeException(e);
        }
    }

    public void zza(String str, String str2, zza com_google_android_gms_internal_zzld_zza) {
        Throwable e;
        zzb((String) zzx.zzy(str), (String) zzx.zzy(str2), (zza) zzx.zzy(com_google_android_gms_internal_zzld_zza));
        try {
            zzne();
        } catch (ClassNotFoundException e2) {
            e = e2;
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e3) {
            e = e3;
            throw new RuntimeException(e);
        } catch (IllegalAccessException e4) {
            e = e4;
            throw new RuntimeException(e);
        } catch (InvocationTargetException e5) {
            e = e5;
            throw new RuntimeException(e);
        } catch (InstantiationException e6) {
            e = e6;
            throw new RuntimeException(e);
        }
    }

    public boolean zza(int i, int i2, Intent intent, zza com_google_android_gms_internal_zzld_zza) {
        Throwable e;
        zzb(com_google_android_gms_internal_zzld_zza);
        if (i != 64206 && this.zzWB == null) {
            return false;
        }
        if (this.zzWB == null || this.zzWC == null || this.zzWD == null) {
            try {
                Class cls = Class.forName("com.facebook.Session");
                Object invoke = cls.getDeclaredMethod("getActiveSession", new Class[0]).invoke(null, new Object[0]);
                Method declaredMethod = cls.getDeclaredMethod("onActivityResult", new Class[]{Activity.class, Integer.TYPE, Integer.TYPE, Intent.class});
                if (invoke == null) {
                    return false;
                }
                return ((Boolean) declaredMethod.invoke(invoke, new Object[]{this.mActivity, Integer.valueOf(i), Integer.valueOf(i2), intent})).booleanValue();
            } catch (ClassNotFoundException e2) {
                e = e2;
                throw new RuntimeException(e);
            } catch (NoSuchMethodException e3) {
                e = e3;
                throw new RuntimeException(e);
            } catch (IllegalAccessException e4) {
                e = e4;
                throw new RuntimeException(e);
            } catch (InvocationTargetException e5) {
                e = e5;
                throw new RuntimeException(e);
            }
        }
        try {
            if (!((Boolean) this.zzWB.getDeclaredMethod("isFacebookRequestCode", new Class[]{Integer.TYPE}).invoke(null, new Object[]{Integer.valueOf(i)})).booleanValue()) {
                return false;
            }
            return ((Boolean) this.zzWC.getDeclaredMethod("onActivityResult", new Class[]{Integer.TYPE, Integer.TYPE, Intent.class}).invoke(this.zzWD, new Object[]{Integer.valueOf(i), Integer.valueOf(i2), intent})).booleanValue();
        } catch (NoSuchMethodException e6) {
            e = e6;
            throw new RuntimeException(e);
        } catch (IllegalAccessException e7) {
            e = e7;
            throw new RuntimeException(e);
        } catch (InvocationTargetException e8) {
            e = e8;
            throw new RuntimeException(e);
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void zzaf(android.content.Context r7) {
        /*
        r6 = this;
        com.google.android.gms.common.internal.zzx.zzy(r7);
        r0 = "com.facebook.FacebookSdk";
        r0 = java.lang.Class.forName(r0);	 Catch:{ ClassNotFoundException -> 0x009e }
        r6.zzWB = r0;	 Catch:{ ClassNotFoundException -> 0x009e }
        r0 = r6.zzWB;	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r1 = "sdkInitialize";
        r2 = 2;
        r2 = new java.lang.Class[r2];	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r3 = 0;
        r4 = android.content.Context.class;
        r2[r3] = r4;	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r3 = 1;
        r4 = java.lang.Integer.TYPE;	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r2[r3] = r4;	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r0 = r0.getDeclaredMethod(r1, r2);	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r1 = 0;
        r2 = 2;
        r2 = new java.lang.Object[r2];	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r3 = 0;
        r4 = r7.getApplicationContext();	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r2[r3] = r4;	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r3 = 1;
        r4 = 64206; // 0xface float:8.9972E-41 double:3.1722E-319;
        r4 = java.lang.Integer.valueOf(r4);	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r2[r3] = r4;	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r0.invoke(r1, r2);	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r0 = "com.facebook.CallbackManager$Factory";
        r0 = java.lang.Class.forName(r0);	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r1 = "create";
        r2 = 0;
        r2 = new java.lang.Class[r2];	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r0 = r0.getDeclaredMethod(r1, r2);	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r1 = 0;
        r2 = 0;
        r2 = new java.lang.Object[r2];	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r0 = r0.invoke(r1, r2);	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r6.zzWD = r0;	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r0 = "com.facebook.login.LoginManager";
        r0 = java.lang.Class.forName(r0);	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r1 = "getInstance";
        r2 = 0;
        r2 = new java.lang.Class[r2];	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r1 = r0.getDeclaredMethod(r1, r2);	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r2 = 0;
        r3 = 0;
        r3 = new java.lang.Object[r3];	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r1 = r1.invoke(r2, r3);	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r6.zzWE = r1;	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r1 = "com.facebook.CallbackManager";
        r1 = java.lang.Class.forName(r1);	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r6.zzWC = r1;	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r1 = "com.facebook.FacebookCallback";
        r1 = java.lang.Class.forName(r1);	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r2 = "registerCallback";
        r3 = 2;
        r3 = new java.lang.Class[r3];	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r4 = 0;
        r5 = r6.zzWC;	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r3[r4] = r5;	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r4 = 1;
        r3[r4] = r1;	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r0 = r0.getDeclaredMethod(r2, r3);	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r1 = r6.zzWE;	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r2 = 2;
        r2 = new java.lang.Object[r2];	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r3 = 0;
        r4 = r6.zzWD;	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r2[r3] = r4;	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r3 = 1;
        r4 = r6.zznf();	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r2[r3] = r4;	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
        r0.invoke(r1, r2);	 Catch:{ ClassNotFoundException -> 0x00ae, NoSuchMethodException -> 0x00b7, IllegalAccessException -> 0x00b5, InvocationTargetException -> 0x00b9 }
    L_0x009d:
        return;
    L_0x009e:
        r0 = move-exception;
        r0 = "com.facebook.Session";
        java.lang.Class.forName(r0);	 Catch:{ ClassNotFoundException -> 0x00a5 }
        goto L_0x009d;
    L_0x00a5:
        r0 = move-exception;
        r0 = new java.lang.RuntimeException;
        r1 = "No supported Facebook sdk found.";
        r0.<init>(r1);
        throw r0;
    L_0x00ae:
        r0 = move-exception;
    L_0x00af:
        r1 = new java.lang.RuntimeException;
        r1.<init>(r0);
        throw r1;
    L_0x00b5:
        r0 = move-exception;
        goto L_0x00af;
    L_0x00b7:
        r0 = move-exception;
        goto L_0x00af;
    L_0x00b9:
        r0 = move-exception;
        goto L_0x00af;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzlf.zzaf(android.content.Context):void");
    }

    public zze zzna() {
        return zze.FACEBOOK;
    }
}
