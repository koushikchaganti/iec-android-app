package com.google.android.gms.internal;

import android.os.Process;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzp;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

@zzha
public final class zzio {
    private static final ExecutorService zzKA;
    private static final ExecutorService zzKB;

    /* renamed from: com.google.android.gms.internal.zzio.1 */
    static class C04201 implements Callable<Void> {
        final /* synthetic */ Runnable zzKC;

        C04201(Runnable runnable) {
            this.zzKC = runnable;
        }

        public /* synthetic */ Object call() throws Exception {
            return zzdm();
        }

        public Void zzdm() {
            this.zzKC.run();
            return null;
        }
    }

    /* renamed from: com.google.android.gms.internal.zzio.2 */
    static class C04212 implements Callable<Void> {
        final /* synthetic */ Runnable zzKC;

        C04212(Runnable runnable) {
            this.zzKC = runnable;
        }

        public /* synthetic */ Object call() throws Exception {
            return zzdm();
        }

        public Void zzdm() {
            this.zzKC.run();
            return null;
        }
    }

    /* renamed from: com.google.android.gms.internal.zzio.3 */
    static class C04223 implements Runnable {
        final /* synthetic */ zzjb zzKD;
        final /* synthetic */ Callable zzKE;

        C04223(zzjb com_google_android_gms_internal_zzjb, Callable callable) {
            this.zzKD = com_google_android_gms_internal_zzjb;
            this.zzKE = callable;
        }

        public void run() {
            try {
                Process.setThreadPriority(10);
                this.zzKD.zzf(this.zzKE.call());
            } catch (Throwable e) {
                zzp.zzbA().zzb(e, true);
                this.zzKD.cancel(true);
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.zzio.4 */
    static class C04234 implements Runnable {
        final /* synthetic */ zzjb zzKD;
        final /* synthetic */ Future zzKF;

        C04234(zzjb com_google_android_gms_internal_zzjb, Future future) {
            this.zzKD = com_google_android_gms_internal_zzjb;
            this.zzKF = future;
        }

        public void run() {
            if (this.zzKD.isCancelled()) {
                this.zzKF.cancel(true);
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.zzio.5 */
    static class C04245 implements ThreadFactory {
        private final AtomicInteger zzKG;
        final /* synthetic */ String zzKH;

        C04245(String str) {
            this.zzKH = str;
            this.zzKG = new AtomicInteger(1);
        }

        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "AdWorker(" + this.zzKH + ") #" + this.zzKG.getAndIncrement());
        }
    }

    static {
        zzKA = Executors.newFixedThreadPool(10, zzay("Default"));
        zzKB = Executors.newFixedThreadPool(5, zzay("Loader"));
    }

    public static zzje<Void> zza(int i, Runnable runnable) {
        return i == 1 ? zza(zzKB, new C04201(runnable)) : zza(zzKA, new C04212(runnable));
    }

    public static zzje<Void> zza(Runnable runnable) {
        return zza(0, runnable);
    }

    public static <T> zzje<T> zza(Callable<T> callable) {
        return zza(zzKA, (Callable) callable);
    }

    public static <T> zzje<T> zza(ExecutorService executorService, Callable<T> callable) {
        Object com_google_android_gms_internal_zzjb = new zzjb();
        try {
            com_google_android_gms_internal_zzjb.zzc(new C04234(com_google_android_gms_internal_zzjb, executorService.submit(new C04223(com_google_android_gms_internal_zzjb, callable))));
        } catch (Throwable e) {
            zzb.zzd("Thread execution is rejected.", e);
            com_google_android_gms_internal_zzjb.cancel(true);
        }
        return com_google_android_gms_internal_zzjb;
    }

    private static ThreadFactory zzay(String str) {
        return new C04245(str);
    }
}
