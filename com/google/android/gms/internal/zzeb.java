package com.google.android.gms.internal;

import android.os.Handler;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzk;
import com.google.android.gms.ads.internal.zzp;
import java.util.LinkedList;
import java.util.List;

@zzha
class zzeb {
    private final List<zza> zzpw;

    /* renamed from: com.google.android.gms.internal.zzeb.6 */
    class C03756 implements Runnable {
        final /* synthetic */ zzeb zzzA;
        final /* synthetic */ zza zzzK;
        final /* synthetic */ zzec zzzL;

        C03756(zzeb com_google_android_gms_internal_zzeb, zza com_google_android_gms_internal_zzeb_zza, zzec com_google_android_gms_internal_zzec) {
            this.zzzA = com_google_android_gms_internal_zzeb;
            this.zzzK = com_google_android_gms_internal_zzeb_zza;
            this.zzzL = com_google_android_gms_internal_zzec;
        }

        public void run() {
            try {
                this.zzzK.zzb(this.zzzL);
            } catch (Throwable e) {
                zzb.zzd("Could not propagate interstitial ad event.", e);
            }
        }
    }

    interface zza {
        void zzb(zzec com_google_android_gms_internal_zzec) throws RemoteException;
    }

    /* renamed from: com.google.android.gms.internal.zzeb.1 */
    class C11571 extends com.google.android.gms.ads.internal.client.zzo.zza {
        final /* synthetic */ zzeb zzzA;

        /* renamed from: com.google.android.gms.internal.zzeb.1.1 */
        class C07561 implements zza {
            final /* synthetic */ C11571 zzzB;

            C07561(C11571 c11571) {
                this.zzzB = c11571;
            }

            public void zzb(zzec com_google_android_gms_internal_zzec) throws RemoteException {
                if (com_google_android_gms_internal_zzec.zzpz != null) {
                    com_google_android_gms_internal_zzec.zzpz.onAdClosed();
                }
                zzp.zzbI().zzdX();
            }
        }

        /* renamed from: com.google.android.gms.internal.zzeb.1.2 */
        class C07572 implements zza {
            final /* synthetic */ C11571 zzzB;
            final /* synthetic */ int zzzC;

            C07572(C11571 c11571, int i) {
                this.zzzB = c11571;
                this.zzzC = i;
            }

            public void zzb(zzec com_google_android_gms_internal_zzec) throws RemoteException {
                if (com_google_android_gms_internal_zzec.zzpz != null) {
                    com_google_android_gms_internal_zzec.zzpz.onAdFailedToLoad(this.zzzC);
                }
            }
        }

        /* renamed from: com.google.android.gms.internal.zzeb.1.3 */
        class C07583 implements zza {
            final /* synthetic */ C11571 zzzB;

            C07583(C11571 c11571) {
                this.zzzB = c11571;
            }

            public void zzb(zzec com_google_android_gms_internal_zzec) throws RemoteException {
                if (com_google_android_gms_internal_zzec.zzpz != null) {
                    com_google_android_gms_internal_zzec.zzpz.onAdLeftApplication();
                }
            }
        }

        /* renamed from: com.google.android.gms.internal.zzeb.1.4 */
        class C07594 implements zza {
            final /* synthetic */ C11571 zzzB;

            C07594(C11571 c11571) {
                this.zzzB = c11571;
            }

            public void zzb(zzec com_google_android_gms_internal_zzec) throws RemoteException {
                if (com_google_android_gms_internal_zzec.zzpz != null) {
                    com_google_android_gms_internal_zzec.zzpz.onAdLoaded();
                }
            }
        }

        /* renamed from: com.google.android.gms.internal.zzeb.1.5 */
        class C07605 implements zza {
            final /* synthetic */ C11571 zzzB;

            C07605(C11571 c11571) {
                this.zzzB = c11571;
            }

            public void zzb(zzec com_google_android_gms_internal_zzec) throws RemoteException {
                if (com_google_android_gms_internal_zzec.zzpz != null) {
                    com_google_android_gms_internal_zzec.zzpz.onAdOpened();
                }
            }
        }

        C11571(zzeb com_google_android_gms_internal_zzeb) {
            this.zzzA = com_google_android_gms_internal_zzeb;
        }

        public void onAdClosed() throws RemoteException {
            this.zzzA.zzpw.add(new C07561(this));
        }

        public void onAdFailedToLoad(int errorCode) throws RemoteException {
            this.zzzA.zzpw.add(new C07572(this, errorCode));
            zzb.m10v("Pooled interstitial failed to load.");
        }

        public void onAdLeftApplication() throws RemoteException {
            this.zzzA.zzpw.add(new C07583(this));
        }

        public void onAdLoaded() throws RemoteException {
            this.zzzA.zzpw.add(new C07594(this));
            zzb.m10v("Pooled interstitial loaded.");
        }

        public void onAdOpened() throws RemoteException {
            this.zzzA.zzpw.add(new C07605(this));
        }
    }

    /* renamed from: com.google.android.gms.internal.zzeb.2 */
    class C11582 extends com.google.android.gms.ads.internal.client.zzu.zza {
        final /* synthetic */ zzeb zzzA;

        /* renamed from: com.google.android.gms.internal.zzeb.2.1 */
        class C07611 implements zza {
            final /* synthetic */ String val$name;
            final /* synthetic */ String zzzD;
            final /* synthetic */ C11582 zzzE;

            C07611(C11582 c11582, String str, String str2) {
                this.zzzE = c11582;
                this.val$name = str;
                this.zzzD = str2;
            }

            public void zzb(zzec com_google_android_gms_internal_zzec) throws RemoteException {
                if (com_google_android_gms_internal_zzec.zzzM != null) {
                    com_google_android_gms_internal_zzec.zzzM.onAppEvent(this.val$name, this.zzzD);
                }
            }
        }

        C11582(zzeb com_google_android_gms_internal_zzeb) {
            this.zzzA = com_google_android_gms_internal_zzeb;
        }

        public void onAppEvent(String name, String info) throws RemoteException {
            this.zzzA.zzpw.add(new C07611(this, name, info));
        }
    }

    /* renamed from: com.google.android.gms.internal.zzeb.3 */
    class C11593 extends com.google.android.gms.internal.zzgc.zza {
        final /* synthetic */ zzeb zzzA;

        /* renamed from: com.google.android.gms.internal.zzeb.3.1 */
        class C07621 implements zza {
            final /* synthetic */ zzgb zzzF;
            final /* synthetic */ C11593 zzzG;

            C07621(C11593 c11593, zzgb com_google_android_gms_internal_zzgb) {
                this.zzzG = c11593;
                this.zzzF = com_google_android_gms_internal_zzgb;
            }

            public void zzb(zzec com_google_android_gms_internal_zzec) throws RemoteException {
                if (com_google_android_gms_internal_zzec.zzzN != null) {
                    com_google_android_gms_internal_zzec.zzzN.zza(this.zzzF);
                }
            }
        }

        C11593(zzeb com_google_android_gms_internal_zzeb) {
            this.zzzA = com_google_android_gms_internal_zzeb;
        }

        public void zza(zzgb com_google_android_gms_internal_zzgb) throws RemoteException {
            this.zzzA.zzpw.add(new C07621(this, com_google_android_gms_internal_zzgb));
        }
    }

    /* renamed from: com.google.android.gms.internal.zzeb.4 */
    class C11604 extends com.google.android.gms.internal.zzcl.zza {
        final /* synthetic */ zzeb zzzA;

        /* renamed from: com.google.android.gms.internal.zzeb.4.1 */
        class C07631 implements zza {
            final /* synthetic */ zzck zzzH;
            final /* synthetic */ C11604 zzzI;

            C07631(C11604 c11604, zzck com_google_android_gms_internal_zzck) {
                this.zzzI = c11604;
                this.zzzH = com_google_android_gms_internal_zzck;
            }

            public void zzb(zzec com_google_android_gms_internal_zzec) throws RemoteException {
                if (com_google_android_gms_internal_zzec.zzzO != null) {
                    com_google_android_gms_internal_zzec.zzzO.zza(this.zzzH);
                }
            }
        }

        C11604(zzeb com_google_android_gms_internal_zzeb) {
            this.zzzA = com_google_android_gms_internal_zzeb;
        }

        public void zza(zzck com_google_android_gms_internal_zzck) throws RemoteException {
            this.zzzA.zzpw.add(new C07631(this, com_google_android_gms_internal_zzck));
        }
    }

    /* renamed from: com.google.android.gms.internal.zzeb.5 */
    class C11615 extends com.google.android.gms.ads.internal.client.zzn.zza {
        final /* synthetic */ zzeb zzzA;

        /* renamed from: com.google.android.gms.internal.zzeb.5.1 */
        class C07641 implements zza {
            final /* synthetic */ C11615 zzzJ;

            C07641(C11615 c11615) {
                this.zzzJ = c11615;
            }

            public void zzb(zzec com_google_android_gms_internal_zzec) throws RemoteException {
                if (com_google_android_gms_internal_zzec.zzzP != null) {
                    com_google_android_gms_internal_zzec.zzzP.onAdClicked();
                }
            }
        }

        C11615(zzeb com_google_android_gms_internal_zzeb) {
            this.zzzA = com_google_android_gms_internal_zzeb;
        }

        public void onAdClicked() throws RemoteException {
            this.zzzA.zzpw.add(new C07641(this));
        }
    }

    zzeb() {
        this.zzpw = new LinkedList();
    }

    void zza(zzec com_google_android_gms_internal_zzec) {
        Handler handler = zzip.zzKO;
        for (zza c03756 : this.zzpw) {
            handler.post(new C03756(this, c03756, com_google_android_gms_internal_zzec));
        }
    }

    void zzc(zzk com_google_android_gms_ads_internal_zzk) {
        com_google_android_gms_ads_internal_zzk.zza(new C11571(this));
        com_google_android_gms_ads_internal_zzk.zza(new C11582(this));
        com_google_android_gms_ads_internal_zzk.zza(new C11593(this));
        com_google_android_gms_ads_internal_zzk.zza(new C11604(this));
        com_google_android_gms_ads_internal_zzk.zza(new C11615(this));
    }
}
