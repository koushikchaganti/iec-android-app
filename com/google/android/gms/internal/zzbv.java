package com.google.android.gms.internal;

import android.content.SharedPreferences;
import com.google.android.gms.ads.internal.zzp;

@zzha
public abstract class zzbv<T> {
    private final int zzuW;
    private final String zzuX;
    private final T zzuY;

    /* renamed from: com.google.android.gms.internal.zzbv.1 */
    static class C07401 extends zzbv<Boolean> {
        C07401(int i, String str, Boolean bool) {
            super(str, bool, null);
        }

        public /* synthetic */ Object zza(SharedPreferences sharedPreferences) {
            return zzb(sharedPreferences);
        }

        public Boolean zzb(SharedPreferences sharedPreferences) {
            return Boolean.valueOf(sharedPreferences.getBoolean(getKey(), ((Boolean) zzdk()).booleanValue()));
        }
    }

    /* renamed from: com.google.android.gms.internal.zzbv.2 */
    static class C07412 extends zzbv<Integer> {
        C07412(int i, String str, Integer num) {
            super(str, num, null);
        }

        public /* synthetic */ Object zza(SharedPreferences sharedPreferences) {
            return zzc(sharedPreferences);
        }

        public Integer zzc(SharedPreferences sharedPreferences) {
            return Integer.valueOf(sharedPreferences.getInt(getKey(), ((Integer) zzdk()).intValue()));
        }
    }

    /* renamed from: com.google.android.gms.internal.zzbv.3 */
    static class C07423 extends zzbv<Long> {
        C07423(int i, String str, Long l) {
            super(str, l, null);
        }

        public /* synthetic */ Object zza(SharedPreferences sharedPreferences) {
            return zzd(sharedPreferences);
        }

        public Long zzd(SharedPreferences sharedPreferences) {
            return Long.valueOf(sharedPreferences.getLong(getKey(), ((Long) zzdk()).longValue()));
        }
    }

    /* renamed from: com.google.android.gms.internal.zzbv.4 */
    static class C07434 extends zzbv<String> {
        C07434(int i, String str, String str2) {
            super(str, str2, null);
        }

        public /* synthetic */ Object zza(SharedPreferences sharedPreferences) {
            return zze(sharedPreferences);
        }

        public String zze(SharedPreferences sharedPreferences) {
            return sharedPreferences.getString(getKey(), (String) zzdk());
        }
    }

    private zzbv(int i, String str, T t) {
        this.zzuW = i;
        this.zzuX = str;
        this.zzuY = t;
        zzp.zzbF().zza(this);
    }

    public static zzbv<Integer> zza(int i, String str, int i2) {
        return new C07412(i, str, Integer.valueOf(i2));
    }

    public static zzbv<Long> zza(int i, String str, long j) {
        return new C07423(i, str, Long.valueOf(j));
    }

    public static zzbv<Boolean> zza(int i, String str, Boolean bool) {
        return new C07401(i, str, bool);
    }

    public static zzbv<String> zza(int i, String str, String str2) {
        return new C07434(i, str, str2);
    }

    public static zzbv<String> zzc(int i, String str) {
        zzbv<String> zza = zza(i, str, (String) null);
        zzp.zzbF().zzb(zza);
        return zza;
    }

    public static zzbv<String> zzd(int i, String str) {
        zzbv<String> zza = zza(i, str, (String) null);
        zzp.zzbF().zzc(zza);
        return zza;
    }

    public T get() {
        return zzp.zzbG().zzd(this);
    }

    public String getKey() {
        return this.zzuX;
    }

    protected abstract T zza(SharedPreferences sharedPreferences);

    public T zzdk() {
        return this.zzuY;
    }
}
