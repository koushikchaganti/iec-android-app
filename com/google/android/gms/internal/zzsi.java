package com.google.android.gms.internal;

import android.content.Context;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class zzsi {
    public static final Integer zzbhq;
    public static final Integer zzbhr;
    private final Context mContext;
    private final ExecutorService zzbfg;

    static {
        zzbhq = Integer.valueOf(0);
        zzbhr = Integer.valueOf(1);
    }

    public zzsi(Context context) {
        this(context, Executors.newSingleThreadExecutor());
    }

    zzsi(Context context, ExecutorService executorService) {
        this.mContext = context;
        this.zzbfg = executorService;
    }
}
