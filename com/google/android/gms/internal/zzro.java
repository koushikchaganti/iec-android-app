package com.google.android.gms.internal;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.zzlx.zzb;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.People.LoadPeopleResult;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.internal.zze;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.PersonBuffer;
import java.util.Collection;

public final class zzro implements People {

    private static abstract class zza extends com.google.android.gms.plus.Plus.zza<LoadPeopleResult> {

        /* renamed from: com.google.android.gms.internal.zzro.zza.1 */
        class C11711 implements LoadPeopleResult {
            final /* synthetic */ Status zzYl;
            final /* synthetic */ zza zzaZy;

            C11711(zza com_google_android_gms_internal_zzro_zza, Status status) {
                this.zzaZy = com_google_android_gms_internal_zzro_zza;
                this.zzYl = status;
            }

            public String getNextPageToken() {
                return null;
            }

            public PersonBuffer getPersonBuffer() {
                return null;
            }

            public Status getStatus() {
                return this.zzYl;
            }

            public void release() {
            }
        }

        private zza(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        public LoadPeopleResult zzbc(Status status) {
            return new C11711(this, status);
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzbc(status);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzro.1 */
    class C14171 extends zza {
        final /* synthetic */ String zzaZn;
        final /* synthetic */ int zzaZu;
        final /* synthetic */ zzro zzaZv;

        C14171(zzro com_google_android_gms_internal_zzro, GoogleApiClient googleApiClient, int i, String str) {
            this.zzaZv = com_google_android_gms_internal_zzro;
            this.zzaZu = i;
            this.zzaZn = str;
            super(null);
        }

        protected void zza(zze com_google_android_gms_plus_internal_zze) {
            zza(com_google_android_gms_plus_internal_zze.zza(this, this.zzaZu, this.zzaZn));
        }
    }

    /* renamed from: com.google.android.gms.internal.zzro.2 */
    class C14182 extends zza {
        final /* synthetic */ String zzaZn;
        final /* synthetic */ zzro zzaZv;

        C14182(zzro com_google_android_gms_internal_zzro, GoogleApiClient googleApiClient, String str) {
            this.zzaZv = com_google_android_gms_internal_zzro;
            this.zzaZn = str;
            super(null);
        }

        protected void zza(zze com_google_android_gms_plus_internal_zze) {
            zza(com_google_android_gms_plus_internal_zze.zzq(this, this.zzaZn));
        }
    }

    /* renamed from: com.google.android.gms.internal.zzro.3 */
    class C14193 extends zza {
        final /* synthetic */ zzro zzaZv;

        C14193(zzro com_google_android_gms_internal_zzro, GoogleApiClient googleApiClient) {
            this.zzaZv = com_google_android_gms_internal_zzro;
            super(null);
        }

        protected void zza(zze com_google_android_gms_plus_internal_zze) {
            com_google_android_gms_plus_internal_zze.zzm(this);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzro.4 */
    class C14204 extends zza {
        final /* synthetic */ zzro zzaZv;
        final /* synthetic */ Collection zzaZw;

        C14204(zzro com_google_android_gms_internal_zzro, GoogleApiClient googleApiClient, Collection collection) {
            this.zzaZv = com_google_android_gms_internal_zzro;
            this.zzaZw = collection;
            super(null);
        }

        protected void zza(zze com_google_android_gms_plus_internal_zze) {
            com_google_android_gms_plus_internal_zze.zza((zzb) this, this.zzaZw);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzro.5 */
    class C14215 extends zza {
        final /* synthetic */ zzro zzaZv;
        final /* synthetic */ String[] zzaZx;

        C14215(zzro com_google_android_gms_internal_zzro, GoogleApiClient googleApiClient, String[] strArr) {
            this.zzaZv = com_google_android_gms_internal_zzro;
            this.zzaZx = strArr;
            super(null);
        }

        protected void zza(zze com_google_android_gms_plus_internal_zze) {
            com_google_android_gms_plus_internal_zze.zzd(this, this.zzaZx);
        }
    }

    public Person getCurrentPerson(GoogleApiClient googleApiClient) {
        return Plus.zzf(googleApiClient, true).zzDf();
    }

    public PendingResult<LoadPeopleResult> load(GoogleApiClient googleApiClient, Collection<String> personIds) {
        return googleApiClient.zza(new C14204(this, googleApiClient, personIds));
    }

    public PendingResult<LoadPeopleResult> load(GoogleApiClient googleApiClient, String... personIds) {
        return googleApiClient.zza(new C14215(this, googleApiClient, personIds));
    }

    public PendingResult<LoadPeopleResult> loadConnected(GoogleApiClient googleApiClient) {
        return googleApiClient.zza(new C14193(this, googleApiClient));
    }

    public PendingResult<LoadPeopleResult> loadVisible(GoogleApiClient googleApiClient, int orderBy, String pageToken) {
        return googleApiClient.zza(new C14171(this, googleApiClient, orderBy, pageToken));
    }

    public PendingResult<LoadPeopleResult> loadVisible(GoogleApiClient googleApiClient, String pageToken) {
        return googleApiClient.zza(new C14182(this, googleApiClient, pageToken));
    }
}
