package com.google.android.gms.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzj;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.nearby.connection.AppMetadata;
import com.google.android.gms.nearby.connection.Connections.ConnectionRequestListener;
import com.google.android.gms.nearby.connection.Connections.ConnectionResponseCallback;
import com.google.android.gms.nearby.connection.Connections.EndpointDiscoveryListener;
import com.google.android.gms.nearby.connection.Connections.MessageListener;
import com.google.android.gms.nearby.connection.Connections.StartAdvertisingResult;

public final class zzqz extends zzj<zzrc> {
    private final long zzaBC;

    private static final class zzf implements StartAdvertisingResult {
        private final Status zzTA;
        private final String zzaWp;

        zzf(Status status, String str) {
            this.zzTA = status;
            this.zzaWp = str;
        }

        public String getLocalEndpointName() {
            return this.zzaWp;
        }

        public Status getStatus() {
            return this.zzTA;
        }
    }

    private static class zzb extends zzqy {
        private final zzmn<MessageListener> zzaWe;

        /* renamed from: com.google.android.gms.internal.zzqz.zzb.1 */
        class C08151 implements com.google.android.gms.internal.zzmn.zzb<MessageListener> {
            final /* synthetic */ byte[] zzaEC;
            final /* synthetic */ String zzaWf;
            final /* synthetic */ boolean zzaWg;
            final /* synthetic */ zzb zzaWh;

            C08151(zzb com_google_android_gms_internal_zzqz_zzb, String str, byte[] bArr, boolean z) {
                this.zzaWh = com_google_android_gms_internal_zzqz_zzb;
                this.zzaWf = str;
                this.zzaEC = bArr;
                this.zzaWg = z;
            }

            public void zza(MessageListener messageListener) {
                messageListener.onMessageReceived(this.zzaWf, this.zzaEC, this.zzaWg);
            }

            public void zzpb() {
            }

            public /* synthetic */ void zzs(Object obj) {
                zza((MessageListener) obj);
            }
        }

        /* renamed from: com.google.android.gms.internal.zzqz.zzb.2 */
        class C08162 implements com.google.android.gms.internal.zzmn.zzb<MessageListener> {
            final /* synthetic */ String zzaWf;
            final /* synthetic */ zzb zzaWh;

            C08162(zzb com_google_android_gms_internal_zzqz_zzb, String str) {
                this.zzaWh = com_google_android_gms_internal_zzqz_zzb;
                this.zzaWf = str;
            }

            public void zza(MessageListener messageListener) {
                messageListener.onDisconnected(this.zzaWf);
            }

            public void zzpb() {
            }

            public /* synthetic */ void zzs(Object obj) {
                zza((MessageListener) obj);
            }
        }

        zzb(zzmn<MessageListener> com_google_android_gms_internal_zzmn_com_google_android_gms_nearby_connection_Connections_MessageListener) {
            this.zzaWe = com_google_android_gms_internal_zzmn_com_google_android_gms_nearby_connection_Connections_MessageListener;
        }

        public void onDisconnected(String remoteEndpointId) throws RemoteException {
            this.zzaWe.zza(new C08162(this, remoteEndpointId));
        }

        public void onMessageReceived(String remoteEndpointId, byte[] payload, boolean isReliable) throws RemoteException {
            this.zzaWe.zza(new C08151(this, remoteEndpointId, payload, isReliable));
        }
    }

    private static class zzc extends zzqy {
        private final com.google.android.gms.internal.zzlx.zzb<Status> zzaWi;

        zzc(com.google.android.gms.internal.zzlx.zzb<Status> com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_common_api_Status) {
            this.zzaWi = com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_common_api_Status;
        }

        public void zziO(int i) throws RemoteException {
            this.zzaWi.zzr(new Status(i));
        }
    }

    private static final class zze extends zzqy {
        private final zzmn<ConnectionRequestListener> zzaWl;
        private final com.google.android.gms.internal.zzlx.zzb<StartAdvertisingResult> zzakL;

        /* renamed from: com.google.android.gms.internal.zzqz.zze.1 */
        class C08181 implements com.google.android.gms.internal.zzmn.zzb<ConnectionRequestListener> {
            final /* synthetic */ byte[] zzaEC;
            final /* synthetic */ String zzaWf;
            final /* synthetic */ String zzaWm;
            final /* synthetic */ String zzaWn;
            final /* synthetic */ zze zzaWo;

            C08181(zze com_google_android_gms_internal_zzqz_zze, String str, String str2, String str3, byte[] bArr) {
                this.zzaWo = com_google_android_gms_internal_zzqz_zze;
                this.zzaWf = str;
                this.zzaWm = str2;
                this.zzaWn = str3;
                this.zzaEC = bArr;
            }

            public void zza(ConnectionRequestListener connectionRequestListener) {
                connectionRequestListener.onConnectionRequest(this.zzaWf, this.zzaWm, this.zzaWn, this.zzaEC);
            }

            public void zzpb() {
            }

            public /* synthetic */ void zzs(Object obj) {
                zza((ConnectionRequestListener) obj);
            }
        }

        zze(com.google.android.gms.internal.zzlx.zzb<StartAdvertisingResult> com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_nearby_connection_Connections_StartAdvertisingResult, zzmn<ConnectionRequestListener> com_google_android_gms_internal_zzmn_com_google_android_gms_nearby_connection_Connections_ConnectionRequestListener) {
            this.zzakL = (com.google.android.gms.internal.zzlx.zzb) zzx.zzy(com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_nearby_connection_Connections_StartAdvertisingResult);
            this.zzaWl = (zzmn) zzx.zzy(com_google_android_gms_internal_zzmn_com_google_android_gms_nearby_connection_Connections_ConnectionRequestListener);
        }

        public void onConnectionRequest(String remoteEndpointId, String remoteDeviceId, String remoteEndpointName, byte[] payload) throws RemoteException {
            this.zzaWl.zza(new C08181(this, remoteEndpointId, remoteDeviceId, remoteEndpointName, payload));
        }

        public void zzo(int i, String str) throws RemoteException {
            this.zzakL.zzr(new zzf(new Status(i), str));
        }
    }

    private static final class zzg extends zzqy {
        private final zzmn<EndpointDiscoveryListener> zzaWl;
        private final com.google.android.gms.internal.zzlx.zzb<Status> zzakL;

        /* renamed from: com.google.android.gms.internal.zzqz.zzg.1 */
        class C08191 implements com.google.android.gms.internal.zzmn.zzb<EndpointDiscoveryListener> {
            final /* synthetic */ String val$name;
            final /* synthetic */ String zzaWq;
            final /* synthetic */ String zzaWr;
            final /* synthetic */ String zzaWs;
            final /* synthetic */ zzg zzaWt;

            C08191(zzg com_google_android_gms_internal_zzqz_zzg, String str, String str2, String str3, String str4) {
                this.zzaWt = com_google_android_gms_internal_zzqz_zzg;
                this.zzaWq = str;
                this.zzaWr = str2;
                this.zzaWs = str3;
                this.val$name = str4;
            }

            public void zza(EndpointDiscoveryListener endpointDiscoveryListener) {
                endpointDiscoveryListener.onEndpointFound(this.zzaWq, this.zzaWr, this.zzaWs, this.val$name);
            }

            public void zzpb() {
            }

            public /* synthetic */ void zzs(Object obj) {
                zza((EndpointDiscoveryListener) obj);
            }
        }

        /* renamed from: com.google.android.gms.internal.zzqz.zzg.2 */
        class C08202 implements com.google.android.gms.internal.zzmn.zzb<EndpointDiscoveryListener> {
            final /* synthetic */ String zzaWq;
            final /* synthetic */ zzg zzaWt;

            C08202(zzg com_google_android_gms_internal_zzqz_zzg, String str) {
                this.zzaWt = com_google_android_gms_internal_zzqz_zzg;
                this.zzaWq = str;
            }

            public void zza(EndpointDiscoveryListener endpointDiscoveryListener) {
                endpointDiscoveryListener.onEndpointLost(this.zzaWq);
            }

            public void zzpb() {
            }

            public /* synthetic */ void zzs(Object obj) {
                zza((EndpointDiscoveryListener) obj);
            }
        }

        zzg(com.google.android.gms.internal.zzlx.zzb<Status> com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_common_api_Status, zzmn<EndpointDiscoveryListener> com_google_android_gms_internal_zzmn_com_google_android_gms_nearby_connection_Connections_EndpointDiscoveryListener) {
            this.zzakL = (com.google.android.gms.internal.zzlx.zzb) zzx.zzy(com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_common_api_Status);
            this.zzaWl = (zzmn) zzx.zzy(com_google_android_gms_internal_zzmn_com_google_android_gms_nearby_connection_Connections_EndpointDiscoveryListener);
        }

        public void onEndpointFound(String endpointId, String deviceId, String serviceId, String name) throws RemoteException {
            this.zzaWl.zza(new C08191(this, endpointId, deviceId, serviceId, name));
        }

        public void onEndpointLost(String endpointId) throws RemoteException {
            this.zzaWl.zza(new C08202(this, endpointId));
        }

        public void zziK(int i) throws RemoteException {
            this.zzakL.zzr(new Status(i));
        }
    }

    private static final class zza extends zzb {
        private final com.google.android.gms.internal.zzlx.zzb<Status> zzakL;

        public zza(com.google.android.gms.internal.zzlx.zzb<Status> com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_common_api_Status, zzmn<MessageListener> com_google_android_gms_internal_zzmn_com_google_android_gms_nearby_connection_Connections_MessageListener) {
            super(com_google_android_gms_internal_zzmn_com_google_android_gms_nearby_connection_Connections_MessageListener);
            this.zzakL = (com.google.android.gms.internal.zzlx.zzb) zzx.zzy(com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_common_api_Status);
        }

        public void zziN(int i) throws RemoteException {
            this.zzakL.zzr(new Status(i));
        }
    }

    private static final class zzd extends zzb {
        private final zzmn<ConnectionResponseCallback> zzaWj;
        private final com.google.android.gms.internal.zzlx.zzb<Status> zzakL;

        /* renamed from: com.google.android.gms.internal.zzqz.zzd.1 */
        class C08171 implements com.google.android.gms.internal.zzmn.zzb<ConnectionResponseCallback> {
            final /* synthetic */ byte[] zzaEC;
            final /* synthetic */ String zzaWf;
            final /* synthetic */ zzd zzaWk;
            final /* synthetic */ int zzacp;

            C08171(zzd com_google_android_gms_internal_zzqz_zzd, String str, int i, byte[] bArr) {
                this.zzaWk = com_google_android_gms_internal_zzqz_zzd;
                this.zzaWf = str;
                this.zzacp = i;
                this.zzaEC = bArr;
            }

            public void zza(ConnectionResponseCallback connectionResponseCallback) {
                connectionResponseCallback.onConnectionResponse(this.zzaWf, new Status(this.zzacp), this.zzaEC);
            }

            public void zzpb() {
            }

            public /* synthetic */ void zzs(Object obj) {
                zza((ConnectionResponseCallback) obj);
            }
        }

        public zzd(com.google.android.gms.internal.zzlx.zzb<Status> com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_common_api_Status, zzmn<ConnectionResponseCallback> com_google_android_gms_internal_zzmn_com_google_android_gms_nearby_connection_Connections_ConnectionResponseCallback, zzmn<MessageListener> com_google_android_gms_internal_zzmn_com_google_android_gms_nearby_connection_Connections_MessageListener) {
            super(com_google_android_gms_internal_zzmn_com_google_android_gms_nearby_connection_Connections_MessageListener);
            this.zzakL = (com.google.android.gms.internal.zzlx.zzb) zzx.zzy(com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_common_api_Status);
            this.zzaWj = (zzmn) zzx.zzy(com_google_android_gms_internal_zzmn_com_google_android_gms_nearby_connection_Connections_ConnectionResponseCallback);
        }

        public void zza(String str, int i, byte[] bArr) throws RemoteException {
            this.zzaWj.zza(new C08171(this, str, i, bArr));
        }

        public void zziM(int i) throws RemoteException {
            this.zzakL.zzr(new Status(i));
        }
    }

    public zzqz(Context context, Looper looper, com.google.android.gms.common.internal.zzf com_google_android_gms_common_internal_zzf, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, 54, com_google_android_gms_common_internal_zzf, connectionCallbacks, onConnectionFailedListener);
        this.zzaBC = (long) hashCode();
    }

    public void disconnect() {
        if (isConnected()) {
            try {
                ((zzrc) zzqs()).zzF(this.zzaBC);
            } catch (Throwable e) {
                Log.w("NearbyConnectionsClient", "Failed to notify client disconnect.", e);
            }
        }
        super.disconnect();
    }

    public String zzCs() {
        try {
            return ((zzrc) zzqs()).zzU(this.zzaBC);
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public String zzCt() {
        try {
            return ((zzrc) zzqs()).zzCt();
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public void zzCu() {
        try {
            ((zzrc) zzqs()).zzR(this.zzaBC);
        } catch (Throwable e) {
            Log.w("NearbyConnectionsClient", "Couldn't stop advertising", e);
        }
    }

    public void zzCv() {
        try {
            ((zzrc) zzqs()).zzT(this.zzaBC);
        } catch (Throwable e) {
            Log.w("NearbyConnectionsClient", "Couldn't stop all endpoints", e);
        }
    }

    protected /* synthetic */ IInterface zzW(IBinder iBinder) {
        return zzdq(iBinder);
    }

    public void zza(com.google.android.gms.internal.zzlx.zzb<Status> com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_common_api_Status, String str, long j, zzmn<EndpointDiscoveryListener> com_google_android_gms_internal_zzmn_com_google_android_gms_nearby_connection_Connections_EndpointDiscoveryListener) throws RemoteException {
        ((zzrc) zzqs()).zza(new zzg(com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_common_api_Status, com_google_android_gms_internal_zzmn_com_google_android_gms_nearby_connection_Connections_EndpointDiscoveryListener), str, j, this.zzaBC);
    }

    public void zza(com.google.android.gms.internal.zzlx.zzb<StartAdvertisingResult> com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_nearby_connection_Connections_StartAdvertisingResult, String str, AppMetadata appMetadata, long j, zzmn<ConnectionRequestListener> com_google_android_gms_internal_zzmn_com_google_android_gms_nearby_connection_Connections_ConnectionRequestListener) throws RemoteException {
        ((zzrc) zzqs()).zza(new zze(com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_nearby_connection_Connections_StartAdvertisingResult, com_google_android_gms_internal_zzmn_com_google_android_gms_nearby_connection_Connections_ConnectionRequestListener), str, appMetadata, j, this.zzaBC);
    }

    public void zza(com.google.android.gms.internal.zzlx.zzb<Status> com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_common_api_Status, String str, String str2, byte[] bArr, zzmn<ConnectionResponseCallback> com_google_android_gms_internal_zzmn_com_google_android_gms_nearby_connection_Connections_ConnectionResponseCallback, zzmn<MessageListener> com_google_android_gms_internal_zzmn_com_google_android_gms_nearby_connection_Connections_MessageListener) throws RemoteException {
        ((zzrc) zzqs()).zza(new zzd(com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_common_api_Status, com_google_android_gms_internal_zzmn_com_google_android_gms_nearby_connection_Connections_ConnectionResponseCallback, com_google_android_gms_internal_zzmn_com_google_android_gms_nearby_connection_Connections_MessageListener), str, str2, bArr, this.zzaBC);
    }

    public void zza(com.google.android.gms.internal.zzlx.zzb<Status> com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_common_api_Status, String str, byte[] bArr, zzmn<MessageListener> com_google_android_gms_internal_zzmn_com_google_android_gms_nearby_connection_Connections_MessageListener) throws RemoteException {
        ((zzrc) zzqs()).zza(new zza(com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_common_api_Status, com_google_android_gms_internal_zzmn_com_google_android_gms_nearby_connection_Connections_MessageListener), str, bArr, this.zzaBC);
    }

    public void zza(String[] strArr, byte[] bArr) {
        try {
            ((zzrc) zzqs()).zza(strArr, bArr, this.zzaBC);
        } catch (Throwable e) {
            Log.w("NearbyConnectionsClient", "Couldn't send reliable message", e);
        }
    }

    public void zzb(String[] strArr, byte[] bArr) {
        try {
            ((zzrc) zzqs()).zzb(strArr, bArr, this.zzaBC);
        } catch (Throwable e) {
            Log.w("NearbyConnectionsClient", "Couldn't send unreliable message", e);
        }
    }

    protected zzrc zzdq(IBinder iBinder) {
        return com.google.android.gms.internal.zzrc.zza.zzds(iBinder);
    }

    public void zzeL(String str) {
        try {
            ((zzrc) zzqs()).zzh(str, this.zzaBC);
        } catch (Throwable e) {
            Log.w("NearbyConnectionsClient", "Couldn't stop discovery", e);
        }
    }

    public void zzeM(String str) {
        try {
            ((zzrc) zzqs()).zzi(str, this.zzaBC);
        } catch (Throwable e) {
            Log.w("NearbyConnectionsClient", "Couldn't disconnect from endpoint", e);
        }
    }

    protected String zzgh() {
        return "com.google.android.gms.nearby.connection.service.START";
    }

    protected String zzgi() {
        return "com.google.android.gms.nearby.internal.connection.INearbyConnectionService";
    }

    public void zzp(com.google.android.gms.internal.zzlx.zzb<Status> com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_common_api_Status, String str) throws RemoteException {
        ((zzrc) zzqs()).zza(new zzc(com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_common_api_Status), str, this.zzaBC);
    }
}
