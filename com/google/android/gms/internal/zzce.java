package com.google.android.gms.internal;

import android.text.TextUtils;
import java.util.Map;

@zzha
public abstract class zzce {
    @zzha
    public static final zzce zzwU;
    @zzha
    public static final zzce zzwV;
    @zzha
    public static final zzce zzwW;

    /* renamed from: com.google.android.gms.internal.zzce.1 */
    static class C07441 extends zzce {
        C07441() {
        }

        public String zzc(String str, String str2) {
            return str2;
        }
    }

    /* renamed from: com.google.android.gms.internal.zzce.2 */
    static class C07452 extends zzce {
        C07452() {
        }

        public String zzc(String str, String str2) {
            return str != null ? str : str2;
        }
    }

    /* renamed from: com.google.android.gms.internal.zzce.3 */
    static class C07463 extends zzce {
        C07463() {
        }

        private String zzQ(String str) {
            if (TextUtils.isEmpty(str)) {
                return str;
            }
            int i = 0;
            int length = str.length();
            while (i < str.length() && str.charAt(i) == ',') {
                i++;
            }
            while (length > 0 && str.charAt(length - 1) == ',') {
                length--;
            }
            return (i == 0 && length == str.length()) ? str : str.substring(i, length);
        }

        public String zzc(String str, String str2) {
            String zzQ = zzQ(str);
            String zzQ2 = zzQ(str2);
            return TextUtils.isEmpty(zzQ) ? zzQ2 : TextUtils.isEmpty(zzQ2) ? zzQ : zzQ + "," + zzQ2;
        }
    }

    static {
        zzwU = new C07441();
        zzwV = new C07452();
        zzwW = new C07463();
    }

    public final void zza(Map<String, String> map, String str, String str2) {
        map.put(str, zzc((String) map.get(str), str2));
    }

    public abstract String zzc(String str, String str2);
}
