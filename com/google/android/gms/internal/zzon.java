package com.google.android.gms.internal;

import com.google.android.gms.internal.zzto.zzb;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class zzon {
    public static final Set<String> AGGREGATE_INPUT_TYPES;
    public static final com.google.android.gms.internal.zzto.zza TYPE_DISTANCE_CUMULATIVE;
    public static final com.google.android.gms.internal.zzto.zza TYPE_STEP_COUNT_CUMULATIVE;
    private static final Map<String, List<com.google.android.gms.internal.zzto.zza>> zzaux;
    public static final com.google.android.gms.internal.zzto.zza zzawA;
    public static final com.google.android.gms.internal.zzto.zza zzawB;
    public static final com.google.android.gms.internal.zzto.zza zzawC;
    public static final com.google.android.gms.internal.zzto.zza zzawD;
    public static final com.google.android.gms.internal.zzto.zza zzawE;
    public static final com.google.android.gms.internal.zzto.zza zzawF;
    public static final com.google.android.gms.internal.zzto.zza zzawG;
    public static final com.google.android.gms.internal.zzto.zza zzawH;
    public static final com.google.android.gms.internal.zzto.zza zzawI;
    public static final com.google.android.gms.internal.zzto.zza zzawJ;
    public static final com.google.android.gms.internal.zzto.zza zzawK;
    public static final com.google.android.gms.internal.zzto.zza zzawL;
    public static final com.google.android.gms.internal.zzto.zza zzawM;
    public static final com.google.android.gms.internal.zzto.zza zzawN;
    public static final com.google.android.gms.internal.zzto.zza zzawO;
    public static final com.google.android.gms.internal.zzto.zza zzawP;
    public static final com.google.android.gms.internal.zzto.zza zzawQ;
    public static final com.google.android.gms.internal.zzto.zza zzawR;
    public static final com.google.android.gms.internal.zzto.zza zzawS;
    public static final com.google.android.gms.internal.zzto.zza zzawT;
    public static final com.google.android.gms.internal.zzto.zza zzawU;
    public static final com.google.android.gms.internal.zzto.zza zzawV;
    public static final com.google.android.gms.internal.zzto.zza zzawW;
    public static final com.google.android.gms.internal.zzto.zza zzawX;
    public static final com.google.android.gms.internal.zzto.zza zzawY;
    public static final com.google.android.gms.internal.zzto.zza zzawZ;
    public static final com.google.android.gms.internal.zzto.zza zzawl;
    public static final com.google.android.gms.internal.zzto.zza zzawm;
    public static final com.google.android.gms.internal.zzto.zza zzawn;
    public static final com.google.android.gms.internal.zzto.zza zzawo;
    public static final com.google.android.gms.internal.zzto.zza zzawp;
    public static final com.google.android.gms.internal.zzto.zza zzawq;
    public static final com.google.android.gms.internal.zzto.zza zzawr;
    public static final com.google.android.gms.internal.zzto.zza zzaws;
    public static final com.google.android.gms.internal.zzto.zza zzawt;
    public static final com.google.android.gms.internal.zzto.zza zzawu;
    public static final com.google.android.gms.internal.zzto.zza zzawv;
    public static final com.google.android.gms.internal.zzto.zza zzaww;
    public static final com.google.android.gms.internal.zzto.zza zzawx;
    public static final com.google.android.gms.internal.zzto.zza zzawy;
    public static final com.google.android.gms.internal.zzto.zza zzawz;
    public static final com.google.android.gms.internal.zzto.zza zzaxa;
    public static final com.google.android.gms.internal.zzto.zza zzaxb;
    public static final String[] zzaxc;
    private static final Map<com.google.android.gms.internal.zzto.zza, zza> zzaxd;

    public enum zza {
        CUMULATIVE,
        DELTA,
        SAMPLE,
        OTHER
    }

    static {
        zzawl = zza("com.google.step_count.delta", zzom.zzavq);
        TYPE_STEP_COUNT_CUMULATIVE = zza("com.google.step_count.cumulative", zzom.zzavq);
        zzawm = zza("com.google.step_count.cadence", zzom.zzavG);
        zzawn = zza("com.google.activity.segment", zzom.zzavn);
        zzawo = zza("com.google.floor_change", zzom.zzavn, zzom.zzavo, zzom.zzavN, zzom.zzavQ);
        zzawp = zza("com.google.calories.consumed", zzom.zzavI);
        zzawq = zza("com.google.calories.expended", zzom.zzavI);
        zzawr = zza("com.google.calories.bmr", zzom.zzavI);
        zzaws = zza("com.google.power.sample", zzom.zzavJ);
        zzawt = zza("com.google.activity.sample", zzom.zzavn, zzom.zzavo);
        zzawu = zza("com.google.accelerometer", zzom.zzawf, zzom.zzawg, zzom.zzawh);
        zzawv = zza("com.google.sensor.events", zzom.zzawk, zzom.zzawi, zzom.zzawj);
        zzaww = zza("com.google.heart_rate.bpm", zzom.zzavv);
        zzawx = zza("com.google.location.sample", zzom.zzavw, zzom.zzavx, zzom.zzavy, zzom.zzavz);
        zzawy = zza("com.google.location.track", zzom.zzavw, zzom.zzavx, zzom.zzavy, zzom.zzavz);
        zzawz = zza("com.google.distance.delta", zzom.zzavA);
        TYPE_DISTANCE_CUMULATIVE = zza("com.google.distance.cumulative", zzom.zzavA);
        zzawA = zza("com.google.speed", zzom.zzavF);
        zzawB = zza("com.google.cycling.wheel_revolution.cumulative", zzom.zzavH);
        zzawC = zza("com.google.cycling.wheel_revolution.rpm", zzom.zzavG);
        zzawD = zza("com.google.cycling.pedaling.cumulative", zzom.zzavH);
        zzawE = zza("com.google.cycling.pedaling.cadence", zzom.zzavG);
        zzawF = zza("com.google.height", zzom.zzavB);
        zzawG = zza("com.google.weight", zzom.zzavC);
        zzawH = zza("com.google.body.fat.percentage", zzom.zzavE);
        zzawI = zza("com.google.body.waist.circumference", zzom.zzavD);
        zzawJ = zza("com.google.body.hip.circumference", zzom.zzavD);
        zzawK = zza("com.google.nutrition", zzom.zzavM, zzom.zzavK, zzom.zzavL);
        zzawL = zza("com.google.activity.exercise", zzom.zzavT, zzom.zzavU, zzom.zzavr, zzom.zzavW, zzom.zzavV);
        AGGREGATE_INPUT_TYPES = Collections.unmodifiableSet(new HashSet(Arrays.asList(new String[]{zzawn.name, zzawp.name, zzawq.name, zzawz.name, zzawo.name, zzaww.name, zzawx.name, zzawK.name, zzawA.name, zzawl.name, zzawG.name})));
        zzawM = zza("com.google.activity.summary", zzom.zzavn, zzom.zzavr, zzom.zzavX);
        zzawN = zza("com.google.floor_change.summary", zzom.zzavt, zzom.zzavu, zzom.zzavO, zzom.zzavP, zzom.zzavR, zzom.zzavS);
        zzawO = zzawl;
        zzawP = zzawz;
        zzawQ = zzawp;
        zzawR = zzawq;
        zzawS = zza("com.google.heart_rate.summary", zzom.zzavY, zzom.zzavZ, zzom.zzawa);
        zzawT = zza("com.google.location.bounding_box", zzom.zzawb, zzom.zzawc, zzom.zzawd, zzom.zzawe);
        zzawU = zza("com.google.power.summary", zzom.zzavY, zzom.zzavZ, zzom.zzawa);
        zzawV = zza("com.google.speed.summary", zzom.zzavY, zzom.zzavZ, zzom.zzawa);
        zzawW = zza("com.google.weight.summary", zzom.zzavY, zzom.zzavZ, zzom.zzawa);
        zzawX = zza("com.google.calories.bmr.summary", zzom.zzavY, zzom.zzavZ, zzom.zzawa);
        zzawY = zza("com.google.body.fat.percentage.summary", zzom.zzavY, zzom.zzavZ, zzom.zzawa);
        zzawZ = zza("com.google.body.hip.circumference.summary", zzom.zzavY, zzom.zzavZ, zzom.zzawa);
        zzaxa = zza("com.google.body.waist.circumference.summary", zzom.zzavY, zzom.zzavZ, zzom.zzawa);
        zzaxb = zza("com.google.nutrition.summary", zzom.zzavM, zzom.zzavK);
        zzaux = zzuc();
        zzaxc = new String[]{"com.google.accelerometer", "com.google.activity.exercise", "com.google.activity.sample", "com.google.activity.segment", "com.google.activity.summary", "com.google.body.fat.percentage", "com.google.body.fat.percentage.summary", "com.google.body.hip.circumference", "com.google.body.hip.circumference.summary", "com.google.body.waist.circumference", "com.google.body.waist.circumference.summary", "com.google.calories.bmr", "com.google.calories.bmr.summary", "com.google.calories.consumed", "com.google.calories.expended", "com.google.cycling.pedaling.cadence", "com.google.cycling.pedaling.cumulative", "com.google.cycling.wheel_revolution.cumulative", "com.google.cycling.wheel_revolution.rpm", "com.google.distance.cumulative", "com.google.distance.delta", "com.google.floor_change", "com.google.floor_change.summary", "com.google.heart_rate.bpm", "com.google.heart_rate.summary", "com.google.height", "com.google.location.bounding_box", "com.google.location.sample", "com.google.location.track", "com.google.nutrition", "com.google.nutrition.summary", "com.google.power.sample", "com.google.power.summary", "com.google.sensor.events", "com.google.speed", "com.google.speed.summary", "com.google.step_count.cadence", "com.google.step_count.cumulative", "com.google.step_count.delta", "com.google.weight", "com.google.weight.summary"};
        Collection hashSet = new HashSet();
        hashSet.add(TYPE_STEP_COUNT_CUMULATIVE);
        hashSet.add(TYPE_DISTANCE_CUMULATIVE);
        hashSet.add(zzawD);
        Collection hashSet2 = new HashSet();
        hashSet2.add(zzawz);
        hashSet2.add(zzawl);
        hashSet2.add(zzawq);
        hashSet2.add(zzawp);
        hashSet2.add(zzawo);
        Collection hashSet3 = new HashSet();
        hashSet3.add(zzawH);
        hashSet3.add(zzawJ);
        hashSet3.add(zzawI);
        hashSet3.add(zzawK);
        hashSet3.add(zzawF);
        hashSet3.add(zzawG);
        hashSet3.add(zzaww);
        Map hashMap = new HashMap();
        zza(hashMap, hashSet, zza.CUMULATIVE);
        zza(hashMap, hashSet2, zza.DELTA);
        zza(hashMap, hashSet3, zza.SAMPLE);
        zzaxd = Collections.unmodifiableMap(hashMap);
    }

    public static com.google.android.gms.internal.zzto.zza zza(String str, zzb... com_google_android_gms_internal_zzto_zzbArr) {
        com.google.android.gms.internal.zzto.zza com_google_android_gms_internal_zzto_zza = new com.google.android.gms.internal.zzto.zza();
        com_google_android_gms_internal_zzto_zza.name = str;
        com_google_android_gms_internal_zzto_zza.zzbqj = com_google_android_gms_internal_zzto_zzbArr;
        return com_google_android_gms_internal_zzto_zza;
    }

    private static void zza(Map<com.google.android.gms.internal.zzto.zza, zza> map, Collection<com.google.android.gms.internal.zzto.zza> collection, zza com_google_android_gms_internal_zzon_zza) {
        for (com.google.android.gms.internal.zzto.zza put : collection) {
            map.put(put, com_google_android_gms_internal_zzon_zza);
        }
    }

    public static boolean zzdp(String str) {
        return Arrays.binarySearch(zzaxc, str) >= 0;
    }

    private static Map<String, List<com.google.android.gms.internal.zzto.zza>> zzuc() {
        Map<String, List<com.google.android.gms.internal.zzto.zza>> hashMap = new HashMap();
        hashMap.put(zzawn.name, Collections.singletonList(zzawM));
        hashMap.put(zzawp.name, Collections.singletonList(zzawQ));
        hashMap.put(zzawq.name, Collections.singletonList(zzawR));
        hashMap.put(zzawz.name, Collections.singletonList(zzawP));
        hashMap.put(zzawo.name, Collections.singletonList(zzawN));
        hashMap.put(zzawx.name, Collections.singletonList(zzawT));
        hashMap.put(zzaws.name, Collections.singletonList(zzawU));
        hashMap.put(zzaww.name, Collections.singletonList(zzawS));
        hashMap.put(zzawA.name, Collections.singletonList(zzawV));
        hashMap.put(zzawl.name, Collections.singletonList(zzawO));
        hashMap.put(zzawG.name, Collections.singletonList(zzawW));
        return hashMap;
    }
}
