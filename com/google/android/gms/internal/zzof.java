package com.google.android.gms.internal;

import com.google.android.gms.common.data.BitmapTeleporter;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.DriveSpace;
import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.SearchableMetadataField;
import com.google.android.gms.drive.metadata.SortableMetadataField;
import com.google.android.gms.drive.metadata.internal.AppVisibleCustomProperties;
import com.google.android.gms.drive.metadata.internal.zzi;
import com.google.android.gms.drive.metadata.internal.zzj;
import com.google.android.gms.drive.metadata.internal.zzl;
import com.google.android.gms.drive.metadata.internal.zzn;
import com.google.android.gms.drive.metadata.internal.zzo;
import com.google.android.gms.drive.metadata.internal.zzp;
import com.google.android.gms.plus.PlusShare;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class zzof {
    public static final MetadataField<DriveId> zzaqO;
    public static final MetadataField<String> zzaqP;
    public static final zza zzaqQ;
    public static final MetadataField<String> zzaqR;
    public static final MetadataField<String> zzaqS;
    public static final MetadataField<String> zzaqT;
    public static final MetadataField<Long> zzaqU;
    public static final MetadataField<String> zzaqV;
    public static final MetadataField<Boolean> zzaqW;
    public static final MetadataField<String> zzaqX;
    public static final MetadataField<Boolean> zzaqY;
    public static final MetadataField<Boolean> zzaqZ;
    public static final MetadataField<String> zzarA;
    public static final MetadataField<String> zzarB;
    public static final zze zzarC;
    public static final MetadataField<String> zzarD;
    public static final MetadataField<Boolean> zzarE;
    public static final MetadataField<Boolean> zzara;
    public static final MetadataField<Boolean> zzarb;
    public static final MetadataField<Boolean> zzarc;
    public static final zzb zzard;
    public static final MetadataField<Boolean> zzare;
    public static final MetadataField<Boolean> zzarf;
    public static final MetadataField<Boolean> zzarg;
    public static final MetadataField<Boolean> zzarh;
    public static final MetadataField<Boolean> zzari;
    public static final MetadataField<Boolean> zzarj;
    public static final MetadataField<Boolean> zzark;
    public static final zzc zzarl;
    public static final MetadataField<String> zzarm;
    public static final com.google.android.gms.drive.metadata.zzb<String> zzarn;
    public static final zzp zzaro;
    public static final zzp zzarp;
    public static final zzl zzarq;
    public static final zzd zzarr;
    public static final zzf zzars;
    public static final MetadataField<BitmapTeleporter> zzart;
    public static final zzg zzaru;
    public static final zzh zzarv;
    public static final MetadataField<String> zzarw;
    public static final MetadataField<String> zzarx;
    public static final MetadataField<String> zzary;
    public static final com.google.android.gms.drive.metadata.internal.zzb zzarz;

    /* renamed from: com.google.android.gms.internal.zzof.1 */
    static class C12081 extends com.google.android.gms.drive.metadata.internal.zzb {
        C12081(String str, Collection collection, Collection collection2, int i) {
            super(str, collection, collection2, i);
        }

        protected /* synthetic */ Object zzc(DataHolder dataHolder, int i, int i2) {
            return zze(dataHolder, i, i2);
        }

        protected Boolean zze(DataHolder dataHolder, int i, int i2) {
            return Boolean.valueOf(dataHolder.zzc("trashed", i, i2) == 2);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzof.2 */
    static class C12092 extends zzj<BitmapTeleporter> {
        C12092(String str, Collection collection, Collection collection2, int i) {
            super(str, collection, collection2, i);
        }

        protected /* synthetic */ Object zzc(DataHolder dataHolder, int i, int i2) {
            return zzk(dataHolder, i, i2);
        }

        protected BitmapTeleporter zzk(DataHolder dataHolder, int i, int i2) {
            throw new IllegalStateException("Thumbnail field is write only");
        }
    }

    public static class zzb extends com.google.android.gms.drive.metadata.internal.zzb implements SearchableMetadataField<Boolean> {
        public zzb(String str, int i) {
            super(str, i);
        }
    }

    public static class zzc extends zzo implements SearchableMetadataField<String> {
        public zzc(int i) {
            super("mimeType", i);
        }
    }

    public static class zzd extends com.google.android.gms.drive.metadata.internal.zzg implements SortableMetadataField<Long> {
        public zzd(String str, int i) {
            super(str, i);
        }
    }

    public static class zzf extends com.google.android.gms.drive.metadata.internal.zzb implements SearchableMetadataField<Boolean> {
        public zzf(String str, int i) {
            super(str, i);
        }
    }

    public static class zzg extends zzo implements SearchableMetadataField<String>, SortableMetadataField<String> {
        public zzg(String str, int i) {
            super(str, i);
        }
    }

    public static class zzh extends com.google.android.gms.drive.metadata.internal.zzb implements SearchableMetadataField<Boolean> {
        public zzh(String str, int i) {
            super(str, i);
        }

        protected /* synthetic */ Object zzc(DataHolder dataHolder, int i, int i2) {
            return zze(dataHolder, i, i2);
        }

        protected Boolean zze(DataHolder dataHolder, int i, int i2) {
            return Boolean.valueOf(dataHolder.zzc(getName(), i, i2) != 0);
        }
    }

    public static class zza extends zzog implements SearchableMetadataField<AppVisibleCustomProperties> {
        public zza(int i) {
            super(i);
        }
    }

    public static class zze extends zzi<DriveSpace> {
        public zze(int i) {
            super("spaces", Arrays.asList(new String[]{"inDriveSpace", "isAppData", "inGooglePhotosSpace"}), Collections.emptySet(), i);
        }

        protected /* synthetic */ Object zzc(DataHolder dataHolder, int i, int i2) {
            return zzd(dataHolder, i, i2);
        }

        protected Collection<DriveSpace> zzd(DataHolder dataHolder, int i, int i2) {
            Collection arrayList = new ArrayList();
            if (dataHolder.zze("inDriveSpace", i, i2)) {
                arrayList.add(DriveSpace.zzamW);
            }
            if (dataHolder.zze("isAppData", i, i2)) {
                arrayList.add(DriveSpace.zzamX);
            }
            if (dataHolder.zze("inGooglePhotosSpace", i, i2)) {
                arrayList.add(DriveSpace.zzamY);
            }
            return arrayList;
        }
    }

    static {
        zzaqO = zzoi.zzarM;
        zzaqP = new zzo("alternateLink", 4300000);
        zzaqQ = new zza(5000000);
        zzaqR = new zzo(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION, 4300000);
        zzaqS = new zzo("embedLink", 4300000);
        zzaqT = new zzo("fileExtension", 4300000);
        zzaqU = new com.google.android.gms.drive.metadata.internal.zzg("fileSize", 4300000);
        zzaqV = new zzo("folderColorRgb", 7500000);
        zzaqW = new com.google.android.gms.drive.metadata.internal.zzb("hasThumbnail", 4300000);
        zzaqX = new zzo("indexableText", 4300000);
        zzaqY = new com.google.android.gms.drive.metadata.internal.zzb("isAppData", 4300000);
        zzaqZ = new com.google.android.gms.drive.metadata.internal.zzb("isCopyable", 4300000);
        zzara = new com.google.android.gms.drive.metadata.internal.zzb("isEditable", 4100000);
        zzarb = new C12081("isExplicitlyTrashed", Collections.singleton("trashed"), Collections.emptySet(), 7000000);
        zzarc = new com.google.android.gms.drive.metadata.internal.zzb("isLocalContentUpToDate", 7800000);
        zzard = new zzb("isPinned", 4100000);
        zzare = new com.google.android.gms.drive.metadata.internal.zzb("isOpenable", 7200000);
        zzarf = new com.google.android.gms.drive.metadata.internal.zzb("isRestricted", 4300000);
        zzarg = new com.google.android.gms.drive.metadata.internal.zzb("isShared", 4300000);
        zzarh = new com.google.android.gms.drive.metadata.internal.zzb("isGooglePhotosFolder", 7000000);
        zzari = new com.google.android.gms.drive.metadata.internal.zzb("isGooglePhotosRootFolder", 7000000);
        zzarj = new com.google.android.gms.drive.metadata.internal.zzb("isTrashable", 4400000);
        zzark = new com.google.android.gms.drive.metadata.internal.zzb("isViewed", 4300000);
        zzarl = new zzc(4100000);
        zzarm = new zzo("originalFilename", 4300000);
        zzarn = new zzn("ownerNames", 4300000);
        zzaro = new zzp("lastModifyingUser", 6000000);
        zzarp = new zzp("sharingUser", 6000000);
        zzarq = new zzl(4100000);
        zzarr = new zzd("quotaBytesUsed", 4300000);
        zzars = new zzf("starred", 4100000);
        zzart = new C12092("thumbnail", Collections.emptySet(), Collections.emptySet(), 4400000);
        zzaru = new zzg(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, 4100000);
        zzarv = new zzh("trashed", 4100000);
        zzarw = new zzo("webContentLink", 4300000);
        zzarx = new zzo("webViewLink", 4300000);
        zzary = new zzo("uniqueIdentifier", 5000000);
        zzarz = new com.google.android.gms.drive.metadata.internal.zzb("writersCanShare", 6000000);
        zzarA = new zzo("role", 6000000);
        zzarB = new zzo("md5Checksum", 7000000);
        zzarC = new zze(7000000);
        zzarD = new zzo("recencyReason", 8000000);
        zzarE = new com.google.android.gms.drive.metadata.internal.zzb("subscribed", 8000000);
    }
}
