package com.google.android.gms.internal;

import android.text.TextUtils;
import com.google.android.gms.ads.internal.client.zzl;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.drive.events.CompletionEvent;
import com.google.android.gms.tagmanager.DataLayer;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.Barcode.Phone;
import java.util.HashMap;
import java.util.Map;

@zzha
public abstract class zzdw implements Releasable {
    protected zzjn zzps;

    /* renamed from: com.google.android.gms.internal.zzdw.1 */
    class C03711 implements Runnable {
        final /* synthetic */ String zzzn;
        final /* synthetic */ String zzzo;
        final /* synthetic */ int zzzp;
        final /* synthetic */ int zzzq;
        final /* synthetic */ boolean zzzr;
        final /* synthetic */ zzdw zzzs;

        C03711(zzdw com_google_android_gms_internal_zzdw, String str, String str2, int i, int i2, boolean z) {
            this.zzzs = com_google_android_gms_internal_zzdw;
            this.zzzn = str;
            this.zzzo = str2;
            this.zzzp = i;
            this.zzzq = i2;
            this.zzzr = z;
        }

        public void run() {
            Map hashMap = new HashMap();
            hashMap.put(DataLayer.EVENT_KEY, "precacheProgress");
            hashMap.put("src", this.zzzn);
            hashMap.put("cachedSrc", this.zzzo);
            hashMap.put("bytesLoaded", Integer.toString(this.zzzp));
            hashMap.put("totalBytes", Integer.toString(this.zzzq));
            hashMap.put("cacheReady", this.zzzr ? "1" : "0");
            this.zzzs.zzps.zzb("onPrecacheEvent", hashMap);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzdw.2 */
    class C03722 implements Runnable {
        final /* synthetic */ String zzzn;
        final /* synthetic */ String zzzo;
        final /* synthetic */ int zzzq;
        final /* synthetic */ zzdw zzzs;

        C03722(zzdw com_google_android_gms_internal_zzdw, String str, String str2, int i) {
            this.zzzs = com_google_android_gms_internal_zzdw;
            this.zzzn = str;
            this.zzzo = str2;
            this.zzzq = i;
        }

        public void run() {
            Map hashMap = new HashMap();
            hashMap.put(DataLayer.EVENT_KEY, "precacheComplete");
            hashMap.put("src", this.zzzn);
            hashMap.put("cachedSrc", this.zzzo);
            hashMap.put("totalBytes", Integer.toString(this.zzzq));
            this.zzzs.zzps.zzb("onPrecacheEvent", hashMap);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzdw.3 */
    class C03733 implements Runnable {
        final /* synthetic */ String zzzn;
        final /* synthetic */ String zzzo;
        final /* synthetic */ zzdw zzzs;
        final /* synthetic */ String zzzt;
        final /* synthetic */ String zzzu;

        C03733(zzdw com_google_android_gms_internal_zzdw, String str, String str2, String str3, String str4) {
            this.zzzs = com_google_android_gms_internal_zzdw;
            this.zzzn = str;
            this.zzzo = str2;
            this.zzzt = str3;
            this.zzzu = str4;
        }

        public void run() {
            Map hashMap = new HashMap();
            hashMap.put(DataLayer.EVENT_KEY, "precacheCanceled");
            hashMap.put("src", this.zzzn);
            if (!TextUtils.isEmpty(this.zzzo)) {
                hashMap.put("cachedSrc", this.zzzo);
            }
            hashMap.put("type", this.zzzs.zzab(this.zzzt));
            hashMap.put("reason", this.zzzt);
            if (!TextUtils.isEmpty(this.zzzu)) {
                hashMap.put("message", this.zzzu);
            }
            this.zzzs.zzps.zzb("onPrecacheEvent", hashMap);
        }
    }

    public zzdw(zzjn com_google_android_gms_internal_zzjn) {
        this.zzps = com_google_android_gms_internal_zzjn;
    }

    private String zzab(String str) {
        String str2 = "internal";
        Object obj = -1;
        switch (str.hashCode()) {
            case -1396664534:
                if (str.equals("badUrl")) {
                    obj = 6;
                    break;
                }
                break;
            case -1347010958:
                if (str.equals("inProgress")) {
                    obj = 2;
                    break;
                }
                break;
            case -918817863:
                if (str.equals("downloadTimeout")) {
                    obj = 7;
                    break;
                }
                break;
            case -659376217:
                if (str.equals("contentLengthMissing")) {
                    obj = 3;
                    break;
                }
                break;
            case -642208130:
                if (str.equals("playerFailed")) {
                    obj = 1;
                    break;
                }
                break;
            case -354048396:
                if (str.equals("sizeExceeded")) {
                    obj = 8;
                    break;
                }
                break;
            case -32082395:
                if (str.equals("externalAbort")) {
                    obj = 9;
                    break;
                }
                break;
            case 96784904:
                if (str.equals(MediaRouteProviderProtocol.SERVICE_DATA_ERROR)) {
                    obj = null;
                    break;
                }
                break;
            case 580119100:
                if (str.equals("expireFailed")) {
                    obj = 5;
                    break;
                }
                break;
            case 725497484:
                if (str.equals("noCacheDir")) {
                    obj = 4;
                    break;
                }
                break;
        }
        switch (obj) {
            case Phone.UNKNOWN /*0*/:
            case CompletionEvent.STATUS_FAILURE /*1*/:
            case CompletionEvent.STATUS_CONFLICT /*2*/:
            case CompletionEvent.STATUS_CANCELED /*3*/:
                return "internal";
            case Barcode.PHONE /*4*/:
            case Barcode.PRODUCT /*5*/:
                return "io";
            case Barcode.SMS /*6*/:
            case Barcode.TEXT /*7*/:
                return "network";
            case Barcode.URL /*8*/:
            case Barcode.WIFI /*9*/:
                return "policy";
            default:
                return str2;
        }
    }

    public abstract void abort();

    public void release() {
    }

    public abstract boolean zzZ(String str);

    protected void zza(String str, String str2, int i) {
        zza.zzLE.post(new C03722(this, str, str2, i));
    }

    protected void zza(String str, String str2, int i, int i2, boolean z) {
        zza.zzLE.post(new C03711(this, str, str2, i, i2, z));
    }

    protected void zza(String str, String str2, String str3, String str4) {
        zza.zzLE.post(new C03733(this, str, str2, str3, str4));
    }

    protected String zzaa(String str) {
        return zzl.zzcN().zzaE(str);
    }
}
