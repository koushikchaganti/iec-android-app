package com.google.android.gms.internal;

import android.os.Binder;

public abstract class zzmt<T> {
    private static zza zzahn;
    private static int zzaho;
    private static String zzahp;
    private static final Object zzqf;
    private T zzRi;
    protected final String zzuX;
    protected final T zzuY;

    private interface zza {
        Long getLong(String str, Long l);

        String getString(String str, String str2);

        Boolean zza(String str, Boolean bool);

        Float zzb(String str, Float f);

        Integer zzb(String str, Integer num);
    }

    /* renamed from: com.google.android.gms.internal.zzmt.1 */
    static class C08061 extends zzmt<Boolean> {
        C08061(String str, Boolean bool) {
            super(str, bool);
        }

        protected /* synthetic */ Object zzcn(String str) {
            return zzco(str);
        }

        protected Boolean zzco(String str) {
            return zzmt.zzahn.zza(this.zzuX, (Boolean) this.zzuY);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzmt.2 */
    static class C08072 extends zzmt<Long> {
        C08072(String str, Long l) {
            super(str, l);
        }

        protected /* synthetic */ Object zzcn(String str) {
            return zzcp(str);
        }

        protected Long zzcp(String str) {
            return zzmt.zzahn.getLong(this.zzuX, (Long) this.zzuY);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzmt.3 */
    static class C08083 extends zzmt<Integer> {
        C08083(String str, Integer num) {
            super(str, num);
        }

        protected /* synthetic */ Object zzcn(String str) {
            return zzcq(str);
        }

        protected Integer zzcq(String str) {
            return zzmt.zzahn.zzb(this.zzuX, (Integer) this.zzuY);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzmt.4 */
    static class C08094 extends zzmt<Float> {
        C08094(String str, Float f) {
            super(str, f);
        }

        protected /* synthetic */ Object zzcn(String str) {
            return zzcr(str);
        }

        protected Float zzcr(String str) {
            return zzmt.zzahn.zzb(this.zzuX, (Float) this.zzuY);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzmt.5 */
    static class C08105 extends zzmt<String> {
        C08105(String str, String str2) {
            super(str, str2);
        }

        protected /* synthetic */ Object zzcn(String str) {
            return zzcs(str);
        }

        protected String zzcs(String str) {
            return zzmt.zzahn.getString(this.zzuX, (String) this.zzuY);
        }
    }

    static {
        zzqf = new Object();
        zzahn = null;
        zzaho = 0;
        zzahp = "com.google.android.providers.gsf.permission.READ_GSERVICES";
    }

    protected zzmt(String str, T t) {
        this.zzRi = null;
        this.zzuX = str;
        this.zzuY = t;
    }

    public static boolean isInitialized() {
        return zzahn != null;
    }

    public static zzmt<Float> zza(String str, Float f) {
        return new C08094(str, f);
    }

    public static zzmt<Integer> zza(String str, Integer num) {
        return new C08083(str, num);
    }

    public static zzmt<Long> zza(String str, Long l) {
        return new C08072(str, l);
    }

    public static zzmt<Boolean> zzg(String str, boolean z) {
        return new C08061(str, Boolean.valueOf(z));
    }

    public static int zzpE() {
        return zzaho;
    }

    public static zzmt<String> zzw(String str, String str2) {
        return new C08105(str, str2);
    }

    public final T get() {
        return this.zzRi != null ? this.zzRi : zzcn(this.zzuX);
    }

    protected abstract T zzcn(String str);

    public final T zzpF() {
        long clearCallingIdentity = Binder.clearCallingIdentity();
        try {
            T t = get();
            return t;
        } finally {
            Binder.restoreCallingIdentity(clearCallingIdentity);
        }
    }
}
