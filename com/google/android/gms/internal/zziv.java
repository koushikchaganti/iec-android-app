package com.google.android.gms.internal;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.common.internal.zzx;

@zzha
public class zziv {
    private Handler mHandler;
    private HandlerThread zzLs;
    private int zzLt;
    private final Object zzpK;

    /* renamed from: com.google.android.gms.internal.zziv.1 */
    class C04311 implements Runnable {
        final /* synthetic */ zziv zzLu;

        C04311(zziv com_google_android_gms_internal_zziv) {
            this.zzLu = com_google_android_gms_internal_zziv;
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
            r2 = this;
            r0 = r2.zzLu;
            r1 = r0.zzpK;
            monitor-enter(r1);
            r0 = "Suspending the looper thread";
            com.google.android.gms.ads.internal.util.client.zzb.m10v(r0);	 Catch:{ all -> 0x002a }
        L_0x000c:
            r0 = r2.zzLu;	 Catch:{ all -> 0x002a }
            r0 = r0.zzLt;	 Catch:{ all -> 0x002a }
            if (r0 != 0) goto L_0x002d;
        L_0x0014:
            r0 = r2.zzLu;	 Catch:{ InterruptedException -> 0x0023 }
            r0 = r0.zzpK;	 Catch:{ InterruptedException -> 0x0023 }
            r0.wait();	 Catch:{ InterruptedException -> 0x0023 }
            r0 = "Looper thread resumed";
            com.google.android.gms.ads.internal.util.client.zzb.m10v(r0);	 Catch:{ InterruptedException -> 0x0023 }
            goto L_0x000c;
        L_0x0023:
            r0 = move-exception;
            r0 = "Looper thread interrupted.";
            com.google.android.gms.ads.internal.util.client.zzb.m10v(r0);	 Catch:{ all -> 0x002a }
            goto L_0x000c;
        L_0x002a:
            r0 = move-exception;
            monitor-exit(r1);	 Catch:{ all -> 0x002a }
            throw r0;
        L_0x002d:
            monitor-exit(r1);	 Catch:{ all -> 0x002a }
            return;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zziv.1.run():void");
        }
    }

    public zziv() {
        this.zzLs = null;
        this.mHandler = null;
        this.zzLt = 0;
        this.zzpK = new Object();
    }

    public Looper zzhk() {
        Looper looper;
        synchronized (this.zzpK) {
            if (this.zzLt != 0) {
                zzx.zzb(this.zzLs, (Object) "Invalid state: mHandlerThread should already been initialized.");
            } else if (this.zzLs == null) {
                zzb.m10v("Starting the looper thread.");
                this.zzLs = new HandlerThread("LooperProvider");
                this.zzLs.start();
                this.mHandler = new Handler(this.zzLs.getLooper());
                zzb.m10v("Looper thread started.");
            } else {
                zzb.m10v("Resuming the looper thread");
                this.zzpK.notifyAll();
            }
            this.zzLt++;
            looper = this.zzLs.getLooper();
        }
        return looper;
    }

    public void zzhl() {
        synchronized (this.zzpK) {
            zzx.zzb(this.zzLt > 0, (Object) "Invalid state: release() called more times than expected.");
            int i = this.zzLt - 1;
            this.zzLt = i;
            if (i == 0) {
                this.mHandler.post(new C04311(this));
            }
        }
    }
}
