package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.android.gms.ads.internal.zzp;
import com.google.android.gms.common.GooglePlayServicesUtil;
import java.util.concurrent.Callable;

@zzha
public class zzby {
    private final Object zzpK;
    private boolean zzqh;
    private SharedPreferences zzvc;

    /* renamed from: com.google.android.gms.internal.zzby.1 */
    class C03641 implements Callable<T> {
        final /* synthetic */ zzbv zzvd;
        final /* synthetic */ zzby zzve;

        C03641(zzby com_google_android_gms_internal_zzby, zzbv com_google_android_gms_internal_zzbv) {
            this.zzve = com_google_android_gms_internal_zzby;
            this.zzvd = com_google_android_gms_internal_zzbv;
        }

        public T call() {
            return this.zzvd.zza(this.zzve.zzvc);
        }
    }

    public zzby() {
        this.zzpK = new Object();
        this.zzqh = false;
        this.zzvc = null;
    }

    public void initialize(Context context) {
        synchronized (this.zzpK) {
            if (this.zzqh) {
                return;
            }
            Context remoteContext = GooglePlayServicesUtil.getRemoteContext(context);
            if (remoteContext == null) {
                return;
            }
            this.zzvc = zzp.zzbE().zzw(remoteContext);
            this.zzqh = true;
        }
    }

    public <T> T zzd(zzbv<T> com_google_android_gms_internal_zzbv_T) {
        synchronized (this.zzpK) {
            if (this.zzqh) {
                return zziz.zzb(new C03641(this, com_google_android_gms_internal_zzbv_T));
            }
            T zzdk = com_google_android_gms_internal_zzbv_T.zzdk();
            return zzdk;
        }
    }
}
