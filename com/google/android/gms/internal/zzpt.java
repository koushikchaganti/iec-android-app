package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.BleApi;
import com.google.android.gms.fitness.data.BleDevice;
import com.google.android.gms.fitness.request.BleScanCallback;
import com.google.android.gms.fitness.request.ClaimBleDeviceRequest;
import com.google.android.gms.fitness.request.ListClaimedBleDevicesRequest;
import com.google.android.gms.fitness.request.StartBleScanRequest;
import com.google.android.gms.fitness.request.StopBleScanRequest;
import com.google.android.gms.fitness.request.UnclaimBleDeviceRequest;
import com.google.android.gms.fitness.result.BleDevicesResult;
import com.google.android.gms.internal.zzlx.zzb;

public class zzpt implements BleApi {

    private static class zza extends com.google.android.gms.internal.zzqc.zza {
        private final zzb<BleDevicesResult> zzakL;

        private zza(zzb<BleDevicesResult> com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_fitness_result_BleDevicesResult) {
            this.zzakL = com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_fitness_result_BleDevicesResult;
        }

        public void zza(BleDevicesResult bleDevicesResult) {
            this.zzakL.zzr(bleDevicesResult);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzpt.6 */
    class C12306 extends zza<BleDevicesResult> {
        final /* synthetic */ zzpt zzaxu;

        C12306(zzpt com_google_android_gms_internal_zzpt, GoogleApiClient googleApiClient) {
            this.zzaxu = com_google_android_gms_internal_zzpt;
            super(googleApiClient);
        }

        protected BleDevicesResult zzJ(Status status) {
            return BleDevicesResult.zzR(status);
        }

        protected void zza(zzos com_google_android_gms_internal_zzos) throws RemoteException {
            ((zzpd) com_google_android_gms_internal_zzos.zzqs()).zza(new ListClaimedBleDevicesRequest(new zza(null)));
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzJ(status);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzpt.1 */
    class C13881 extends zzc {
        final /* synthetic */ StartBleScanRequest zzaxt;
        final /* synthetic */ zzpt zzaxu;

        C13881(zzpt com_google_android_gms_internal_zzpt, GoogleApiClient googleApiClient, StartBleScanRequest startBleScanRequest) {
            this.zzaxu = com_google_android_gms_internal_zzpt;
            this.zzaxt = startBleScanRequest;
            super(googleApiClient);
        }

        protected void zza(zzos com_google_android_gms_internal_zzos) throws RemoteException {
            ((zzpd) com_google_android_gms_internal_zzos.zzqs()).zza(new StartBleScanRequest(this.zzaxt, new zzqa(this)));
        }
    }

    /* renamed from: com.google.android.gms.internal.zzpt.2 */
    class C13892 extends zzc {
        final /* synthetic */ zzpt zzaxu;
        final /* synthetic */ BleScanCallback zzaxv;

        C13892(zzpt com_google_android_gms_internal_zzpt, GoogleApiClient googleApiClient, BleScanCallback bleScanCallback) {
            this.zzaxu = com_google_android_gms_internal_zzpt;
            this.zzaxv = bleScanCallback;
            super(googleApiClient);
        }

        protected void zza(zzos com_google_android_gms_internal_zzos) throws RemoteException {
            ((zzpd) com_google_android_gms_internal_zzos.zzqs()).zza(new StopBleScanRequest(this.zzaxv, new zzqa(this)));
        }
    }

    /* renamed from: com.google.android.gms.internal.zzpt.3 */
    class C13903 extends zzc {
        final /* synthetic */ zzpt zzaxu;
        final /* synthetic */ String zzaxw;

        C13903(zzpt com_google_android_gms_internal_zzpt, GoogleApiClient googleApiClient, String str) {
            this.zzaxu = com_google_android_gms_internal_zzpt;
            this.zzaxw = str;
            super(googleApiClient);
        }

        protected void zza(zzos com_google_android_gms_internal_zzos) throws RemoteException {
            ((zzpd) com_google_android_gms_internal_zzos.zzqs()).zza(new ClaimBleDeviceRequest(this.zzaxw, null, new zzqa(this)));
        }
    }

    /* renamed from: com.google.android.gms.internal.zzpt.4 */
    class C13914 extends zzc {
        final /* synthetic */ zzpt zzaxu;
        final /* synthetic */ BleDevice zzaxx;

        C13914(zzpt com_google_android_gms_internal_zzpt, GoogleApiClient googleApiClient, BleDevice bleDevice) {
            this.zzaxu = com_google_android_gms_internal_zzpt;
            this.zzaxx = bleDevice;
            super(googleApiClient);
        }

        protected void zza(zzos com_google_android_gms_internal_zzos) throws RemoteException {
            ((zzpd) com_google_android_gms_internal_zzos.zzqs()).zza(new ClaimBleDeviceRequest(this.zzaxx.getAddress(), this.zzaxx, new zzqa(this)));
        }
    }

    /* renamed from: com.google.android.gms.internal.zzpt.5 */
    class C13925 extends zzc {
        final /* synthetic */ zzpt zzaxu;
        final /* synthetic */ String zzaxw;

        C13925(zzpt com_google_android_gms_internal_zzpt, GoogleApiClient googleApiClient, String str) {
            this.zzaxu = com_google_android_gms_internal_zzpt;
            this.zzaxw = str;
            super(googleApiClient);
        }

        protected void zza(zzos com_google_android_gms_internal_zzos) throws RemoteException {
            ((zzpd) com_google_android_gms_internal_zzos.zzqs()).zza(new UnclaimBleDeviceRequest(this.zzaxw, new zzqa(this)));
        }
    }

    public PendingResult<Status> claimBleDevice(GoogleApiClient client, BleDevice bleDevice) {
        return client.zzb(new C13914(this, client, bleDevice));
    }

    public PendingResult<Status> claimBleDevice(GoogleApiClient client, String deviceAddress) {
        return client.zzb(new C13903(this, client, deviceAddress));
    }

    public PendingResult<BleDevicesResult> listClaimedBleDevices(GoogleApiClient client) {
        return client.zza(new C12306(this, client));
    }

    public PendingResult<Status> startBleScan(GoogleApiClient client, StartBleScanRequest request) {
        return client.zza(new C13881(this, client, request));
    }

    public PendingResult<Status> stopBleScan(GoogleApiClient client, BleScanCallback requestCallback) {
        return client.zza(new C13892(this, client, requestCallback));
    }

    public PendingResult<Status> unclaimBleDevice(GoogleApiClient client, BleDevice bleDevice) {
        return unclaimBleDevice(client, bleDevice.getAddress());
    }

    public PendingResult<Status> unclaimBleDevice(GoogleApiClient client, String deviceAddress) {
        return client.zzb(new C13925(this, client, deviceAddress));
    }
}
