package com.google.android.gms.internal;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.wallet.FullWalletRequest;
import com.google.android.gms.wallet.MaskedWalletRequest;
import com.google.android.gms.wallet.NotifyTransactionStatusRequest;
import com.google.android.gms.wallet.Payments;
import com.google.android.gms.wallet.Wallet.zzb;

public class zzst implements Payments {

    /* renamed from: com.google.android.gms.internal.zzst.1 */
    class C14241 extends zzb {
        final /* synthetic */ int zzaJM;
        final /* synthetic */ zzst zzbln;

        C14241(zzst com_google_android_gms_internal_zzst, GoogleApiClient googleApiClient, int i) {
            this.zzbln = com_google_android_gms_internal_zzst;
            this.zzaJM = i;
            super(googleApiClient);
        }

        protected void zza(zzsu com_google_android_gms_internal_zzsu) {
            com_google_android_gms_internal_zzsu.zzkV(this.zzaJM);
            zzb(Status.zzaeX);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzst.2 */
    class C14252 extends zzb {
        final /* synthetic */ int zzaJM;
        final /* synthetic */ zzst zzbln;
        final /* synthetic */ MaskedWalletRequest zzblo;

        C14252(zzst com_google_android_gms_internal_zzst, GoogleApiClient googleApiClient, MaskedWalletRequest maskedWalletRequest, int i) {
            this.zzbln = com_google_android_gms_internal_zzst;
            this.zzblo = maskedWalletRequest;
            this.zzaJM = i;
            super(googleApiClient);
        }

        protected void zza(zzsu com_google_android_gms_internal_zzsu) {
            com_google_android_gms_internal_zzsu.zza(this.zzblo, this.zzaJM);
            zzb(Status.zzaeX);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzst.3 */
    class C14263 extends zzb {
        final /* synthetic */ int zzaJM;
        final /* synthetic */ zzst zzbln;
        final /* synthetic */ FullWalletRequest zzblp;

        C14263(zzst com_google_android_gms_internal_zzst, GoogleApiClient googleApiClient, FullWalletRequest fullWalletRequest, int i) {
            this.zzbln = com_google_android_gms_internal_zzst;
            this.zzblp = fullWalletRequest;
            this.zzaJM = i;
            super(googleApiClient);
        }

        protected void zza(zzsu com_google_android_gms_internal_zzsu) {
            com_google_android_gms_internal_zzsu.zza(this.zzblp, this.zzaJM);
            zzb(Status.zzaeX);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzst.4 */
    class C14274 extends zzb {
        final /* synthetic */ int zzaJM;
        final /* synthetic */ zzst zzbln;
        final /* synthetic */ String zzblq;
        final /* synthetic */ String zzblr;

        C14274(zzst com_google_android_gms_internal_zzst, GoogleApiClient googleApiClient, String str, String str2, int i) {
            this.zzbln = com_google_android_gms_internal_zzst;
            this.zzblq = str;
            this.zzblr = str2;
            this.zzaJM = i;
            super(googleApiClient);
        }

        protected void zza(zzsu com_google_android_gms_internal_zzsu) {
            com_google_android_gms_internal_zzsu.zzf(this.zzblq, this.zzblr, this.zzaJM);
            zzb(Status.zzaeX);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzst.5 */
    class C14285 extends zzb {
        final /* synthetic */ zzst zzbln;
        final /* synthetic */ NotifyTransactionStatusRequest zzbls;

        C14285(zzst com_google_android_gms_internal_zzst, GoogleApiClient googleApiClient, NotifyTransactionStatusRequest notifyTransactionStatusRequest) {
            this.zzbln = com_google_android_gms_internal_zzst;
            this.zzbls = notifyTransactionStatusRequest;
            super(googleApiClient);
        }

        protected void zza(zzsu com_google_android_gms_internal_zzsu) {
            com_google_android_gms_internal_zzsu.zza(this.zzbls);
            zzb(Status.zzaeX);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzst.6 */
    class C14296 extends zzb {
        final /* synthetic */ int zzaJM;
        final /* synthetic */ zzst zzbln;

        C14296(zzst com_google_android_gms_internal_zzst, GoogleApiClient googleApiClient, int i) {
            this.zzbln = com_google_android_gms_internal_zzst;
            this.zzaJM = i;
            super(googleApiClient);
        }

        protected void zza(zzsu com_google_android_gms_internal_zzsu) {
            com_google_android_gms_internal_zzsu.zzkW(this.zzaJM);
            zzb(Status.zzaeX);
        }
    }

    public void changeMaskedWallet(GoogleApiClient googleApiClient, String googleTransactionId, String merchantTransactionId, int requestCode) {
        googleApiClient.zza(new C14274(this, googleApiClient, googleTransactionId, merchantTransactionId, requestCode));
    }

    public void checkForPreAuthorization(GoogleApiClient googleApiClient, int requestCode) {
        googleApiClient.zza(new C14241(this, googleApiClient, requestCode));
    }

    public void isNewUser(GoogleApiClient googleApiClient, int requestCode) {
        googleApiClient.zza(new C14296(this, googleApiClient, requestCode));
    }

    public void loadFullWallet(GoogleApiClient googleApiClient, FullWalletRequest request, int requestCode) {
        googleApiClient.zza(new C14263(this, googleApiClient, request, requestCode));
    }

    public void loadMaskedWallet(GoogleApiClient googleApiClient, MaskedWalletRequest request, int requestCode) {
        googleApiClient.zza(new C14252(this, googleApiClient, request, requestCode));
    }

    public void notifyTransactionStatus(GoogleApiClient googleApiClient, NotifyTransactionStatusRequest request) {
        googleApiClient.zza(new C14285(this, googleApiClient, request));
    }
}
