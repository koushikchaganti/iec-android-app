package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;

@zzha
public class zzjd {

    /* renamed from: com.google.android.gms.internal.zzjd.1 */
    static class C04321 implements Runnable {
        final /* synthetic */ zzjb zzLL;
        final /* synthetic */ zza zzLM;
        final /* synthetic */ zzje zzLN;

        C04321(zzjb com_google_android_gms_internal_zzjb, zza com_google_android_gms_internal_zzjd_zza, zzje com_google_android_gms_internal_zzje) {
            this.zzLL = com_google_android_gms_internal_zzjb;
            this.zzLM = com_google_android_gms_internal_zzjd_zza;
            this.zzLN = com_google_android_gms_internal_zzje;
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
            r3 = this;
            r0 = r3.zzLL;	 Catch:{ CancellationException -> 0x001c, InterruptedException -> 0x001a, ExecutionException -> 0x0012 }
            r1 = r3.zzLM;	 Catch:{ CancellationException -> 0x001c, InterruptedException -> 0x001a, ExecutionException -> 0x0012 }
            r2 = r3.zzLN;	 Catch:{ CancellationException -> 0x001c, InterruptedException -> 0x001a, ExecutionException -> 0x0012 }
            r2 = r2.get();	 Catch:{ CancellationException -> 0x001c, InterruptedException -> 0x001a, ExecutionException -> 0x0012 }
            r1 = r1.zze(r2);	 Catch:{ CancellationException -> 0x001c, InterruptedException -> 0x001a, ExecutionException -> 0x0012 }
            r0.zzf(r1);	 Catch:{ CancellationException -> 0x001c, InterruptedException -> 0x001a, ExecutionException -> 0x0012 }
        L_0x0011:
            return;
        L_0x0012:
            r0 = move-exception;
        L_0x0013:
            r0 = r3.zzLL;
            r1 = 1;
            r0.cancel(r1);
            goto L_0x0011;
        L_0x001a:
            r0 = move-exception;
            goto L_0x0013;
        L_0x001c:
            r0 = move-exception;
            goto L_0x0013;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzjd.1.run():void");
        }
    }

    /* renamed from: com.google.android.gms.internal.zzjd.2 */
    static class C04332 implements Runnable {
        final /* synthetic */ AtomicInteger zzLO;
        final /* synthetic */ int zzLP;
        final /* synthetic */ zzjb zzLQ;
        final /* synthetic */ List zzLR;

        C04332(AtomicInteger atomicInteger, int i, zzjb com_google_android_gms_internal_zzjb, List list) {
            this.zzLO = atomicInteger;
            this.zzLP = i;
            this.zzLQ = com_google_android_gms_internal_zzjb;
            this.zzLR = list;
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
            r2 = this;
            r0 = r2.zzLO;
            r0 = r0.incrementAndGet();
            r1 = r2.zzLP;
            if (r0 < r1) goto L_0x0015;
        L_0x000a:
            r0 = r2.zzLQ;	 Catch:{ ExecutionException -> 0x001d, InterruptedException -> 0x0016 }
            r1 = r2.zzLR;	 Catch:{ ExecutionException -> 0x001d, InterruptedException -> 0x0016 }
            r1 = com.google.android.gms.internal.zzjd.zzk(r1);	 Catch:{ ExecutionException -> 0x001d, InterruptedException -> 0x0016 }
            r0.zzf(r1);	 Catch:{ ExecutionException -> 0x001d, InterruptedException -> 0x0016 }
        L_0x0015:
            return;
        L_0x0016:
            r0 = move-exception;
        L_0x0017:
            r1 = "Unable to convert list of futures to a future of list";
            com.google.android.gms.ads.internal.util.client.zzb.zzd(r1, r0);
            goto L_0x0015;
        L_0x001d:
            r0 = move-exception;
            goto L_0x0017;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzjd.2.run():void");
        }
    }

    public interface zza<D, R> {
        R zze(D d);
    }

    public static <A, B> zzje<B> zza(zzje<A> com_google_android_gms_internal_zzje_A, zza<A, B> com_google_android_gms_internal_zzjd_zza_A__B) {
        zzje com_google_android_gms_internal_zzjb = new zzjb();
        com_google_android_gms_internal_zzje_A.zzb(new C04321(com_google_android_gms_internal_zzjb, com_google_android_gms_internal_zzjd_zza_A__B, com_google_android_gms_internal_zzje_A));
        return com_google_android_gms_internal_zzjb;
    }

    public static <V> zzje<List<V>> zzj(List<zzje<V>> list) {
        zzje com_google_android_gms_internal_zzjb = new zzjb();
        int size = list.size();
        AtomicInteger atomicInteger = new AtomicInteger(0);
        for (zzje zzb : list) {
            zzb.zzb(new C04332(atomicInteger, size, com_google_android_gms_internal_zzjb, list));
        }
        return com_google_android_gms_internal_zzjb;
    }

    private static <V> List<V> zzk(List<zzje<V>> list) throws ExecutionException, InterruptedException {
        List<V> arrayList = new ArrayList();
        for (zzje com_google_android_gms_internal_zzje : list) {
            Object obj = com_google_android_gms_internal_zzje.get();
            if (obj != null) {
                arrayList.add(obj);
            }
        }
        return arrayList;
    }
}
