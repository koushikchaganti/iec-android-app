package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzw;
import java.util.ArrayList;
import java.util.List;

@zzha
public class zzit {
    private final String[] zzKZ;
    private final double[] zzLa;
    private final double[] zzLb;
    private final int[] zzLc;
    private int zzLd;

    public static class zza {
        public final int count;
        public final String name;
        public final double zzLe;
        public final double zzLf;
        public final double zzLg;

        public zza(String str, double d, double d2, double d3, int i) {
            this.name = str;
            this.zzLf = d;
            this.zzLe = d2;
            this.zzLg = d3;
            this.count = i;
        }

        public boolean equals(Object other) {
            if (!(other instanceof zza)) {
                return false;
            }
            zza com_google_android_gms_internal_zzit_zza = (zza) other;
            return zzw.equal(this.name, com_google_android_gms_internal_zzit_zza.name) && this.zzLe == com_google_android_gms_internal_zzit_zza.zzLe && this.zzLf == com_google_android_gms_internal_zzit_zza.zzLf && this.count == com_google_android_gms_internal_zzit_zza.count && Double.compare(this.zzLg, com_google_android_gms_internal_zzit_zza.zzLg) == 0;
        }

        public int hashCode() {
            return zzw.hashCode(this.name, Double.valueOf(this.zzLe), Double.valueOf(this.zzLf), Double.valueOf(this.zzLg), Integer.valueOf(this.count));
        }

        public String toString() {
            return zzw.zzx(this).zzg("name", this.name).zzg("minBound", Double.valueOf(this.zzLf)).zzg("maxBound", Double.valueOf(this.zzLe)).zzg("percent", Double.valueOf(this.zzLg)).zzg("count", Integer.valueOf(this.count)).toString();
        }
    }

    public static class zzb {
        private final List<String> zzLh;
        private final List<Double> zzLi;
        private final List<Double> zzLj;

        public zzb() {
            this.zzLh = new ArrayList();
            this.zzLi = new ArrayList();
            this.zzLj = new ArrayList();
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public com.google.android.gms.internal.zzit.zzb zza(java.lang.String r7, double r8, double r10) {
            /*
            r6 = this;
            r0 = 0;
            r1 = r0;
        L_0x0002:
            r0 = r6.zzLh;
            r0 = r0.size();
            if (r1 >= r0) goto L_0x0026;
        L_0x000a:
            r0 = r6.zzLj;
            r0 = r0.get(r1);
            r0 = (java.lang.Double) r0;
            r2 = r0.doubleValue();
            r0 = r6.zzLi;
            r0 = r0.get(r1);
            r0 = (java.lang.Double) r0;
            r4 = r0.doubleValue();
            r0 = (r8 > r2 ? 1 : (r8 == r2 ? 0 : -1));
            if (r0 >= 0) goto L_0x003e;
        L_0x0026:
            r0 = r6.zzLh;
            r0.add(r1, r7);
            r0 = r6.zzLj;
            r2 = java.lang.Double.valueOf(r8);
            r0.add(r1, r2);
            r0 = r6.zzLi;
            r2 = java.lang.Double.valueOf(r10);
            r0.add(r1, r2);
            return r6;
        L_0x003e:
            r0 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1));
            if (r0 != 0) goto L_0x0046;
        L_0x0042:
            r0 = (r10 > r4 ? 1 : (r10 == r4 ? 0 : -1));
            if (r0 < 0) goto L_0x0026;
        L_0x0046:
            r0 = r1 + 1;
            r1 = r0;
            goto L_0x0002;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzit.zzb.zza(java.lang.String, double, double):com.google.android.gms.internal.zzit$zzb");
        }

        public zzit zzhi() {
            return new zzit();
        }
    }

    private zzit(zzb com_google_android_gms_internal_zzit_zzb) {
        int size = com_google_android_gms_internal_zzit_zzb.zzLi.size();
        this.zzKZ = (String[]) com_google_android_gms_internal_zzit_zzb.zzLh.toArray(new String[size]);
        this.zzLa = zzi(com_google_android_gms_internal_zzit_zzb.zzLi);
        this.zzLb = zzi(com_google_android_gms_internal_zzit_zzb.zzLj);
        this.zzLc = new int[size];
        this.zzLd = 0;
    }

    private double[] zzi(List<Double> list) {
        double[] dArr = new double[list.size()];
        for (int i = 0; i < dArr.length; i++) {
            dArr[i] = ((Double) list.get(i)).doubleValue();
        }
        return dArr;
    }

    public List<zza> getBuckets() {
        List<zza> arrayList = new ArrayList(this.zzKZ.length);
        for (int i = 0; i < this.zzKZ.length; i++) {
            arrayList.add(new zza(this.zzKZ[i], this.zzLb[i], this.zzLa[i], ((double) this.zzLc[i]) / ((double) this.zzLd), this.zzLc[i]));
        }
        return arrayList;
    }

    public void zza(double d) {
        this.zzLd++;
        int i = 0;
        while (i < this.zzLb.length) {
            if (this.zzLb[i] <= d && d < this.zzLa[i]) {
                int[] iArr = this.zzLc;
                iArr[i] = iArr[i] + 1;
            }
            if (d >= this.zzLb[i]) {
                i++;
            } else {
                return;
            }
        }
    }
}
