package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.zzlx.zzb;

public final class zznb implements zzna {

    private static class zza extends zzmy {
        private final zzb<Status> zzakL;

        public zza(zzb<Status> com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_common_api_Status) {
            this.zzakL = com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_common_api_Status;
        }

        public void zzcd(int i) throws RemoteException {
            this.zzakL.zzr(new Status(i));
        }
    }

    /* renamed from: com.google.android.gms.internal.zznb.1 */
    class C13871 extends zza {
        final /* synthetic */ zznb zzakK;

        C13871(zznb com_google_android_gms_internal_zznb, GoogleApiClient googleApiClient) {
            this.zzakK = com_google_android_gms_internal_zznb;
            super(googleApiClient);
        }

        protected void zza(zznd com_google_android_gms_internal_zznd) throws RemoteException {
            ((zznf) com_google_android_gms_internal_zznd.zzqs()).zza(new zza(this));
        }
    }

    public PendingResult<Status> zze(GoogleApiClient googleApiClient) {
        return googleApiClient.zzb(new C13871(this, googleApiClient));
    }
}
