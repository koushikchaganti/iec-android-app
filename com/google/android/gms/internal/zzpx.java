package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.RecordingApi;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Subscription;
import com.google.android.gms.fitness.request.ListSubscriptionsRequest;
import com.google.android.gms.fitness.request.SubscribeRequest;
import com.google.android.gms.fitness.request.UnsubscribeRequest;
import com.google.android.gms.fitness.result.ListSubscriptionsResult;
import com.google.android.gms.internal.zzlx.zzb;

public class zzpx implements RecordingApi {

    private static class zza extends com.google.android.gms.internal.zzpk.zza {
        private final zzb<ListSubscriptionsResult> zzakL;

        private zza(zzb<ListSubscriptionsResult> com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_fitness_result_ListSubscriptionsResult) {
            this.zzakL = com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_fitness_result_ListSubscriptionsResult;
        }

        public void zza(ListSubscriptionsResult listSubscriptionsResult) {
            this.zzakL.zzr(listSubscriptionsResult);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzpx.1 */
    class C12351 extends zza<ListSubscriptionsResult> {
        final /* synthetic */ zzpx zzaxK;

        C12351(zzpx com_google_android_gms_internal_zzpx, GoogleApiClient googleApiClient) {
            this.zzaxK = com_google_android_gms_internal_zzpx;
            super(googleApiClient);
        }

        protected ListSubscriptionsResult zzN(Status status) {
            return ListSubscriptionsResult.zzU(status);
        }

        protected void zza(zzow com_google_android_gms_internal_zzow) throws RemoteException {
            ((zzph) com_google_android_gms_internal_zzow.zzqs()).zza(new ListSubscriptionsRequest(null, new zza(null)));
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzN(status);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzpx.2 */
    class C12362 extends zza<ListSubscriptionsResult> {
        final /* synthetic */ DataType zzaxG;
        final /* synthetic */ zzpx zzaxK;

        C12362(zzpx com_google_android_gms_internal_zzpx, GoogleApiClient googleApiClient, DataType dataType) {
            this.zzaxK = com_google_android_gms_internal_zzpx;
            this.zzaxG = dataType;
            super(googleApiClient);
        }

        protected ListSubscriptionsResult zzN(Status status) {
            return ListSubscriptionsResult.zzU(status);
        }

        protected void zza(zzow com_google_android_gms_internal_zzow) throws RemoteException {
            ((zzph) com_google_android_gms_internal_zzow.zzqs()).zza(new ListSubscriptionsRequest(this.zzaxG, new zza(null)));
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzN(status);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzpx.3 */
    class C13963 extends zzc {
        final /* synthetic */ zzpx zzaxK;
        final /* synthetic */ Subscription zzaxL;

        C13963(zzpx com_google_android_gms_internal_zzpx, GoogleApiClient googleApiClient, Subscription subscription) {
            this.zzaxK = com_google_android_gms_internal_zzpx;
            this.zzaxL = subscription;
            super(googleApiClient);
        }

        protected void zza(zzow com_google_android_gms_internal_zzow) throws RemoteException {
            ((zzph) com_google_android_gms_internal_zzow.zzqs()).zza(new SubscribeRequest(this.zzaxL, false, new zzqa(this)));
        }
    }

    /* renamed from: com.google.android.gms.internal.zzpx.4 */
    class C13974 extends zzc {
        final /* synthetic */ DataType zzaxG;
        final /* synthetic */ zzpx zzaxK;

        C13974(zzpx com_google_android_gms_internal_zzpx, GoogleApiClient googleApiClient, DataType dataType) {
            this.zzaxK = com_google_android_gms_internal_zzpx;
            this.zzaxG = dataType;
            super(googleApiClient);
        }

        protected void zza(zzow com_google_android_gms_internal_zzow) throws RemoteException {
            ((zzph) com_google_android_gms_internal_zzow.zzqs()).zza(new UnsubscribeRequest(this.zzaxG, null, new zzqa(this)));
        }
    }

    /* renamed from: com.google.android.gms.internal.zzpx.5 */
    class C13985 extends zzc {
        final /* synthetic */ zzpx zzaxK;
        final /* synthetic */ DataSource zzaxM;

        C13985(zzpx com_google_android_gms_internal_zzpx, GoogleApiClient googleApiClient, DataSource dataSource) {
            this.zzaxK = com_google_android_gms_internal_zzpx;
            this.zzaxM = dataSource;
            super(googleApiClient);
        }

        protected void zza(zzow com_google_android_gms_internal_zzow) throws RemoteException {
            ((zzph) com_google_android_gms_internal_zzow.zzqs()).zza(new UnsubscribeRequest(null, this.zzaxM, new zzqa(this)));
        }
    }

    private PendingResult<Status> zza(GoogleApiClient googleApiClient, Subscription subscription) {
        return googleApiClient.zza(new C13963(this, googleApiClient, subscription));
    }

    public PendingResult<ListSubscriptionsResult> listSubscriptions(GoogleApiClient client) {
        return client.zza(new C12351(this, client));
    }

    public PendingResult<ListSubscriptionsResult> listSubscriptions(GoogleApiClient client, DataType dataType) {
        return client.zza(new C12362(this, client, dataType));
    }

    public PendingResult<Status> subscribe(GoogleApiClient client, DataSource dataSource) {
        return zza(client, new com.google.android.gms.fitness.data.Subscription.zza().zzb(dataSource).zztW());
    }

    public PendingResult<Status> subscribe(GoogleApiClient client, DataType dataType) {
        return zza(client, new com.google.android.gms.fitness.data.Subscription.zza().zzb(dataType).zztW());
    }

    public PendingResult<Status> unsubscribe(GoogleApiClient client, DataSource dataSource) {
        return client.zzb(new C13985(this, client, dataSource));
    }

    public PendingResult<Status> unsubscribe(GoogleApiClient client, DataType dataType) {
        return client.zzb(new C13974(this, client, dataType));
    }

    public PendingResult<Status> unsubscribe(GoogleApiClient client, Subscription subscription) {
        return subscription.getDataType() == null ? unsubscribe(client, subscription.getDataSource()) : unsubscribe(client, subscription.getDataType());
    }
}
