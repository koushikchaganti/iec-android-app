package com.google.android.gms.internal;

import com.google.android.gms.internal.zzey.zza;

@zzha
public final class zzep extends zza {
    private zzer.zza zzBb;
    private zzeo zzBc;
    private final Object zzpK;

    public zzep() {
        this.zzpK = new Object();
    }

    public void onAdClicked() {
        synchronized (this.zzpK) {
            if (this.zzBc != null) {
                this.zzBc.zzaZ();
            }
        }
    }

    public void onAdClosed() {
        synchronized (this.zzpK) {
            if (this.zzBc != null) {
                this.zzBc.zzba();
            }
        }
    }

    public void onAdFailedToLoad(int error) {
        synchronized (this.zzpK) {
            if (this.zzBb != null) {
                this.zzBb.zzr(error == 3 ? 1 : 2);
                this.zzBb = null;
            }
        }
    }

    public void onAdLeftApplication() {
        synchronized (this.zzpK) {
            if (this.zzBc != null) {
                this.zzBc.zzbb();
            }
        }
    }

    public void onAdLoaded() {
        synchronized (this.zzpK) {
            if (this.zzBb != null) {
                this.zzBb.zzr(0);
                this.zzBb = null;
                return;
            }
            if (this.zzBc != null) {
                this.zzBc.zzbd();
            }
        }
    }

    public void onAdOpened() {
        synchronized (this.zzpK) {
            if (this.zzBc != null) {
                this.zzBc.zzbc();
            }
        }
    }

    public void zza(zzeo com_google_android_gms_internal_zzeo) {
        synchronized (this.zzpK) {
            this.zzBc = com_google_android_gms_internal_zzeo;
        }
    }

    public void zza(zzer.zza com_google_android_gms_internal_zzer_zza) {
        synchronized (this.zzpK) {
            this.zzBb = com_google_android_gms_internal_zzer_zza;
        }
    }

    public void zza(zzez com_google_android_gms_internal_zzez) {
        synchronized (this.zzpK) {
            if (this.zzBb != null) {
                this.zzBb.zza(0, com_google_android_gms_internal_zzez);
                this.zzBb = null;
                return;
            }
            if (this.zzBc != null) {
                this.zzBc.zzbd();
            }
        }
    }
}
