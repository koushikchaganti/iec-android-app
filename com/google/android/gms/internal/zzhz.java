package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.internal.zzie.zza;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.concurrent.Future;

@zzha
public class zzhz extends zzil implements zzhy {
    private final Context mContext;
    private final zza zzFc;
    private final zzhs zzJA;
    private final String zzJg;
    private final ArrayList<Future> zzJx;
    private final ArrayList<String> zzJy;
    private final HashSet<String> zzJz;
    private final Object zzpK;

    /* renamed from: com.google.android.gms.internal.zzhz.1 */
    class C04171 implements Runnable {
        final /* synthetic */ zzhz zzJB;
        final /* synthetic */ zzie zzqm;

        C04171(zzhz com_google_android_gms_internal_zzhz, zzie com_google_android_gms_internal_zzie) {
            this.zzJB = com_google_android_gms_internal_zzhz;
            this.zzqm = com_google_android_gms_internal_zzie;
        }

        public void run() {
            this.zzJB.zzJA.zzb(this.zzqm);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzhz.2 */
    class C04182 implements Runnable {
        final /* synthetic */ zzhz zzJB;
        final /* synthetic */ zzie zzqm;

        C04182(zzhz com_google_android_gms_internal_zzhz, zzie com_google_android_gms_internal_zzie) {
            this.zzJB = com_google_android_gms_internal_zzhz;
            this.zzqm = com_google_android_gms_internal_zzie;
        }

        public void run() {
            this.zzJB.zzJA.zzb(this.zzqm);
        }
    }

    public zzhz(Context context, String str, zza com_google_android_gms_internal_zzie_zza, zzhs com_google_android_gms_internal_zzhs) {
        this.zzJx = new ArrayList();
        this.zzJy = new ArrayList();
        this.zzJz = new HashSet();
        this.zzpK = new Object();
        this.mContext = context;
        this.zzJg = str;
        this.zzFc = com_google_android_gms_internal_zzie_zza;
        this.zzJA = com_google_android_gms_internal_zzhs;
    }

    private void zzj(String str, String str2) {
        synchronized (this.zzpK) {
            zzht zzav = this.zzJA.zzav(str);
            if (zzav == null || zzav.zzgB() == null || zzav.zzgA() == null) {
                return;
            }
            this.zzJx.add(new zzhu(this.mContext, str, this.zzJg, str2, this.zzFc, zzav, this).zzgX());
            this.zzJy.add(str);
        }
    }

    public void onStop() {
    }

    public void zza(String str, int i) {
    }

    public void zzaw(String str) {
        synchronized (this.zzpK) {
            this.zzJz.add(str);
        }
    }

    public void zzbp() {
        for (zzem com_google_android_gms_internal_zzem : this.zzFc.zzJF.zzAO) {
            String str = com_google_android_gms_internal_zzem.zzAL;
            for (String zzj : com_google_android_gms_internal_zzem.zzAG) {
                zzj(zzj, str);
            }
        }
        int i = 0;
        while (i < this.zzJx.size()) {
            try {
                ((Future) this.zzJx.get(i)).get();
                synchronized (this.zzpK) {
                    if (this.zzJz.contains(this.zzJy.get(i))) {
                        com.google.android.gms.ads.internal.util.client.zza.zzLE.post(new C04171(this, new zzie(this.zzFc.zzJK.zzGq, null, this.zzFc.zzJL.zzAQ, -2, this.zzFc.zzJL.zzAR, this.zzFc.zzJL.zzGP, this.zzFc.zzJL.orientation, this.zzFc.zzJL.zzAU, this.zzFc.zzJK.zzGt, this.zzFc.zzJL.zzGN, (zzem) this.zzFc.zzJF.zzAO.get(i), null, (String) this.zzJy.get(i), this.zzFc.zzJF, null, this.zzFc.zzJL.zzGO, this.zzFc.zzqV, this.zzFc.zzJL.zzGM, this.zzFc.zzJH, this.zzFc.zzJL.zzGR, this.zzFc.zzJL.zzGS, this.zzFc.zzJE, null)));
                        return;
                    }
                    i++;
                }
            } catch (InterruptedException e) {
            } catch (Exception e2) {
            }
        }
        com.google.android.gms.ads.internal.util.client.zza.zzLE.post(new C04182(this, new zzie(this.zzFc.zzJK.zzGq, null, this.zzFc.zzJL.zzAQ, 3, this.zzFc.zzJL.zzAR, this.zzFc.zzJL.zzGP, this.zzFc.zzJL.orientation, this.zzFc.zzJL.zzAU, this.zzFc.zzJK.zzGt, this.zzFc.zzJL.zzGN, null, null, null, this.zzFc.zzJF, null, this.zzFc.zzJL.zzGO, this.zzFc.zzqV, this.zzFc.zzJL.zzGM, this.zzFc.zzJH, this.zzFc.zzJL.zzGR, this.zzFc.zzJL.zzGS, this.zzFc.zzJE, null)));
    }
}
