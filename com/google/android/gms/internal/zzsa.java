package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions.HasOptions;
import com.google.android.gms.common.api.Api.zzc;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.signin.internal.zzh;
import com.google.android.gms.signin.internal.zzi;
import java.util.concurrent.Executors;

public final class zzsa {
    public static final Api<zzsd> API;
    public static final zzc<zzi> zzTo;
    public static final com.google.android.gms.common.api.Api.zza<zzi, zzsd> zzTp;
    public static final Scope zzVA;
    public static final Scope zzVB;
    public static final Api<zza> zzamM;
    public static final zzc<zzi> zzatI;
    static final com.google.android.gms.common.api.Api.zza<zzi, zza> zzbbE;
    public static final zzsb zzbbF;

    /* renamed from: com.google.android.gms.internal.zzsa.1 */
    static class C08221 extends com.google.android.gms.common.api.Api.zza<zzi, zzsd> {
        C08221() {
        }

        public zzi zza(Context context, Looper looper, zzf com_google_android_gms_common_internal_zzf, zzsd com_google_android_gms_internal_zzsd, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            return new zzi(context, looper, true, com_google_android_gms_common_internal_zzf, com_google_android_gms_internal_zzsd == null ? zzsd.zzbbH : com_google_android_gms_internal_zzsd, connectionCallbacks, onConnectionFailedListener, Executors.newSingleThreadExecutor());
        }
    }

    /* renamed from: com.google.android.gms.internal.zzsa.2 */
    static class C08232 extends com.google.android.gms.common.api.Api.zza<zzi, zza> {
        C08232() {
        }

        public zzi zza(Context context, Looper looper, zzf com_google_android_gms_common_internal_zzf, zza com_google_android_gms_internal_zzsa_zza, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            return new zzi(context, looper, false, com_google_android_gms_common_internal_zzf, com_google_android_gms_internal_zzsa_zza.zzDK(), connectionCallbacks, onConnectionFailedListener);
        }
    }

    public static class zza implements HasOptions {
        private final Bundle zzbbG;

        public Bundle zzDK() {
            return this.zzbbG;
        }
    }

    static {
        zzTo = new zzc();
        zzatI = new zzc();
        zzTp = new C08221();
        zzbbE = new C08232();
        zzVA = new Scope(Scopes.PROFILE);
        zzVB = new Scope(Scopes.EMAIL);
        API = new Api("SignIn.API", zzTp, zzTo);
        zzamM = new Api("SignIn.INTERNAL_API", zzbbE, zzatI);
        zzbbF = new zzh();
    }
}
