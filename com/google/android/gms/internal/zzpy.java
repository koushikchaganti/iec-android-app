package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.PendingResults;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.SensorsApi;
import com.google.android.gms.fitness.data.zzj;
import com.google.android.gms.fitness.request.DataSourcesRequest;
import com.google.android.gms.fitness.request.OnDataPointListener;
import com.google.android.gms.fitness.request.SensorRegistrationRequest;
import com.google.android.gms.fitness.request.SensorRequest;
import com.google.android.gms.fitness.request.SensorUnregistrationRequest;
import com.google.android.gms.fitness.result.DataSourcesResult;

public class zzpy implements SensorsApi {

    private interface zza {
        void zzuf();
    }

    /* renamed from: com.google.android.gms.internal.zzpy.3 */
    class C08133 implements zza {
        final /* synthetic */ zzpy zzaxO;
        final /* synthetic */ OnDataPointListener zzaxS;

        C08133(zzpy com_google_android_gms_internal_zzpy, OnDataPointListener onDataPointListener) {
            this.zzaxO = com_google_android_gms_internal_zzpy;
            this.zzaxS = onDataPointListener;
        }

        public void zzuf() {
            com.google.android.gms.fitness.data.zzk.zza.zztQ().zzc(this.zzaxS);
        }
    }

    private static class zzb extends com.google.android.gms.internal.zzpb.zza {
        private final com.google.android.gms.internal.zzlx.zzb<DataSourcesResult> zzakL;

        private zzb(com.google.android.gms.internal.zzlx.zzb<DataSourcesResult> com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_fitness_result_DataSourcesResult) {
            this.zzakL = com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_fitness_result_DataSourcesResult;
        }

        public void zza(DataSourcesResult dataSourcesResult) {
            this.zzakL.zzr(dataSourcesResult);
        }
    }

    private static class zzc extends com.google.android.gms.internal.zzpp.zza {
        private final com.google.android.gms.internal.zzlx.zzb<Status> zzakL;
        private final zza zzaxW;

        private zzc(com.google.android.gms.internal.zzlx.zzb<Status> com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_common_api_Status, zza com_google_android_gms_internal_zzpy_zza) {
            this.zzakL = com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_common_api_Status;
            this.zzaxW = com_google_android_gms_internal_zzpy_zza;
        }

        public void zzp(Status status) {
            if (this.zzaxW != null && status.isSuccess()) {
                this.zzaxW.zzuf();
            }
            this.zzakL.zzr(status);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzpy.1 */
    class C12371 extends zza<DataSourcesResult> {
        final /* synthetic */ DataSourcesRequest zzaxN;
        final /* synthetic */ zzpy zzaxO;

        C12371(zzpy com_google_android_gms_internal_zzpy, GoogleApiClient googleApiClient, DataSourcesRequest dataSourcesRequest) {
            this.zzaxO = com_google_android_gms_internal_zzpy;
            this.zzaxN = dataSourcesRequest;
            super(googleApiClient);
        }

        protected DataSourcesResult zzO(Status status) {
            return DataSourcesResult.zzS(status);
        }

        protected void zza(zzox com_google_android_gms_internal_zzox) throws RemoteException {
            ((zzpi) com_google_android_gms_internal_zzox.zzqs()).zza(new DataSourcesRequest(this.zzaxN, new zzb(null)));
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzO(status);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzpy.2 */
    class C13992 extends zzc {
        final /* synthetic */ zzpy zzaxO;
        final /* synthetic */ SensorRequest zzaxP;
        final /* synthetic */ zzj zzaxQ;
        final /* synthetic */ PendingIntent zzaxR;

        C13992(zzpy com_google_android_gms_internal_zzpy, GoogleApiClient googleApiClient, SensorRequest sensorRequest, zzj com_google_android_gms_fitness_data_zzj, PendingIntent pendingIntent) {
            this.zzaxO = com_google_android_gms_internal_zzpy;
            this.zzaxP = sensorRequest;
            this.zzaxQ = com_google_android_gms_fitness_data_zzj;
            this.zzaxR = pendingIntent;
            super(googleApiClient);
        }

        protected void zza(zzox com_google_android_gms_internal_zzox) throws RemoteException {
            ((zzpi) com_google_android_gms_internal_zzox.zzqs()).zza(new SensorRegistrationRequest(this.zzaxP, this.zzaxQ, this.zzaxR, new zzqa(this)));
        }

        protected Status zzb(Status status) {
            return status;
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzb(status);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzpy.4 */
    class C14004 extends zzc {
        final /* synthetic */ zzpy zzaxO;
        final /* synthetic */ zza zzaxT;
        final /* synthetic */ zzj zzaxU;
        final /* synthetic */ PendingIntent zzaxV;

        C14004(zzpy com_google_android_gms_internal_zzpy, GoogleApiClient googleApiClient, zza com_google_android_gms_internal_zzpy_zza, zzj com_google_android_gms_fitness_data_zzj, PendingIntent pendingIntent) {
            this.zzaxO = com_google_android_gms_internal_zzpy;
            this.zzaxT = com_google_android_gms_internal_zzpy_zza;
            this.zzaxU = com_google_android_gms_fitness_data_zzj;
            this.zzaxV = pendingIntent;
            super(googleApiClient);
        }

        protected void zza(zzox com_google_android_gms_internal_zzox) throws RemoteException {
            ((zzpi) com_google_android_gms_internal_zzox.zzqs()).zza(new SensorUnregistrationRequest(this.zzaxU, this.zzaxV, new zzc(this.zzaxT, null)));
        }

        protected Status zzb(Status status) {
            return status;
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzb(status);
        }
    }

    private PendingResult<Status> zza(GoogleApiClient googleApiClient, zzj com_google_android_gms_fitness_data_zzj, PendingIntent pendingIntent, zza com_google_android_gms_internal_zzpy_zza) {
        return googleApiClient.zzb(new C14004(this, googleApiClient, com_google_android_gms_internal_zzpy_zza, com_google_android_gms_fitness_data_zzj, pendingIntent));
    }

    private PendingResult<Status> zza(GoogleApiClient googleApiClient, SensorRequest sensorRequest, zzj com_google_android_gms_fitness_data_zzj, PendingIntent pendingIntent) {
        return googleApiClient.zza(new C13992(this, googleApiClient, sensorRequest, com_google_android_gms_fitness_data_zzj, pendingIntent));
    }

    public PendingResult<Status> add(GoogleApiClient client, SensorRequest request, PendingIntent intent) {
        return zza(client, request, null, intent);
    }

    public PendingResult<Status> add(GoogleApiClient client, SensorRequest request, OnDataPointListener listener) {
        return zza(client, request, com.google.android.gms.fitness.data.zzk.zza.zztQ().zza(listener), null);
    }

    public PendingResult<DataSourcesResult> findDataSources(GoogleApiClient client, DataSourcesRequest request) {
        return client.zza(new C12371(this, client, request));
    }

    public PendingResult<Status> remove(GoogleApiClient client, PendingIntent pendingIntent) {
        return zza(client, null, pendingIntent, null);
    }

    public PendingResult<Status> remove(GoogleApiClient client, OnDataPointListener listener) {
        zzj zzb = com.google.android.gms.fitness.data.zzk.zza.zztQ().zzb(listener);
        return zzb == null ? PendingResults.zza(Status.zzaeX, client) : zza(client, zzb, null, new C08133(this, listener));
    }
}
