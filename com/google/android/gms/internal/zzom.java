package com.google.android.gms.internal;

import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.internal.zzto.zzb;

public class zzom {
    public static final zzb zzavA;
    public static final zzb zzavB;
    public static final zzb zzavC;
    public static final zzb zzavD;
    public static final zzb zzavE;
    public static final zzb zzavF;
    public static final zzb zzavG;
    public static final zzb zzavH;
    public static final zzb zzavI;
    public static final zzb zzavJ;
    public static final zzb zzavK;
    public static final zzb zzavL;
    public static final zzb zzavM;
    public static final zzb zzavN;
    public static final zzb zzavO;
    public static final zzb zzavP;
    public static final zzb zzavQ;
    public static final zzb zzavR;
    public static final zzb zzavS;
    public static final zzb zzavT;
    public static final zzb zzavU;
    public static final zzb zzavV;
    public static final zzb zzavW;
    public static final zzb zzavX;
    public static final zzb zzavY;
    public static final zzb zzavZ;
    public static final zzb zzavn;
    public static final zzb zzavo;
    public static final zzb zzavp;
    public static final zzb zzavq;
    public static final zzb zzavr;
    public static final zzb zzavs;
    public static final zzb zzavt;
    public static final zzb zzavu;
    public static final zzb zzavv;
    public static final zzb zzavw;
    public static final zzb zzavx;
    public static final zzb zzavy;
    public static final zzb zzavz;
    public static final zzb zzawa;
    public static final zzb zzawb;
    public static final zzb zzawc;
    public static final zzb zzawd;
    public static final zzb zzawe;
    public static final zzb zzawf;
    public static final zzb zzawg;
    public static final zzb zzawh;
    public static final zzb zzawi;
    public static final zzb zzawj;
    public static final zzb zzawk;

    static {
        zzavn = zzdj("activity");
        zzavo = zzdk("confidence");
        zzavp = zzdm("activity_confidence");
        zzavq = zzdj("steps");
        zzavr = zzdj("duration");
        zzavs = zzdm("activity_duration");
        zzavt = zzdm("activity_duration.ascending");
        zzavu = zzdm("activity_duration.descending");
        zzavv = zzdk("bpm");
        zzavw = zzdk("latitude");
        zzavx = zzdk("longitude");
        zzavy = zzdk("accuracy");
        zzavz = zzdk("altitude");
        zzavA = zzdk("distance");
        zzavB = zzdk("height");
        zzavC = zzdk("weight");
        zzavD = zzdk("circumference");
        zzavE = zzdk("percentage");
        zzavF = zzdk("speed");
        zzavG = zzdk("rpm");
        zzavH = zzdj("revolutions");
        zzavI = zzdk(Field.NUTRIENT_CALORIES);
        zzavJ = zzdk("watts");
        zzavK = zzdj("meal_type");
        zzavL = zzdl("food_item");
        zzavM = zzdm("nutrients");
        zzavN = zzdk("elevation.change");
        zzavO = zzdm("elevation.gain");
        zzavP = zzdm("elevation.loss");
        zzavQ = zzdk("floors");
        zzavR = zzdm("floor.gain");
        zzavS = zzdm("floor.loss");
        zzavT = zzdl("exercise");
        zzavU = zzdj("repetitions");
        zzavV = zzdk("resistance");
        zzavW = zzdj("resistance_type");
        zzavX = zzdj("num_segments");
        zzavY = zzdk("average");
        zzavZ = zzdk("max");
        zzawa = zzdk("min");
        zzawb = zzdk("low_latitude");
        zzawc = zzdk("low_longitude");
        zzawd = zzdk("high_latitude");
        zzawe = zzdk("high_longitude");
        zzawf = zzdk("x");
        zzawg = zzdk("y");
        zzawh = zzdk("z");
        zzawi = zzdn("timestamps");
        zzawj = zzdo("sensor_values");
        zzawk = zzdj("sensor_type");
    }

    private static zzb zza(String str, int i, Boolean bool) {
        zzb com_google_android_gms_internal_zzto_zzb = new zzb();
        com_google_android_gms_internal_zzto_zzb.name = str;
        com_google_android_gms_internal_zzto_zzb.zzbql = Integer.valueOf(i);
        if (bool != null) {
            com_google_android_gms_internal_zzto_zzb.zzbqm = bool;
        }
        return com_google_android_gms_internal_zzto_zzb;
    }

    private static zzb zzdj(String str) {
        return zzo(str, 1);
    }

    private static zzb zzdk(String str) {
        return zzo(str, 2);
    }

    private static zzb zzdl(String str) {
        return zzo(str, 3);
    }

    private static zzb zzdm(String str) {
        return zzo(str, 4);
    }

    private static zzb zzdn(String str) {
        return zzo(str, 5);
    }

    private static zzb zzdo(String str) {
        return zzo(str, 6);
    }

    public static zzb zzo(String str, int i) {
        return zza(str, i, null);
    }
}
