package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import java.util.concurrent.Future;

@zzha
public final class zzin {

    public interface zzb {
        void zze(Bundle bundle);
    }

    private static abstract class zza extends zzil {
        private zza() {
        }

        public void onStop() {
        }
    }

    /* renamed from: com.google.android.gms.internal.zzin.1 */
    static class C11981 extends zza {
        final /* synthetic */ boolean zzKw;
        final /* synthetic */ Context zzsm;

        C11981(Context context, boolean z) {
            this.zzsm = context;
            this.zzKw = z;
            super();
        }

        public void zzbp() {
            Editor edit = zzin.zzw(this.zzsm).edit();
            edit.putBoolean("use_https", this.zzKw);
            edit.apply();
        }
    }

    /* renamed from: com.google.android.gms.internal.zzin.2 */
    static class C11992 extends zza {
        final /* synthetic */ zzb zzKx;
        final /* synthetic */ Context zzsm;

        C11992(Context context, zzb com_google_android_gms_internal_zzin_zzb) {
            this.zzsm = context;
            this.zzKx = com_google_android_gms_internal_zzin_zzb;
            super();
        }

        public void zzbp() {
            SharedPreferences zzI = zzin.zzw(this.zzsm);
            Bundle bundle = new Bundle();
            bundle.putBoolean("use_https", zzI.getBoolean("use_https", true));
            if (this.zzKx != null) {
                this.zzKx.zze(bundle);
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.zzin.3 */
    static class C12003 extends zza {
        final /* synthetic */ int zzKy;
        final /* synthetic */ Context zzsm;

        C12003(Context context, int i) {
            this.zzsm = context;
            this.zzKy = i;
            super();
        }

        public void zzbp() {
            Editor edit = zzin.zzw(this.zzsm).edit();
            edit.putInt("webview_cache_version", this.zzKy);
            edit.apply();
        }
    }

    /* renamed from: com.google.android.gms.internal.zzin.4 */
    static class C12014 extends zza {
        final /* synthetic */ zzb zzKx;
        final /* synthetic */ Context zzsm;

        C12014(Context context, zzb com_google_android_gms_internal_zzin_zzb) {
            this.zzsm = context;
            this.zzKx = com_google_android_gms_internal_zzin_zzb;
            super();
        }

        public void zzbp() {
            SharedPreferences zzI = zzin.zzw(this.zzsm);
            Bundle bundle = new Bundle();
            bundle.putInt("webview_cache_version", zzI.getInt("webview_cache_version", 0));
            if (this.zzKx != null) {
                this.zzKx.zze(bundle);
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.zzin.5 */
    static class C12025 extends zza {
        final /* synthetic */ boolean zzKz;
        final /* synthetic */ Context zzsm;

        C12025(Context context, boolean z) {
            this.zzsm = context;
            this.zzKz = z;
            super();
        }

        public void zzbp() {
            Editor edit = zzin.zzw(this.zzsm).edit();
            edit.putBoolean("content_url_opted_out", this.zzKz);
            edit.apply();
        }
    }

    /* renamed from: com.google.android.gms.internal.zzin.6 */
    static class C12036 extends zza {
        final /* synthetic */ zzb zzKx;
        final /* synthetic */ Context zzsm;

        C12036(Context context, zzb com_google_android_gms_internal_zzin_zzb) {
            this.zzsm = context;
            this.zzKx = com_google_android_gms_internal_zzin_zzb;
            super();
        }

        public void zzbp() {
            SharedPreferences zzI = zzin.zzw(this.zzsm);
            Bundle bundle = new Bundle();
            bundle.putBoolean("content_url_opted_out", zzI.getBoolean("content_url_opted_out", true));
            if (this.zzKx != null) {
                this.zzKx.zze(bundle);
            }
        }
    }

    public static Future zza(Context context, int i) {
        return new C12003(context, i).zzgX();
    }

    public static Future zza(Context context, zzb com_google_android_gms_internal_zzin_zzb) {
        return new C11992(context, com_google_android_gms_internal_zzin_zzb).zzgX();
    }

    public static Future zza(Context context, boolean z) {
        return new C11981(context, z).zzgX();
    }

    public static Future zzb(Context context, zzb com_google_android_gms_internal_zzin_zzb) {
        return new C12014(context, com_google_android_gms_internal_zzin_zzb).zzgX();
    }

    public static Future zzb(Context context, boolean z) {
        return new C12025(context, z).zzgX();
    }

    public static Future zzc(Context context, zzb com_google_android_gms_internal_zzin_zzb) {
        return new C12036(context, com_google_android_gms_internal_zzin_zzb).zzgX();
    }

    private static SharedPreferences zzw(Context context) {
        return context.getSharedPreferences("admob", 0);
    }
}
