package com.google.android.gms.internal;

import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.safetynet.AttestationData;
import com.google.android.gms.safetynet.SafeBrowsingData;
import com.google.android.gms.safetynet.SafetyNetApi;
import com.google.android.gms.safetynet.SafetyNetApi.AttestationResult;
import com.google.android.gms.safetynet.SafetyNetApi.SafeBrowsingResult;
import java.util.List;

public class zzrt implements SafetyNetApi {

    static class zza implements AttestationResult {
        private final Status zzTA;
        private final AttestationData zzbbo;

        public zza(Status status, AttestationData attestationData) {
            this.zzTA = status;
            this.zzbbo = attestationData;
        }

        public String getJwsResult() {
            return this.zzbbo == null ? null : this.zzbbo.getJwsResult();
        }

        public Status getStatus() {
            return this.zzTA;
        }
    }

    static class zzd implements SafeBrowsingResult {
        private Status zzTA;
        private String zzbbi;
        private final SafeBrowsingData zzbbs;

        public zzd(Status status, SafeBrowsingData safeBrowsingData) {
            this.zzTA = status;
            this.zzbbs = safeBrowsingData;
            this.zzbbi = null;
            if (this.zzbbs != null) {
                this.zzbbi = this.zzbbs.getMetadata();
            } else if (this.zzTA.isSuccess()) {
                this.zzTA = new Status(8);
            }
        }

        public String getMetadata() {
            return this.zzbbi;
        }

        public Status getStatus() {
            return this.zzTA;
        }
    }

    static abstract class zzb extends zzrq<AttestationResult> {
        protected zzrr zzbbp;

        /* renamed from: com.google.android.gms.internal.zzrt.zzb.1 */
        class C12101 extends zzrp {
            final /* synthetic */ zzb zzbbq;

            C12101(zzb com_google_android_gms_internal_zzrt_zzb) {
                this.zzbbq = com_google_android_gms_internal_zzrt_zzb;
            }

            public void zza(Status status, AttestationData attestationData) {
                this.zzbbq.zzb(new zza(status, attestationData));
            }
        }

        public zzb(GoogleApiClient googleApiClient) {
            super(googleApiClient);
            this.zzbbp = new C12101(this);
        }

        protected AttestationResult zzbd(Status status) {
            return new zza(status, null);
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzbd(status);
        }
    }

    static abstract class zzc extends zzrq<SafeBrowsingResult> {
        protected zzrr zzbbp;

        /* renamed from: com.google.android.gms.internal.zzrt.zzc.1 */
        class C12111 extends zzrp {
            final /* synthetic */ zzc zzbbr;

            C12111(zzc com_google_android_gms_internal_zzrt_zzc) {
                this.zzbbr = com_google_android_gms_internal_zzrt_zzc;
            }

            public void zza(Status status, SafeBrowsingData safeBrowsingData) {
                this.zzbbr.zzb(new zzd(status, safeBrowsingData));
            }
        }

        public zzc(GoogleApiClient googleApiClient) {
            super(googleApiClient);
            this.zzbbp = new C12111(this);
        }

        protected SafeBrowsingResult zzbe(Status status) {
            return new zzd(status, null);
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzbe(status);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzrt.1 */
    class C14221 extends zzb {
        final /* synthetic */ byte[] zzbbk;
        final /* synthetic */ zzrt zzbbl;

        C14221(zzrt com_google_android_gms_internal_zzrt, GoogleApiClient googleApiClient, byte[] bArr) {
            this.zzbbl = com_google_android_gms_internal_zzrt;
            this.zzbbk = bArr;
            super(googleApiClient);
        }

        protected void zza(zzru com_google_android_gms_internal_zzru) throws RemoteException {
            com_google_android_gms_internal_zzru.zza(this.zzbbp, this.zzbbk);
        }
    }

    /* renamed from: com.google.android.gms.internal.zzrt.2 */
    class C14232 extends zzc {
        final /* synthetic */ zzrt zzbbl;
        final /* synthetic */ List zzbbm;
        final /* synthetic */ String zzbbn;

        C14232(zzrt com_google_android_gms_internal_zzrt, GoogleApiClient googleApiClient, List list, String str) {
            this.zzbbl = com_google_android_gms_internal_zzrt;
            this.zzbbm = list;
            this.zzbbn = str;
            super(googleApiClient);
        }

        protected void zza(zzru com_google_android_gms_internal_zzru) throws RemoteException {
            com_google_android_gms_internal_zzru.zza(this.zzbbp, this.zzbbm, 1, this.zzbbn);
        }
    }

    public PendingResult<AttestationResult> attest(GoogleApiClient googleApiClient, byte[] nonce) {
        return googleApiClient.zza(new C14221(this, googleApiClient, nonce));
    }

    public PendingResult<SafeBrowsingResult> lookupUri(GoogleApiClient googleApiClient, List<Integer> threatTypes, String uri) {
        if (threatTypes == null) {
            throw new IllegalArgumentException("Null threatTypes in lookupUri");
        } else if (!TextUtils.isEmpty(uri)) {
            return googleApiClient.zza(new C14232(this, googleApiClient, threatTypes, uri));
        } else {
            throw new IllegalArgumentException("Null or empty uri in lookupUri");
        }
    }
}
