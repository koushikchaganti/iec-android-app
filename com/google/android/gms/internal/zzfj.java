package com.google.android.gms.internal;

import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationBannerListener;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.MediationInterstitialListener;
import com.google.ads.mediation.MediationServerParameters;
import com.google.ads.mediation.NetworkExtras;
import com.google.android.gms.ads.internal.client.zzl;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.util.client.zzb;

@zzha
public final class zzfj<NETWORK_EXTRAS extends NetworkExtras, SERVER_PARAMETERS extends MediationServerParameters> implements MediationBannerListener, MediationInterstitialListener {
    private final zzey zzBK;

    /* renamed from: com.google.android.gms.internal.zzfj.10 */
    class AnonymousClass10 implements Runnable {
        final /* synthetic */ zzfj zzBQ;
        final /* synthetic */ ErrorCode zzBR;

        AnonymousClass10(zzfj com_google_android_gms_internal_zzfj, ErrorCode errorCode) {
            this.zzBQ = com_google_android_gms_internal_zzfj;
            this.zzBR = errorCode;
        }

        public void run() {
            try {
                this.zzBQ.zzBK.onAdFailedToLoad(zzfk.zza(this.zzBR));
            } catch (Throwable e) {
                zzb.zzd("Could not call onAdFailedToLoad.", e);
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.zzfj.1 */
    class C03861 implements Runnable {
        final /* synthetic */ zzfj zzBQ;

        C03861(zzfj com_google_android_gms_internal_zzfj) {
            this.zzBQ = com_google_android_gms_internal_zzfj;
        }

        public void run() {
            try {
                this.zzBQ.zzBK.onAdClicked();
            } catch (Throwable e) {
                zzb.zzd("Could not call onAdClicked.", e);
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.zzfj.2 */
    class C03872 implements Runnable {
        final /* synthetic */ zzfj zzBQ;

        C03872(zzfj com_google_android_gms_internal_zzfj) {
            this.zzBQ = com_google_android_gms_internal_zzfj;
        }

        public void run() {
            try {
                this.zzBQ.zzBK.onAdOpened();
            } catch (Throwable e) {
                zzb.zzd("Could not call onAdOpened.", e);
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.zzfj.3 */
    class C03883 implements Runnable {
        final /* synthetic */ zzfj zzBQ;

        C03883(zzfj com_google_android_gms_internal_zzfj) {
            this.zzBQ = com_google_android_gms_internal_zzfj;
        }

        public void run() {
            try {
                this.zzBQ.zzBK.onAdLoaded();
            } catch (Throwable e) {
                zzb.zzd("Could not call onAdLoaded.", e);
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.zzfj.4 */
    class C03894 implements Runnable {
        final /* synthetic */ zzfj zzBQ;

        C03894(zzfj com_google_android_gms_internal_zzfj) {
            this.zzBQ = com_google_android_gms_internal_zzfj;
        }

        public void run() {
            try {
                this.zzBQ.zzBK.onAdClosed();
            } catch (Throwable e) {
                zzb.zzd("Could not call onAdClosed.", e);
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.zzfj.5 */
    class C03905 implements Runnable {
        final /* synthetic */ zzfj zzBQ;
        final /* synthetic */ ErrorCode zzBR;

        C03905(zzfj com_google_android_gms_internal_zzfj, ErrorCode errorCode) {
            this.zzBQ = com_google_android_gms_internal_zzfj;
            this.zzBR = errorCode;
        }

        public void run() {
            try {
                this.zzBQ.zzBK.onAdFailedToLoad(zzfk.zza(this.zzBR));
            } catch (Throwable e) {
                zzb.zzd("Could not call onAdFailedToLoad.", e);
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.zzfj.6 */
    class C03916 implements Runnable {
        final /* synthetic */ zzfj zzBQ;

        C03916(zzfj com_google_android_gms_internal_zzfj) {
            this.zzBQ = com_google_android_gms_internal_zzfj;
        }

        public void run() {
            try {
                this.zzBQ.zzBK.onAdLeftApplication();
            } catch (Throwable e) {
                zzb.zzd("Could not call onAdLeftApplication.", e);
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.zzfj.7 */
    class C03927 implements Runnable {
        final /* synthetic */ zzfj zzBQ;

        C03927(zzfj com_google_android_gms_internal_zzfj) {
            this.zzBQ = com_google_android_gms_internal_zzfj;
        }

        public void run() {
            try {
                this.zzBQ.zzBK.onAdOpened();
            } catch (Throwable e) {
                zzb.zzd("Could not call onAdOpened.", e);
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.zzfj.8 */
    class C03938 implements Runnable {
        final /* synthetic */ zzfj zzBQ;

        C03938(zzfj com_google_android_gms_internal_zzfj) {
            this.zzBQ = com_google_android_gms_internal_zzfj;
        }

        public void run() {
            try {
                this.zzBQ.zzBK.onAdLoaded();
            } catch (Throwable e) {
                zzb.zzd("Could not call onAdLoaded.", e);
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.zzfj.9 */
    class C03949 implements Runnable {
        final /* synthetic */ zzfj zzBQ;

        C03949(zzfj com_google_android_gms_internal_zzfj) {
            this.zzBQ = com_google_android_gms_internal_zzfj;
        }

        public void run() {
            try {
                this.zzBQ.zzBK.onAdClosed();
            } catch (Throwable e) {
                zzb.zzd("Could not call onAdClosed.", e);
            }
        }
    }

    public zzfj(zzey com_google_android_gms_internal_zzey) {
        this.zzBK = com_google_android_gms_internal_zzey;
    }

    public void onClick(MediationBannerAdapter<?, ?> mediationBannerAdapter) {
        zzb.zzaF("Adapter called onClick.");
        if (zzl.zzcN().zzhr()) {
            try {
                this.zzBK.onAdClicked();
                return;
            } catch (Throwable e) {
                zzb.zzd("Could not call onAdClicked.", e);
                return;
            }
        }
        zzb.zzaH("onClick must be called on the main UI thread.");
        zza.zzLE.post(new C03861(this));
    }

    public void onDismissScreen(MediationBannerAdapter<?, ?> mediationBannerAdapter) {
        zzb.zzaF("Adapter called onDismissScreen.");
        if (zzl.zzcN().zzhr()) {
            try {
                this.zzBK.onAdClosed();
                return;
            } catch (Throwable e) {
                zzb.zzd("Could not call onAdClosed.", e);
                return;
            }
        }
        zzb.zzaH("onDismissScreen must be called on the main UI thread.");
        zza.zzLE.post(new C03894(this));
    }

    public void onDismissScreen(MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter) {
        zzb.zzaF("Adapter called onDismissScreen.");
        if (zzl.zzcN().zzhr()) {
            try {
                this.zzBK.onAdClosed();
                return;
            } catch (Throwable e) {
                zzb.zzd("Could not call onAdClosed.", e);
                return;
            }
        }
        zzb.zzaH("onDismissScreen must be called on the main UI thread.");
        zza.zzLE.post(new C03949(this));
    }

    public void onFailedToReceiveAd(MediationBannerAdapter<?, ?> mediationBannerAdapter, ErrorCode errorCode) {
        zzb.zzaF("Adapter called onFailedToReceiveAd with error. " + errorCode);
        if (zzl.zzcN().zzhr()) {
            try {
                this.zzBK.onAdFailedToLoad(zzfk.zza(errorCode));
                return;
            } catch (Throwable e) {
                zzb.zzd("Could not call onAdFailedToLoad.", e);
                return;
            }
        }
        zzb.zzaH("onFailedToReceiveAd must be called on the main UI thread.");
        zza.zzLE.post(new C03905(this, errorCode));
    }

    public void onFailedToReceiveAd(MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter, ErrorCode errorCode) {
        zzb.zzaF("Adapter called onFailedToReceiveAd with error " + errorCode + ".");
        if (zzl.zzcN().zzhr()) {
            try {
                this.zzBK.onAdFailedToLoad(zzfk.zza(errorCode));
                return;
            } catch (Throwable e) {
                zzb.zzd("Could not call onAdFailedToLoad.", e);
                return;
            }
        }
        zzb.zzaH("onFailedToReceiveAd must be called on the main UI thread.");
        zza.zzLE.post(new AnonymousClass10(this, errorCode));
    }

    public void onLeaveApplication(MediationBannerAdapter<?, ?> mediationBannerAdapter) {
        zzb.zzaF("Adapter called onLeaveApplication.");
        if (zzl.zzcN().zzhr()) {
            try {
                this.zzBK.onAdLeftApplication();
                return;
            } catch (Throwable e) {
                zzb.zzd("Could not call onAdLeftApplication.", e);
                return;
            }
        }
        zzb.zzaH("onLeaveApplication must be called on the main UI thread.");
        zza.zzLE.post(new C03916(this));
    }

    public void onLeaveApplication(MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter) {
        zzb.zzaF("Adapter called onLeaveApplication.");
        if (zzl.zzcN().zzhr()) {
            try {
                this.zzBK.onAdLeftApplication();
                return;
            } catch (Throwable e) {
                zzb.zzd("Could not call onAdLeftApplication.", e);
                return;
            }
        }
        zzb.zzaH("onLeaveApplication must be called on the main UI thread.");
        zza.zzLE.post(new Runnable() {
            final /* synthetic */ zzfj zzBQ;

            {
                this.zzBQ = r1;
            }

            public void run() {
                try {
                    this.zzBQ.zzBK.onAdLeftApplication();
                } catch (Throwable e) {
                    zzb.zzd("Could not call onAdLeftApplication.", e);
                }
            }
        });
    }

    public void onPresentScreen(MediationBannerAdapter<?, ?> mediationBannerAdapter) {
        zzb.zzaF("Adapter called onPresentScreen.");
        if (zzl.zzcN().zzhr()) {
            try {
                this.zzBK.onAdOpened();
                return;
            } catch (Throwable e) {
                zzb.zzd("Could not call onAdOpened.", e);
                return;
            }
        }
        zzb.zzaH("onPresentScreen must be called on the main UI thread.");
        zza.zzLE.post(new C03927(this));
    }

    public void onPresentScreen(MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter) {
        zzb.zzaF("Adapter called onPresentScreen.");
        if (zzl.zzcN().zzhr()) {
            try {
                this.zzBK.onAdOpened();
                return;
            } catch (Throwable e) {
                zzb.zzd("Could not call onAdOpened.", e);
                return;
            }
        }
        zzb.zzaH("onPresentScreen must be called on the main UI thread.");
        zza.zzLE.post(new C03872(this));
    }

    public void onReceivedAd(MediationBannerAdapter<?, ?> mediationBannerAdapter) {
        zzb.zzaF("Adapter called onReceivedAd.");
        if (zzl.zzcN().zzhr()) {
            try {
                this.zzBK.onAdLoaded();
                return;
            } catch (Throwable e) {
                zzb.zzd("Could not call onAdLoaded.", e);
                return;
            }
        }
        zzb.zzaH("onReceivedAd must be called on the main UI thread.");
        zza.zzLE.post(new C03938(this));
    }

    public void onReceivedAd(MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter) {
        zzb.zzaF("Adapter called onReceivedAd.");
        if (zzl.zzcN().zzhr()) {
            try {
                this.zzBK.onAdLoaded();
                return;
            } catch (Throwable e) {
                zzb.zzd("Could not call onAdLoaded.", e);
                return;
            }
        }
        zzb.zzaH("onReceivedAd must be called on the main UI thread.");
        zza.zzLE.post(new C03883(this));
    }
}
