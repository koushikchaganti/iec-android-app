package com.google.android.gms.internal;

import android.content.Context;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Map;

@zzha
public class zziu {
    private static zzl zzLk;
    public static final zza<Void> zzLl;
    private static final Object zzqf;

    public interface zza<T> {
        T zzgc();

        T zzh(InputStream inputStream);
    }

    /* renamed from: com.google.android.gms.internal.zziu.1 */
    static class C07861 implements zza {
        C07861() {
        }

        public /* synthetic */ Object zzgc() {
            return zzhj();
        }

        public /* synthetic */ Object zzh(InputStream inputStream) {
            return zzi(inputStream);
        }

        public Void zzhj() {
            return null;
        }

        public Void zzi(InputStream inputStream) {
            return null;
        }
    }

    /* renamed from: com.google.android.gms.internal.zziu.2 */
    class C07872 implements com.google.android.gms.internal.zzm.zza {
        final /* synthetic */ zzc zzLm;
        final /* synthetic */ zziu zzLn;
        final /* synthetic */ String zzzn;

        C07872(zziu com_google_android_gms_internal_zziu, String str, zzc com_google_android_gms_internal_zziu_zzc) {
            this.zzLn = com_google_android_gms_internal_zziu;
            this.zzzn = str;
            this.zzLm = com_google_android_gms_internal_zziu_zzc;
        }

        public void zze(zzr com_google_android_gms_internal_zzr) {
            com.google.android.gms.ads.internal.util.client.zzb.zzaH("Failed to load URL: " + this.zzzn + "\n" + com_google_android_gms_internal_zzr.toString());
            this.zzLm.zzb(null);
        }
    }

    private static class zzb<T> extends zzk<InputStream> {
        private final zza<T> zzLp;
        private final com.google.android.gms.internal.zzm.zzb<T> zzaG;

        /* renamed from: com.google.android.gms.internal.zziu.zzb.1 */
        class C07881 implements com.google.android.gms.internal.zzm.zza {
            final /* synthetic */ com.google.android.gms.internal.zzm.zzb zzLq;
            final /* synthetic */ zza zzLr;

            C07881(com.google.android.gms.internal.zzm.zzb com_google_android_gms_internal_zzm_zzb, zza com_google_android_gms_internal_zziu_zza) {
                this.zzLq = com_google_android_gms_internal_zzm_zzb;
                this.zzLr = com_google_android_gms_internal_zziu_zza;
            }

            public void zze(zzr com_google_android_gms_internal_zzr) {
                this.zzLq.zzb(this.zzLr.zzgc());
            }
        }

        public zzb(String str, zza<T> com_google_android_gms_internal_zziu_zza_T, com.google.android.gms.internal.zzm.zzb<T> com_google_android_gms_internal_zzm_zzb_T) {
            super(0, str, new C07881(com_google_android_gms_internal_zzm_zzb_T, com_google_android_gms_internal_zziu_zza_T));
            this.zzLp = com_google_android_gms_internal_zziu_zza_T;
            this.zzaG = com_google_android_gms_internal_zzm_zzb_T;
        }

        protected zzm<InputStream> zza(zzi com_google_android_gms_internal_zzi) {
            return zzm.zza(new ByteArrayInputStream(com_google_android_gms_internal_zzi.data), zzx.zzb(com_google_android_gms_internal_zzi));
        }

        protected /* synthetic */ void zza(Object obj) {
            zzj((InputStream) obj);
        }

        protected void zzj(InputStream inputStream) {
            this.zzaG.zzb(this.zzLp.zzh(inputStream));
        }
    }

    /* renamed from: com.google.android.gms.internal.zziu.3 */
    class C11633 extends zzab {
        final /* synthetic */ zziu zzLn;
        final /* synthetic */ Map zzLo;

        C11633(zziu com_google_android_gms_internal_zziu, String str, com.google.android.gms.internal.zzm.zzb com_google_android_gms_internal_zzm_zzb, com.google.android.gms.internal.zzm.zza com_google_android_gms_internal_zzm_zza, Map map) {
            this.zzLn = com_google_android_gms_internal_zziu;
            this.zzLo = map;
            super(str, com_google_android_gms_internal_zzm_zzb, com_google_android_gms_internal_zzm_zza);
        }

        public Map<String, String> getHeaders() throws zza {
            return this.zzLo == null ? super.getHeaders() : this.zzLo;
        }
    }

    private class zzc<T> extends zzjb<T> implements com.google.android.gms.internal.zzm.zzb<T> {
        final /* synthetic */ zziu zzLn;

        private zzc(zziu com_google_android_gms_internal_zziu) {
            this.zzLn = com_google_android_gms_internal_zziu;
        }

        public void zzb(T t) {
            super.zzf(t);
        }
    }

    static {
        zzqf = new Object();
        zzLl = new C07861();
    }

    public zziu(Context context) {
        zzLk = zzR(context);
    }

    private static zzl zzR(Context context) {
        zzl com_google_android_gms_internal_zzl;
        synchronized (zzqf) {
            if (zzLk == null) {
                zzLk = zzac.zza(context.getApplicationContext());
            }
            com_google_android_gms_internal_zzl = zzLk;
        }
        return com_google_android_gms_internal_zzl;
    }

    public <T> zzje<T> zza(String str, zza<T> com_google_android_gms_internal_zziu_zza_T) {
        Object com_google_android_gms_internal_zziu_zzc = new zzc();
        zzLk.zze(new zzb(str, com_google_android_gms_internal_zziu_zza_T, com_google_android_gms_internal_zziu_zzc));
        return com_google_android_gms_internal_zziu_zzc;
    }

    public zzje<String> zza(String str, Map<String, String> map) {
        Object com_google_android_gms_internal_zziu_zzc = new zzc();
        zzLk.zze(new C11633(this, str, com_google_android_gms_internal_zziu_zzc, new C07872(this, str, com_google_android_gms_internal_zziu_zzc), map));
        return com_google_android_gms_internal_zziu_zzc;
    }
}
