package com.google.android.gms.analytics.internal;

import android.content.Context;
import android.content.Intent;
import com.google.android.gms.analytics.AnalyticsReceiver;
import com.google.android.gms.analytics.AnalyticsService;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.measurement.zzg;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class zzb extends zzd {
    private final zzl zzOH;

    /* renamed from: com.google.android.gms.analytics.internal.zzb.1 */
    class C02641 implements Runnable {
        final /* synthetic */ int zzOI;
        final /* synthetic */ zzb zzOJ;

        C02641(zzb com_google_android_gms_analytics_internal_zzb, int i) {
            this.zzOJ = com_google_android_gms_analytics_internal_zzb;
            this.zzOI = i;
        }

        public void run() {
            this.zzOJ.zzOH.zzs(((long) this.zzOI) * 1000);
        }
    }

    /* renamed from: com.google.android.gms.analytics.internal.zzb.2 */
    class C02652 implements Runnable {
        final /* synthetic */ zzb zzOJ;
        final /* synthetic */ boolean zzOK;

        C02652(zzb com_google_android_gms_analytics_internal_zzb, boolean z) {
            this.zzOJ = com_google_android_gms_analytics_internal_zzb;
            this.zzOK = z;
        }

        public void run() {
            this.zzOJ.zzOH.zzJ(this.zzOK);
        }
    }

    /* renamed from: com.google.android.gms.analytics.internal.zzb.3 */
    class C02663 implements Runnable {
        final /* synthetic */ zzb zzOJ;
        final /* synthetic */ String zzOL;
        final /* synthetic */ Runnable zzOM;

        C02663(zzb com_google_android_gms_analytics_internal_zzb, String str, Runnable runnable) {
            this.zzOJ = com_google_android_gms_analytics_internal_zzb;
            this.zzOL = str;
            this.zzOM = runnable;
        }

        public void run() {
            this.zzOJ.zzOH.zzbi(this.zzOL);
            if (this.zzOM != null) {
                this.zzOM.run();
            }
        }
    }

    /* renamed from: com.google.android.gms.analytics.internal.zzb.4 */
    class C02674 implements Runnable {
        final /* synthetic */ zzb zzOJ;
        final /* synthetic */ zzab zzON;

        C02674(zzb com_google_android_gms_analytics_internal_zzb, zzab com_google_android_gms_analytics_internal_zzab) {
            this.zzOJ = com_google_android_gms_analytics_internal_zzb;
            this.zzON = com_google_android_gms_analytics_internal_zzab;
        }

        public void run() {
            this.zzOJ.zzOH.zza(this.zzON);
        }
    }

    /* renamed from: com.google.android.gms.analytics.internal.zzb.5 */
    class C02685 implements Runnable {
        final /* synthetic */ zzb zzOJ;

        C02685(zzb com_google_android_gms_analytics_internal_zzb) {
            this.zzOJ = com_google_android_gms_analytics_internal_zzb;
        }

        public void run() {
            this.zzOJ.zzOH.zziK();
        }
    }

    /* renamed from: com.google.android.gms.analytics.internal.zzb.6 */
    class C02696 implements Runnable {
        final /* synthetic */ zzb zzOJ;
        final /* synthetic */ zzw zzOO;

        C02696(zzb com_google_android_gms_analytics_internal_zzb, zzw com_google_android_gms_analytics_internal_zzw) {
            this.zzOJ = com_google_android_gms_analytics_internal_zzb;
            this.zzOO = com_google_android_gms_analytics_internal_zzw;
        }

        public void run() {
            this.zzOJ.zzOH.zzb(this.zzOO);
        }
    }

    /* renamed from: com.google.android.gms.analytics.internal.zzb.7 */
    class C02707 implements Callable<Void> {
        final /* synthetic */ zzb zzOJ;

        C02707(zzb com_google_android_gms_analytics_internal_zzb) {
            this.zzOJ = com_google_android_gms_analytics_internal_zzb;
        }

        public /* synthetic */ Object call() throws Exception {
            return zzdm();
        }

        public Void zzdm() throws Exception {
            this.zzOJ.zzOH.zzjJ();
            return null;
        }
    }

    public zzb(zzf com_google_android_gms_analytics_internal_zzf, zzg com_google_android_gms_analytics_internal_zzg) {
        super(com_google_android_gms_analytics_internal_zzf);
        zzx.zzy(com_google_android_gms_analytics_internal_zzg);
        this.zzOH = com_google_android_gms_analytics_internal_zzg.zzj(com_google_android_gms_analytics_internal_zzf);
    }

    void onServiceConnected() {
        zziS();
        this.zzOH.onServiceConnected();
    }

    public void setLocalDispatchPeriod(int dispatchPeriodInSeconds) {
        zzje();
        zzb("setLocalDispatchPeriod (sec)", Integer.valueOf(dispatchPeriodInSeconds));
        zziW().zzf(new C02641(this, dispatchPeriodInSeconds));
    }

    public void start() {
        this.zzOH.start();
    }

    public void zzJ(boolean z) {
        zza("Network connectivity status changed", Boolean.valueOf(z));
        zziW().zzf(new C02652(this, z));
    }

    public long zza(zzh com_google_android_gms_analytics_internal_zzh) {
        zzje();
        zzx.zzy(com_google_android_gms_analytics_internal_zzh);
        zziS();
        long zza = this.zzOH.zza(com_google_android_gms_analytics_internal_zzh, true);
        if (zza == 0) {
            this.zzOH.zzc(com_google_android_gms_analytics_internal_zzh);
        }
        return zza;
    }

    public void zza(zzab com_google_android_gms_analytics_internal_zzab) {
        zzx.zzy(com_google_android_gms_analytics_internal_zzab);
        zzje();
        zzb("Hit delivery requested", com_google_android_gms_analytics_internal_zzab);
        zziW().zzf(new C02674(this, com_google_android_gms_analytics_internal_zzab));
    }

    public void zza(zzw com_google_android_gms_analytics_internal_zzw) {
        zzje();
        zziW().zzf(new C02696(this, com_google_android_gms_analytics_internal_zzw));
    }

    public void zza(String str, Runnable runnable) {
        zzx.zzh(str, "campaign param can't be empty");
        zziW().zzf(new C02663(this, str, runnable));
    }

    public void zziK() {
        zzje();
        zziR();
        zziW().zzf(new C02685(this));
    }

    public void zziL() {
        zzje();
        Context context = getContext();
        if (AnalyticsReceiver.zzX(context) && AnalyticsService.zzY(context)) {
            Intent intent = new Intent(context, AnalyticsService.class);
            intent.setAction("com.google.android.gms.analytics.ANALYTICS_DISPATCH");
            context.startService(intent);
            return;
        }
        zza(null);
    }

    public boolean zziM() {
        zzje();
        try {
            zziW().zzc(new C02707(this)).get(4, TimeUnit.SECONDS);
            return true;
        } catch (InterruptedException e) {
            zzd("syncDispatchLocalHits interrupted", e);
            return false;
        } catch (ExecutionException e2) {
            zze("syncDispatchLocalHits failed", e2);
            return false;
        } catch (TimeoutException e3) {
            zzd("syncDispatchLocalHits timed out", e3);
            return false;
        }
    }

    public void zziN() {
        zzje();
        zzg.zziS();
        this.zzOH.zziN();
    }

    public void zziO() {
        zzba("Radio powered up");
        zziL();
    }

    void zziP() {
        zziS();
        this.zzOH.zziP();
    }

    protected void zzir() {
        this.zzOH.zza();
    }
}
