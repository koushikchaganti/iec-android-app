package com.google.android.gms.analytics.internal;

import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import com.google.android.gms.common.internal.zzd;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.internal.zzmt;
import com.softelite.testapp.BuildConfig;

public final class zzy {
    public static zza<Long> zzQA;
    public static zza<Long> zzQB;
    public static zza<Long> zzQC;
    public static zza<Integer> zzQD;
    public static zza<Integer> zzQE;
    public static zza<String> zzQF;
    public static zza<String> zzQG;
    public static zza<String> zzQH;
    public static zza<String> zzQI;
    public static zza<Integer> zzQJ;
    public static zza<String> zzQK;
    public static zza<String> zzQL;
    public static zza<Integer> zzQM;
    public static zza<Integer> zzQN;
    public static zza<Integer> zzQO;
    public static zza<Integer> zzQP;
    public static zza<String> zzQQ;
    public static zza<Integer> zzQR;
    public static zza<Long> zzQS;
    public static zza<Integer> zzQT;
    public static zza<Integer> zzQU;
    public static zza<Long> zzQV;
    public static zza<String> zzQW;
    public static zza<Integer> zzQX;
    public static zza<Boolean> zzQY;
    public static zza<Long> zzQZ;
    public static zza<Boolean> zzQp;
    public static zza<Boolean> zzQq;
    public static zza<String> zzQr;
    public static zza<Long> zzQs;
    public static zza<Float> zzQt;
    public static zza<Integer> zzQu;
    public static zza<Integer> zzQv;
    public static zza<Integer> zzQw;
    public static zza<Long> zzQx;
    public static zza<Long> zzQy;
    public static zza<Long> zzQz;
    public static zza<Long> zzRa;
    public static zza<Long> zzRb;
    public static zza<Long> zzRc;
    public static zza<Long> zzRd;
    public static zza<Long> zzRe;
    public static zza<Long> zzRf;

    public static final class zza<V> {
        private final V zzRg;
        private final zzmt<V> zzRh;
        private V zzRi;

        private zza(zzmt<V> com_google_android_gms_internal_zzmt_V, V v) {
            zzx.zzy(com_google_android_gms_internal_zzmt_V);
            this.zzRh = com_google_android_gms_internal_zzmt_V;
            this.zzRg = v;
        }

        static zza<Float> zza(String str, float f) {
            return zza(str, f, f);
        }

        static zza<Float> zza(String str, float f, float f2) {
            return new zza(zzmt.zza(str, Float.valueOf(f2)), Float.valueOf(f));
        }

        static zza<Integer> zza(String str, int i, int i2) {
            return new zza(zzmt.zza(str, Integer.valueOf(i2)), Integer.valueOf(i));
        }

        static zza<Long> zza(String str, long j, long j2) {
            return new zza(zzmt.zza(str, Long.valueOf(j2)), Long.valueOf(j));
        }

        static zza<Boolean> zza(String str, boolean z, boolean z2) {
            return new zza(zzmt.zzg(str, z2), Boolean.valueOf(z));
        }

        static zza<Long> zzb(String str, long j) {
            return zza(str, j, j);
        }

        static zza<Integer> zzd(String str, int i) {
            return zza(str, i, i);
        }

        static zza<String> zzd(String str, String str2, String str3) {
            return new zza(zzmt.zzw(str, str3), str2);
        }

        static zza<Boolean> zzd(String str, boolean z) {
            return zza(str, z, z);
        }

        static zza<String> zzm(String str, String str2) {
            return zzd(str, str2, str2);
        }

        public V get() {
            return this.zzRi != null ? this.zzRi : (zzd.zzaiU && zzmt.isInitialized()) ? this.zzRh.zzpF() : this.zzRg;
        }
    }

    static {
        zzQp = zza.zzd("analytics.service_enabled", false);
        zzQq = zza.zzd("analytics.service_client_enabled", true);
        zzQr = zza.zzd("analytics.log_tag", "GAv4", "GAv4-SVC");
        zzQs = zza.zzb("analytics.max_tokens", 60);
        zzQt = zza.zza("analytics.tokens_per_sec", 0.5f);
        zzQu = zza.zza("analytics.max_stored_hits", (int) GamesStatusCodes.STATUS_REQUEST_UPDATE_PARTIAL_SUCCESS, 20000);
        zzQv = zza.zzd("analytics.max_stored_hits_per_app", (int) GamesStatusCodes.STATUS_REQUEST_UPDATE_PARTIAL_SUCCESS);
        zzQw = zza.zzd("analytics.max_stored_properties_per_app", 100);
        zzQx = zza.zza("analytics.local_dispatch_millis", 1800000, 120000);
        zzQy = zza.zza("analytics.initial_local_dispatch_millis", 5000, 5000);
        zzQz = zza.zzb("analytics.min_local_dispatch_millis", 120000);
        zzQA = zza.zzb("analytics.max_local_dispatch_millis", 7200000);
        zzQB = zza.zzb("analytics.dispatch_alarm_millis", 7200000);
        zzQC = zza.zzb("analytics.max_dispatch_alarm_millis", 32400000);
        zzQD = zza.zzd("analytics.max_hits_per_dispatch", 20);
        zzQE = zza.zzd("analytics.max_hits_per_batch", 20);
        zzQF = zza.zzm("analytics.insecure_host", "http://www.google-analytics.com");
        zzQG = zza.zzm("analytics.secure_host", "https://ssl.google-analytics.com");
        zzQH = zza.zzm("analytics.simple_endpoint", "/collect");
        zzQI = zza.zzm("analytics.batching_endpoint", "/batch");
        zzQJ = zza.zzd("analytics.max_get_length", 2036);
        zzQK = zza.zzd("analytics.batching_strategy.k", zzm.BATCH_BY_COUNT.name(), zzm.BATCH_BY_COUNT.name());
        zzQL = zza.zzm("analytics.compression_strategy.k", zzo.GZIP.name());
        zzQM = zza.zzd("analytics.max_hits_per_request.k", 20);
        zzQN = zza.zzd("analytics.max_hit_length.k", (int) AccessibilityNodeInfoCompat.ACTION_SCROLL_BACKWARD);
        zzQO = zza.zzd("analytics.max_post_length.k", (int) AccessibilityNodeInfoCompat.ACTION_SCROLL_BACKWARD);
        zzQP = zza.zzd("analytics.max_batch_post_length", (int) AccessibilityNodeInfoCompat.ACTION_SCROLL_BACKWARD);
        zzQQ = zza.zzm("analytics.fallback_responses.k", "404,502");
        zzQR = zza.zzd("analytics.batch_retry_interval.seconds.k", 3600);
        zzQS = zza.zzb("analytics.service_monitor_interval", 86400000);
        zzQT = zza.zzd("analytics.http_connection.connect_timeout_millis", 60000);
        zzQU = zza.zzd("analytics.http_connection.read_timeout_millis", 61000);
        zzQV = zza.zzb("analytics.campaigns.time_limit", 86400000);
        zzQW = zza.zzm("analytics.first_party_experiment_id", BuildConfig.FLAVOR);
        zzQX = zza.zzd("analytics.first_party_experiment_variant", 0);
        zzQY = zza.zzd("analytics.test.disable_receiver", false);
        zzQZ = zza.zza("analytics.service_client.idle_disconnect_millis", 10000, 10000);
        zzRa = zza.zzb("analytics.service_client.connect_timeout_millis", 5000);
        zzRb = zza.zzb("analytics.service_client.second_connect_delay_millis", 5000);
        zzRc = zza.zzb("analytics.service_client.unexpected_reconnect_millis", 60000);
        zzRd = zza.zzb("analytics.service_client.reconnect_throttle_millis", 1800000);
        zzRe = zza.zzb("analytics.monitoring.sample_period_millis", 86400000);
        zzRf = zza.zzb("analytics.initialization_warning_threshold", 5000);
    }
}
