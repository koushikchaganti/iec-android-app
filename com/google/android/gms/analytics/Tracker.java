package com.google.android.gms.analytics;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.analytics.internal.zzab;
import com.google.android.gms.analytics.internal.zzad;
import com.google.android.gms.analytics.internal.zzal;
import com.google.android.gms.analytics.internal.zzam;
import com.google.android.gms.analytics.internal.zzd;
import com.google.android.gms.analytics.internal.zze;
import com.google.android.gms.analytics.internal.zzf;
import com.google.android.gms.analytics.internal.zzh;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzqh;
import com.google.android.gms.nearby.messages.Strategy;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

public class Tracker extends zzd {
    private boolean zzNY;
    private final Map<String, String> zzNZ;
    private final zzad zzOa;
    private final zza zzOb;
    private ExceptionReporter zzOc;
    private zzal zzOd;
    private final Map<String, String> zzxc;

    /* renamed from: com.google.android.gms.analytics.Tracker.1 */
    class C02611 implements Runnable {
        final /* synthetic */ Map zzOe;
        final /* synthetic */ boolean zzOf;
        final /* synthetic */ String zzOg;
        final /* synthetic */ long zzOh;
        final /* synthetic */ boolean zzOi;
        final /* synthetic */ boolean zzOj;
        final /* synthetic */ String zzOk;
        final /* synthetic */ Tracker zzOl;

        C02611(Tracker tracker, Map map, boolean z, String str, long j, boolean z2, boolean z3, String str2) {
            this.zzOl = tracker;
            this.zzOe = map;
            this.zzOf = z;
            this.zzOg = str;
            this.zzOh = j;
            this.zzOi = z2;
            this.zzOj = z3;
            this.zzOk = str2;
        }

        public void run() {
            boolean z = true;
            if (this.zzOl.zzOb.zziu()) {
                this.zzOe.put("sc", "start");
            }
            zzam.zzd(this.zzOe, "cid", this.zzOl.zzik().getClientId());
            String str = (String) this.zzOe.get("sf");
            if (str != null) {
                double zza = zzam.zza(str, 100.0d);
                if (zzam.zza(zza, (String) this.zzOe.get("cid"))) {
                    this.zzOl.zzb("Sampling enabled. Hit sampled out. sample rate", Double.valueOf(zza));
                    return;
                }
            }
            com.google.android.gms.analytics.internal.zza zzb = this.zzOl.zzja();
            if (this.zzOf) {
                zzam.zzb(this.zzOe, "ate", zzb.zziC());
                zzam.zzc(this.zzOe, "adid", zzb.zziG());
            } else {
                this.zzOe.remove("ate");
                this.zzOe.remove("adid");
            }
            zzqh zzjB = this.zzOl.zzjb().zzjB();
            zzam.zzc(this.zzOe, "an", zzjB.zzkP());
            zzam.zzc(this.zzOe, "av", zzjB.zzkR());
            zzam.zzc(this.zzOe, "aid", zzjB.zzwg());
            zzam.zzc(this.zzOe, "aiid", zzjB.zzzT());
            this.zzOe.put("v", "1");
            this.zzOe.put("_v", zze.zzOS);
            zzam.zzc(this.zzOe, "ul", this.zzOl.zzjc().zzkI().getLanguage());
            zzam.zzc(this.zzOe, "sr", this.zzOl.zzjc().zzkJ());
            boolean z2 = this.zzOg.equals("transaction") || this.zzOg.equals("item");
            if (z2 || this.zzOl.zzOa.zzlf()) {
                long zzbq = zzam.zzbq((String) this.zzOe.get("ht"));
                if (zzbq == 0) {
                    zzbq = this.zzOh;
                }
                if (this.zzOi) {
                    this.zzOl.zziU().zzc("Dry run enabled. Would have sent hit", new zzab(this.zzOl, this.zzOe, zzbq, this.zzOj));
                    return;
                }
                String str2 = (String) this.zzOe.get("cid");
                Map hashMap = new HashMap();
                zzam.zza(hashMap, "uid", this.zzOe);
                zzam.zza(hashMap, "an", this.zzOe);
                zzam.zza(hashMap, "aid", this.zzOe);
                zzam.zza(hashMap, "av", this.zzOe);
                zzam.zza(hashMap, "aiid", this.zzOe);
                String str3 = this.zzOk;
                if (TextUtils.isEmpty((CharSequence) this.zzOe.get("adid"))) {
                    z = false;
                }
                this.zzOe.put("_s", String.valueOf(this.zzOl.zzip().zza(new zzh(0, str2, str3, z, 0, hashMap))));
                this.zzOl.zzip().zza(new zzab(this.zzOl, this.zzOe, zzbq, this.zzOj));
                return;
            }
            this.zzOl.zziU().zzh(this.zzOe, "Too many hits sent too quickly, rate limiting invoked");
        }
    }

    private class zza extends zzd implements zza {
        final /* synthetic */ Tracker zzOl;
        private boolean zzOm;
        private int zzOn;
        private long zzOo;
        private boolean zzOp;
        private long zzOq;

        protected zza(Tracker tracker, zzf com_google_android_gms_analytics_internal_zzf) {
            this.zzOl = tracker;
            super(com_google_android_gms_analytics_internal_zzf);
            this.zzOo = -1;
        }

        private void zziv() {
            if (this.zzOo >= 0 || this.zzOm) {
                zzik().zza(this.zzOl.zzOb);
            } else {
                zzik().zzb(this.zzOl.zzOb);
            }
        }

        public void enableAutoActivityTracking(boolean enabled) {
            this.zzOm = enabled;
            zziv();
        }

        public void setSessionTimeout(long sessionTimeout) {
            this.zzOo = sessionTimeout;
            zziv();
        }

        protected void zzir() {
        }

        public synchronized boolean zziu() {
            boolean z;
            z = this.zzOp;
            this.zzOp = false;
            return z;
        }

        boolean zziw() {
            return zziT().elapsedRealtime() >= this.zzOq + Math.max(1000, this.zzOo);
        }

        public void zzn(Activity activity) {
            if (this.zzOn == 0 && zziw()) {
                this.zzOp = true;
            }
            this.zzOn++;
            if (this.zzOm) {
                Intent intent = activity.getIntent();
                if (intent != null) {
                    this.zzOl.setCampaignParamsOnNextHit(intent.getData());
                }
                Map hashMap = new HashMap();
                hashMap.put("&t", "screenview");
                this.zzOl.set("&cd", this.zzOl.zzOd != null ? this.zzOl.zzOd.zzq(activity) : activity.getClass().getCanonicalName());
                if (TextUtils.isEmpty((CharSequence) hashMap.get("&dr"))) {
                    CharSequence zzp = Tracker.zzp(activity);
                    if (!TextUtils.isEmpty(zzp)) {
                        hashMap.put("&dr", zzp);
                    }
                }
                this.zzOl.send(hashMap);
            }
        }

        public void zzo(Activity activity) {
            this.zzOn--;
            this.zzOn = Math.max(0, this.zzOn);
            if (this.zzOn == 0) {
                this.zzOq = zziT().elapsedRealtime();
            }
        }
    }

    Tracker(zzf analytics, String trackingId, zzad rateLimiter) {
        super(analytics);
        this.zzxc = new HashMap();
        this.zzNZ = new HashMap();
        if (trackingId != null) {
            this.zzxc.put("&tid", trackingId);
        }
        this.zzxc.put("useSecure", "1");
        this.zzxc.put("&a", Integer.toString(new Random().nextInt(Strategy.TTL_SECONDS_INFINITE) + 1));
        if (rateLimiter == null) {
            this.zzOa = new zzad("tracking");
        } else {
            this.zzOa = rateLimiter;
        }
        this.zzOb = new zza(this, analytics);
    }

    private static boolean zza(Entry<String, String> entry) {
        String str = (String) entry.getKey();
        String str2 = (String) entry.getValue();
        return str.startsWith("&") && str.length() >= 2;
    }

    private static String zzb(Entry<String, String> entry) {
        return !zza((Entry) entry) ? null : ((String) entry.getKey()).substring(1);
    }

    private static void zzb(Map<String, String> map, Map<String, String> map2) {
        zzx.zzy(map2);
        if (map != null) {
            for (Entry entry : map.entrySet()) {
                String zzb = zzb(entry);
                if (zzb != null) {
                    map2.put(zzb, entry.getValue());
                }
            }
        }
    }

    private static void zzc(Map<String, String> map, Map<String, String> map2) {
        zzx.zzy(map2);
        if (map != null) {
            for (Entry entry : map.entrySet()) {
                String zzb = zzb(entry);
                if (!(zzb == null || map2.containsKey(zzb))) {
                    map2.put(zzb, entry.getValue());
                }
            }
        }
    }

    private boolean zzis() {
        return this.zzOc != null;
    }

    static String zzp(Activity activity) {
        zzx.zzy(activity);
        Intent intent = activity.getIntent();
        if (intent == null) {
            return null;
        }
        CharSequence stringExtra = intent.getStringExtra("android.intent.extra.REFERRER_NAME");
        return !TextUtils.isEmpty(stringExtra) ? stringExtra : null;
    }

    public void enableAdvertisingIdCollection(boolean enabled) {
        this.zzNY = enabled;
    }

    public void enableAutoActivityTracking(boolean enabled) {
        this.zzOb.enableAutoActivityTracking(enabled);
    }

    public void enableExceptionReporting(boolean enable) {
        synchronized (this) {
            if (zzis() == enable) {
                return;
            }
            if (enable) {
                this.zzOc = new ExceptionReporter(this, Thread.getDefaultUncaughtExceptionHandler(), getContext());
                Thread.setDefaultUncaughtExceptionHandler(this.zzOc);
                zzba("Uncaught exceptions will be reported to Google Analytics");
            } else {
                Thread.setDefaultUncaughtExceptionHandler(this.zzOc.zzil());
                zzba("Uncaught exceptions will not be reported to Google Analytics");
            }
        }
    }

    public String get(String key) {
        zzje();
        if (TextUtils.isEmpty(key)) {
            return null;
        }
        if (this.zzxc.containsKey(key)) {
            return (String) this.zzxc.get(key);
        }
        if (key.equals("&ul")) {
            return zzam.zza(Locale.getDefault());
        }
        if (key.equals("&cid")) {
            return zziZ().zzjT();
        }
        if (key.equals("&sr")) {
            return zzjc().zzkJ();
        }
        if (key.equals("&aid")) {
            return zzjb().zzjB().zzwg();
        }
        if (key.equals("&an")) {
            return zzjb().zzjB().zzkP();
        }
        if (key.equals("&av")) {
            return zzjb().zzjB().zzkR();
        }
        return key.equals("&aiid") ? zzjb().zzjB().zzzT() : null;
    }

    public void send(Map<String, String> params) {
        long currentTimeMillis = zziT().currentTimeMillis();
        if (zzik().getAppOptOut()) {
            zzbb("AppOptOut is set to true. Not sending Google Analytics hit");
            return;
        }
        boolean isDryRunEnabled = zzik().isDryRunEnabled();
        Map hashMap = new HashMap();
        zzb(this.zzxc, hashMap);
        zzb(params, hashMap);
        boolean zze = zzam.zze((String) this.zzxc.get("useSecure"), true);
        zzc(this.zzNZ, hashMap);
        this.zzNZ.clear();
        String str = (String) hashMap.get("t");
        if (TextUtils.isEmpty(str)) {
            zziU().zzh(hashMap, "Missing hit type parameter");
            return;
        }
        String str2 = (String) hashMap.get("tid");
        if (TextUtils.isEmpty(str2)) {
            zziU().zzh(hashMap, "Missing tracking id parameter");
            return;
        }
        boolean zzit = zzit();
        synchronized (this) {
            if ("screenview".equalsIgnoreCase(str) || "pageview".equalsIgnoreCase(str) || "appview".equalsIgnoreCase(str) || TextUtils.isEmpty(str)) {
                int parseInt = Integer.parseInt((String) this.zzxc.get("&a")) + 1;
                if (parseInt >= Strategy.TTL_SECONDS_INFINITE) {
                    parseInt = 1;
                }
                this.zzxc.put("&a", Integer.toString(parseInt));
            }
        }
        zziW().zzf(new C02611(this, hashMap, zzit, str, currentTimeMillis, isDryRunEnabled, zze, str2));
    }

    public void set(String key, String value) {
        zzx.zzb((Object) key, (Object) "Key should be non-null");
        if (!TextUtils.isEmpty(key)) {
            this.zzxc.put(key, value);
        }
    }

    public void setAnonymizeIp(boolean anonymize) {
        set("&aip", zzam.zzK(anonymize));
    }

    public void setAppId(String appId) {
        set("&aid", appId);
    }

    public void setAppInstallerId(String appInstallerId) {
        set("&aiid", appInstallerId);
    }

    public void setAppName(String appName) {
        set("&an", appName);
    }

    public void setAppVersion(String appVersion) {
        set("&av", appVersion);
    }

    public void setCampaignParamsOnNextHit(Uri uri) {
        if (uri != null && !uri.isOpaque()) {
            Object queryParameter = uri.getQueryParameter("referrer");
            if (!TextUtils.isEmpty(queryParameter)) {
                Uri parse = Uri.parse("http://hostname/?" + queryParameter);
                String queryParameter2 = parse.getQueryParameter("utm_id");
                if (queryParameter2 != null) {
                    this.zzNZ.put("&ci", queryParameter2);
                }
                queryParameter2 = parse.getQueryParameter("anid");
                if (queryParameter2 != null) {
                    this.zzNZ.put("&anid", queryParameter2);
                }
                queryParameter2 = parse.getQueryParameter("utm_campaign");
                if (queryParameter2 != null) {
                    this.zzNZ.put("&cn", queryParameter2);
                }
                queryParameter2 = parse.getQueryParameter("utm_content");
                if (queryParameter2 != null) {
                    this.zzNZ.put("&cc", queryParameter2);
                }
                queryParameter2 = parse.getQueryParameter("utm_medium");
                if (queryParameter2 != null) {
                    this.zzNZ.put("&cm", queryParameter2);
                }
                queryParameter2 = parse.getQueryParameter("utm_source");
                if (queryParameter2 != null) {
                    this.zzNZ.put("&cs", queryParameter2);
                }
                queryParameter2 = parse.getQueryParameter("utm_term");
                if (queryParameter2 != null) {
                    this.zzNZ.put("&ck", queryParameter2);
                }
                queryParameter2 = parse.getQueryParameter("dclid");
                if (queryParameter2 != null) {
                    this.zzNZ.put("&dclid", queryParameter2);
                }
                queryParameter2 = parse.getQueryParameter("gclid");
                if (queryParameter2 != null) {
                    this.zzNZ.put("&gclid", queryParameter2);
                }
                String queryParameter3 = parse.getQueryParameter("aclid");
                if (queryParameter3 != null) {
                    this.zzNZ.put("&aclid", queryParameter3);
                }
            }
        }
    }

    public void setClientId(String clientId) {
        set("&cid", clientId);
    }

    public void setEncoding(String encoding) {
        set("&de", encoding);
    }

    public void setHostname(String hostname) {
        set("&dh", hostname);
    }

    public void setLanguage(String language) {
        set("&ul", language);
    }

    public void setLocation(String location) {
        set("&dl", location);
    }

    public void setPage(String page) {
        set("&dp", page);
    }

    public void setReferrer(String referrer) {
        set("&dr", referrer);
    }

    public void setSampleRate(double sampleRate) {
        set("&sf", Double.toString(sampleRate));
    }

    public void setScreenColors(String screenColors) {
        set("&sd", screenColors);
    }

    public void setScreenName(String screenName) {
        set("&cd", screenName);
    }

    public void setScreenResolution(int width, int height) {
        if (width >= 0 || height >= 0) {
            set("&sr", width + "x" + height);
        } else {
            zzbd("Invalid width or height. The values should be non-negative.");
        }
    }

    public void setSessionTimeout(long sessionTimeout) {
        this.zzOb.setSessionTimeout(1000 * sessionTimeout);
    }

    public void setTitle(String title) {
        set("&dt", title);
    }

    public void setUseSecure(boolean useSecure) {
        set("useSecure", zzam.zzK(useSecure));
    }

    public void setViewportSize(String viewportSize) {
        set("&vp", viewportSize);
    }

    void zza(zzal com_google_android_gms_analytics_internal_zzal) {
        zzba("Loading Tracker config values");
        this.zzOd = com_google_android_gms_analytics_internal_zzal;
        if (this.zzOd.zzlC()) {
            String trackingId = this.zzOd.getTrackingId();
            set("&tid", trackingId);
            zza("trackingId loaded", trackingId);
        }
        if (this.zzOd.zzlD()) {
            trackingId = Double.toString(this.zzOd.zzlE());
            set("&sf", trackingId);
            zza("Sample frequency loaded", trackingId);
        }
        if (this.zzOd.zzlF()) {
            int sessionTimeout = this.zzOd.getSessionTimeout();
            setSessionTimeout((long) sessionTimeout);
            zza("Session timeout loaded", Integer.valueOf(sessionTimeout));
        }
        if (this.zzOd.zzlG()) {
            boolean zzlH = this.zzOd.zzlH();
            enableAutoActivityTracking(zzlH);
            zza("Auto activity tracking loaded", Boolean.valueOf(zzlH));
        }
        if (this.zzOd.zzlI()) {
            zzlH = this.zzOd.zzlJ();
            if (zzlH) {
                set("&aip", "1");
            }
            zza("Anonymize ip loaded", Boolean.valueOf(zzlH));
        }
        enableExceptionReporting(this.zzOd.zzlK());
    }

    protected void zzir() {
        this.zzOb.zza();
        String zzkP = zziq().zzkP();
        if (zzkP != null) {
            set("&an", zzkP);
        }
        zzkP = zziq().zzkR();
        if (zzkP != null) {
            set("&av", zzkP);
        }
    }

    boolean zzit() {
        return this.zzNY;
    }
}
