package com.google.android.gms.analytics;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import com.google.android.gms.analytics.internal.zzaf;
import com.google.android.gms.analytics.internal.zzam;
import com.google.android.gms.analytics.internal.zzf;
import com.google.android.gms.analytics.internal.zzw;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzse;

public final class AnalyticsService extends Service {
    private static Boolean zzNu;
    private final Handler mHandler;

    /* renamed from: com.google.android.gms.analytics.AnalyticsService.1 */
    class C06761 implements zzw {
        final /* synthetic */ int zzNv;
        final /* synthetic */ zzf zzNw;
        final /* synthetic */ zzaf zzNx;
        final /* synthetic */ AnalyticsService zzNy;

        /* renamed from: com.google.android.gms.analytics.AnalyticsService.1.1 */
        class C02561 implements Runnable {
            final /* synthetic */ C06761 zzNz;

            C02561(C06761 c06761) {
                this.zzNz = c06761;
            }

            public void run() {
                if (!this.zzNz.zzNy.stopSelfResult(this.zzNz.zzNv)) {
                    return;
                }
                if (this.zzNz.zzNw.zziV().zzka()) {
                    this.zzNz.zzNx.zzba("Device AnalyticsService processed last dispatch request");
                } else {
                    this.zzNz.zzNx.zzba("Local AnalyticsService processed last dispatch request");
                }
            }
        }

        C06761(AnalyticsService analyticsService, int i, zzf com_google_android_gms_analytics_internal_zzf, zzaf com_google_android_gms_analytics_internal_zzaf) {
            this.zzNy = analyticsService;
            this.zzNv = i;
            this.zzNw = com_google_android_gms_analytics_internal_zzf;
            this.zzNx = com_google_android_gms_analytics_internal_zzaf;
        }

        public void zzc(Throwable th) {
            this.zzNy.mHandler.post(new C02561(this));
        }
    }

    public AnalyticsService() {
        this.mHandler = new Handler();
    }

    public static boolean zzY(Context context) {
        zzx.zzy(context);
        if (zzNu != null) {
            return zzNu.booleanValue();
        }
        boolean zza = zzam.zza(context, AnalyticsService.class);
        zzNu = Boolean.valueOf(zza);
        return zza;
    }

    private void zzih() {
        try {
            synchronized (AnalyticsReceiver.zzqf) {
                zzse com_google_android_gms_internal_zzse = AnalyticsReceiver.zzNs;
                if (com_google_android_gms_internal_zzse != null && com_google_android_gms_internal_zzse.isHeld()) {
                    com_google_android_gms_internal_zzse.release();
                }
            }
        } catch (SecurityException e) {
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        zzf zzZ = zzf.zzZ(this);
        zzaf zziU = zzZ.zziU();
        if (zzZ.zziV().zzka()) {
            zziU.zzba("Device AnalyticsService is starting up");
        } else {
            zziU.zzba("Local AnalyticsService is starting up");
        }
    }

    public void onDestroy() {
        zzf zzZ = zzf.zzZ(this);
        zzaf zziU = zzZ.zziU();
        if (zzZ.zziV().zzka()) {
            zziU.zzba("Device AnalyticsService is shutting down");
        } else {
            zziU.zzba("Local AnalyticsService is shutting down");
        }
        super.onDestroy();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        zzih();
        zzf zzZ = zzf.zzZ(this);
        zzaf zziU = zzZ.zziU();
        String action = intent.getAction();
        if (zzZ.zziV().zzka()) {
            zziU.zza("Device AnalyticsService called. startId, action", Integer.valueOf(startId), action);
        } else {
            zziU.zza("Local AnalyticsService called. startId, action", Integer.valueOf(startId), action);
        }
        if ("com.google.android.gms.analytics.ANALYTICS_DISPATCH".equals(action)) {
            zzZ.zzip().zza(new C06761(this, startId, zzZ, zziU));
        }
        return 2;
    }
}
