package com.google.android.gms.dynamic;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.internal.zzg;
import java.util.Iterator;
import java.util.LinkedList;

public abstract class zza<T extends LifecycleDelegate> {
    private T zzatr;
    private Bundle zzats;
    private LinkedList<zza> zzatt;
    private final zzf<T> zzatu;

    /* renamed from: com.google.android.gms.dynamic.zza.5 */
    static class C03125 implements OnClickListener {
        final /* synthetic */ Context zzsm;
        final /* synthetic */ int zzzC;

        C03125(Context context, int i) {
            this.zzsm = context;
            this.zzzC = i;
        }

        public void onClick(View v) {
            this.zzsm.startActivity(GooglePlayServicesUtil.zzbv(this.zzzC));
        }
    }

    private interface zza {
        int getState();

        void zzb(LifecycleDelegate lifecycleDelegate);
    }

    /* renamed from: com.google.android.gms.dynamic.zza.1 */
    class C07171 implements zzf<T> {
        final /* synthetic */ zza zzatv;

        C07171(zza com_google_android_gms_dynamic_zza) {
            this.zzatv = com_google_android_gms_dynamic_zza;
        }

        public void zza(T t) {
            this.zzatv.zzatr = t;
            Iterator it = this.zzatv.zzatt.iterator();
            while (it.hasNext()) {
                ((zza) it.next()).zzb(this.zzatv.zzatr);
            }
            this.zzatv.zzatt.clear();
            this.zzatv.zzats = null;
        }
    }

    /* renamed from: com.google.android.gms.dynamic.zza.2 */
    class C07182 implements zza {
        final /* synthetic */ zza zzatv;
        final /* synthetic */ Activity zzatw;
        final /* synthetic */ Bundle zzatx;
        final /* synthetic */ Bundle zzaty;

        C07182(zza com_google_android_gms_dynamic_zza, Activity activity, Bundle bundle, Bundle bundle2) {
            this.zzatv = com_google_android_gms_dynamic_zza;
            this.zzatw = activity;
            this.zzatx = bundle;
            this.zzaty = bundle2;
        }

        public int getState() {
            return 0;
        }

        public void zzb(LifecycleDelegate lifecycleDelegate) {
            this.zzatv.zzatr.onInflate(this.zzatw, this.zzatx, this.zzaty);
        }
    }

    /* renamed from: com.google.android.gms.dynamic.zza.3 */
    class C07193 implements zza {
        final /* synthetic */ zza zzatv;
        final /* synthetic */ Bundle zzaty;

        C07193(zza com_google_android_gms_dynamic_zza, Bundle bundle) {
            this.zzatv = com_google_android_gms_dynamic_zza;
            this.zzaty = bundle;
        }

        public int getState() {
            return 1;
        }

        public void zzb(LifecycleDelegate lifecycleDelegate) {
            this.zzatv.zzatr.onCreate(this.zzaty);
        }
    }

    /* renamed from: com.google.android.gms.dynamic.zza.4 */
    class C07204 implements zza {
        final /* synthetic */ LayoutInflater zzatA;
        final /* synthetic */ ViewGroup zzatB;
        final /* synthetic */ zza zzatv;
        final /* synthetic */ Bundle zzaty;
        final /* synthetic */ FrameLayout zzatz;

        C07204(zza com_google_android_gms_dynamic_zza, FrameLayout frameLayout, LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
            this.zzatv = com_google_android_gms_dynamic_zza;
            this.zzatz = frameLayout;
            this.zzatA = layoutInflater;
            this.zzatB = viewGroup;
            this.zzaty = bundle;
        }

        public int getState() {
            return 2;
        }

        public void zzb(LifecycleDelegate lifecycleDelegate) {
            this.zzatz.removeAllViews();
            this.zzatz.addView(this.zzatv.zzatr.onCreateView(this.zzatA, this.zzatB, this.zzaty));
        }
    }

    /* renamed from: com.google.android.gms.dynamic.zza.6 */
    class C07216 implements zza {
        final /* synthetic */ zza zzatv;

        C07216(zza com_google_android_gms_dynamic_zza) {
            this.zzatv = com_google_android_gms_dynamic_zza;
        }

        public int getState() {
            return 4;
        }

        public void zzb(LifecycleDelegate lifecycleDelegate) {
            this.zzatv.zzatr.onStart();
        }
    }

    /* renamed from: com.google.android.gms.dynamic.zza.7 */
    class C07227 implements zza {
        final /* synthetic */ zza zzatv;

        C07227(zza com_google_android_gms_dynamic_zza) {
            this.zzatv = com_google_android_gms_dynamic_zza;
        }

        public int getState() {
            return 5;
        }

        public void zzb(LifecycleDelegate lifecycleDelegate) {
            this.zzatv.zzatr.onResume();
        }
    }

    public zza() {
        this.zzatu = new C07171(this);
    }

    private void zza(Bundle bundle, zza com_google_android_gms_dynamic_zza_zza) {
        if (this.zzatr != null) {
            com_google_android_gms_dynamic_zza_zza.zzb(this.zzatr);
            return;
        }
        if (this.zzatt == null) {
            this.zzatt = new LinkedList();
        }
        this.zzatt.add(com_google_android_gms_dynamic_zza_zza);
        if (bundle != null) {
            if (this.zzats == null) {
                this.zzats = (Bundle) bundle.clone();
            } else {
                this.zzats.putAll(bundle);
            }
        }
        zza(this.zzatu);
    }

    public static void zzb(FrameLayout frameLayout) {
        Context context = frameLayout.getContext();
        int isGooglePlayServicesAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        CharSequence zzc = zzg.zzc(context, isGooglePlayServicesAvailable, GooglePlayServicesUtil.zzam(context));
        CharSequence zzh = zzg.zzh(context, isGooglePlayServicesAvailable);
        View linearLayout = new LinearLayout(frameLayout.getContext());
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(new LayoutParams(-2, -2));
        frameLayout.addView(linearLayout);
        View textView = new TextView(frameLayout.getContext());
        textView.setLayoutParams(new LayoutParams(-2, -2));
        textView.setText(zzc);
        linearLayout.addView(textView);
        if (zzh != null) {
            View button = new Button(context);
            button.setLayoutParams(new LayoutParams(-2, -2));
            button.setText(zzh);
            linearLayout.addView(button);
            button.setOnClickListener(new C03125(context, isGooglePlayServicesAvailable));
        }
    }

    private void zzeF(int i) {
        while (!this.zzatt.isEmpty() && ((zza) this.zzatt.getLast()).getState() >= i) {
            this.zzatt.removeLast();
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        zza(savedInstanceState, new C07193(this, savedInstanceState));
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FrameLayout frameLayout = new FrameLayout(inflater.getContext());
        zza(savedInstanceState, new C07204(this, frameLayout, inflater, container, savedInstanceState));
        if (this.zzatr == null) {
            zza(frameLayout);
        }
        return frameLayout;
    }

    public void onDestroy() {
        if (this.zzatr != null) {
            this.zzatr.onDestroy();
        } else {
            zzeF(1);
        }
    }

    public void onDestroyView() {
        if (this.zzatr != null) {
            this.zzatr.onDestroyView();
        } else {
            zzeF(2);
        }
    }

    public void onInflate(Activity activity, Bundle attrs, Bundle savedInstanceState) {
        zza(savedInstanceState, new C07182(this, activity, attrs, savedInstanceState));
    }

    public void onLowMemory() {
        if (this.zzatr != null) {
            this.zzatr.onLowMemory();
        }
    }

    public void onPause() {
        if (this.zzatr != null) {
            this.zzatr.onPause();
        } else {
            zzeF(5);
        }
    }

    public void onResume() {
        zza(null, new C07227(this));
    }

    public void onSaveInstanceState(Bundle outState) {
        if (this.zzatr != null) {
            this.zzatr.onSaveInstanceState(outState);
        } else if (this.zzats != null) {
            outState.putAll(this.zzats);
        }
    }

    public void onStart() {
        zza(null, new C07216(this));
    }

    public void onStop() {
        if (this.zzatr != null) {
            this.zzatr.onStop();
        } else {
            zzeF(4);
        }
    }

    protected void zza(FrameLayout frameLayout) {
        zzb(frameLayout);
    }

    protected abstract void zza(zzf<T> com_google_android_gms_dynamic_zzf_T);

    public T zzts() {
        return this.zzatr;
    }
}
