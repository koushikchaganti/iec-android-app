package com.google.android.gms.cast;

import com.google.android.gms.cast.Cast.MessageReceivedCallback;
import com.google.android.gms.cast.internal.zze;
import com.google.android.gms.cast.internal.zzm;
import com.google.android.gms.cast.internal.zzn;
import com.google.android.gms.cast.internal.zzo;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.GamesStatusCodes;
import java.io.IOException;
import java.util.Locale;
import org.json.JSONObject;

public class RemoteMediaPlayer implements MessageReceivedCallback {
    public static final int RESUME_STATE_PAUSE = 2;
    public static final int RESUME_STATE_PLAY = 1;
    public static final int RESUME_STATE_UNCHANGED = 0;
    public static final int STATUS_CANCELED = 2101;
    public static final int STATUS_FAILED = 2100;
    public static final int STATUS_REPLACED = 2103;
    public static final int STATUS_SUCCEEDED = 0;
    public static final int STATUS_TIMED_OUT = 2102;
    private final zzm zzZX;
    private final zza zzZY;
    private OnPreloadStatusUpdatedListener zzZZ;
    private OnQueueStatusUpdatedListener zzaaa;
    private OnMetadataUpdatedListener zzaab;
    private OnStatusUpdatedListener zzaac;
    private final Object zzpK;

    public interface OnMetadataUpdatedListener {
        void onMetadataUpdated();
    }

    public interface OnPreloadStatusUpdatedListener {
        void onPreloadStatusUpdated();
    }

    public interface OnQueueStatusUpdatedListener {
        void onQueueStatusUpdated();
    }

    public interface OnStatusUpdatedListener {
        void onStatusUpdated();
    }

    public interface MediaChannelResult extends Result {
        JSONObject getCustomData();
    }

    private class zza implements zzn {
        private GoogleApiClient zzaaB;
        private long zzaaC;
        final /* synthetic */ RemoteMediaPlayer zzaad;

        private final class zza implements ResultCallback<Status> {
            private final long zzaaD;
            final /* synthetic */ zza zzaaE;

            zza(zza com_google_android_gms_cast_RemoteMediaPlayer_zza, long j) {
                this.zzaaE = com_google_android_gms_cast_RemoteMediaPlayer_zza;
                this.zzaaD = j;
            }

            public /* synthetic */ void onResult(Result x0) {
                zzp((Status) x0);
            }

            public void zzp(Status status) {
                if (!status.isSuccess()) {
                    this.zzaaE.zzaad.zzZX.zzb(this.zzaaD, status.getStatusCode());
                }
            }
        }

        public zza(RemoteMediaPlayer remoteMediaPlayer) {
            this.zzaad = remoteMediaPlayer;
            this.zzaaC = 0;
        }

        public void zza(String str, String str2, long j, String str3) throws IOException {
            if (this.zzaaB == null) {
                throw new IOException("No GoogleApiClient available");
            }
            Cast.CastApi.sendMessage(this.zzaaB, str, str2).setResultCallback(new zza(this, j));
        }

        public void zzb(GoogleApiClient googleApiClient) {
            this.zzaaB = googleApiClient;
        }

        public long zzny() {
            long j = this.zzaaC + 1;
            this.zzaaC = j;
            return j;
        }
    }

    private static final class zzc implements MediaChannelResult {
        private final Status zzTA;
        private final JSONObject zzZn;

        zzc(Status status, JSONObject jSONObject) {
            this.zzTA = status;
            this.zzZn = jSONObject;
        }

        public JSONObject getCustomData() {
            return this.zzZn;
        }

        public Status getStatus() {
            return this.zzTA;
        }
    }

    /* renamed from: com.google.android.gms.cast.RemoteMediaPlayer.1 */
    class C11961 extends zzm {
        final /* synthetic */ RemoteMediaPlayer zzaad;

        C11961(RemoteMediaPlayer remoteMediaPlayer, String str) {
            this.zzaad = remoteMediaPlayer;
            super(str);
        }

        protected void onMetadataUpdated() {
            this.zzaad.onMetadataUpdated();
        }

        protected void onPreloadStatusUpdated() {
            this.zzaad.onPreloadStatusUpdated();
        }

        protected void onQueueStatusUpdated() {
            this.zzaad.onQueueStatusUpdated();
        }

        protected void onStatusUpdated() {
            this.zzaad.onStatusUpdated();
        }
    }

    private static abstract class zzb extends com.google.android.gms.cast.internal.zzb<MediaChannelResult> {
        zzo zzaaF;

        /* renamed from: com.google.android.gms.cast.RemoteMediaPlayer.zzb.1 */
        class C07001 implements zzo {
            final /* synthetic */ zzb zzaaG;

            C07001(zzb com_google_android_gms_cast_RemoteMediaPlayer_zzb) {
                this.zzaaG = com_google_android_gms_cast_RemoteMediaPlayer_zzb;
            }

            public void zza(long j, int i, Object obj) {
                this.zzaaG.zzb(new zzc(new Status(i), obj instanceof JSONObject ? (JSONObject) obj : null));
            }

            public void zzy(long j) {
                this.zzaaG.zzb(this.zzaaG.zzq(new Status(RemoteMediaPlayer.STATUS_REPLACED)));
            }
        }

        /* renamed from: com.google.android.gms.cast.RemoteMediaPlayer.zzb.2 */
        class C08962 implements MediaChannelResult {
            final /* synthetic */ Status zzYl;
            final /* synthetic */ zzb zzaaG;

            C08962(zzb com_google_android_gms_cast_RemoteMediaPlayer_zzb, Status status) {
                this.zzaaG = com_google_android_gms_cast_RemoteMediaPlayer_zzb;
                this.zzYl = status;
            }

            public JSONObject getCustomData() {
                return null;
            }

            public Status getStatus() {
                return this.zzYl;
            }
        }

        zzb(GoogleApiClient googleApiClient) {
            super(googleApiClient);
            this.zzaaF = new C07001(this);
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzq(status);
        }

        public MediaChannelResult zzq(Status status) {
            return new C08962(this, status);
        }
    }

    /* renamed from: com.google.android.gms.cast.RemoteMediaPlayer.10 */
    class AnonymousClass10 extends zzb {
        final /* synthetic */ RemoteMediaPlayer zzaad;
        final /* synthetic */ GoogleApiClient zzaae;
        final /* synthetic */ JSONObject zzaal;

        AnonymousClass10(RemoteMediaPlayer remoteMediaPlayer, GoogleApiClient googleApiClient, GoogleApiClient googleApiClient2, JSONObject jSONObject) {
            this.zzaad = remoteMediaPlayer;
            this.zzaae = googleApiClient2;
            this.zzaal = jSONObject;
            super(googleApiClient);
        }

        protected void zza(zze com_google_android_gms_cast_internal_zze) {
            synchronized (this.zzaad.zzpK) {
                this.zzaad.zzZY.zzb(this.zzaae);
                try {
                    this.zzaad.zzZX.zza(this.zzaaF, (int) RemoteMediaPlayer.STATUS_SUCCEEDED, -1, null, -1, null, this.zzaal);
                    this.zzaad.zzZY.zzb(null);
                } catch (IOException e) {
                    zzb(zzq(new Status(RemoteMediaPlayer.STATUS_FAILED)));
                    this.zzaad.zzZY.zzb(null);
                } catch (Throwable th) {
                    this.zzaad.zzZY.zzb(null);
                }
            }
        }
    }

    /* renamed from: com.google.android.gms.cast.RemoteMediaPlayer.11 */
    class AnonymousClass11 extends zzb {
        final /* synthetic */ RemoteMediaPlayer zzaad;
        final /* synthetic */ GoogleApiClient zzaae;
        final /* synthetic */ JSONObject zzaal;

        AnonymousClass11(RemoteMediaPlayer remoteMediaPlayer, GoogleApiClient googleApiClient, GoogleApiClient googleApiClient2, JSONObject jSONObject) {
            this.zzaad = remoteMediaPlayer;
            this.zzaae = googleApiClient2;
            this.zzaal = jSONObject;
            super(googleApiClient);
        }

        protected void zza(zze com_google_android_gms_cast_internal_zze) {
            synchronized (this.zzaad.zzpK) {
                this.zzaad.zzZY.zzb(this.zzaae);
                try {
                    this.zzaad.zzZX.zza(this.zzaaF, (int) RemoteMediaPlayer.STATUS_SUCCEEDED, -1, null, (int) RemoteMediaPlayer.RESUME_STATE_PLAY, null, this.zzaal);
                    this.zzaad.zzZY.zzb(null);
                } catch (IOException e) {
                    zzb(zzq(new Status(RemoteMediaPlayer.STATUS_FAILED)));
                    this.zzaad.zzZY.zzb(null);
                } catch (Throwable th) {
                    this.zzaad.zzZY.zzb(null);
                }
            }
        }
    }

    /* renamed from: com.google.android.gms.cast.RemoteMediaPlayer.12 */
    class AnonymousClass12 extends zzb {
        final /* synthetic */ RemoteMediaPlayer zzaad;
        final /* synthetic */ GoogleApiClient zzaae;
        final /* synthetic */ long zzaak;
        final /* synthetic */ JSONObject zzaal;
        final /* synthetic */ MediaInfo zzaas;
        final /* synthetic */ boolean zzaat;
        final /* synthetic */ long[] zzaau;

        AnonymousClass12(RemoteMediaPlayer remoteMediaPlayer, GoogleApiClient googleApiClient, GoogleApiClient googleApiClient2, MediaInfo mediaInfo, boolean z, long j, long[] jArr, JSONObject jSONObject) {
            this.zzaad = remoteMediaPlayer;
            this.zzaae = googleApiClient2;
            this.zzaas = mediaInfo;
            this.zzaat = z;
            this.zzaak = j;
            this.zzaau = jArr;
            this.zzaal = jSONObject;
            super(googleApiClient);
        }

        protected void zza(zze com_google_android_gms_cast_internal_zze) {
            synchronized (this.zzaad.zzpK) {
                this.zzaad.zzZY.zzb(this.zzaae);
                try {
                    this.zzaad.zzZX.zza(this.zzaaF, this.zzaas, this.zzaat, this.zzaak, this.zzaau, this.zzaal);
                    this.zzaad.zzZY.zzb(null);
                } catch (IOException e) {
                    zzb(zzq(new Status(RemoteMediaPlayer.STATUS_FAILED)));
                    this.zzaad.zzZY.zzb(null);
                } catch (Throwable th) {
                    this.zzaad.zzZY.zzb(null);
                }
            }
        }
    }

    /* renamed from: com.google.android.gms.cast.RemoteMediaPlayer.13 */
    class AnonymousClass13 extends zzb {
        final /* synthetic */ RemoteMediaPlayer zzaad;
        final /* synthetic */ GoogleApiClient zzaae;
        final /* synthetic */ int zzaaj;
        final /* synthetic */ JSONObject zzaal;

        AnonymousClass13(RemoteMediaPlayer remoteMediaPlayer, GoogleApiClient googleApiClient, GoogleApiClient googleApiClient2, int i, JSONObject jSONObject) {
            this.zzaad = remoteMediaPlayer;
            this.zzaae = googleApiClient2;
            this.zzaaj = i;
            this.zzaal = jSONObject;
            super(googleApiClient);
        }

        protected void zza(zze com_google_android_gms_cast_internal_zze) {
            synchronized (this.zzaad.zzpK) {
                this.zzaad.zzZY.zzb(this.zzaae);
                try {
                    this.zzaad.zzZX.zza(this.zzaaF, (int) RemoteMediaPlayer.STATUS_SUCCEEDED, -1, null, (int) RemoteMediaPlayer.STATUS_SUCCEEDED, Integer.valueOf(this.zzaaj), this.zzaal);
                    this.zzaad.zzZY.zzb(null);
                } catch (IOException e) {
                    zzb(zzq(new Status(RemoteMediaPlayer.STATUS_FAILED)));
                    this.zzaad.zzZY.zzb(null);
                } catch (Throwable th) {
                    this.zzaad.zzZY.zzb(null);
                }
            }
        }
    }

    /* renamed from: com.google.android.gms.cast.RemoteMediaPlayer.14 */
    class AnonymousClass14 extends zzb {
        final /* synthetic */ RemoteMediaPlayer zzaad;
        final /* synthetic */ GoogleApiClient zzaae;
        final /* synthetic */ JSONObject zzaal;
        final /* synthetic */ int zzaav;

        AnonymousClass14(RemoteMediaPlayer remoteMediaPlayer, GoogleApiClient googleApiClient, int i, GoogleApiClient googleApiClient2, JSONObject jSONObject) {
            this.zzaad = remoteMediaPlayer;
            this.zzaav = i;
            this.zzaae = googleApiClient2;
            this.zzaal = jSONObject;
            super(googleApiClient);
        }

        protected void zza(zze com_google_android_gms_cast_internal_zze) {
            synchronized (this.zzaad.zzpK) {
                if (this.zzaad.zzbf(this.zzaav) == -1) {
                    zzb(zzq(new Status(RemoteMediaPlayer.STATUS_SUCCEEDED)));
                    return;
                }
                this.zzaad.zzZY.zzb(this.zzaae);
                try {
                    zzm zzg = this.zzaad.zzZX;
                    zzo com_google_android_gms_cast_internal_zzo = this.zzaaF;
                    int[] iArr = new int[RemoteMediaPlayer.RESUME_STATE_PLAY];
                    iArr[RemoteMediaPlayer.STATUS_SUCCEEDED] = this.zzaav;
                    zzg.zza(com_google_android_gms_cast_internal_zzo, iArr, this.zzaal);
                } catch (IOException e) {
                    zzb(zzq(new Status(RemoteMediaPlayer.STATUS_FAILED)));
                } finally {
                    this.zzaad.zzZY.zzb(null);
                }
            }
        }
    }

    /* renamed from: com.google.android.gms.cast.RemoteMediaPlayer.15 */
    class AnonymousClass15 extends zzb {
        final /* synthetic */ RemoteMediaPlayer zzaad;
        final /* synthetic */ GoogleApiClient zzaae;
        final /* synthetic */ long zzaak;
        final /* synthetic */ JSONObject zzaal;
        final /* synthetic */ int zzaav;

        AnonymousClass15(RemoteMediaPlayer remoteMediaPlayer, GoogleApiClient googleApiClient, int i, GoogleApiClient googleApiClient2, long j, JSONObject jSONObject) {
            this.zzaad = remoteMediaPlayer;
            this.zzaav = i;
            this.zzaae = googleApiClient2;
            this.zzaak = j;
            this.zzaal = jSONObject;
            super(googleApiClient);
        }

        protected void zza(zze com_google_android_gms_cast_internal_zze) {
            synchronized (this.zzaad.zzpK) {
                if (this.zzaad.zzbf(this.zzaav) == -1) {
                    zzb(zzq(new Status(RemoteMediaPlayer.STATUS_SUCCEEDED)));
                    return;
                }
                this.zzaad.zzZY.zzb(this.zzaae);
                try {
                    this.zzaad.zzZX.zza(this.zzaaF, this.zzaav, this.zzaak, null, (int) RemoteMediaPlayer.STATUS_SUCCEEDED, null, this.zzaal);
                } catch (IOException e) {
                    zzb(zzq(new Status(RemoteMediaPlayer.STATUS_FAILED)));
                } finally {
                    this.zzaad.zzZY.zzb(null);
                }
            }
        }
    }

    /* renamed from: com.google.android.gms.cast.RemoteMediaPlayer.16 */
    class AnonymousClass16 extends zzb {
        final /* synthetic */ RemoteMediaPlayer zzaad;
        final /* synthetic */ GoogleApiClient zzaae;
        final /* synthetic */ JSONObject zzaal;
        final /* synthetic */ int zzaav;
        final /* synthetic */ int zzaaw;

        AnonymousClass16(RemoteMediaPlayer remoteMediaPlayer, GoogleApiClient googleApiClient, int i, int i2, GoogleApiClient googleApiClient2, JSONObject jSONObject) {
            this.zzaad = remoteMediaPlayer;
            this.zzaav = i;
            this.zzaaw = i2;
            this.zzaae = googleApiClient2;
            this.zzaal = jSONObject;
            super(googleApiClient);
        }

        protected void zza(zze com_google_android_gms_cast_internal_zze) {
            int i = RemoteMediaPlayer.STATUS_SUCCEEDED;
            synchronized (this.zzaad.zzpK) {
                int zza = this.zzaad.zzbf(this.zzaav);
                if (zza == -1) {
                    zzb(zzq(new Status(RemoteMediaPlayer.STATUS_SUCCEEDED)));
                } else if (this.zzaaw < 0) {
                    Object[] objArr = new Object[RemoteMediaPlayer.RESUME_STATE_PLAY];
                    objArr[RemoteMediaPlayer.STATUS_SUCCEEDED] = Integer.valueOf(this.zzaaw);
                    zzb(zzq(new Status(GamesStatusCodes.STATUS_REQUEST_UPDATE_TOTAL_FAILURE, String.format(Locale.ROOT, "Invalid request: Invalid newIndex %d.", objArr))));
                } else if (zza == this.zzaaw) {
                    zzb(zzq(new Status(RemoteMediaPlayer.STATUS_SUCCEEDED)));
                } else {
                    MediaQueueItem queueItem = this.zzaad.getMediaStatus().getQueueItem(this.zzaaw > zza ? this.zzaaw + RemoteMediaPlayer.RESUME_STATE_PLAY : this.zzaaw);
                    if (queueItem != null) {
                        i = queueItem.getItemId();
                    }
                    this.zzaad.zzZY.zzb(this.zzaae);
                    try {
                        zzm zzg = this.zzaad.zzZX;
                        zzo com_google_android_gms_cast_internal_zzo = this.zzaaF;
                        int[] iArr = new int[RemoteMediaPlayer.RESUME_STATE_PLAY];
                        iArr[RemoteMediaPlayer.STATUS_SUCCEEDED] = this.zzaav;
                        zzg.zza(com_google_android_gms_cast_internal_zzo, iArr, i, this.zzaal);
                    } catch (IOException e) {
                        zzb(zzq(new Status(RemoteMediaPlayer.STATUS_FAILED)));
                    } finally {
                        this.zzaad.zzZY.zzb(null);
                    }
                }
            }
        }
    }

    /* renamed from: com.google.android.gms.cast.RemoteMediaPlayer.17 */
    class AnonymousClass17 extends zzb {
        final /* synthetic */ RemoteMediaPlayer zzaad;
        final /* synthetic */ GoogleApiClient zzaae;
        final /* synthetic */ JSONObject zzaal;

        AnonymousClass17(RemoteMediaPlayer remoteMediaPlayer, GoogleApiClient googleApiClient, GoogleApiClient googleApiClient2, JSONObject jSONObject) {
            this.zzaad = remoteMediaPlayer;
            this.zzaae = googleApiClient2;
            this.zzaal = jSONObject;
            super(googleApiClient);
        }

        protected void zza(zze com_google_android_gms_cast_internal_zze) {
            synchronized (this.zzaad.zzpK) {
                this.zzaad.zzZY.zzb(this.zzaae);
                try {
                    this.zzaad.zzZX.zza(this.zzaaF, this.zzaal);
                    this.zzaad.zzZY.zzb(null);
                } catch (IOException e) {
                    zzb(zzq(new Status(RemoteMediaPlayer.STATUS_FAILED)));
                    this.zzaad.zzZY.zzb(null);
                } catch (Throwable th) {
                    this.zzaad.zzZY.zzb(null);
                }
            }
        }
    }

    /* renamed from: com.google.android.gms.cast.RemoteMediaPlayer.18 */
    class AnonymousClass18 extends zzb {
        final /* synthetic */ RemoteMediaPlayer zzaad;
        final /* synthetic */ GoogleApiClient zzaae;
        final /* synthetic */ JSONObject zzaal;

        AnonymousClass18(RemoteMediaPlayer remoteMediaPlayer, GoogleApiClient googleApiClient, GoogleApiClient googleApiClient2, JSONObject jSONObject) {
            this.zzaad = remoteMediaPlayer;
            this.zzaae = googleApiClient2;
            this.zzaal = jSONObject;
            super(googleApiClient);
        }

        protected void zza(zze com_google_android_gms_cast_internal_zze) {
            synchronized (this.zzaad.zzpK) {
                this.zzaad.zzZY.zzb(this.zzaae);
                try {
                    this.zzaad.zzZX.zzb(this.zzaaF, this.zzaal);
                    this.zzaad.zzZY.zzb(null);
                } catch (IOException e) {
                    zzb(zzq(new Status(RemoteMediaPlayer.STATUS_FAILED)));
                    this.zzaad.zzZY.zzb(null);
                } catch (Throwable th) {
                    this.zzaad.zzZY.zzb(null);
                }
            }
        }
    }

    /* renamed from: com.google.android.gms.cast.RemoteMediaPlayer.19 */
    class AnonymousClass19 extends zzb {
        final /* synthetic */ RemoteMediaPlayer zzaad;
        final /* synthetic */ GoogleApiClient zzaae;
        final /* synthetic */ JSONObject zzaal;

        AnonymousClass19(RemoteMediaPlayer remoteMediaPlayer, GoogleApiClient googleApiClient, GoogleApiClient googleApiClient2, JSONObject jSONObject) {
            this.zzaad = remoteMediaPlayer;
            this.zzaae = googleApiClient2;
            this.zzaal = jSONObject;
            super(googleApiClient);
        }

        protected void zza(zze com_google_android_gms_cast_internal_zze) {
            synchronized (this.zzaad.zzpK) {
                this.zzaad.zzZY.zzb(this.zzaae);
                try {
                    this.zzaad.zzZX.zzc(this.zzaaF, this.zzaal);
                    this.zzaad.zzZY.zzb(null);
                } catch (IOException e) {
                    zzb(zzq(new Status(RemoteMediaPlayer.STATUS_FAILED)));
                    this.zzaad.zzZY.zzb(null);
                } catch (Throwable th) {
                    this.zzaad.zzZY.zzb(null);
                }
            }
        }
    }

    /* renamed from: com.google.android.gms.cast.RemoteMediaPlayer.20 */
    class AnonymousClass20 extends zzb {
        final /* synthetic */ RemoteMediaPlayer zzaad;
        final /* synthetic */ GoogleApiClient zzaae;
        final /* synthetic */ JSONObject zzaal;
        final /* synthetic */ long zzaax;
        final /* synthetic */ int zzaay;

        AnonymousClass20(RemoteMediaPlayer remoteMediaPlayer, GoogleApiClient googleApiClient, GoogleApiClient googleApiClient2, long j, int i, JSONObject jSONObject) {
            this.zzaad = remoteMediaPlayer;
            this.zzaae = googleApiClient2;
            this.zzaax = j;
            this.zzaay = i;
            this.zzaal = jSONObject;
            super(googleApiClient);
        }

        protected void zza(zze com_google_android_gms_cast_internal_zze) {
            synchronized (this.zzaad.zzpK) {
                this.zzaad.zzZY.zzb(this.zzaae);
                try {
                    this.zzaad.zzZX.zza(this.zzaaF, this.zzaax, this.zzaay, this.zzaal);
                    this.zzaad.zzZY.zzb(null);
                } catch (IOException e) {
                    zzb(zzq(new Status(RemoteMediaPlayer.STATUS_FAILED)));
                    this.zzaad.zzZY.zzb(null);
                } catch (Throwable th) {
                    this.zzaad.zzZY.zzb(null);
                }
            }
        }
    }

    /* renamed from: com.google.android.gms.cast.RemoteMediaPlayer.21 */
    class AnonymousClass21 extends zzb {
        final /* synthetic */ RemoteMediaPlayer zzaad;
        final /* synthetic */ GoogleApiClient zzaae;
        final /* synthetic */ JSONObject zzaal;
        final /* synthetic */ double zzaaz;

        AnonymousClass21(RemoteMediaPlayer remoteMediaPlayer, GoogleApiClient googleApiClient, GoogleApiClient googleApiClient2, double d, JSONObject jSONObject) {
            this.zzaad = remoteMediaPlayer;
            this.zzaae = googleApiClient2;
            this.zzaaz = d;
            this.zzaal = jSONObject;
            super(googleApiClient);
        }

        protected void zza(zze com_google_android_gms_cast_internal_zze) {
            synchronized (this.zzaad.zzpK) {
                this.zzaad.zzZY.zzb(this.zzaae);
                try {
                    this.zzaad.zzZX.zza(this.zzaaF, this.zzaaz, this.zzaal);
                    this.zzaad.zzZY.zzb(null);
                } catch (IllegalStateException e) {
                    try {
                        zzb(zzq(new Status(RemoteMediaPlayer.STATUS_FAILED)));
                        this.zzaad.zzZY.zzb(null);
                    } catch (Throwable th) {
                        this.zzaad.zzZY.zzb(null);
                    }
                } catch (IllegalArgumentException e2) {
                    zzb(zzq(new Status(RemoteMediaPlayer.STATUS_FAILED)));
                    this.zzaad.zzZY.zzb(null);
                } catch (IOException e3) {
                    zzb(zzq(new Status(RemoteMediaPlayer.STATUS_FAILED)));
                    this.zzaad.zzZY.zzb(null);
                }
            }
        }
    }

    /* renamed from: com.google.android.gms.cast.RemoteMediaPlayer.22 */
    class AnonymousClass22 extends zzb {
        final /* synthetic */ boolean zzaaA;
        final /* synthetic */ RemoteMediaPlayer zzaad;
        final /* synthetic */ GoogleApiClient zzaae;
        final /* synthetic */ JSONObject zzaal;

        AnonymousClass22(RemoteMediaPlayer remoteMediaPlayer, GoogleApiClient googleApiClient, GoogleApiClient googleApiClient2, boolean z, JSONObject jSONObject) {
            this.zzaad = remoteMediaPlayer;
            this.zzaae = googleApiClient2;
            this.zzaaA = z;
            this.zzaal = jSONObject;
            super(googleApiClient);
        }

        protected void zza(zze com_google_android_gms_cast_internal_zze) {
            synchronized (this.zzaad.zzpK) {
                this.zzaad.zzZY.zzb(this.zzaae);
                try {
                    this.zzaad.zzZX.zza(this.zzaaF, this.zzaaA, this.zzaal);
                    this.zzaad.zzZY.zzb(null);
                } catch (IllegalStateException e) {
                    try {
                        zzb(zzq(new Status(RemoteMediaPlayer.STATUS_FAILED)));
                        this.zzaad.zzZY.zzb(null);
                    } catch (Throwable th) {
                        this.zzaad.zzZY.zzb(null);
                    }
                } catch (IOException e2) {
                    zzb(zzq(new Status(RemoteMediaPlayer.STATUS_FAILED)));
                    this.zzaad.zzZY.zzb(null);
                }
            }
        }
    }

    /* renamed from: com.google.android.gms.cast.RemoteMediaPlayer.23 */
    class AnonymousClass23 extends zzb {
        final /* synthetic */ RemoteMediaPlayer zzaad;
        final /* synthetic */ GoogleApiClient zzaae;

        AnonymousClass23(RemoteMediaPlayer remoteMediaPlayer, GoogleApiClient googleApiClient, GoogleApiClient googleApiClient2) {
            this.zzaad = remoteMediaPlayer;
            this.zzaae = googleApiClient2;
            super(googleApiClient);
        }

        protected void zza(zze com_google_android_gms_cast_internal_zze) {
            synchronized (this.zzaad.zzpK) {
                this.zzaad.zzZY.zzb(this.zzaae);
                try {
                    this.zzaad.zzZX.zza(this.zzaaF);
                    this.zzaad.zzZY.zzb(null);
                } catch (IOException e) {
                    zzb(zzq(new Status(RemoteMediaPlayer.STATUS_FAILED)));
                    this.zzaad.zzZY.zzb(null);
                } catch (Throwable th) {
                    this.zzaad.zzZY.zzb(null);
                }
            }
        }
    }

    /* renamed from: com.google.android.gms.cast.RemoteMediaPlayer.2 */
    class C12792 extends zzb {
        final /* synthetic */ RemoteMediaPlayer zzaad;
        final /* synthetic */ GoogleApiClient zzaae;
        final /* synthetic */ long[] zzaaf;

        C12792(RemoteMediaPlayer remoteMediaPlayer, GoogleApiClient googleApiClient, GoogleApiClient googleApiClient2, long[] jArr) {
            this.zzaad = remoteMediaPlayer;
            this.zzaae = googleApiClient2;
            this.zzaaf = jArr;
            super(googleApiClient);
        }

        protected void zza(zze com_google_android_gms_cast_internal_zze) {
            synchronized (this.zzaad.zzpK) {
                this.zzaad.zzZY.zzb(this.zzaae);
                try {
                    this.zzaad.zzZX.zza(this.zzaaF, this.zzaaf);
                    this.zzaad.zzZY.zzb(null);
                } catch (IOException e) {
                    zzb(zzq(new Status(RemoteMediaPlayer.STATUS_FAILED)));
                    this.zzaad.zzZY.zzb(null);
                } catch (Throwable th) {
                    this.zzaad.zzZY.zzb(null);
                }
            }
        }
    }

    /* renamed from: com.google.android.gms.cast.RemoteMediaPlayer.3 */
    class C12803 extends zzb {
        final /* synthetic */ RemoteMediaPlayer zzaad;
        final /* synthetic */ GoogleApiClient zzaae;
        final /* synthetic */ TextTrackStyle zzaag;

        C12803(RemoteMediaPlayer remoteMediaPlayer, GoogleApiClient googleApiClient, GoogleApiClient googleApiClient2, TextTrackStyle textTrackStyle) {
            this.zzaad = remoteMediaPlayer;
            this.zzaae = googleApiClient2;
            this.zzaag = textTrackStyle;
            super(googleApiClient);
        }

        protected void zza(zze com_google_android_gms_cast_internal_zze) {
            synchronized (this.zzaad.zzpK) {
                this.zzaad.zzZY.zzb(this.zzaae);
                try {
                    this.zzaad.zzZX.zza(this.zzaaF, this.zzaag);
                    this.zzaad.zzZY.zzb(null);
                } catch (IOException e) {
                    zzb(zzq(new Status(RemoteMediaPlayer.STATUS_FAILED)));
                    this.zzaad.zzZY.zzb(null);
                } catch (Throwable th) {
                    this.zzaad.zzZY.zzb(null);
                }
            }
        }
    }

    /* renamed from: com.google.android.gms.cast.RemoteMediaPlayer.4 */
    class C12814 extends zzb {
        final /* synthetic */ RemoteMediaPlayer zzaad;
        final /* synthetic */ GoogleApiClient zzaae;
        final /* synthetic */ MediaQueueItem[] zzaah;
        final /* synthetic */ int zzaai;
        final /* synthetic */ int zzaaj;
        final /* synthetic */ long zzaak;
        final /* synthetic */ JSONObject zzaal;

        C12814(RemoteMediaPlayer remoteMediaPlayer, GoogleApiClient googleApiClient, GoogleApiClient googleApiClient2, MediaQueueItem[] mediaQueueItemArr, int i, int i2, long j, JSONObject jSONObject) {
            this.zzaad = remoteMediaPlayer;
            this.zzaae = googleApiClient2;
            this.zzaah = mediaQueueItemArr;
            this.zzaai = i;
            this.zzaaj = i2;
            this.zzaak = j;
            this.zzaal = jSONObject;
            super(googleApiClient);
        }

        protected void zza(zze com_google_android_gms_cast_internal_zze) {
            synchronized (this.zzaad.zzpK) {
                this.zzaad.zzZY.zzb(this.zzaae);
                try {
                    this.zzaad.zzZX.zza(this.zzaaF, this.zzaah, this.zzaai, this.zzaaj, this.zzaak, this.zzaal);
                    this.zzaad.zzZY.zzb(null);
                } catch (IOException e) {
                    zzb(zzq(new Status(RemoteMediaPlayer.STATUS_FAILED)));
                    this.zzaad.zzZY.zzb(null);
                } catch (Throwable th) {
                    this.zzaad.zzZY.zzb(null);
                }
            }
        }
    }

    /* renamed from: com.google.android.gms.cast.RemoteMediaPlayer.5 */
    class C12825 extends zzb {
        final /* synthetic */ RemoteMediaPlayer zzaad;
        final /* synthetic */ GoogleApiClient zzaae;
        final /* synthetic */ JSONObject zzaal;
        final /* synthetic */ MediaQueueItem[] zzaam;
        final /* synthetic */ int zzaan;

        C12825(RemoteMediaPlayer remoteMediaPlayer, GoogleApiClient googleApiClient, GoogleApiClient googleApiClient2, MediaQueueItem[] mediaQueueItemArr, int i, JSONObject jSONObject) {
            this.zzaad = remoteMediaPlayer;
            this.zzaae = googleApiClient2;
            this.zzaam = mediaQueueItemArr;
            this.zzaan = i;
            this.zzaal = jSONObject;
            super(googleApiClient);
        }

        protected void zza(zze com_google_android_gms_cast_internal_zze) {
            synchronized (this.zzaad.zzpK) {
                this.zzaad.zzZY.zzb(this.zzaae);
                try {
                    this.zzaad.zzZX.zza(this.zzaaF, this.zzaam, this.zzaan, (int) RemoteMediaPlayer.STATUS_SUCCEEDED, -1, -1, this.zzaal);
                    this.zzaad.zzZY.zzb(null);
                } catch (IOException e) {
                    zzb(zzq(new Status(RemoteMediaPlayer.STATUS_FAILED)));
                    this.zzaad.zzZY.zzb(null);
                } catch (Throwable th) {
                    this.zzaad.zzZY.zzb(null);
                }
            }
        }
    }

    /* renamed from: com.google.android.gms.cast.RemoteMediaPlayer.6 */
    class C12836 extends zzb {
        final /* synthetic */ RemoteMediaPlayer zzaad;
        final /* synthetic */ GoogleApiClient zzaae;
        final /* synthetic */ long zzaak;
        final /* synthetic */ JSONObject zzaal;
        final /* synthetic */ int zzaan;
        final /* synthetic */ MediaQueueItem zzaao;

        C12836(RemoteMediaPlayer remoteMediaPlayer, GoogleApiClient googleApiClient, GoogleApiClient googleApiClient2, MediaQueueItem mediaQueueItem, int i, long j, JSONObject jSONObject) {
            this.zzaad = remoteMediaPlayer;
            this.zzaae = googleApiClient2;
            this.zzaao = mediaQueueItem;
            this.zzaan = i;
            this.zzaak = j;
            this.zzaal = jSONObject;
            super(googleApiClient);
        }

        protected void zza(zze com_google_android_gms_cast_internal_zze) {
            synchronized (this.zzaad.zzpK) {
                this.zzaad.zzZY.zzb(this.zzaae);
                try {
                    zzm zzg = this.zzaad.zzZX;
                    zzo com_google_android_gms_cast_internal_zzo = this.zzaaF;
                    MediaQueueItem[] mediaQueueItemArr = new MediaQueueItem[RemoteMediaPlayer.RESUME_STATE_PLAY];
                    mediaQueueItemArr[RemoteMediaPlayer.STATUS_SUCCEEDED] = this.zzaao;
                    zzg.zza(com_google_android_gms_cast_internal_zzo, mediaQueueItemArr, this.zzaan, (int) RemoteMediaPlayer.STATUS_SUCCEEDED, (int) RemoteMediaPlayer.STATUS_SUCCEEDED, this.zzaak, this.zzaal);
                    this.zzaad.zzZY.zzb(null);
                } catch (IOException e) {
                    zzb(zzq(new Status(RemoteMediaPlayer.STATUS_FAILED)));
                    this.zzaad.zzZY.zzb(null);
                } catch (Throwable th) {
                    this.zzaad.zzZY.zzb(null);
                }
            }
        }
    }

    /* renamed from: com.google.android.gms.cast.RemoteMediaPlayer.7 */
    class C12847 extends zzb {
        final /* synthetic */ RemoteMediaPlayer zzaad;
        final /* synthetic */ GoogleApiClient zzaae;
        final /* synthetic */ JSONObject zzaal;
        final /* synthetic */ MediaQueueItem[] zzaap;

        C12847(RemoteMediaPlayer remoteMediaPlayer, GoogleApiClient googleApiClient, GoogleApiClient googleApiClient2, MediaQueueItem[] mediaQueueItemArr, JSONObject jSONObject) {
            this.zzaad = remoteMediaPlayer;
            this.zzaae = googleApiClient2;
            this.zzaap = mediaQueueItemArr;
            this.zzaal = jSONObject;
            super(googleApiClient);
        }

        protected void zza(zze com_google_android_gms_cast_internal_zze) {
            synchronized (this.zzaad.zzpK) {
                this.zzaad.zzZY.zzb(this.zzaae);
                try {
                    this.zzaad.zzZX.zza(this.zzaaF, (int) RemoteMediaPlayer.STATUS_SUCCEEDED, -1, this.zzaap, (int) RemoteMediaPlayer.STATUS_SUCCEEDED, null, this.zzaal);
                    this.zzaad.zzZY.zzb(null);
                } catch (IOException e) {
                    zzb(zzq(new Status(RemoteMediaPlayer.STATUS_FAILED)));
                    this.zzaad.zzZY.zzb(null);
                } catch (Throwable th) {
                    this.zzaad.zzZY.zzb(null);
                }
            }
        }
    }

    /* renamed from: com.google.android.gms.cast.RemoteMediaPlayer.8 */
    class C12858 extends zzb {
        final /* synthetic */ RemoteMediaPlayer zzaad;
        final /* synthetic */ GoogleApiClient zzaae;
        final /* synthetic */ JSONObject zzaal;
        final /* synthetic */ int[] zzaaq;

        C12858(RemoteMediaPlayer remoteMediaPlayer, GoogleApiClient googleApiClient, GoogleApiClient googleApiClient2, int[] iArr, JSONObject jSONObject) {
            this.zzaad = remoteMediaPlayer;
            this.zzaae = googleApiClient2;
            this.zzaaq = iArr;
            this.zzaal = jSONObject;
            super(googleApiClient);
        }

        protected void zza(zze com_google_android_gms_cast_internal_zze) {
            synchronized (this.zzaad.zzpK) {
                this.zzaad.zzZY.zzb(this.zzaae);
                try {
                    this.zzaad.zzZX.zza(this.zzaaF, this.zzaaq, this.zzaal);
                    this.zzaad.zzZY.zzb(null);
                } catch (IOException e) {
                    zzb(zzq(new Status(RemoteMediaPlayer.STATUS_FAILED)));
                    this.zzaad.zzZY.zzb(null);
                } catch (Throwable th) {
                    this.zzaad.zzZY.zzb(null);
                }
            }
        }
    }

    /* renamed from: com.google.android.gms.cast.RemoteMediaPlayer.9 */
    class C12869 extends zzb {
        final /* synthetic */ RemoteMediaPlayer zzaad;
        final /* synthetic */ GoogleApiClient zzaae;
        final /* synthetic */ JSONObject zzaal;
        final /* synthetic */ int zzaan;
        final /* synthetic */ int[] zzaar;

        C12869(RemoteMediaPlayer remoteMediaPlayer, GoogleApiClient googleApiClient, GoogleApiClient googleApiClient2, int[] iArr, int i, JSONObject jSONObject) {
            this.zzaad = remoteMediaPlayer;
            this.zzaae = googleApiClient2;
            this.zzaar = iArr;
            this.zzaan = i;
            this.zzaal = jSONObject;
            super(googleApiClient);
        }

        protected void zza(zze com_google_android_gms_cast_internal_zze) {
            synchronized (this.zzaad.zzpK) {
                this.zzaad.zzZY.zzb(this.zzaae);
                try {
                    this.zzaad.zzZX.zza(this.zzaaF, this.zzaar, this.zzaan, this.zzaal);
                    this.zzaad.zzZY.zzb(null);
                } catch (IOException e) {
                    zzb(zzq(new Status(RemoteMediaPlayer.STATUS_FAILED)));
                    this.zzaad.zzZY.zzb(null);
                } catch (Throwable th) {
                    this.zzaad.zzZY.zzb(null);
                }
            }
        }
    }

    public RemoteMediaPlayer() {
        this.zzpK = new Object();
        this.zzZY = new zza(this);
        this.zzZX = new C11961(this, null);
        this.zzZX.zza(this.zzZY);
    }

    private void onMetadataUpdated() {
        if (this.zzaab != null) {
            this.zzaab.onMetadataUpdated();
        }
    }

    private void onPreloadStatusUpdated() {
        if (this.zzZZ != null) {
            this.zzZZ.onPreloadStatusUpdated();
        }
    }

    private void onQueueStatusUpdated() {
        if (this.zzaaa != null) {
            this.zzaaa.onQueueStatusUpdated();
        }
    }

    private void onStatusUpdated() {
        if (this.zzaac != null) {
            this.zzaac.onStatusUpdated();
        }
    }

    private int zzbf(int i) {
        MediaStatus mediaStatus = getMediaStatus();
        for (int i2 = STATUS_SUCCEEDED; i2 < mediaStatus.getQueueItemCount(); i2 += RESUME_STATE_PLAY) {
            if (mediaStatus.getQueueItem(i2).getItemId() == i) {
                return i2;
            }
        }
        return -1;
    }

    public long getApproximateStreamPosition() {
        long approximateStreamPosition;
        synchronized (this.zzpK) {
            approximateStreamPosition = this.zzZX.getApproximateStreamPosition();
        }
        return approximateStreamPosition;
    }

    public MediaInfo getMediaInfo() {
        MediaInfo mediaInfo;
        synchronized (this.zzpK) {
            mediaInfo = this.zzZX.getMediaInfo();
        }
        return mediaInfo;
    }

    public MediaStatus getMediaStatus() {
        MediaStatus mediaStatus;
        synchronized (this.zzpK) {
            mediaStatus = this.zzZX.getMediaStatus();
        }
        return mediaStatus;
    }

    public String getNamespace() {
        return this.zzZX.getNamespace();
    }

    public long getStreamDuration() {
        long streamDuration;
        synchronized (this.zzpK) {
            streamDuration = this.zzZX.getStreamDuration();
        }
        return streamDuration;
    }

    public PendingResult<MediaChannelResult> load(GoogleApiClient apiClient, MediaInfo mediaInfo) {
        return load(apiClient, mediaInfo, true, 0, null, null);
    }

    public PendingResult<MediaChannelResult> load(GoogleApiClient apiClient, MediaInfo mediaInfo, boolean autoplay) {
        return load(apiClient, mediaInfo, autoplay, 0, null, null);
    }

    public PendingResult<MediaChannelResult> load(GoogleApiClient apiClient, MediaInfo mediaInfo, boolean autoplay, long playPosition) {
        return load(apiClient, mediaInfo, autoplay, playPosition, null, null);
    }

    public PendingResult<MediaChannelResult> load(GoogleApiClient apiClient, MediaInfo mediaInfo, boolean autoplay, long playPosition, JSONObject customData) {
        return load(apiClient, mediaInfo, autoplay, playPosition, null, customData);
    }

    public PendingResult<MediaChannelResult> load(GoogleApiClient apiClient, MediaInfo mediaInfo, boolean autoplay, long playPosition, long[] activeTrackIds, JSONObject customData) {
        return apiClient.zzb(new AnonymousClass12(this, apiClient, apiClient, mediaInfo, autoplay, playPosition, activeTrackIds, customData));
    }

    public void onMessageReceived(CastDevice castDevice, String namespace, String message) {
        this.zzZX.zzbZ(message);
    }

    public PendingResult<MediaChannelResult> pause(GoogleApiClient apiClient) {
        return pause(apiClient, null);
    }

    public PendingResult<MediaChannelResult> pause(GoogleApiClient apiClient, JSONObject customData) {
        return apiClient.zzb(new AnonymousClass17(this, apiClient, apiClient, customData));
    }

    public PendingResult<MediaChannelResult> play(GoogleApiClient apiClient) {
        return play(apiClient, null);
    }

    public PendingResult<MediaChannelResult> play(GoogleApiClient apiClient, JSONObject customData) {
        return apiClient.zzb(new AnonymousClass19(this, apiClient, apiClient, customData));
    }

    public PendingResult<MediaChannelResult> queueAppendItem(GoogleApiClient apiClient, MediaQueueItem item, JSONObject customData) throws IllegalArgumentException {
        MediaQueueItem[] mediaQueueItemArr = new MediaQueueItem[RESUME_STATE_PLAY];
        mediaQueueItemArr[STATUS_SUCCEEDED] = item;
        return queueInsertItems(apiClient, mediaQueueItemArr, STATUS_SUCCEEDED, customData);
    }

    public PendingResult<MediaChannelResult> queueInsertAndPlayItem(GoogleApiClient apiClient, MediaQueueItem item, int insertBeforeItemId, long playPosition, JSONObject customData) {
        return apiClient.zzb(new C12836(this, apiClient, apiClient, item, insertBeforeItemId, playPosition, customData));
    }

    public PendingResult<MediaChannelResult> queueInsertAndPlayItem(GoogleApiClient apiClient, MediaQueueItem item, int insertBeforeItemId, JSONObject customData) {
        return queueInsertAndPlayItem(apiClient, item, insertBeforeItemId, -1, customData);
    }

    public PendingResult<MediaChannelResult> queueInsertItems(GoogleApiClient apiClient, MediaQueueItem[] itemsToInsert, int insertBeforeItemId, JSONObject customData) throws IllegalArgumentException {
        return apiClient.zzb(new C12825(this, apiClient, apiClient, itemsToInsert, insertBeforeItemId, customData));
    }

    public PendingResult<MediaChannelResult> queueJumpToItem(GoogleApiClient apiClient, int itemId, long playPosition, JSONObject customData) {
        return apiClient.zzb(new AnonymousClass15(this, apiClient, itemId, apiClient, playPosition, customData));
    }

    public PendingResult<MediaChannelResult> queueJumpToItem(GoogleApiClient apiClient, int itemId, JSONObject customData) {
        return queueJumpToItem(apiClient, itemId, -1, customData);
    }

    public PendingResult<MediaChannelResult> queueLoad(GoogleApiClient apiClient, MediaQueueItem[] items, int startIndex, int repeatMode, long playPosition, JSONObject customData) throws IllegalArgumentException {
        return apiClient.zzb(new C12814(this, apiClient, apiClient, items, startIndex, repeatMode, playPosition, customData));
    }

    public PendingResult<MediaChannelResult> queueLoad(GoogleApiClient apiClient, MediaQueueItem[] items, int startIndex, int repeatMode, JSONObject customData) throws IllegalArgumentException {
        return queueLoad(apiClient, items, startIndex, repeatMode, -1, customData);
    }

    public PendingResult<MediaChannelResult> queueMoveItemToNewIndex(GoogleApiClient apiClient, int itemId, int newIndex, JSONObject customData) {
        return apiClient.zzb(new AnonymousClass16(this, apiClient, itemId, newIndex, apiClient, customData));
    }

    public PendingResult<MediaChannelResult> queueNext(GoogleApiClient apiClient, JSONObject customData) {
        return apiClient.zzb(new AnonymousClass11(this, apiClient, apiClient, customData));
    }

    public PendingResult<MediaChannelResult> queuePrev(GoogleApiClient apiClient, JSONObject customData) {
        return apiClient.zzb(new AnonymousClass10(this, apiClient, apiClient, customData));
    }

    public PendingResult<MediaChannelResult> queueRemoveItem(GoogleApiClient apiClient, int itemId, JSONObject customData) {
        return apiClient.zzb(new AnonymousClass14(this, apiClient, itemId, apiClient, customData));
    }

    public PendingResult<MediaChannelResult> queueRemoveItems(GoogleApiClient apiClient, int[] itemIdsToRemove, JSONObject customData) throws IllegalArgumentException {
        return apiClient.zzb(new C12858(this, apiClient, apiClient, itemIdsToRemove, customData));
    }

    public PendingResult<MediaChannelResult> queueReorderItems(GoogleApiClient apiClient, int[] itemIdsToReorder, int insertBeforeItemId, JSONObject customData) throws IllegalArgumentException {
        return apiClient.zzb(new C12869(this, apiClient, apiClient, itemIdsToReorder, insertBeforeItemId, customData));
    }

    public PendingResult<MediaChannelResult> queueSetRepeatMode(GoogleApiClient apiClient, int repeatMode, JSONObject customData) {
        return apiClient.zzb(new AnonymousClass13(this, apiClient, apiClient, repeatMode, customData));
    }

    public PendingResult<MediaChannelResult> queueUpdateItems(GoogleApiClient apiClient, MediaQueueItem[] itemsToUpdate, JSONObject customData) {
        return apiClient.zzb(new C12847(this, apiClient, apiClient, itemsToUpdate, customData));
    }

    public PendingResult<MediaChannelResult> requestStatus(GoogleApiClient apiClient) {
        return apiClient.zzb(new AnonymousClass23(this, apiClient, apiClient));
    }

    public PendingResult<MediaChannelResult> seek(GoogleApiClient apiClient, long position) {
        return seek(apiClient, position, STATUS_SUCCEEDED, null);
    }

    public PendingResult<MediaChannelResult> seek(GoogleApiClient apiClient, long position, int resumeState) {
        return seek(apiClient, position, resumeState, null);
    }

    public PendingResult<MediaChannelResult> seek(GoogleApiClient apiClient, long position, int resumeState, JSONObject customData) {
        return apiClient.zzb(new AnonymousClass20(this, apiClient, apiClient, position, resumeState, customData));
    }

    public PendingResult<MediaChannelResult> setActiveMediaTracks(GoogleApiClient apiClient, long[] trackIds) {
        if (trackIds != null) {
            return apiClient.zzb(new C12792(this, apiClient, apiClient, trackIds));
        }
        throw new IllegalArgumentException("trackIds cannot be null");
    }

    public void setOnMetadataUpdatedListener(OnMetadataUpdatedListener listener) {
        this.zzaab = listener;
    }

    public void setOnPreloadStatusUpdatedListener(OnPreloadStatusUpdatedListener listener) {
        this.zzZZ = listener;
    }

    public void setOnQueueStatusUpdatedListener(OnQueueStatusUpdatedListener listener) {
        this.zzaaa = listener;
    }

    public void setOnStatusUpdatedListener(OnStatusUpdatedListener listener) {
        this.zzaac = listener;
    }

    public PendingResult<MediaChannelResult> setStreamMute(GoogleApiClient apiClient, boolean muteState) {
        return setStreamMute(apiClient, muteState, null);
    }

    public PendingResult<MediaChannelResult> setStreamMute(GoogleApiClient apiClient, boolean muteState, JSONObject customData) {
        return apiClient.zzb(new AnonymousClass22(this, apiClient, apiClient, muteState, customData));
    }

    public PendingResult<MediaChannelResult> setStreamVolume(GoogleApiClient apiClient, double volume) throws IllegalArgumentException {
        return setStreamVolume(apiClient, volume, null);
    }

    public PendingResult<MediaChannelResult> setStreamVolume(GoogleApiClient apiClient, double volume, JSONObject customData) throws IllegalArgumentException {
        if (!Double.isInfinite(volume) && !Double.isNaN(volume)) {
            return apiClient.zzb(new AnonymousClass21(this, apiClient, apiClient, volume, customData));
        }
        throw new IllegalArgumentException("Volume cannot be " + volume);
    }

    public PendingResult<MediaChannelResult> setTextTrackStyle(GoogleApiClient apiClient, TextTrackStyle trackStyle) {
        if (trackStyle != null) {
            return apiClient.zzb(new C12803(this, apiClient, apiClient, trackStyle));
        }
        throw new IllegalArgumentException("trackStyle cannot be null");
    }

    public PendingResult<MediaChannelResult> stop(GoogleApiClient apiClient) {
        return stop(apiClient, null);
    }

    public PendingResult<MediaChannelResult> stop(GoogleApiClient apiClient, JSONObject customData) {
        return apiClient.zzb(new AnonymousClass18(this, apiClient, apiClient, customData));
    }
}
