package com.google.android.gms.cast;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ServiceInfo;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v7.media.MediaRouteSelector;
import android.support.v7.media.MediaRouter;
import android.support.v7.media.MediaRouter.Callback;
import android.support.v7.media.MediaRouter.RouteInfo;
import android.text.TextUtils;
import android.view.Display;
import com.google.android.gms.C0220R;
import com.google.android.gms.cast.CastRemoteDisplay.CastRemoteDisplayOptions;
import com.google.android.gms.cast.CastRemoteDisplay.CastRemoteDisplaySessionCallbacks;
import com.google.android.gms.cast.CastRemoteDisplay.CastRemoteDisplaySessionResult;
import com.google.android.gms.cast.internal.zzl;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.vision.barcode.Barcode;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class CastRemoteDisplayLocalService extends Service {
    private static final Object zzYA;
    private static AtomicBoolean zzYB;
    private static CastRemoteDisplayLocalService zzYQ;
    private static final zzl zzYy;
    private static final int zzYz;
    private Handler mHandler;
    private Notification mNotification;
    private String zzXW;
    private GoogleApiClient zzYC;
    private CastRemoteDisplaySessionCallbacks zzYD;
    private Callbacks zzYE;
    private zzb zzYF;
    private NotificationSettings zzYG;
    private boolean zzYH;
    private PendingIntent zzYI;
    private CastDevice zzYJ;
    private Display zzYK;
    private Context zzYL;
    private ServiceConnection zzYM;
    private MediaRouter zzYN;
    private boolean zzYO;
    private final Callback zzYP;
    private final IBinder zzYR;

    /* renamed from: com.google.android.gms.cast.CastRemoteDisplayLocalService.3 */
    class C02923 implements Runnable {
        final /* synthetic */ CastRemoteDisplayLocalService zzYS;

        C02923(CastRemoteDisplayLocalService castRemoteDisplayLocalService) {
            this.zzYS = castRemoteDisplayLocalService;
        }

        public void run() {
            this.zzYS.zzbb("onCreate after delay. The local service been started: " + this.zzYS.zzYO);
            if (!this.zzYS.zzYO) {
                this.zzYS.zzbe("The local service has not been been started, stopping it");
                this.zzYS.stopSelf();
            }
        }
    }

    /* renamed from: com.google.android.gms.cast.CastRemoteDisplayLocalService.4 */
    static class C02934 implements ServiceConnection {
        final /* synthetic */ CastDevice zzYT;
        final /* synthetic */ NotificationSettings zzYU;
        final /* synthetic */ Context zzYV;
        final /* synthetic */ Callbacks zzYW;
        final /* synthetic */ String zzYc;

        C02934(String str, CastDevice castDevice, NotificationSettings notificationSettings, Context context, Callbacks callbacks) {
            this.zzYc = str;
            this.zzYT = castDevice;
            this.zzYU = notificationSettings;
            this.zzYV = context;
            this.zzYW = callbacks;
        }

        public void onServiceConnected(ComponentName className, IBinder binder) {
            CastRemoteDisplayLocalService zznu = ((zza) binder).zznu();
            if (zznu == null || !zznu.zza(this.zzYc, this.zzYT, this.zzYU, this.zzYV, this, this.zzYW)) {
                CastRemoteDisplayLocalService.zzYy.zzc("Connected but unable to get the service instance", new Object[0]);
                this.zzYW.onRemoteDisplaySessionError(new Status(CastStatusCodes.ERROR_SERVICE_CREATION_FAILED));
                CastRemoteDisplayLocalService.zzYB.set(false);
                try {
                    this.zzYV.unbindService(this);
                } catch (IllegalArgumentException e) {
                    CastRemoteDisplayLocalService.zzYy.zzb("No need to unbind service, already unbound", new Object[0]);
                }
            }
        }

        public void onServiceDisconnected(ComponentName arg0) {
            CastRemoteDisplayLocalService.zzYy.zzb("onServiceDisconnected", new Object[0]);
            this.zzYW.onRemoteDisplaySessionError(new Status(CastStatusCodes.ERROR_SERVICE_DISCONNECTED, "Service Disconnected"));
            CastRemoteDisplayLocalService.zzYB.set(false);
            try {
                this.zzYV.unbindService(this);
            } catch (IllegalArgumentException e) {
                CastRemoteDisplayLocalService.zzYy.zzb("No need to unbind service, already unbound", new Object[0]);
            }
        }
    }

    /* renamed from: com.google.android.gms.cast.CastRemoteDisplayLocalService.5 */
    class C02945 implements Runnable {
        final /* synthetic */ CastRemoteDisplayLocalService zzYS;
        final /* synthetic */ boolean zzYX;

        C02945(CastRemoteDisplayLocalService castRemoteDisplayLocalService, boolean z) {
            this.zzYS = castRemoteDisplayLocalService;
            this.zzYX = z;
        }

        public void run() {
            this.zzYS.zzR(this.zzYX);
        }
    }

    /* renamed from: com.google.android.gms.cast.CastRemoteDisplayLocalService.6 */
    class C02956 implements Runnable {
        final /* synthetic */ CastRemoteDisplayLocalService zzYS;
        final /* synthetic */ NotificationSettings zzYU;

        C02956(CastRemoteDisplayLocalService castRemoteDisplayLocalService, NotificationSettings notificationSettings) {
            this.zzYS = castRemoteDisplayLocalService;
            this.zzYU = notificationSettings;
        }

        public void run() {
            this.zzYS.zza(this.zzYU);
        }
    }

    public interface Callbacks {
        void onRemoteDisplaySessionError(Status status);

        void onRemoteDisplaySessionStarted(CastRemoteDisplayLocalService castRemoteDisplayLocalService);

        void onServiceCreated(CastRemoteDisplayLocalService castRemoteDisplayLocalService);
    }

    public static final class NotificationSettings {
        private Notification mNotification;
        private PendingIntent zzYY;
        private String zzYZ;
        private String zzZa;

        public static final class Builder {
            private NotificationSettings zzZb;

            public Builder() {
                this.zzZb = new NotificationSettings();
            }

            public NotificationSettings build() {
                if (this.zzZb.mNotification != null) {
                    if (!TextUtils.isEmpty(this.zzZb.zzYZ)) {
                        throw new IllegalArgumentException("notificationTitle requires using the default notification");
                    } else if (!TextUtils.isEmpty(this.zzZb.zzZa)) {
                        throw new IllegalArgumentException("notificationText requires using the default notification");
                    } else if (this.zzZb.zzYY != null) {
                        throw new IllegalArgumentException("notificationPendingIntent requires using the default notification");
                    }
                } else if (TextUtils.isEmpty(this.zzZb.zzYZ) && TextUtils.isEmpty(this.zzZb.zzZa) && this.zzZb.zzYY == null) {
                    throw new IllegalArgumentException("At least an argument must be provided");
                }
                return this.zzZb;
            }

            public Builder setNotification(Notification notification) {
                this.zzZb.mNotification = notification;
                return this;
            }

            public Builder setNotificationPendingIntent(PendingIntent notificationPendingIntent) {
                this.zzZb.zzYY = notificationPendingIntent;
                return this;
            }

            public Builder setNotificationText(String notificationText) {
                this.zzZb.zzZa = notificationText;
                return this;
            }

            public Builder setNotificationTitle(String notificationTitle) {
                this.zzZb.zzYZ = notificationTitle;
                return this;
            }
        }

        private NotificationSettings() {
        }

        private NotificationSettings(NotificationSettings newSettings) {
            this.mNotification = newSettings.mNotification;
            this.zzYY = newSettings.zzYY;
            this.zzYZ = newSettings.zzYZ;
            this.zzZa = newSettings.zzZa;
        }
    }

    private class zza extends Binder {
        final /* synthetic */ CastRemoteDisplayLocalService zzYS;

        private zza(CastRemoteDisplayLocalService castRemoteDisplayLocalService) {
            this.zzYS = castRemoteDisplayLocalService;
        }

        CastRemoteDisplayLocalService zznu() {
            return this.zzYS;
        }
    }

    private static final class zzb extends BroadcastReceiver {
        private zzb() {
        }

        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.google.android.gms.cast.remote_display.ACTION_NOTIFICATION_DISCONNECT")) {
                CastRemoteDisplayLocalService.zzYy.zzb("disconnecting", new Object[0]);
                CastRemoteDisplayLocalService.stopService();
            }
        }
    }

    /* renamed from: com.google.android.gms.cast.CastRemoteDisplayLocalService.1 */
    class C06951 extends Callback {
        final /* synthetic */ CastRemoteDisplayLocalService zzYS;

        C06951(CastRemoteDisplayLocalService castRemoteDisplayLocalService) {
            this.zzYS = castRemoteDisplayLocalService;
        }

        public void onRouteUnselected(MediaRouter router, RouteInfo info) {
            this.zzYS.zzbb("onRouteUnselected");
            if (this.zzYS.zzYJ == null) {
                this.zzYS.zzbb("onRouteUnselected, no device was selected");
            } else if (CastDevice.getFromBundle(info.getExtras()).getDeviceId().equals(this.zzYS.zzYJ.getDeviceId())) {
                CastRemoteDisplayLocalService.stopService();
            } else {
                this.zzYS.zzbb("onRouteUnselected, device does not match");
            }
        }
    }

    /* renamed from: com.google.android.gms.cast.CastRemoteDisplayLocalService.2 */
    class C06962 implements OnConnectionFailedListener {
        final /* synthetic */ CastRemoteDisplayLocalService zzYS;

        C06962(CastRemoteDisplayLocalService castRemoteDisplayLocalService) {
            this.zzYS = castRemoteDisplayLocalService;
        }

        public void onConnectionFailed(ConnectionResult connectionResult) {
            this.zzYS.zzbe("Connection failed: " + connectionResult);
            this.zzYS.zznm();
        }
    }

    /* renamed from: com.google.android.gms.cast.CastRemoteDisplayLocalService.7 */
    class C06977 implements CastRemoteDisplaySessionCallbacks {
        final /* synthetic */ CastRemoteDisplayLocalService zzYS;

        C06977(CastRemoteDisplayLocalService castRemoteDisplayLocalService) {
            this.zzYS = castRemoteDisplayLocalService;
        }

        public void onRemoteDisplayEnded(Status status) {
            CastRemoteDisplayLocalService.zzYy.zzb(String.format("Cast screen has ended: %d", new Object[]{Integer.valueOf(status.getStatusCode())}), new Object[0]);
            CastRemoteDisplayLocalService.zzS(false);
        }
    }

    /* renamed from: com.google.android.gms.cast.CastRemoteDisplayLocalService.8 */
    class C06988 implements ResultCallback<CastRemoteDisplaySessionResult> {
        final /* synthetic */ CastRemoteDisplayLocalService zzYS;

        C06988(CastRemoteDisplayLocalService castRemoteDisplayLocalService) {
            this.zzYS = castRemoteDisplayLocalService;
        }

        public /* synthetic */ void onResult(Result x0) {
            zza((CastRemoteDisplaySessionResult) x0);
        }

        public void zza(CastRemoteDisplaySessionResult castRemoteDisplaySessionResult) {
            if (castRemoteDisplaySessionResult.getStatus().isSuccess()) {
                CastRemoteDisplayLocalService.zzYy.zzb("startRemoteDisplay successful", new Object[0]);
                synchronized (CastRemoteDisplayLocalService.zzYA) {
                    if (CastRemoteDisplayLocalService.zzYQ == null) {
                        CastRemoteDisplayLocalService.zzYy.zzb("Remote Display started but session already cancelled", new Object[0]);
                        this.zzYS.zznm();
                        return;
                    }
                    Display presentationDisplay = castRemoteDisplaySessionResult.getPresentationDisplay();
                    if (presentationDisplay != null) {
                        this.zzYS.zza(presentationDisplay);
                    } else {
                        CastRemoteDisplayLocalService.zzYy.zzc("Cast Remote Display session created without display", new Object[0]);
                    }
                    CastRemoteDisplayLocalService.zzYB.set(false);
                    if (this.zzYS.zzYL != null && this.zzYS.zzYM != null) {
                        try {
                            this.zzYS.zzYL.unbindService(this.zzYS.zzYM);
                        } catch (IllegalArgumentException e) {
                            CastRemoteDisplayLocalService.zzYy.zzb("No need to unbind service, already unbound", new Object[0]);
                        }
                        this.zzYS.zzYM = null;
                        this.zzYS.zzYL = null;
                        return;
                    }
                    return;
                }
            }
            CastRemoteDisplayLocalService.zzYy.zzc("Connection was not successful", new Object[0]);
            this.zzYS.zznm();
        }
    }

    /* renamed from: com.google.android.gms.cast.CastRemoteDisplayLocalService.9 */
    class C06999 implements ResultCallback<CastRemoteDisplaySessionResult> {
        final /* synthetic */ CastRemoteDisplayLocalService zzYS;

        C06999(CastRemoteDisplayLocalService castRemoteDisplayLocalService) {
            this.zzYS = castRemoteDisplayLocalService;
        }

        public /* synthetic */ void onResult(Result x0) {
            zza((CastRemoteDisplaySessionResult) x0);
        }

        public void zza(CastRemoteDisplaySessionResult castRemoteDisplaySessionResult) {
            if (castRemoteDisplaySessionResult.getStatus().isSuccess()) {
                this.zzYS.zzbb("remote display stopped");
            } else {
                this.zzYS.zzbb("Unable to stop the remote display, result unsuccessful");
            }
            this.zzYS.zzYK = null;
        }
    }

    static {
        zzYy = new zzl("CastRemoteDisplayLocalService");
        zzYz = C0220R.id.cast_notification_id;
        zzYA = new Object();
        zzYB = new AtomicBoolean(false);
    }

    public CastRemoteDisplayLocalService() {
        this.zzYO = false;
        this.zzYP = new C06951(this);
        this.zzYR = new zza();
    }

    public static CastRemoteDisplayLocalService getInstance() {
        CastRemoteDisplayLocalService castRemoteDisplayLocalService;
        synchronized (zzYA) {
            castRemoteDisplayLocalService = zzYQ;
        }
        return castRemoteDisplayLocalService;
    }

    protected static void setDebugEnabled() {
        zzYy.zzY(true);
    }

    public static void startService(Context activityContext, Class<? extends CastRemoteDisplayLocalService> serviceClass, String applicationId, CastDevice device, NotificationSettings notificationSettings, Callbacks callbacks) {
        zzYy.zzb("Starting Service", new Object[0]);
        synchronized (zzYA) {
            if (zzYQ != null) {
                zzYy.zzf("An existing service had not been stopped before starting one", new Object[0]);
                zzS(true);
            }
        }
        zzb(activityContext, (Class) serviceClass);
        zzx.zzb((Object) activityContext, (Object) "activityContext is required.");
        zzx.zzb((Object) serviceClass, (Object) "serviceClass is required.");
        zzx.zzb((Object) applicationId, (Object) "applicationId is required.");
        zzx.zzb((Object) device, (Object) "device is required.");
        zzx.zzb((Object) notificationSettings, (Object) "notificationSettings is required.");
        zzx.zzb((Object) callbacks, (Object) "callbacks is required.");
        if (notificationSettings.mNotification == null && notificationSettings.zzYY == null) {
            throw new IllegalArgumentException("notificationSettings: Either the notification or the notificationPendingIntent must be provided");
        } else if (zzYB.getAndSet(true)) {
            zzYy.zzc("Service is already being started, startService has been called twice", new Object[0]);
        } else {
            Intent intent = new Intent(activityContext, serviceClass);
            activityContext.startService(intent);
            activityContext.bindService(intent, new C02934(applicationId, device, notificationSettings, activityContext, callbacks), 64);
        }
    }

    public static void stopService() {
        zzS(false);
    }

    private void zzQ(boolean z) {
        if (this.mHandler == null) {
            return;
        }
        if (Looper.myLooper() != Looper.getMainLooper()) {
            this.mHandler.post(new C02945(this, z));
        } else {
            zzR(z);
        }
    }

    private void zzR(boolean z) {
        zzbb("Stopping Service");
        zzx.zzcx("stopServiceInstanceInternal must be called on the main thread");
        if (!(z || this.zzYN == null)) {
            zzbb("Setting default route");
            this.zzYN.selectRoute(this.zzYN.getDefaultRoute());
        }
        if (this.zzYF != null) {
            zzbb("Unregistering notification receiver");
            unregisterReceiver(this.zzYF);
        }
        zznn();
        zzno();
        zznj();
        if (this.zzYC != null) {
            this.zzYC.disconnect();
            this.zzYC = null;
        }
        if (!(this.zzYL == null || this.zzYM == null)) {
            try {
                this.zzYL.unbindService(this.zzYM);
            } catch (IllegalArgumentException e) {
                zzbb("No need to unbind service, already unbound");
            }
            this.zzYM = null;
            this.zzYL = null;
        }
        this.zzXW = null;
        this.zzYC = null;
        this.mNotification = null;
        this.zzYK = null;
    }

    private static void zzS(boolean z) {
        zzYy.zzb("Stopping Service", new Object[0]);
        zzYB.set(false);
        synchronized (zzYA) {
            if (zzYQ == null) {
                zzYy.zzc("Service is already being stopped", new Object[0]);
                return;
            }
            CastRemoteDisplayLocalService castRemoteDisplayLocalService = zzYQ;
            zzYQ = null;
            castRemoteDisplayLocalService.zzQ(z);
        }
    }

    private Notification zzT(boolean z) {
        int i;
        int i2;
        CharSequence string;
        zzbb("createDefaultNotification");
        int i3 = getApplicationInfo().labelRes;
        CharSequence zzc = this.zzYG.zzYZ;
        CharSequence zzd = this.zzYG.zzZa;
        if (z) {
            i = C0220R.string.cast_notification_connected_message;
            i2 = C0220R.drawable.cast_ic_notification_on;
        } else {
            i = C0220R.string.cast_notification_connecting_message;
            i2 = C0220R.drawable.cast_ic_notification_connecting;
        }
        if (TextUtils.isEmpty(zzc)) {
            zzc = getString(i3);
        }
        if (TextUtils.isEmpty(zzd)) {
            string = getString(i, new Object[]{this.zzYJ.getFriendlyName()});
        } else {
            string = zzd;
        }
        return new Builder(this).setContentTitle(zzc).setContentText(string).setContentIntent(this.zzYG.zzYY).setSmallIcon(i2).setOngoing(true).addAction(17301560, getString(C0220R.string.cast_notification_disconnect), zznp()).build();
    }

    private GoogleApiClient zza(CastDevice castDevice) {
        return new GoogleApiClient.Builder(this, new ConnectionCallbacks() {
            final /* synthetic */ CastRemoteDisplayLocalService zzYS;

            {
                this.zzYS = r1;
            }

            public void onConnected(Bundle bundle) {
                this.zzYS.zzbb("onConnected");
                this.zzYS.zznk();
            }

            public void onConnectionSuspended(int cause) {
                CastRemoteDisplayLocalService.zzYy.zzf(String.format("[Instance: %s] ConnectionSuspended %d", new Object[]{this, Integer.valueOf(cause)}), new Object[0]);
            }
        }, new C06962(this)).addApi(CastRemoteDisplay.API, new CastRemoteDisplayOptions.Builder(castDevice, this.zzYD).build()).build();
    }

    private void zza(Display display) {
        this.zzYK = display;
        if (this.zzYH) {
            this.mNotification = zzT(true);
            startForeground(zzYz, this.mNotification);
        }
        if (this.zzYE != null) {
            this.zzYE.onRemoteDisplaySessionStarted(this);
            this.zzYE = null;
        }
        onCreatePresentation(this.zzYK);
    }

    private void zza(NotificationSettings notificationSettings) {
        zzx.zzcx("updateNotificationSettingsInternal must be called on the main thread");
        if (this.zzYG == null) {
            throw new IllegalStateException("No current notification settings to update");
        }
        if (!this.zzYH) {
            zzx.zzb(notificationSettings.mNotification, (Object) "notification is required.");
            this.mNotification = notificationSettings.mNotification;
            this.zzYG.mNotification = this.mNotification;
        } else if (notificationSettings.mNotification != null) {
            throw new IllegalStateException("Current mode is default notification, notification attribute must not be provided");
        } else {
            if (notificationSettings.zzYY != null) {
                this.zzYG.zzYY = notificationSettings.zzYY;
            }
            if (!TextUtils.isEmpty(notificationSettings.zzYZ)) {
                this.zzYG.zzYZ = notificationSettings.zzYZ;
            }
            if (!TextUtils.isEmpty(notificationSettings.zzZa)) {
                this.zzYG.zzZa = notificationSettings.zzZa;
            }
            this.mNotification = zzT(true);
        }
        startForeground(zzYz, this.mNotification);
    }

    private boolean zza(String str, CastDevice castDevice, NotificationSettings notificationSettings, Context context, ServiceConnection serviceConnection, Callbacks callbacks) {
        zzbb("startRemoteDisplaySession");
        zzx.zzcx("Starting the Cast Remote Display must be done on the main thread");
        synchronized (zzYA) {
            if (zzYQ != null) {
                zzYy.zzf("An existing service had not been stopped before starting one", new Object[0]);
                return false;
            }
            zzYQ = this;
            this.zzYE = callbacks;
            this.zzXW = str;
            this.zzYJ = castDevice;
            this.zzYL = context;
            this.zzYM = serviceConnection;
            this.zzYN = MediaRouter.getInstance(getApplicationContext());
            MediaRouteSelector build = new MediaRouteSelector.Builder().addControlCategory(CastMediaControlIntent.categoryForCast(this.zzXW)).build();
            zzbb("addMediaRouterCallback");
            this.zzYN.addCallback(build, this.zzYP, 4);
            this.zzYD = new C06977(this);
            this.mNotification = notificationSettings.mNotification;
            this.zzYF = new zzb();
            registerReceiver(this.zzYF, new IntentFilter("com.google.android.gms.cast.remote_display.ACTION_NOTIFICATION_DISCONNECT"));
            this.zzYG = new NotificationSettings(null);
            if (this.zzYG.mNotification == null) {
                this.zzYH = true;
                this.mNotification = zzT(false);
            } else {
                this.zzYH = false;
                this.mNotification = this.zzYG.mNotification;
            }
            startForeground(zzYz, this.mNotification);
            this.zzYC = zza(castDevice);
            this.zzYC.connect();
            if (this.zzYE != null) {
                this.zzYE.onServiceCreated(this);
            }
            return true;
        }
    }

    private static void zzb(Context context, Class cls) {
        try {
            ServiceInfo serviceInfo = context.getPackageManager().getServiceInfo(new ComponentName(context, cls), Barcode.ITF);
            if (serviceInfo != null && serviceInfo.exported) {
                throw new IllegalStateException("The service must not be exported, verify the manifest configuration");
            }
        } catch (NameNotFoundException e) {
            throw new IllegalStateException("Service not found, did you forget to configure it in the manifest?");
        }
    }

    private void zzbb(String str) {
        zzYy.zzb("[Instance: %s] %s", this, str);
    }

    private void zzbe(String str) {
        zzYy.zzc("[Instance: %s] %s", this, str);
    }

    private void zznj() {
        if (this.zzYN != null) {
            zzx.zzcx("CastRemoteDisplayLocalService calls must be done on the main thread");
            zzbb("removeMediaRouterCallback");
            this.zzYN.removeCallback(this.zzYP);
        }
    }

    private void zznk() {
        zzbb("startRemoteDisplay");
        if (this.zzYC == null || !this.zzYC.isConnected()) {
            zzYy.zzc("Unable to start the remote display as the API client is not ready", new Object[0]);
        } else {
            CastRemoteDisplay.CastRemoteDisplayApi.startRemoteDisplay(this.zzYC, this.zzXW).setResultCallback(new C06988(this));
        }
    }

    private void zznl() {
        zzbb("stopRemoteDisplay");
        if (this.zzYC == null || !this.zzYC.isConnected()) {
            zzYy.zzc("Unable to stop the remote display as the API client is not ready", new Object[0]);
        } else {
            CastRemoteDisplay.CastRemoteDisplayApi.stopRemoteDisplay(this.zzYC).setResultCallback(new C06999(this));
        }
    }

    private void zznm() {
        if (this.zzYE != null) {
            this.zzYE.onRemoteDisplaySessionError(new Status(CastStatusCodes.ERROR_SERVICE_CREATION_FAILED));
            this.zzYE = null;
        }
        stopService();
    }

    private void zznn() {
        zzbb("stopRemoteDisplaySession");
        zznl();
        onDismissPresentation();
    }

    private void zzno() {
        zzbb("Stopping the remote display Service");
        stopForeground(true);
        stopSelf();
    }

    private PendingIntent zznp() {
        if (this.zzYI == null) {
            this.zzYI = PendingIntent.getBroadcast(this, 0, new Intent("com.google.android.gms.cast.remote_display.ACTION_NOTIFICATION_DISCONNECT"), DriveFile.MODE_READ_ONLY);
        }
        return this.zzYI;
    }

    protected Display getDisplay() {
        return this.zzYK;
    }

    public IBinder onBind(Intent intent) {
        zzbb("onBind");
        return this.zzYR;
    }

    public void onCreate() {
        zzbb("onCreate");
        super.onCreate();
        this.mHandler = new Handler(getMainLooper());
        this.mHandler.postDelayed(new C02923(this), 100);
    }

    public abstract void onCreatePresentation(Display display);

    public abstract void onDismissPresentation();

    public int onStartCommand(Intent intent, int flags, int startId) {
        zzbb("onStartCommand");
        this.zzYO = true;
        return 2;
    }

    public void updateNotificationSettings(NotificationSettings notificationSettings) {
        zzx.zzb((Object) notificationSettings, (Object) "notificationSettings is required.");
        zzx.zzb(this.mHandler, (Object) "Service is not ready yet.");
        this.mHandler.post(new C02956(this, notificationSettings));
    }
}
