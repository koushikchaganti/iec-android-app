package com.google.android.gms.games.internal.data;

import android.net.Uri;
import com.google.android.gms.games.Games;

public final class GamesDataChangeUris {
    private static final Uri zzaFq;
    public static final Uri zzaFr;
    public static final Uri zzaFs;

    static {
        zzaFq = Uri.parse("content://com.google.android.gms.games/").buildUpon().appendPath("data_change").build();
        zzaFr = zzaFq.buildUpon().appendPath("invitations").build();
        zzaFs = zzaFq.buildUpon().appendEncodedPath(Games.EXTRA_PLAYER_IDS).build();
    }
}
