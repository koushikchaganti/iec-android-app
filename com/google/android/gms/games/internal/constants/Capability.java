package com.google.android.gms.games.internal.constants;

import java.util.ArrayList;

public class Capability {
    public static final ArrayList<String> zzaFp;

    static {
        zzaFp = new ArrayList();
        zzaFp.add("ibb");
        zzaFp.add("rtp");
        zzaFp.add("unreliable_ping");
    }
}
