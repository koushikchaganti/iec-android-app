package com.google.android.gms.games.internal.constants;

import com.google.android.gms.drive.events.CompletionEvent;
import com.google.android.gms.vision.barcode.Barcode.Phone;

public final class LeaderboardCollection {
    private LeaderboardCollection() {
    }

    public static String zzgo(int i) {
        switch (i) {
            case Phone.UNKNOWN /*0*/:
                return "PUBLIC";
            case CompletionEvent.STATUS_FAILURE /*1*/:
                return "SOCIAL";
            default:
                throw new IllegalArgumentException("Unknown leaderboard collection: " + i);
        }
    }
}
