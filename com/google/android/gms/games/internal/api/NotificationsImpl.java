package com.google.android.gms.games.internal.api;

import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.Games.BaseGamesApiMethodImpl;
import com.google.android.gms.games.Notifications;
import com.google.android.gms.games.Notifications.ContactSettingLoadResult;
import com.google.android.gms.games.Notifications.GameMuteStatusChangeResult;
import com.google.android.gms.games.Notifications.GameMuteStatusLoadResult;
import com.google.android.gms.games.Notifications.InboxCountResult;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.internal.zzlx.zzb;

public final class NotificationsImpl implements Notifications {

    /* renamed from: com.google.android.gms.games.internal.api.NotificationsImpl.1 */
    class C12221 extends BaseGamesApiMethodImpl<GameMuteStatusChangeResult> {
        final /* synthetic */ String zzaDU;

        /* renamed from: com.google.android.gms.games.internal.api.NotificationsImpl.1.1 */
        class C11291 implements GameMuteStatusChangeResult {
            final /* synthetic */ Status zzYl;
            final /* synthetic */ C12221 zzaDV;

            C11291(C12221 c12221, Status status) {
                this.zzaDV = c12221;
                this.zzYl = status;
            }

            public Status getStatus() {
                return this.zzYl;
            }
        }

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zzd((zzb) this, this.zzaDU, true);
        }

        public GameMuteStatusChangeResult zzan(Status status) {
            return new C11291(this, status);
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzan(status);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.NotificationsImpl.2 */
    class C12232 extends BaseGamesApiMethodImpl<GameMuteStatusChangeResult> {
        final /* synthetic */ String zzaDU;

        /* renamed from: com.google.android.gms.games.internal.api.NotificationsImpl.2.1 */
        class C11301 implements GameMuteStatusChangeResult {
            final /* synthetic */ Status zzYl;
            final /* synthetic */ C12232 zzaDW;

            C11301(C12232 c12232, Status status) {
                this.zzaDW = c12232;
                this.zzYl = status;
            }

            public Status getStatus() {
                return this.zzYl;
            }
        }

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zzd((zzb) this, this.zzaDU, false);
        }

        public GameMuteStatusChangeResult zzan(Status status) {
            return new C11301(this, status);
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzan(status);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.NotificationsImpl.3 */
    class C12243 extends BaseGamesApiMethodImpl<GameMuteStatusLoadResult> {
        final /* synthetic */ String zzaDU;

        /* renamed from: com.google.android.gms.games.internal.api.NotificationsImpl.3.1 */
        class C11311 implements GameMuteStatusLoadResult {
            final /* synthetic */ Status zzYl;
            final /* synthetic */ C12243 zzaDX;

            C11311(C12243 c12243, Status status) {
                this.zzaDX = c12243;
                this.zzYl = status;
            }

            public Status getStatus() {
                return this.zzYl;
            }
        }

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zzo(this, this.zzaDU);
        }

        public GameMuteStatusLoadResult zzao(Status status) {
            return new C11311(this, status);
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzao(status);
        }
    }

    private static abstract class ContactSettingLoadImpl extends BaseGamesApiMethodImpl<ContactSettingLoadResult> {

        /* renamed from: com.google.android.gms.games.internal.api.NotificationsImpl.ContactSettingLoadImpl.1 */
        class C11321 implements ContactSettingLoadResult {
            final /* synthetic */ Status zzYl;
            final /* synthetic */ ContactSettingLoadImpl zzaEa;

            C11321(ContactSettingLoadImpl contactSettingLoadImpl, Status status) {
                this.zzaEa = contactSettingLoadImpl;
                this.zzYl = status;
            }

            public Status getStatus() {
                return this.zzYl;
            }
        }

        public ContactSettingLoadResult zzap(Status status) {
            return new C11321(this, status);
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzap(status);
        }
    }

    private static abstract class ContactSettingUpdateImpl extends BaseGamesApiMethodImpl<Status> {
        public Status zzb(Status status) {
            return status;
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzb(status);
        }
    }

    private static abstract class InboxCountImpl extends BaseGamesApiMethodImpl<InboxCountResult> {

        /* renamed from: com.google.android.gms.games.internal.api.NotificationsImpl.InboxCountImpl.1 */
        class C11331 implements InboxCountResult {
            final /* synthetic */ Status zzYl;
            final /* synthetic */ InboxCountImpl zzaEb;

            C11331(InboxCountImpl inboxCountImpl, Status status) {
                this.zzaEb = inboxCountImpl;
                this.zzYl = status;
            }

            public Status getStatus() {
                return this.zzYl;
            }
        }

        public InboxCountResult zzaq(Status status) {
            return new C11331(this, status);
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzaq(status);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.NotificationsImpl.4 */
    class C13434 extends ContactSettingLoadImpl {
        final /* synthetic */ boolean zzaDg;

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zzi((zzb) this, this.zzaDg);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.NotificationsImpl.5 */
    class C13445 extends ContactSettingUpdateImpl {
        final /* synthetic */ boolean zzaDY;
        final /* synthetic */ Bundle zzaDZ;

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zza((zzb) this, this.zzaDY, this.zzaDZ);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.NotificationsImpl.6 */
    class C13456 extends InboxCountImpl {
        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zzi(this);
        }
    }

    public void clear(GoogleApiClient apiClient, int notificationTypes) {
        GamesClientImpl zzb = Games.zzb(apiClient, false);
        if (zzb != null) {
            zzb.zzgl(notificationTypes);
        }
    }

    public void clearAll(GoogleApiClient apiClient) {
        clear(apiClient, 31);
    }
}
