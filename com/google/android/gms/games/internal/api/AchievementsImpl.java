package com.google.android.gms.games.internal.api;

import android.content.Intent;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.Games.BaseGamesApiMethodImpl;
import com.google.android.gms.games.achievement.AchievementBuffer;
import com.google.android.gms.games.achievement.Achievements;
import com.google.android.gms.games.achievement.Achievements.LoadAchievementsResult;
import com.google.android.gms.games.achievement.Achievements.UpdateAchievementResult;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.internal.zzlx.zzb;

public final class AchievementsImpl implements Achievements {

    private static abstract class LoadImpl extends BaseGamesApiMethodImpl<LoadAchievementsResult> {

        /* renamed from: com.google.android.gms.games.internal.api.AchievementsImpl.LoadImpl.1 */
        class C11161 implements LoadAchievementsResult {
            final /* synthetic */ Status zzYl;
            final /* synthetic */ LoadImpl zzaDl;

            C11161(LoadImpl loadImpl, Status status) {
                this.zzaDl = loadImpl;
                this.zzYl = status;
            }

            public AchievementBuffer getAchievements() {
                return new AchievementBuffer(DataHolder.zzbJ(14));
            }

            public Status getStatus() {
                return this.zzYl;
            }

            public void release() {
            }
        }

        private LoadImpl(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        public LoadAchievementsResult zzY(Status status) {
            return new C11161(this, status);
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzY(status);
        }
    }

    private static abstract class UpdateImpl extends BaseGamesApiMethodImpl<UpdateAchievementResult> {
        private final String zzxX;

        /* renamed from: com.google.android.gms.games.internal.api.AchievementsImpl.UpdateImpl.1 */
        class C11171 implements UpdateAchievementResult {
            final /* synthetic */ Status zzYl;
            final /* synthetic */ UpdateImpl zzaDm;

            C11171(UpdateImpl updateImpl, Status status) {
                this.zzaDm = updateImpl;
                this.zzYl = status;
            }

            public String getAchievementId() {
                return this.zzaDm.zzxX;
            }

            public Status getStatus() {
                return this.zzYl;
            }
        }

        public UpdateImpl(String id, GoogleApiClient googleApiClient) {
            super(googleApiClient);
            this.zzxX = id;
        }

        public UpdateAchievementResult zzZ(Status status) {
            return new C11171(this, status);
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzZ(status);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.AchievementsImpl.10 */
    class AnonymousClass10 extends LoadImpl {
        final /* synthetic */ boolean zzaDg;
        final /* synthetic */ String zzaDi;
        final /* synthetic */ String zzabj;

        public void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zzb((zzb) this, this.zzabj, this.zzaDi, this.zzaDg);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.AchievementsImpl.1 */
    class C13131 extends LoadImpl {
        final /* synthetic */ boolean zzaDg;
        final /* synthetic */ AchievementsImpl zzaDh;

        C13131(AchievementsImpl achievementsImpl, GoogleApiClient x0, boolean z) {
            this.zzaDh = achievementsImpl;
            this.zzaDg = z;
            super(null);
        }

        public void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zzc((zzb) this, this.zzaDg);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.AchievementsImpl.2 */
    class C13142 extends UpdateImpl {
        final /* synthetic */ AchievementsImpl zzaDh;
        final /* synthetic */ String zzaDj;

        C13142(AchievementsImpl achievementsImpl, String x0, GoogleApiClient x1, String str) {
            this.zzaDh = achievementsImpl;
            this.zzaDj = str;
            super(x0, x1);
        }

        public void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zza(null, this.zzaDj);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.AchievementsImpl.3 */
    class C13153 extends UpdateImpl {
        final /* synthetic */ AchievementsImpl zzaDh;
        final /* synthetic */ String zzaDj;

        C13153(AchievementsImpl achievementsImpl, String x0, GoogleApiClient x1, String str) {
            this.zzaDh = achievementsImpl;
            this.zzaDj = str;
            super(x0, x1);
        }

        public void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zza((zzb) this, this.zzaDj);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.AchievementsImpl.4 */
    class C13164 extends UpdateImpl {
        final /* synthetic */ AchievementsImpl zzaDh;
        final /* synthetic */ String zzaDj;

        C13164(AchievementsImpl achievementsImpl, String x0, GoogleApiClient x1, String str) {
            this.zzaDh = achievementsImpl;
            this.zzaDj = str;
            super(x0, x1);
        }

        public void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zzb(null, this.zzaDj);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.AchievementsImpl.5 */
    class C13175 extends UpdateImpl {
        final /* synthetic */ AchievementsImpl zzaDh;
        final /* synthetic */ String zzaDj;

        C13175(AchievementsImpl achievementsImpl, String x0, GoogleApiClient x1, String str) {
            this.zzaDh = achievementsImpl;
            this.zzaDj = str;
            super(x0, x1);
        }

        public void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zzb((zzb) this, this.zzaDj);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.AchievementsImpl.6 */
    class C13186 extends UpdateImpl {
        final /* synthetic */ AchievementsImpl zzaDh;
        final /* synthetic */ String zzaDj;
        final /* synthetic */ int zzaDk;

        C13186(AchievementsImpl achievementsImpl, String x0, GoogleApiClient x1, String str, int i) {
            this.zzaDh = achievementsImpl;
            this.zzaDj = str;
            this.zzaDk = i;
            super(x0, x1);
        }

        public void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zza(null, this.zzaDj, this.zzaDk);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.AchievementsImpl.7 */
    class C13197 extends UpdateImpl {
        final /* synthetic */ AchievementsImpl zzaDh;
        final /* synthetic */ String zzaDj;
        final /* synthetic */ int zzaDk;

        C13197(AchievementsImpl achievementsImpl, String x0, GoogleApiClient x1, String str, int i) {
            this.zzaDh = achievementsImpl;
            this.zzaDj = str;
            this.zzaDk = i;
            super(x0, x1);
        }

        public void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zza((zzb) this, this.zzaDj, this.zzaDk);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.AchievementsImpl.8 */
    class C13208 extends UpdateImpl {
        final /* synthetic */ AchievementsImpl zzaDh;
        final /* synthetic */ String zzaDj;
        final /* synthetic */ int zzaDk;

        C13208(AchievementsImpl achievementsImpl, String x0, GoogleApiClient x1, String str, int i) {
            this.zzaDh = achievementsImpl;
            this.zzaDj = str;
            this.zzaDk = i;
            super(x0, x1);
        }

        public void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zzb(null, this.zzaDj, this.zzaDk);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.AchievementsImpl.9 */
    class C13219 extends UpdateImpl {
        final /* synthetic */ AchievementsImpl zzaDh;
        final /* synthetic */ String zzaDj;
        final /* synthetic */ int zzaDk;

        C13219(AchievementsImpl achievementsImpl, String x0, GoogleApiClient x1, String str, int i) {
            this.zzaDh = achievementsImpl;
            this.zzaDj = str;
            this.zzaDk = i;
            super(x0, x1);
        }

        public void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zzb((zzb) this, this.zzaDj, this.zzaDk);
        }
    }

    public Intent getAchievementsIntent(GoogleApiClient apiClient) {
        return Games.zzf(apiClient).zzvW();
    }

    public void increment(GoogleApiClient apiClient, String id, int numSteps) {
        apiClient.zzb(new C13186(this, id, apiClient, id, numSteps));
    }

    public PendingResult<UpdateAchievementResult> incrementImmediate(GoogleApiClient apiClient, String id, int numSteps) {
        return apiClient.zzb(new C13197(this, id, apiClient, id, numSteps));
    }

    public PendingResult<LoadAchievementsResult> load(GoogleApiClient apiClient, boolean forceReload) {
        return apiClient.zza(new C13131(this, apiClient, forceReload));
    }

    public void reveal(GoogleApiClient apiClient, String id) {
        apiClient.zzb(new C13142(this, id, apiClient, id));
    }

    public PendingResult<UpdateAchievementResult> revealImmediate(GoogleApiClient apiClient, String id) {
        return apiClient.zzb(new C13153(this, id, apiClient, id));
    }

    public void setSteps(GoogleApiClient apiClient, String id, int numSteps) {
        apiClient.zzb(new C13208(this, id, apiClient, id, numSteps));
    }

    public PendingResult<UpdateAchievementResult> setStepsImmediate(GoogleApiClient apiClient, String id, int numSteps) {
        return apiClient.zzb(new C13219(this, id, apiClient, id, numSteps));
    }

    public void unlock(GoogleApiClient apiClient, String id) {
        apiClient.zzb(new C13164(this, id, apiClient, id));
    }

    public PendingResult<UpdateAchievementResult> unlockImmediate(GoogleApiClient apiClient, String id) {
        return apiClient.zzb(new C13175(this, id, apiClient, id));
    }
}
