package com.google.android.gms.games.internal.api;

import android.content.Intent;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.Games.BaseGamesApiMethodImpl;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.quest.Milestone;
import com.google.android.gms.games.quest.Quest;
import com.google.android.gms.games.quest.QuestBuffer;
import com.google.android.gms.games.quest.QuestUpdateListener;
import com.google.android.gms.games.quest.Quests;
import com.google.android.gms.games.quest.Quests.AcceptQuestResult;
import com.google.android.gms.games.quest.Quests.ClaimMilestoneResult;
import com.google.android.gms.games.quest.Quests.LoadQuestsResult;
import com.google.android.gms.internal.zzlx.zzb;

public final class QuestsImpl implements Quests {

    private static abstract class AcceptImpl extends BaseGamesApiMethodImpl<AcceptQuestResult> {

        /* renamed from: com.google.android.gms.games.internal.api.QuestsImpl.AcceptImpl.1 */
        class C11381 implements AcceptQuestResult {
            final /* synthetic */ Status zzYl;
            final /* synthetic */ AcceptImpl zzaEt;

            C11381(AcceptImpl acceptImpl, Status status) {
                this.zzaEt = acceptImpl;
                this.zzYl = status;
            }

            public Quest getQuest() {
                return null;
            }

            public Status getStatus() {
                return this.zzYl;
            }
        }

        private AcceptImpl(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        public AcceptQuestResult zzav(Status status) {
            return new C11381(this, status);
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzav(status);
        }
    }

    private static abstract class ClaimImpl extends BaseGamesApiMethodImpl<ClaimMilestoneResult> {

        /* renamed from: com.google.android.gms.games.internal.api.QuestsImpl.ClaimImpl.1 */
        class C11391 implements ClaimMilestoneResult {
            final /* synthetic */ Status zzYl;
            final /* synthetic */ ClaimImpl zzaEu;

            C11391(ClaimImpl claimImpl, Status status) {
                this.zzaEu = claimImpl;
                this.zzYl = status;
            }

            public Milestone getMilestone() {
                return null;
            }

            public Quest getQuest() {
                return null;
            }

            public Status getStatus() {
                return this.zzYl;
            }
        }

        private ClaimImpl(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        public ClaimMilestoneResult zzaw(Status status) {
            return new C11391(this, status);
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzaw(status);
        }
    }

    private static abstract class LoadsImpl extends BaseGamesApiMethodImpl<LoadQuestsResult> {

        /* renamed from: com.google.android.gms.games.internal.api.QuestsImpl.LoadsImpl.1 */
        class C11401 implements LoadQuestsResult {
            final /* synthetic */ Status zzYl;
            final /* synthetic */ LoadsImpl zzaEv;

            C11401(LoadsImpl loadsImpl, Status status) {
                this.zzaEv = loadsImpl;
                this.zzYl = status;
            }

            public QuestBuffer getQuests() {
                return new QuestBuffer(DataHolder.zzbJ(this.zzYl.getStatusCode()));
            }

            public Status getStatus() {
                return this.zzYl;
            }

            public void release() {
            }
        }

        private LoadsImpl(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        public LoadQuestsResult zzax(Status status) {
            return new C11401(this, status);
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzax(status);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.QuestsImpl.1 */
    class C13551 extends AcceptImpl {
        final /* synthetic */ String zzaEn;
        final /* synthetic */ QuestsImpl zzaEo;

        C13551(QuestsImpl questsImpl, GoogleApiClient x0, String str) {
            this.zzaEo = questsImpl;
            this.zzaEn = str;
            super(null);
        }

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zzh((zzb) this, this.zzaEn);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.QuestsImpl.2 */
    class C13562 extends ClaimImpl {
        final /* synthetic */ String zzaEn;
        final /* synthetic */ QuestsImpl zzaEo;
        final /* synthetic */ String zzaEp;

        C13562(QuestsImpl questsImpl, GoogleApiClient x0, String str, String str2) {
            this.zzaEo = questsImpl;
            this.zzaEn = str;
            this.zzaEp = str2;
            super(null);
        }

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zzb((zzb) this, this.zzaEn, this.zzaEp);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.QuestsImpl.3 */
    class C13573 extends LoadsImpl {
        final /* synthetic */ int zzaDD;
        final /* synthetic */ boolean zzaDg;
        final /* synthetic */ QuestsImpl zzaEo;
        final /* synthetic */ int[] zzaEq;

        C13573(QuestsImpl questsImpl, GoogleApiClient x0, int[] iArr, int i, boolean z) {
            this.zzaEo = questsImpl;
            this.zzaEq = iArr;
            this.zzaDD = i;
            this.zzaDg = z;
            super(null);
        }

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zza((zzb) this, this.zzaEq, this.zzaDD, this.zzaDg);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.QuestsImpl.4 */
    class C13584 extends LoadsImpl {
        final /* synthetic */ boolean zzaDg;
        final /* synthetic */ QuestsImpl zzaEo;
        final /* synthetic */ String[] zzaEr;

        C13584(QuestsImpl questsImpl, GoogleApiClient x0, boolean z, String[] strArr) {
            this.zzaEo = questsImpl;
            this.zzaDg = z;
            this.zzaEr = strArr;
            super(null);
        }

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zzb((zzb) this, this.zzaDg, this.zzaEr);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.QuestsImpl.5 */
    class C13595 extends LoadsImpl {
        final /* synthetic */ int zzaDD;
        final /* synthetic */ boolean zzaDg;
        final /* synthetic */ String zzaDi;
        final /* synthetic */ int[] zzaEq;
        final /* synthetic */ String zzaEs;

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zza((zzb) this, this.zzaDi, this.zzaEs, this.zzaEq, this.zzaDD, this.zzaDg);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.QuestsImpl.6 */
    class C13606 extends LoadsImpl {
        final /* synthetic */ boolean zzaDg;
        final /* synthetic */ String zzaDi;
        final /* synthetic */ String[] zzaEr;
        final /* synthetic */ String zzaEs;

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zza((zzb) this, this.zzaDi, this.zzaEs, this.zzaDg, this.zzaEr);
        }
    }

    public PendingResult<AcceptQuestResult> accept(GoogleApiClient apiClient, String questId) {
        return apiClient.zzb(new C13551(this, apiClient, questId));
    }

    public PendingResult<ClaimMilestoneResult> claim(GoogleApiClient apiClient, String questId, String milestoneId) {
        return apiClient.zzb(new C13562(this, apiClient, questId, milestoneId));
    }

    public Intent getQuestIntent(GoogleApiClient apiClient, String questId) {
        return Games.zzf(apiClient).zzdu(questId);
    }

    public Intent getQuestsIntent(GoogleApiClient apiClient, int[] questSelectors) {
        return Games.zzf(apiClient).zzb(questSelectors);
    }

    public PendingResult<LoadQuestsResult> load(GoogleApiClient apiClient, int[] questSelectors, int sortOrder, boolean forceReload) {
        return apiClient.zza(new C13573(this, apiClient, questSelectors, sortOrder, forceReload));
    }

    public PendingResult<LoadQuestsResult> loadByIds(GoogleApiClient apiClient, boolean forceReload, String... questIds) {
        return apiClient.zza(new C13584(this, apiClient, forceReload, questIds));
    }

    public void registerQuestUpdateListener(GoogleApiClient apiClient, QuestUpdateListener listener) {
        GamesClientImpl zzb = Games.zzb(apiClient, false);
        if (zzb != null) {
            zzb.zzc(apiClient.zzq(listener));
        }
    }

    public void showStateChangedPopup(GoogleApiClient apiClient, String questId) {
        GamesClientImpl zzb = Games.zzb(apiClient, false);
        if (zzb != null) {
            zzb.zzdv(questId);
        }
    }

    public void unregisterQuestUpdateListener(GoogleApiClient apiClient) {
        GamesClientImpl zzb = Games.zzb(apiClient, false);
        if (zzb != null) {
            zzb.zzwb();
        }
    }
}
