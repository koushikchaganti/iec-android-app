package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.Games.BaseGamesApiMethodImpl;
import com.google.android.gms.games.appcontent.AppContents;
import com.google.android.gms.games.appcontent.AppContents.LoadAppContentResult;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.internal.zzlx.zzb;

public final class AppContentsImpl implements AppContents {

    private static abstract class LoadsImpl extends BaseGamesApiMethodImpl<LoadAppContentResult> {

        /* renamed from: com.google.android.gms.games.internal.api.AppContentsImpl.LoadsImpl.1 */
        class C11191 implements LoadAppContentResult {
            final /* synthetic */ Status zzYl;
            final /* synthetic */ LoadsImpl zzaDr;

            C11191(LoadsImpl loadsImpl, Status status) {
                this.zzaDr = loadsImpl;
                this.zzYl = status;
            }

            public Status getStatus() {
                return this.zzYl;
            }

            public void release() {
            }
        }

        public LoadAppContentResult zzad(Status status) {
            return new C11191(this, status);
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzad(status);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.AppContentsImpl.1 */
    class C13241 extends LoadsImpl {
        final /* synthetic */ boolean zzaDg;
        final /* synthetic */ int zzaDo;
        final /* synthetic */ String zzaDp;
        final /* synthetic */ String[] zzaDq;

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zza((zzb) this, this.zzaDo, this.zzaDp, this.zzaDq, this.zzaDg);
        }
    }
}
