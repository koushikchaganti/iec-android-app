package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.Games.BaseGamesApiMethodImpl;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.video.VideoConfiguration;
import com.google.android.gms.games.video.Videos;
import com.google.android.gms.games.video.Videos.ListVideosResult;

public final class VideosImpl implements Videos {

    /* renamed from: com.google.android.gms.games.internal.api.VideosImpl.1 */
    class C12251 extends BaseGamesApiMethodImpl<Status> {
        final /* synthetic */ String zzaDi;
        final /* synthetic */ String zzaFm;
        final /* synthetic */ VideoConfiguration zzaFn;

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zza((BaseGamesApiMethodImpl) this, this.zzaDi, this.zzaFm, this.zzaFn);
        }

        public Status zzb(Status status) {
            return status;
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzb(status);
        }
    }

    private static abstract class ListImpl extends BaseGamesApiMethodImpl<ListVideosResult> {

        /* renamed from: com.google.android.gms.games.internal.api.VideosImpl.ListImpl.1 */
        class C11561 implements ListVideosResult {
            final /* synthetic */ Status zzYl;
            final /* synthetic */ ListImpl zzaFo;

            C11561(ListImpl listImpl, Status status) {
                this.zzaFo = listImpl;
                this.zzYl = status;
            }

            public Status getStatus() {
                return this.zzYl;
            }
        }

        public ListVideosResult zzaN(Status status) {
            return new C11561(this, status);
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzaN(status);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.VideosImpl.2 */
    class C13852 extends ListImpl {
        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zzg(this);
        }
    }
}
