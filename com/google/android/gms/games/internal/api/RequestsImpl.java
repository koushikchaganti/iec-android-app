package com.google.android.gms.games.internal.api;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.Games.BaseGamesApiMethodImpl;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.request.GameRequest;
import com.google.android.gms.games.request.GameRequestBuffer;
import com.google.android.gms.games.request.OnRequestReceivedListener;
import com.google.android.gms.games.request.Requests;
import com.google.android.gms.games.request.Requests.LoadRequestSummariesResult;
import com.google.android.gms.games.request.Requests.LoadRequestsResult;
import com.google.android.gms.games.request.Requests.SendRequestResult;
import com.google.android.gms.games.request.Requests.UpdateRequestsResult;
import com.google.android.gms.internal.zzlx.zzb;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public final class RequestsImpl implements Requests {

    private static abstract class LoadRequestSummariesImpl extends BaseGamesApiMethodImpl<LoadRequestSummariesResult> {

        /* renamed from: com.google.android.gms.games.internal.api.RequestsImpl.LoadRequestSummariesImpl.1 */
        class C11411 implements LoadRequestSummariesResult {
            final /* synthetic */ Status zzYl;
            final /* synthetic */ LoadRequestSummariesImpl zzaEE;

            C11411(LoadRequestSummariesImpl loadRequestSummariesImpl, Status status) {
                this.zzaEE = loadRequestSummariesImpl;
                this.zzYl = status;
            }

            public Status getStatus() {
                return this.zzYl;
            }

            public void release() {
            }
        }

        public LoadRequestSummariesResult zzay(Status status) {
            return new C11411(this, status);
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzay(status);
        }
    }

    private static abstract class LoadRequestsImpl extends BaseGamesApiMethodImpl<LoadRequestsResult> {

        /* renamed from: com.google.android.gms.games.internal.api.RequestsImpl.LoadRequestsImpl.1 */
        class C11421 implements LoadRequestsResult {
            final /* synthetic */ Status zzYl;
            final /* synthetic */ LoadRequestsImpl zzaEF;

            C11421(LoadRequestsImpl loadRequestsImpl, Status status) {
                this.zzaEF = loadRequestsImpl;
                this.zzYl = status;
            }

            public GameRequestBuffer getRequests(int type) {
                return new GameRequestBuffer(DataHolder.zzbJ(this.zzYl.getStatusCode()));
            }

            public Status getStatus() {
                return this.zzYl;
            }

            public void release() {
            }
        }

        private LoadRequestsImpl(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        public LoadRequestsResult zzaz(Status status) {
            return new C11421(this, status);
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzaz(status);
        }
    }

    private static abstract class SendRequestImpl extends BaseGamesApiMethodImpl<SendRequestResult> {

        /* renamed from: com.google.android.gms.games.internal.api.RequestsImpl.SendRequestImpl.1 */
        class C11431 implements SendRequestResult {
            final /* synthetic */ Status zzYl;
            final /* synthetic */ SendRequestImpl zzaEG;

            C11431(SendRequestImpl sendRequestImpl, Status status) {
                this.zzaEG = sendRequestImpl;
                this.zzYl = status;
            }

            public Status getStatus() {
                return this.zzYl;
            }
        }

        public SendRequestResult zzaA(Status status) {
            return new C11431(this, status);
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzaA(status);
        }
    }

    private static abstract class UpdateRequestsImpl extends BaseGamesApiMethodImpl<UpdateRequestsResult> {

        /* renamed from: com.google.android.gms.games.internal.api.RequestsImpl.UpdateRequestsImpl.1 */
        class C11441 implements UpdateRequestsResult {
            final /* synthetic */ Status zzYl;
            final /* synthetic */ UpdateRequestsImpl zzaEH;

            C11441(UpdateRequestsImpl updateRequestsImpl, Status status) {
                this.zzaEH = updateRequestsImpl;
                this.zzYl = status;
            }

            public Set<String> getRequestIds() {
                return null;
            }

            public int getRequestOutcome(String requestId) {
                throw new IllegalArgumentException("Unknown request ID " + requestId);
            }

            public Status getStatus() {
                return this.zzYl;
            }

            public void release() {
            }
        }

        private UpdateRequestsImpl(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        public UpdateRequestsResult zzaB(Status status) {
            return new C11441(this, status);
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzaB(status);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.RequestsImpl.1 */
    class C13611 extends UpdateRequestsImpl {
        final /* synthetic */ String[] zzaEw;
        final /* synthetic */ RequestsImpl zzaEx;

        C13611(RequestsImpl requestsImpl, GoogleApiClient x0, String[] strArr) {
            this.zzaEx = requestsImpl;
            this.zzaEw = strArr;
            super(null);
        }

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zzb((zzb) this, this.zzaEw);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.RequestsImpl.2 */
    class C13622 extends UpdateRequestsImpl {
        final /* synthetic */ String[] zzaEw;
        final /* synthetic */ RequestsImpl zzaEx;

        C13622(RequestsImpl requestsImpl, GoogleApiClient x0, String[] strArr) {
            this.zzaEx = requestsImpl;
            this.zzaEw = strArr;
            super(null);
        }

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zzc((zzb) this, this.zzaEw);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.RequestsImpl.3 */
    class C13633 extends LoadRequestsImpl {
        final /* synthetic */ int zzaDD;
        final /* synthetic */ RequestsImpl zzaEx;
        final /* synthetic */ int zzaEy;
        final /* synthetic */ int zzaEz;

        C13633(RequestsImpl requestsImpl, GoogleApiClient x0, int i, int i2, int i3) {
            this.zzaEx = requestsImpl;
            this.zzaEy = i;
            this.zzaEz = i2;
            this.zzaDD = i3;
            super(null);
        }

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zza((zzb) this, this.zzaEy, this.zzaEz, this.zzaDD);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.RequestsImpl.4 */
    class C13644 extends SendRequestImpl {
        final /* synthetic */ String zzaDi;
        final /* synthetic */ String[] zzaEA;
        final /* synthetic */ int zzaEB;
        final /* synthetic */ byte[] zzaEC;
        final /* synthetic */ int zzaED;

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zza((zzb) this, this.zzaDi, this.zzaEA, this.zzaEB, this.zzaEC, this.zzaED);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.RequestsImpl.5 */
    class C13655 extends SendRequestImpl {
        final /* synthetic */ String zzaDi;
        final /* synthetic */ String[] zzaEA;
        final /* synthetic */ int zzaEB;
        final /* synthetic */ byte[] zzaEC;
        final /* synthetic */ int zzaED;

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zza((zzb) this, this.zzaDi, this.zzaEA, this.zzaEB, this.zzaEC, this.zzaED);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.RequestsImpl.6 */
    class C13666 extends UpdateRequestsImpl {
        final /* synthetic */ String zzaDi;
        final /* synthetic */ String zzaEs;
        final /* synthetic */ String[] zzaEw;

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zza((zzb) this, this.zzaDi, this.zzaEs, this.zzaEw);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.RequestsImpl.7 */
    class C13677 extends LoadRequestsImpl {
        final /* synthetic */ int zzaDD;
        final /* synthetic */ String zzaDi;
        final /* synthetic */ String zzaEs;
        final /* synthetic */ int zzaEy;
        final /* synthetic */ int zzaEz;

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zza((zzb) this, this.zzaDi, this.zzaEs, this.zzaEy, this.zzaEz, this.zzaDD);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.RequestsImpl.8 */
    class C13688 extends LoadRequestSummariesImpl {
        final /* synthetic */ String zzaEs;
        final /* synthetic */ int zzaEz;

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zzf(this, this.zzaEs, this.zzaEz);
        }
    }

    public PendingResult<UpdateRequestsResult> acceptRequest(GoogleApiClient apiClient, String requestId) {
        List arrayList = new ArrayList();
        arrayList.add(requestId);
        return acceptRequests(apiClient, arrayList);
    }

    public PendingResult<UpdateRequestsResult> acceptRequests(GoogleApiClient apiClient, List<String> requestIds) {
        return apiClient.zzb(new C13611(this, apiClient, requestIds == null ? null : (String[]) requestIds.toArray(new String[requestIds.size()])));
    }

    public PendingResult<UpdateRequestsResult> dismissRequest(GoogleApiClient apiClient, String requestId) {
        List arrayList = new ArrayList();
        arrayList.add(requestId);
        return dismissRequests(apiClient, arrayList);
    }

    public PendingResult<UpdateRequestsResult> dismissRequests(GoogleApiClient apiClient, List<String> requestIds) {
        return apiClient.zzb(new C13622(this, apiClient, requestIds == null ? null : (String[]) requestIds.toArray(new String[requestIds.size()])));
    }

    public ArrayList<GameRequest> getGameRequestsFromBundle(Bundle extras) {
        if (extras == null || !extras.containsKey(Requests.EXTRA_REQUESTS)) {
            return new ArrayList();
        }
        ArrayList arrayList = (ArrayList) extras.get(Requests.EXTRA_REQUESTS);
        ArrayList<GameRequest> arrayList2 = new ArrayList();
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            arrayList2.add((GameRequest) arrayList.get(i));
        }
        return arrayList2;
    }

    public ArrayList<GameRequest> getGameRequestsFromInboxResponse(Intent response) {
        return response == null ? new ArrayList() : getGameRequestsFromBundle(response.getExtras());
    }

    public Intent getInboxIntent(GoogleApiClient apiClient) {
        return Games.zzf(apiClient).zzwi();
    }

    public int getMaxLifetimeDays(GoogleApiClient apiClient) {
        return Games.zzf(apiClient).zzwk();
    }

    public int getMaxPayloadSize(GoogleApiClient apiClient) {
        return Games.zzf(apiClient).zzwj();
    }

    public Intent getSendIntent(GoogleApiClient apiClient, int type, byte[] payload, int requestLifetimeDays, Bitmap icon, String description) {
        return Games.zzf(apiClient).zza(type, payload, requestLifetimeDays, icon, description);
    }

    public PendingResult<LoadRequestsResult> loadRequests(GoogleApiClient apiClient, int requestDirection, int types, int sortOrder) {
        return apiClient.zza(new C13633(this, apiClient, requestDirection, types, sortOrder));
    }

    public void registerRequestListener(GoogleApiClient apiClient, OnRequestReceivedListener listener) {
        GamesClientImpl zzb = Games.zzb(apiClient, false);
        if (zzb != null) {
            zzb.zzd(apiClient.zzq(listener));
        }
    }

    public void unregisterRequestListener(GoogleApiClient apiClient) {
        GamesClientImpl zzb = Games.zzb(apiClient, false);
        if (zzb != null) {
            zzb.zzwc();
        }
    }
}
