package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.Games.BaseGamesApiMethodImpl;
import com.google.android.gms.games.event.EventBuffer;
import com.google.android.gms.games.event.Events;
import com.google.android.gms.games.event.Events.LoadEventsResult;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.internal.zzlx.zzb;

public final class EventsImpl implements Events {

    private static abstract class LoadImpl extends BaseGamesApiMethodImpl<LoadEventsResult> {

        /* renamed from: com.google.android.gms.games.internal.api.EventsImpl.LoadImpl.1 */
        class C11201 implements LoadEventsResult {
            final /* synthetic */ Status zzYl;
            final /* synthetic */ LoadImpl zzaDw;

            C11201(LoadImpl loadImpl, Status status) {
                this.zzaDw = loadImpl;
                this.zzYl = status;
            }

            public EventBuffer getEvents() {
                return new EventBuffer(DataHolder.zzbJ(14));
            }

            public Status getStatus() {
                return this.zzYl;
            }

            public void release() {
            }
        }

        private LoadImpl(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        public LoadEventsResult zzae(Status status) {
            return new C11201(this, status);
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzae(status);
        }
    }

    private static abstract class UpdateImpl extends BaseGamesApiMethodImpl<Result> {

        /* renamed from: com.google.android.gms.games.internal.api.EventsImpl.UpdateImpl.1 */
        class C07281 implements Result {
            final /* synthetic */ Status zzYl;
            final /* synthetic */ UpdateImpl zzaDx;

            C07281(UpdateImpl updateImpl, Status status) {
                this.zzaDx = updateImpl;
                this.zzYl = status;
            }

            public Status getStatus() {
                return this.zzYl;
            }
        }

        private UpdateImpl(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        public Result zzc(Status status) {
            return new C07281(this, status);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.EventsImpl.1 */
    class C13251 extends LoadImpl {
        final /* synthetic */ boolean zzaDg;
        final /* synthetic */ String[] zzaDs;
        final /* synthetic */ EventsImpl zzaDt;

        C13251(EventsImpl eventsImpl, GoogleApiClient x0, boolean z, String[] strArr) {
            this.zzaDt = eventsImpl;
            this.zzaDg = z;
            this.zzaDs = strArr;
            super(null);
        }

        public void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zza((zzb) this, this.zzaDg, this.zzaDs);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.EventsImpl.2 */
    class C13262 extends LoadImpl {
        final /* synthetic */ boolean zzaDg;
        final /* synthetic */ EventsImpl zzaDt;

        C13262(EventsImpl eventsImpl, GoogleApiClient x0, boolean z) {
            this.zzaDt = eventsImpl;
            this.zzaDg = z;
            super(null);
        }

        public void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zzd((zzb) this, this.zzaDg);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.EventsImpl.3 */
    class C13273 extends UpdateImpl {
        final /* synthetic */ EventsImpl zzaDt;
        final /* synthetic */ String zzaDu;
        final /* synthetic */ int zzaDv;

        C13273(EventsImpl eventsImpl, GoogleApiClient x0, String str, int i) {
            this.zzaDt = eventsImpl;
            this.zzaDu = str;
            this.zzaDv = i;
            super(null);
        }

        public void zza(GamesClientImpl gamesClientImpl) {
            gamesClientImpl.zzp(this.zzaDu, this.zzaDv);
        }
    }

    public void increment(GoogleApiClient apiClient, String eventId, int incrementAmount) {
        GamesClientImpl zzc = Games.zzc(apiClient, false);
        if (zzc != null) {
            if (zzc.isConnected()) {
                zzc.zzp(eventId, incrementAmount);
            } else {
                apiClient.zzb(new C13273(this, apiClient, eventId, incrementAmount));
            }
        }
    }

    public PendingResult<LoadEventsResult> load(GoogleApiClient apiClient, boolean forceReload) {
        return apiClient.zza(new C13262(this, apiClient, forceReload));
    }

    public PendingResult<LoadEventsResult> loadByIds(GoogleApiClient apiClient, boolean forceReload, String... eventIds) {
        return apiClient.zza(new C13251(this, apiClient, forceReload, eventIds));
    }
}
