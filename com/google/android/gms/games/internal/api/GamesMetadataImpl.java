package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameBuffer;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.Games.BaseGamesApiMethodImpl;
import com.google.android.gms.games.GamesMetadata;
import com.google.android.gms.games.GamesMetadata.LoadGameInstancesResult;
import com.google.android.gms.games.GamesMetadata.LoadGameSearchSuggestionsResult;
import com.google.android.gms.games.GamesMetadata.LoadGamesResult;
import com.google.android.gms.games.internal.GamesClientImpl;

public final class GamesMetadataImpl implements GamesMetadata {

    private static abstract class LoadGameInstancesImpl extends BaseGamesApiMethodImpl<LoadGameInstancesResult> {

        /* renamed from: com.google.android.gms.games.internal.api.GamesMetadataImpl.LoadGameInstancesImpl.1 */
        class C11211 implements LoadGameInstancesResult {
            final /* synthetic */ Status zzYl;
            final /* synthetic */ LoadGameInstancesImpl zzaDA;

            C11211(LoadGameInstancesImpl loadGameInstancesImpl, Status status) {
                this.zzaDA = loadGameInstancesImpl;
                this.zzYl = status;
            }

            public Status getStatus() {
                return this.zzYl;
            }

            public void release() {
            }
        }

        public LoadGameInstancesResult zzaf(Status status) {
            return new C11211(this, status);
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzaf(status);
        }
    }

    private static abstract class LoadGameSearchSuggestionsImpl extends BaseGamesApiMethodImpl<LoadGameSearchSuggestionsResult> {

        /* renamed from: com.google.android.gms.games.internal.api.GamesMetadataImpl.LoadGameSearchSuggestionsImpl.1 */
        class C11221 implements LoadGameSearchSuggestionsResult {
            final /* synthetic */ Status zzYl;
            final /* synthetic */ LoadGameSearchSuggestionsImpl zzaDB;

            C11221(LoadGameSearchSuggestionsImpl loadGameSearchSuggestionsImpl, Status status) {
                this.zzaDB = loadGameSearchSuggestionsImpl;
                this.zzYl = status;
            }

            public Status getStatus() {
                return this.zzYl;
            }

            public void release() {
            }
        }

        public LoadGameSearchSuggestionsResult zzag(Status status) {
            return new C11221(this, status);
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzag(status);
        }
    }

    private static abstract class LoadGamesImpl extends BaseGamesApiMethodImpl<LoadGamesResult> {

        /* renamed from: com.google.android.gms.games.internal.api.GamesMetadataImpl.LoadGamesImpl.1 */
        class C11231 implements LoadGamesResult {
            final /* synthetic */ Status zzYl;
            final /* synthetic */ LoadGamesImpl zzaDC;

            C11231(LoadGamesImpl loadGamesImpl, Status status) {
                this.zzaDC = loadGamesImpl;
                this.zzYl = status;
            }

            public GameBuffer getGames() {
                return new GameBuffer(DataHolder.zzbJ(14));
            }

            public Status getStatus() {
                return this.zzYl;
            }

            public void release() {
            }
        }

        private LoadGamesImpl(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        public LoadGamesResult zzah(Status status) {
            return new C11231(this, status);
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzah(status);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.GamesMetadataImpl.1 */
    class C13281 extends LoadGamesImpl {
        final /* synthetic */ GamesMetadataImpl zzaDy;

        C13281(GamesMetadataImpl gamesMetadataImpl, GoogleApiClient x0) {
            this.zzaDy = gamesMetadataImpl;
            super(null);
        }

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zze(this);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.GamesMetadataImpl.2 */
    class C13292 extends LoadGameInstancesImpl {
        final /* synthetic */ String zzaDi;

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zzj(this, this.zzaDi);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.GamesMetadataImpl.3 */
    class C13303 extends LoadGameSearchSuggestionsImpl {
        final /* synthetic */ String zzaDz;

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zzk(this, this.zzaDz);
        }
    }

    public Game getCurrentGame(GoogleApiClient apiClient) {
        return Games.zzf(apiClient).zzvU();
    }

    public PendingResult<LoadGamesResult> loadGame(GoogleApiClient apiClient) {
        return apiClient.zza(new C13281(this, apiClient));
    }
}
