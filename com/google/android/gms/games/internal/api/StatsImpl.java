package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.Games.BaseGamesApiMethodImpl;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.stats.PlayerStats;
import com.google.android.gms.games.stats.Stats;
import com.google.android.gms.games.stats.Stats.LoadPlayerStatsResult;
import com.google.android.gms.internal.zzlx.zzb;

public final class StatsImpl implements Stats {

    private static abstract class LoadsImpl extends BaseGamesApiMethodImpl<LoadPlayerStatsResult> {

        /* renamed from: com.google.android.gms.games.internal.api.StatsImpl.LoadsImpl.1 */
        class C11491 implements LoadPlayerStatsResult {
            final /* synthetic */ Status zzYl;
            final /* synthetic */ LoadsImpl zzaEX;

            C11491(LoadsImpl loadsImpl, Status status) {
                this.zzaEX = loadsImpl;
                this.zzYl = status;
            }

            public PlayerStats getPlayerStats() {
                return null;
            }

            public Status getStatus() {
                return this.zzYl;
            }

            public void release() {
            }
        }

        private LoadsImpl(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        public LoadPlayerStatsResult zzaG(Status status) {
            return new C11491(this, status);
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzaG(status);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.StatsImpl.1 */
    class C13751 extends LoadsImpl {
        final /* synthetic */ boolean zzaDg;
        final /* synthetic */ StatsImpl zzaEW;

        C13751(StatsImpl statsImpl, GoogleApiClient x0, boolean z) {
            this.zzaEW = statsImpl;
            this.zzaDg = z;
            super(null);
        }

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zze((zzb) this, this.zzaDg);
        }
    }

    public PendingResult<LoadPlayerStatsResult> loadPlayerStats(GoogleApiClient apiClient, boolean forceReload) {
        return apiClient.zza(new C13751(this, apiClient, forceReload));
    }
}
