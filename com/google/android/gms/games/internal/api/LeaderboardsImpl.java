package com.google.android.gms.games.internal.api;

import android.content.Intent;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.Games.BaseGamesApiMethodImpl;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.internal.GamesLog;
import com.google.android.gms.games.leaderboard.Leaderboard;
import com.google.android.gms.games.leaderboard.LeaderboardBuffer;
import com.google.android.gms.games.leaderboard.LeaderboardScore;
import com.google.android.gms.games.leaderboard.LeaderboardScoreBuffer;
import com.google.android.gms.games.leaderboard.Leaderboards;
import com.google.android.gms.games.leaderboard.Leaderboards.LeaderboardMetadataResult;
import com.google.android.gms.games.leaderboard.Leaderboards.LoadPlayerScoreResult;
import com.google.android.gms.games.leaderboard.Leaderboards.LoadScoresResult;
import com.google.android.gms.games.leaderboard.Leaderboards.SubmitScoreResult;
import com.google.android.gms.games.leaderboard.ScoreSubmissionData;
import com.google.android.gms.internal.zzlx.zzb;

public final class LeaderboardsImpl implements Leaderboards {

    private static abstract class LoadMetadataImpl extends BaseGamesApiMethodImpl<LeaderboardMetadataResult> {

        /* renamed from: com.google.android.gms.games.internal.api.LeaderboardsImpl.LoadMetadataImpl.1 */
        class C11251 implements LeaderboardMetadataResult {
            final /* synthetic */ Status zzYl;
            final /* synthetic */ LoadMetadataImpl zzaDQ;

            C11251(LoadMetadataImpl loadMetadataImpl, Status status) {
                this.zzaDQ = loadMetadataImpl;
                this.zzYl = status;
            }

            public LeaderboardBuffer getLeaderboards() {
                return new LeaderboardBuffer(DataHolder.zzbJ(14));
            }

            public Status getStatus() {
                return this.zzYl;
            }

            public void release() {
            }
        }

        private LoadMetadataImpl(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        public LeaderboardMetadataResult zzaj(Status status) {
            return new C11251(this, status);
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzaj(status);
        }
    }

    private static abstract class LoadPlayerScoreImpl extends BaseGamesApiMethodImpl<LoadPlayerScoreResult> {

        /* renamed from: com.google.android.gms.games.internal.api.LeaderboardsImpl.LoadPlayerScoreImpl.1 */
        class C11261 implements LoadPlayerScoreResult {
            final /* synthetic */ Status zzYl;
            final /* synthetic */ LoadPlayerScoreImpl zzaDR;

            C11261(LoadPlayerScoreImpl loadPlayerScoreImpl, Status status) {
                this.zzaDR = loadPlayerScoreImpl;
                this.zzYl = status;
            }

            public LeaderboardScore getScore() {
                return null;
            }

            public Status getStatus() {
                return this.zzYl;
            }
        }

        private LoadPlayerScoreImpl(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        public LoadPlayerScoreResult zzak(Status status) {
            return new C11261(this, status);
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzak(status);
        }
    }

    private static abstract class LoadScoresImpl extends BaseGamesApiMethodImpl<LoadScoresResult> {

        /* renamed from: com.google.android.gms.games.internal.api.LeaderboardsImpl.LoadScoresImpl.1 */
        class C11271 implements LoadScoresResult {
            final /* synthetic */ Status zzYl;
            final /* synthetic */ LoadScoresImpl zzaDS;

            C11271(LoadScoresImpl loadScoresImpl, Status status) {
                this.zzaDS = loadScoresImpl;
                this.zzYl = status;
            }

            public Leaderboard getLeaderboard() {
                return null;
            }

            public LeaderboardScoreBuffer getScores() {
                return new LeaderboardScoreBuffer(DataHolder.zzbJ(14));
            }

            public Status getStatus() {
                return this.zzYl;
            }

            public void release() {
            }
        }

        private LoadScoresImpl(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        public LoadScoresResult zzal(Status status) {
            return new C11271(this, status);
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzal(status);
        }
    }

    protected static abstract class SubmitScoreImpl extends BaseGamesApiMethodImpl<SubmitScoreResult> {

        /* renamed from: com.google.android.gms.games.internal.api.LeaderboardsImpl.SubmitScoreImpl.1 */
        class C11281 implements SubmitScoreResult {
            final /* synthetic */ Status zzYl;
            final /* synthetic */ SubmitScoreImpl zzaDT;

            C11281(SubmitScoreImpl submitScoreImpl, Status status) {
                this.zzaDT = submitScoreImpl;
                this.zzYl = status;
            }

            public ScoreSubmissionData getScoreData() {
                return new ScoreSubmissionData(DataHolder.zzbJ(14));
            }

            public Status getStatus() {
                return this.zzYl;
            }

            public void release() {
            }
        }

        protected SubmitScoreImpl(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        public SubmitScoreResult zzam(Status status) {
            return new C11281(this, status);
        }

        public /* synthetic */ Result zzc(Status status) {
            return zzam(status);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.LeaderboardsImpl.10 */
    class AnonymousClass10 extends LoadScoresImpl {
        final /* synthetic */ String zzaDI;
        final /* synthetic */ int zzaDJ;
        final /* synthetic */ int zzaDK;
        final /* synthetic */ int zzaDL;
        final /* synthetic */ boolean zzaDg;
        final /* synthetic */ String zzaDi;

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zza(this, this.zzaDi, this.zzaDI, this.zzaDJ, this.zzaDK, this.zzaDL, this.zzaDg);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.LeaderboardsImpl.11 */
    class AnonymousClass11 extends LoadScoresImpl {
        final /* synthetic */ String zzaDI;
        final /* synthetic */ int zzaDJ;
        final /* synthetic */ int zzaDK;
        final /* synthetic */ int zzaDL;
        final /* synthetic */ boolean zzaDg;
        final /* synthetic */ String zzaDi;

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zzb(this, this.zzaDi, this.zzaDI, this.zzaDJ, this.zzaDK, this.zzaDL, this.zzaDg);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.LeaderboardsImpl.1 */
    class C13341 extends LoadMetadataImpl {
        final /* synthetic */ LeaderboardsImpl zzaDH;
        final /* synthetic */ boolean zzaDg;

        C13341(LeaderboardsImpl leaderboardsImpl, GoogleApiClient x0, boolean z) {
            this.zzaDH = leaderboardsImpl;
            this.zzaDg = z;
            super(null);
        }

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zzb((zzb) this, this.zzaDg);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.LeaderboardsImpl.2 */
    class C13352 extends LoadMetadataImpl {
        final /* synthetic */ LeaderboardsImpl zzaDH;
        final /* synthetic */ String zzaDI;
        final /* synthetic */ boolean zzaDg;

        C13352(LeaderboardsImpl leaderboardsImpl, GoogleApiClient x0, String str, boolean z) {
            this.zzaDH = leaderboardsImpl;
            this.zzaDI = str;
            this.zzaDg = z;
            super(null);
        }

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zzb((zzb) this, this.zzaDI, this.zzaDg);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.LeaderboardsImpl.3 */
    class C13363 extends LoadPlayerScoreImpl {
        final /* synthetic */ LeaderboardsImpl zzaDH;
        final /* synthetic */ String zzaDI;
        final /* synthetic */ int zzaDJ;
        final /* synthetic */ int zzaDK;

        C13363(LeaderboardsImpl leaderboardsImpl, GoogleApiClient x0, String str, int i, int i2) {
            this.zzaDH = leaderboardsImpl;
            this.zzaDI = str;
            this.zzaDJ = i;
            this.zzaDK = i2;
            super(null);
        }

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zza((zzb) this, null, this.zzaDI, this.zzaDJ, this.zzaDK);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.LeaderboardsImpl.4 */
    class C13374 extends LoadScoresImpl {
        final /* synthetic */ LeaderboardsImpl zzaDH;
        final /* synthetic */ String zzaDI;
        final /* synthetic */ int zzaDJ;
        final /* synthetic */ int zzaDK;
        final /* synthetic */ int zzaDL;
        final /* synthetic */ boolean zzaDg;

        C13374(LeaderboardsImpl leaderboardsImpl, GoogleApiClient x0, String str, int i, int i2, int i3, boolean z) {
            this.zzaDH = leaderboardsImpl;
            this.zzaDI = str;
            this.zzaDJ = i;
            this.zzaDK = i2;
            this.zzaDL = i3;
            this.zzaDg = z;
            super(null);
        }

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zza((zzb) this, this.zzaDI, this.zzaDJ, this.zzaDK, this.zzaDL, this.zzaDg);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.LeaderboardsImpl.5 */
    class C13385 extends LoadScoresImpl {
        final /* synthetic */ LeaderboardsImpl zzaDH;
        final /* synthetic */ String zzaDI;
        final /* synthetic */ int zzaDJ;
        final /* synthetic */ int zzaDK;
        final /* synthetic */ int zzaDL;
        final /* synthetic */ boolean zzaDg;

        C13385(LeaderboardsImpl leaderboardsImpl, GoogleApiClient x0, String str, int i, int i2, int i3, boolean z) {
            this.zzaDH = leaderboardsImpl;
            this.zzaDI = str;
            this.zzaDJ = i;
            this.zzaDK = i2;
            this.zzaDL = i3;
            this.zzaDg = z;
            super(null);
        }

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zzb((zzb) this, this.zzaDI, this.zzaDJ, this.zzaDK, this.zzaDL, this.zzaDg);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.LeaderboardsImpl.6 */
    class C13396 extends LoadScoresImpl {
        final /* synthetic */ LeaderboardsImpl zzaDH;
        final /* synthetic */ int zzaDL;
        final /* synthetic */ LeaderboardScoreBuffer zzaDM;
        final /* synthetic */ int zzaDN;

        C13396(LeaderboardsImpl leaderboardsImpl, GoogleApiClient x0, LeaderboardScoreBuffer leaderboardScoreBuffer, int i, int i2) {
            this.zzaDH = leaderboardsImpl;
            this.zzaDM = leaderboardScoreBuffer;
            this.zzaDL = i;
            this.zzaDN = i2;
            super(null);
        }

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zza((zzb) this, this.zzaDM, this.zzaDL, this.zzaDN);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.LeaderboardsImpl.7 */
    class C13407 extends SubmitScoreImpl {
        final /* synthetic */ LeaderboardsImpl zzaDH;
        final /* synthetic */ String zzaDI;
        final /* synthetic */ long zzaDO;
        final /* synthetic */ String zzaDP;

        C13407(LeaderboardsImpl leaderboardsImpl, GoogleApiClient x0, String str, long j, String str2) {
            this.zzaDH = leaderboardsImpl;
            this.zzaDI = str;
            this.zzaDO = j;
            this.zzaDP = str2;
            super(x0);
        }

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zza((zzb) this, this.zzaDI, this.zzaDO, this.zzaDP);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.LeaderboardsImpl.8 */
    class C13418 extends LoadMetadataImpl {
        final /* synthetic */ boolean zzaDg;
        final /* synthetic */ String zzaDi;

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zzc((zzb) this, this.zzaDi, this.zzaDg);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.LeaderboardsImpl.9 */
    class C13429 extends LoadMetadataImpl {
        final /* synthetic */ String zzaDI;
        final /* synthetic */ boolean zzaDg;
        final /* synthetic */ String zzaDi;

        protected void zza(GamesClientImpl gamesClientImpl) throws RemoteException {
            gamesClientImpl.zza((zzb) this, this.zzaDi, this.zzaDI, this.zzaDg);
        }
    }

    public Intent getAllLeaderboardsIntent(GoogleApiClient apiClient) {
        return Games.zzf(apiClient).zzvV();
    }

    public Intent getLeaderboardIntent(GoogleApiClient apiClient, String leaderboardId) {
        return getLeaderboardIntent(apiClient, leaderboardId, -1);
    }

    public Intent getLeaderboardIntent(GoogleApiClient apiClient, String leaderboardId, int timeSpan) {
        return getLeaderboardIntent(apiClient, leaderboardId, timeSpan, -1);
    }

    public Intent getLeaderboardIntent(GoogleApiClient apiClient, String leaderboardId, int timeSpan, int collection) {
        return Games.zzf(apiClient).zzl(leaderboardId, timeSpan, collection);
    }

    public PendingResult<LoadPlayerScoreResult> loadCurrentPlayerLeaderboardScore(GoogleApiClient apiClient, String leaderboardId, int span, int leaderboardCollection) {
        return apiClient.zza(new C13363(this, apiClient, leaderboardId, span, leaderboardCollection));
    }

    public PendingResult<LeaderboardMetadataResult> loadLeaderboardMetadata(GoogleApiClient apiClient, String leaderboardId, boolean forceReload) {
        return apiClient.zza(new C13352(this, apiClient, leaderboardId, forceReload));
    }

    public PendingResult<LeaderboardMetadataResult> loadLeaderboardMetadata(GoogleApiClient apiClient, boolean forceReload) {
        return apiClient.zza(new C13341(this, apiClient, forceReload));
    }

    public PendingResult<LoadScoresResult> loadMoreScores(GoogleApiClient apiClient, LeaderboardScoreBuffer buffer, int maxResults, int pageDirection) {
        return apiClient.zza(new C13396(this, apiClient, buffer, maxResults, pageDirection));
    }

    public PendingResult<LoadScoresResult> loadPlayerCenteredScores(GoogleApiClient apiClient, String leaderboardId, int span, int leaderboardCollection, int maxResults) {
        return loadPlayerCenteredScores(apiClient, leaderboardId, span, leaderboardCollection, maxResults, false);
    }

    public PendingResult<LoadScoresResult> loadPlayerCenteredScores(GoogleApiClient apiClient, String leaderboardId, int span, int leaderboardCollection, int maxResults, boolean forceReload) {
        return apiClient.zza(new C13385(this, apiClient, leaderboardId, span, leaderboardCollection, maxResults, forceReload));
    }

    public PendingResult<LoadScoresResult> loadTopScores(GoogleApiClient apiClient, String leaderboardId, int span, int leaderboardCollection, int maxResults) {
        return loadTopScores(apiClient, leaderboardId, span, leaderboardCollection, maxResults, false);
    }

    public PendingResult<LoadScoresResult> loadTopScores(GoogleApiClient apiClient, String leaderboardId, int span, int leaderboardCollection, int maxResults, boolean forceReload) {
        return apiClient.zza(new C13374(this, apiClient, leaderboardId, span, leaderboardCollection, maxResults, forceReload));
    }

    public void submitScore(GoogleApiClient apiClient, String leaderboardId, long score) {
        submitScore(apiClient, leaderboardId, score, null);
    }

    public void submitScore(GoogleApiClient apiClient, String leaderboardId, long score, String scoreTag) {
        GamesClientImpl zzb = Games.zzb(apiClient, false);
        if (zzb != null) {
            try {
                zzb.zza(null, leaderboardId, score, scoreTag);
            } catch (RemoteException e) {
                GamesLog.zzA("LeaderboardsImpl", "service died");
            }
        }
    }

    public PendingResult<SubmitScoreResult> submitScoreImmediate(GoogleApiClient apiClient, String leaderboardId, long score) {
        return submitScoreImmediate(apiClient, leaderboardId, score, null);
    }

    public PendingResult<SubmitScoreResult> submitScoreImmediate(GoogleApiClient apiClient, String leaderboardId, long score, String scoreTag) {
        return apiClient.zzb(new C13407(this, apiClient, leaderboardId, score, scoreTag));
    }
}
