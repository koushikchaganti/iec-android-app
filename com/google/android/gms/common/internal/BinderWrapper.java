package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public final class BinderWrapper implements Parcelable {
    public static final Creator<BinderWrapper> CREATOR;
    private IBinder zzaiT;

    /* renamed from: com.google.android.gms.common.internal.BinderWrapper.1 */
    static class C03041 implements Creator<BinderWrapper> {
        C03041() {
        }

        public /* synthetic */ Object createFromParcel(Parcel x0) {
            return zzan(x0);
        }

        public /* synthetic */ Object[] newArray(int x0) {
            return zzbR(x0);
        }

        public BinderWrapper zzan(Parcel parcel) {
            return new BinderWrapper(null);
        }

        public BinderWrapper[] zzbR(int i) {
            return new BinderWrapper[i];
        }
    }

    static {
        CREATOR = new C03041();
    }

    public BinderWrapper() {
        this.zzaiT = null;
    }

    public BinderWrapper(IBinder binder) {
        this.zzaiT = null;
        this.zzaiT = binder;
    }

    private BinderWrapper(Parcel in) {
        this.zzaiT = null;
        this.zzaiT = in.readStrongBinder();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStrongBinder(this.zzaiT);
    }
}
