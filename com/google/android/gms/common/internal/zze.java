package com.google.android.gms.common.internal;

import com.google.android.gms.drive.events.CompletionEvent;
import com.google.android.gms.vision.barcode.Barcode.Phone;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class zze {
    public static final zze zzaiV;
    public static final zze zzaiW;
    public static final zze zzaiX;
    public static final zze zzaiY;
    public static final zze zzaiZ;
    public static final zze zzaja;
    public static final zze zzajb;
    public static final zze zzajc;
    public static final zze zzajd;
    public static final zze zzaje;
    public static final zze zzajf;
    public static final zze zzajg;
    public static final zze zzajh;
    public static final zze zzaji;
    public static final zze zzajj;

    /* renamed from: com.google.android.gms.common.internal.zze.11 */
    static class AnonymousClass11 extends zze {
        final /* synthetic */ char zzajp;

        AnonymousClass11(char c) {
            this.zzajp = c;
        }

        public zze zza(zze com_google_android_gms_common_internal_zze) {
            return com_google_android_gms_common_internal_zze.zzd(this.zzajp) ? com_google_android_gms_common_internal_zze : super.zza(com_google_android_gms_common_internal_zze);
        }

        public boolean zzd(char c) {
            return c == this.zzajp;
        }
    }

    /* renamed from: com.google.android.gms.common.internal.zze.1 */
    static class C07041 extends zze {
        C07041() {
        }

        public boolean zzd(char c) {
            return Character.isDigit(c);
        }
    }

    /* renamed from: com.google.android.gms.common.internal.zze.2 */
    static class C07052 extends zze {
        final /* synthetic */ char zzajk;
        final /* synthetic */ char zzajl;

        C07052(char c, char c2) {
            this.zzajk = c;
            this.zzajl = c2;
        }

        public boolean zzd(char c) {
            return c == this.zzajk || c == this.zzajl;
        }
    }

    /* renamed from: com.google.android.gms.common.internal.zze.3 */
    static class C07063 extends zze {
        final /* synthetic */ char[] zzajm;

        C07063(char[] cArr) {
            this.zzajm = cArr;
        }

        public boolean zzd(char c) {
            return Arrays.binarySearch(this.zzajm, c) >= 0;
        }
    }

    /* renamed from: com.google.android.gms.common.internal.zze.4 */
    static class C07074 extends zze {
        final /* synthetic */ char zzajn;
        final /* synthetic */ char zzajo;

        C07074(char c, char c2) {
            this.zzajn = c;
            this.zzajo = c2;
        }

        public boolean zzd(char c) {
            return this.zzajn <= c && c <= this.zzajo;
        }
    }

    /* renamed from: com.google.android.gms.common.internal.zze.5 */
    static class C07085 extends zze {
        C07085() {
        }

        public boolean zzd(char c) {
            return Character.isLetter(c);
        }
    }

    /* renamed from: com.google.android.gms.common.internal.zze.6 */
    static class C07096 extends zze {
        C07096() {
        }

        public boolean zzd(char c) {
            return Character.isLetterOrDigit(c);
        }
    }

    /* renamed from: com.google.android.gms.common.internal.zze.7 */
    static class C07107 extends zze {
        C07107() {
        }

        public boolean zzd(char c) {
            return Character.isUpperCase(c);
        }
    }

    /* renamed from: com.google.android.gms.common.internal.zze.8 */
    static class C07118 extends zze {
        C07118() {
        }

        public boolean zzd(char c) {
            return Character.isLowerCase(c);
        }
    }

    /* renamed from: com.google.android.gms.common.internal.zze.9 */
    static class C07129 extends zze {
        C07129() {
        }

        public zze zza(zze com_google_android_gms_common_internal_zze) {
            zzx.zzy(com_google_android_gms_common_internal_zze);
            return this;
        }

        public boolean zzb(CharSequence charSequence) {
            zzx.zzy(charSequence);
            return true;
        }

        public boolean zzd(char c) {
            return true;
        }
    }

    private static class zza extends zze {
        List<zze> zzajq;

        zza(List<zze> list) {
            this.zzajq = list;
        }

        public zze zza(zze com_google_android_gms_common_internal_zze) {
            List arrayList = new ArrayList(this.zzajq);
            arrayList.add(zzx.zzy(com_google_android_gms_common_internal_zze));
            return new zza(arrayList);
        }

        public boolean zzd(char c) {
            for (zze zzd : this.zzajq) {
                if (zzd.zzd(c)) {
                    return true;
                }
            }
            return false;
        }
    }

    static {
        zzaiV = zza((CharSequence) "\t\n\u000b\f\r \u0085\u1680\u2028\u2029\u205f\u3000\u00a0\u180e\u202f").zza(zza('\u2000', '\u200a'));
        zzaiW = zza((CharSequence) "\t\n\u000b\f\r \u0085\u1680\u2028\u2029\u205f\u3000").zza(zza('\u2000', '\u2006')).zza(zza('\u2008', '\u200a'));
        zzaiX = zza('\u0000', '\u007f');
        zze zza = zza('0', '9');
        zze com_google_android_gms_common_internal_zze = zza;
        for (char c : "\u0660\u06f0\u07c0\u0966\u09e6\u0a66\u0ae6\u0b66\u0be6\u0c66\u0ce6\u0d66\u0e50\u0ed0\u0f20\u1040\u1090\u17e0\u1810\u1946\u19d0\u1b50\u1bb0\u1c40\u1c50\ua620\ua8d0\ua900\uaa50\uff10".toCharArray()) {
            com_google_android_gms_common_internal_zze = com_google_android_gms_common_internal_zze.zza(zza(c, (char) (c + 9)));
        }
        zzaiY = com_google_android_gms_common_internal_zze;
        zzaiZ = zza('\t', '\r').zza(zza('\u001c', ' ')).zza(zzc('\u1680')).zza(zzc('\u180e')).zza(zza('\u2000', '\u2006')).zza(zza('\u2008', '\u200b')).zza(zza('\u2028', '\u2029')).zza(zzc('\u205f')).zza(zzc('\u3000'));
        zzaja = new C07041();
        zzajb = new C07085();
        zzajc = new C07096();
        zzajd = new C07107();
        zzaje = new C07118();
        zzajf = zza('\u0000', '\u001f').zza(zza('\u007f', '\u009f'));
        zzajg = zza('\u0000', ' ').zza(zza('\u007f', '\u00a0')).zza(zzc('\u00ad')).zza(zza('\u0600', '\u0603')).zza(zza((CharSequence) "\u06dd\u070f\u1680\u17b4\u17b5\u180e")).zza(zza('\u2000', '\u200f')).zza(zza('\u2028', '\u202f')).zza(zza('\u205f', '\u2064')).zza(zza('\u206a', '\u206f')).zza(zzc('\u3000')).zza(zza('\ud800', '\uf8ff')).zza(zza((CharSequence) "\ufeff\ufff9\ufffa\ufffb"));
        zzajh = zza('\u0000', '\u04f9').zza(zzc('\u05be')).zza(zza('\u05d0', '\u05ea')).zza(zzc('\u05f3')).zza(zzc('\u05f4')).zza(zza('\u0600', '\u06ff')).zza(zza('\u0750', '\u077f')).zza(zza('\u0e00', '\u0e7f')).zza(zza('\u1e00', '\u20af')).zza(zza('\u2100', '\u213a')).zza(zza('\ufb50', '\ufdff')).zza(zza('\ufe70', '\ufeff')).zza(zza('\uff61', '\uffdc'));
        zzaji = new C07129();
        zzajj = new zze() {
            public zze zza(zze com_google_android_gms_common_internal_zze) {
                return (zze) zzx.zzy(com_google_android_gms_common_internal_zze);
            }

            public boolean zzb(CharSequence charSequence) {
                return charSequence.length() == 0;
            }

            public boolean zzd(char c) {
                return false;
            }
        };
    }

    public static zze zza(char c, char c2) {
        zzx.zzab(c2 >= c);
        return new C07074(c, c2);
    }

    public static zze zza(CharSequence charSequence) {
        switch (charSequence.length()) {
            case Phone.UNKNOWN /*0*/:
                return zzajj;
            case CompletionEvent.STATUS_FAILURE /*1*/:
                return zzc(charSequence.charAt(0));
            case CompletionEvent.STATUS_CONFLICT /*2*/:
                return new C07052(charSequence.charAt(0), charSequence.charAt(1));
            default:
                char[] toCharArray = charSequence.toString().toCharArray();
                Arrays.sort(toCharArray);
                return new C07063(toCharArray);
        }
    }

    public static zze zzc(char c) {
        return new AnonymousClass11(c);
    }

    public zze zza(zze com_google_android_gms_common_internal_zze) {
        return new zza(Arrays.asList(new zze[]{this, (zze) zzx.zzy(com_google_android_gms_common_internal_zze)}));
    }

    public boolean zzb(CharSequence charSequence) {
        for (int length = charSequence.length() - 1; length >= 0; length--) {
            if (!zzd(charSequence.charAt(length))) {
                return false;
            }
        }
        return true;
    }

    public abstract boolean zzd(char c);
}
