package com.google.android.gms.common.api;

import android.app.Activity;
import java.util.Map;
import java.util.WeakHashMap;

public abstract class zza {
    private static final Map<Activity, zza> zzaeV;
    private static final Object zzqf;

    static {
        zzaeV = new WeakHashMap();
        zzqf = new Object();
    }

    public abstract void remove(int i);
}
