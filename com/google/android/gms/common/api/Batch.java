package com.google.android.gms.common.api;

import com.google.android.gms.common.api.PendingResult.zza;
import com.google.android.gms.internal.zzly;
import java.util.ArrayList;
import java.util.List;

public final class Batch extends zzly<BatchResult> {
    private int zzaes;
    private boolean zzaet;
    private boolean zzaeu;
    private final PendingResult<?>[] zzaev;
    private final Object zzpK;

    public static final class Builder {
        private GoogleApiClient zzYC;
        private List<PendingResult<?>> zzaex;

        public Builder(GoogleApiClient googleApiClient) {
            this.zzaex = new ArrayList();
            this.zzYC = googleApiClient;
        }

        public <R extends Result> BatchResultToken<R> add(PendingResult<R> pendingResult) {
            BatchResultToken<R> batchResultToken = new BatchResultToken(this.zzaex.size());
            this.zzaex.add(pendingResult);
            return batchResultToken;
        }

        public Batch build() {
            return new Batch(this.zzYC, null);
        }
    }

    /* renamed from: com.google.android.gms.common.api.Batch.1 */
    class C07021 implements zza {
        final /* synthetic */ Batch zzaew;

        C07021(Batch batch) {
            this.zzaew = batch;
        }

        public void zzu(Status status) {
            synchronized (this.zzaew.zzpK) {
                if (this.zzaew.isCanceled()) {
                    return;
                }
                if (status.isCanceled()) {
                    this.zzaew.zzaeu = true;
                } else if (!status.isSuccess()) {
                    this.zzaew.zzaet = true;
                }
                this.zzaew.zzaes = this.zzaew.zzaes - 1;
                if (this.zzaew.zzaes == 0) {
                    if (this.zzaew.zzaeu) {
                        super.cancel();
                    } else {
                        this.zzaew.zzb(new BatchResult(this.zzaew.zzaet ? new Status(13) : Status.zzaeX, this.zzaew.zzaev));
                    }
                }
            }
        }
    }

    private Batch(List<PendingResult<?>> pendingResultList, GoogleApiClient apiClient) {
        super(apiClient);
        this.zzpK = new Object();
        this.zzaes = pendingResultList.size();
        this.zzaev = new PendingResult[this.zzaes];
        for (int i = 0; i < pendingResultList.size(); i++) {
            PendingResult pendingResult = (PendingResult) pendingResultList.get(i);
            this.zzaev[i] = pendingResult;
            pendingResult.zza(new C07021(this));
        }
    }

    public void cancel() {
        super.cancel();
        for (PendingResult cancel : this.zzaev) {
            cancel.cancel();
        }
    }

    public BatchResult createFailedResult(Status status) {
        return new BatchResult(status, this.zzaev);
    }

    public /* synthetic */ Result zzc(Status status) {
        return createFailedResult(status);
    }
}
