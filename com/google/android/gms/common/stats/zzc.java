package com.google.android.gms.common.stats;

import com.google.android.gms.internal.zzmt;
import com.softelite.testapp.BuildConfig;

public final class zzc {
    public static zzmt<Integer> zzalG;
    public static zzmt<Integer> zzalH;

    public static final class zza {
        public static zzmt<Integer> zzalI;
        public static zzmt<String> zzalJ;
        public static zzmt<String> zzalK;
        public static zzmt<String> zzalL;
        public static zzmt<String> zzalM;
        public static zzmt<Long> zzalN;

        static {
            zzalI = zzmt.zza("gms:common:stats:connections:level", Integer.valueOf(zzd.LOG_LEVEL_OFF));
            zzalJ = zzmt.zzw("gms:common:stats:connections:ignored_calling_processes", BuildConfig.FLAVOR);
            zzalK = zzmt.zzw("gms:common:stats:connections:ignored_calling_services", BuildConfig.FLAVOR);
            zzalL = zzmt.zzw("gms:common:stats:connections:ignored_target_processes", BuildConfig.FLAVOR);
            zzalM = zzmt.zzw("gms:common:stats:connections:ignored_target_services", "com.google.android.gms.auth.GetToken");
            zzalN = zzmt.zza("gms:common:stats:connections:time_out_duration", Long.valueOf(600000));
        }
    }

    public static final class zzb {
        public static zzmt<Integer> zzalI;
        public static zzmt<Long> zzalN;

        static {
            zzalI = zzmt.zza("gms:common:stats:wakeLocks:level", Integer.valueOf(zzd.LOG_LEVEL_OFF));
            zzalN = zzmt.zza("gms:common:stats:wakelocks:time_out_duration", Long.valueOf(600000));
        }
    }

    static {
        zzalG = zzmt.zza("gms:common:stats:max_num_of_events", Integer.valueOf(100));
        zzalH = zzmt.zza("gms:common:stats:max_chunk_size", Integer.valueOf(100));
    }
}
