package com.google.android.gms.common.stats;

import android.content.ComponentName;
import com.google.android.gms.common.GooglePlayServicesUtil;

public final class zzd {
    public static int LOG_LEVEL_OFF;
    public static final ComponentName zzalO;
    public static int zzalP;
    public static int zzalQ;
    public static int zzalR;
    public static int zzalS;
    public static int zzalT;
    public static int zzalU;
    public static int zzalV;

    static {
        zzalO = new ComponentName(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE, "com.google.android.gms.common.stats.GmsCoreStatsService");
        LOG_LEVEL_OFF = 0;
        zzalP = 1;
        zzalQ = 2;
        zzalR = 4;
        zzalS = 8;
        zzalT = 16;
        zzalU = 32;
        zzalV = 1;
    }
}
