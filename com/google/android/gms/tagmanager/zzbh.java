package com.google.android.gms.tagmanager;

interface zzbh {
    void m14e(String str);

    void setLogLevel(int i);

    void m15v(String str);

    void zzaF(String str);

    void zzaG(String str);

    void zzaH(String str);

    void zzb(String str, Throwable th);

    void zzd(String str, Throwable th);
}
