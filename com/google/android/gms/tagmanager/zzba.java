package com.google.android.gms.tagmanager;

import android.util.LruCache;
import com.google.android.gms.tagmanager.zzm.zza;

class zzba<K, V> implements zzl<K, V> {
    private LruCache<K, V> zzbej;

    /* renamed from: com.google.android.gms.tagmanager.zzba.1 */
    class C05171 extends LruCache<K, V> {
        final /* synthetic */ zza zzbek;
        final /* synthetic */ zzba zzbel;

        C05171(zzba com_google_android_gms_tagmanager_zzba, int i, zza com_google_android_gms_tagmanager_zzm_zza) {
            this.zzbel = com_google_android_gms_tagmanager_zzba;
            this.zzbek = com_google_android_gms_tagmanager_zzm_zza;
            super(i);
        }

        protected int sizeOf(K key, V value) {
            return this.zzbek.sizeOf(key, value);
        }
    }

    zzba(int i, zza<K, V> com_google_android_gms_tagmanager_zzm_zza_K__V) {
        this.zzbej = new C05171(this, i, com_google_android_gms_tagmanager_zzm_zza_K__V);
    }

    public V get(K key) {
        return this.zzbej.get(key);
    }

    public void zzh(K k, V v) {
        this.zzbej.put(k, v);
    }
}
