package com.google.android.gms.tagmanager;

import android.content.Context;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.concurrent.LinkedBlockingQueue;

class zzat extends Thread implements zzas {
    private static zzat zzbdU;
    private volatile boolean mClosed;
    private final Context mContext;
    private volatile boolean zzQk;
    private final LinkedBlockingQueue<Runnable> zzbdT;
    private volatile zzau zzbdV;

    /* renamed from: com.google.android.gms.tagmanager.zzat.1 */
    class C05151 implements Runnable {
        final /* synthetic */ zzas zzbdW;
        final /* synthetic */ long zzbdX;
        final /* synthetic */ zzat zzbdY;
        final /* synthetic */ String zzzn;

        C05151(zzat com_google_android_gms_tagmanager_zzat, zzas com_google_android_gms_tagmanager_zzas, long j, String str) {
            this.zzbdY = com_google_android_gms_tagmanager_zzat;
            this.zzbdW = com_google_android_gms_tagmanager_zzas;
            this.zzbdX = j;
            this.zzzn = str;
        }

        public void run() {
            if (this.zzbdY.zzbdV == null) {
                zzcu zzFs = zzcu.zzFs();
                zzFs.zza(this.zzbdY.mContext, this.zzbdW);
                this.zzbdY.zzbdV = zzFs.zzFv();
            }
            this.zzbdY.zzbdV.zzg(this.zzbdX, this.zzzn);
        }
    }

    private zzat(Context context) {
        super("GAThread");
        this.zzbdT = new LinkedBlockingQueue();
        this.zzQk = false;
        this.mClosed = false;
        if (context != null) {
            this.mContext = context.getApplicationContext();
        } else {
            this.mContext = context;
        }
        start();
    }

    static zzat zzaZ(Context context) {
        if (zzbdU == null) {
            zzbdU = new zzat(context);
        }
        return zzbdU;
    }

    private String zzd(Throwable th) {
        OutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(byteArrayOutputStream);
        th.printStackTrace(printStream);
        printStream.flush();
        return new String(byteArrayOutputStream.toByteArray());
    }

    public void run() {
        while (!this.mClosed) {
            try {
                Runnable runnable = (Runnable) this.zzbdT.take();
                if (!this.zzQk) {
                    runnable.run();
                }
            } catch (InterruptedException e) {
                zzbg.zzaG(e.toString());
            } catch (Throwable th) {
                zzbg.m12e("Error on Google TagManager Thread: " + zzd(th));
                zzbg.m12e("Google TagManager is shutting down.");
                this.zzQk = true;
            }
        }
    }

    public void zzfs(String str) {
        zzk(str, System.currentTimeMillis());
    }

    public void zzj(Runnable runnable) {
        this.zzbdT.add(runnable);
    }

    void zzk(String str, long j) {
        zzj(new C05151(this, this, j, str));
    }
}
