package com.google.android.gms.tagmanager;

import com.google.android.gms.drive.events.CompletionEvent;
import com.google.android.gms.internal.zzad;
import com.google.android.gms.internal.zzae;
import com.google.android.gms.plus.PlusShare;
import com.softelite.testapp.BuildConfig;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

class zzay extends zzak {
    private static final String ID;
    private static final String zzbdJ;
    private static final String zzbeb;
    private static final String zzbec;
    private static final String zzbed;

    /* renamed from: com.google.android.gms.tagmanager.zzay.1 */
    static /* synthetic */ class C05161 {
        static final /* synthetic */ int[] zzbee;

        static {
            zzbee = new int[zza.values().length];
            try {
                zzbee[zza.URL.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                zzbee[zza.BACKSLASH.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                zzbee[zza.NONE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    private enum zza {
        NONE,
        URL,
        BACKSLASH
    }

    static {
        ID = zzad.JOINER.toString();
        zzbdJ = zzae.ARG0.toString();
        zzbeb = zzae.ITEM_SEPARATOR.toString();
        zzbec = zzae.KEY_VALUE_SEPARATOR.toString();
        zzbed = zzae.ESCAPE.toString();
    }

    public zzay() {
        super(ID, zzbdJ);
    }

    private String zza(String str, zza com_google_android_gms_tagmanager_zzay_zza, Set<Character> set) {
        switch (C05161.zzbee[com_google_android_gms_tagmanager_zzay_zza.ordinal()]) {
            case CompletionEvent.STATUS_FAILURE /*1*/:
                try {
                    return zzdj.zzfM(str);
                } catch (Throwable e) {
                    zzbg.zzb("Joiner: unsupported encoding", e);
                    return str;
                }
            case CompletionEvent.STATUS_CONFLICT /*2*/:
                String replace = str.replace("\\", "\\\\");
                String str2 = replace;
                for (Character ch : set) {
                    CharSequence ch2 = ch.toString();
                    str2 = str2.replace(ch2, "\\" + ch2);
                }
                return str2;
            default:
                return str;
        }
    }

    private void zza(StringBuilder stringBuilder, String str, zza com_google_android_gms_tagmanager_zzay_zza, Set<Character> set) {
        stringBuilder.append(zza(str, com_google_android_gms_tagmanager_zzay_zza, set));
    }

    private void zza(Set<Character> set, String str) {
        for (int i = 0; i < str.length(); i++) {
            set.add(Character.valueOf(str.charAt(i)));
        }
    }

    public boolean zzEa() {
        return true;
    }

    public com.google.android.gms.internal.zzag.zza zzI(Map<String, com.google.android.gms.internal.zzag.zza> map) {
        com.google.android.gms.internal.zzag.zza com_google_android_gms_internal_zzag_zza = (com.google.android.gms.internal.zzag.zza) map.get(zzbdJ);
        if (com_google_android_gms_internal_zzag_zza == null) {
            return zzdf.zzFJ();
        }
        zza com_google_android_gms_tagmanager_zzay_zza;
        Set set;
        com.google.android.gms.internal.zzag.zza com_google_android_gms_internal_zzag_zza2 = (com.google.android.gms.internal.zzag.zza) map.get(zzbeb);
        String zzg = com_google_android_gms_internal_zzag_zza2 != null ? zzdf.zzg(com_google_android_gms_internal_zzag_zza2) : BuildConfig.FLAVOR;
        com_google_android_gms_internal_zzag_zza2 = (com.google.android.gms.internal.zzag.zza) map.get(zzbec);
        String zzg2 = com_google_android_gms_internal_zzag_zza2 != null ? zzdf.zzg(com_google_android_gms_internal_zzag_zza2) : "=";
        zza com_google_android_gms_tagmanager_zzay_zza2 = zza.NONE;
        com_google_android_gms_internal_zzag_zza2 = (com.google.android.gms.internal.zzag.zza) map.get(zzbed);
        if (com_google_android_gms_internal_zzag_zza2 != null) {
            String zzg3 = zzdf.zzg(com_google_android_gms_internal_zzag_zza2);
            if (PlusShare.KEY_CALL_TO_ACTION_URL.equals(zzg3)) {
                com_google_android_gms_tagmanager_zzay_zza = zza.URL;
                set = null;
            } else if ("backslash".equals(zzg3)) {
                com_google_android_gms_tagmanager_zzay_zza = zza.BACKSLASH;
                set = new HashSet();
                zza(set, zzg);
                zza(set, zzg2);
                set.remove(Character.valueOf('\\'));
            } else {
                zzbg.m12e("Joiner: unsupported escape type: " + zzg3);
                return zzdf.zzFJ();
            }
        }
        set = null;
        com_google_android_gms_tagmanager_zzay_zza = com_google_android_gms_tagmanager_zzay_zza2;
        StringBuilder stringBuilder = new StringBuilder();
        switch (com_google_android_gms_internal_zzag_zza.type) {
            case CompletionEvent.STATUS_CONFLICT /*2*/:
                Object obj = 1;
                com.google.android.gms.internal.zzag.zza[] com_google_android_gms_internal_zzag_zzaArr = com_google_android_gms_internal_zzag_zza.zzjp;
                int length = com_google_android_gms_internal_zzag_zzaArr.length;
                int i = 0;
                while (i < length) {
                    com.google.android.gms.internal.zzag.zza com_google_android_gms_internal_zzag_zza3 = com_google_android_gms_internal_zzag_zzaArr[i];
                    if (obj == null) {
                        stringBuilder.append(zzg);
                    }
                    zza(stringBuilder, zzdf.zzg(com_google_android_gms_internal_zzag_zza3), com_google_android_gms_tagmanager_zzay_zza, set);
                    i++;
                    obj = null;
                }
                break;
            case CompletionEvent.STATUS_CANCELED /*3*/:
                for (int i2 = 0; i2 < com_google_android_gms_internal_zzag_zza.zzjq.length; i2++) {
                    if (i2 > 0) {
                        stringBuilder.append(zzg);
                    }
                    String zzg4 = zzdf.zzg(com_google_android_gms_internal_zzag_zza.zzjq[i2]);
                    String zzg5 = zzdf.zzg(com_google_android_gms_internal_zzag_zza.zzjr[i2]);
                    zza(stringBuilder, zzg4, com_google_android_gms_tagmanager_zzay_zza, set);
                    stringBuilder.append(zzg2);
                    zza(stringBuilder, zzg5, com_google_android_gms_tagmanager_zzay_zza, set);
                }
                break;
            default:
                zza(stringBuilder, zzdf.zzg(com_google_android_gms_internal_zzag_zza), com_google_android_gms_tagmanager_zzay_zza, set);
                break;
        }
        return zzdf.zzR(stringBuilder.toString());
    }
}
