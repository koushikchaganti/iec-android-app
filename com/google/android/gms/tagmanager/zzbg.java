package com.google.android.gms.tagmanager;

public final class zzbg {
    static zzbh zzber;
    static int zzbes;

    static {
        zzber = new zzy();
    }

    public static void m12e(String message) {
        zzber.m14e(message);
    }

    public static int getLogLevel() {
        return zzbes;
    }

    public static void setLogLevel(int logLevel) {
        zzber.setLogLevel(logLevel);
    }

    public static void m13v(String message) {
        zzber.m15v(message);
    }

    public static void zzaF(String str) {
        zzber.zzaF(str);
    }

    public static void zzaG(String str) {
        zzber.zzaG(str);
    }

    public static void zzaH(String str) {
        zzber.zzaH(str);
    }

    public static void zzb(String str, Throwable th) {
        zzber.zzb(str, th);
    }

    public static void zzd(String str, Throwable th) {
        zzber.zzd(str, th);
    }
}
