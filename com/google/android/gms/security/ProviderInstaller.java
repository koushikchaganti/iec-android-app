package com.google.android.gms.security;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.internal.zzx;
import java.lang.reflect.Method;

public class ProviderInstaller {
    public static final String PROVIDER_NAME = "GmsCore_OpenSSL";
    private static final GoogleApiAvailability zzagU;
    private static Method zzbbC;
    private static final Object zzqf;

    /* renamed from: com.google.android.gms.security.ProviderInstaller.1 */
    static class C05071 extends AsyncTask<Void, Void, Integer> {
        final /* synthetic */ ProviderInstallListener zzbbD;
        final /* synthetic */ Context zzsm;

        C05071(Context context, ProviderInstallListener providerInstallListener) {
            this.zzsm = context;
            this.zzbbD = providerInstallListener;
        }

        protected /* synthetic */ Object doInBackground(Object[] x0) {
            return zzc((Void[]) x0);
        }

        protected /* synthetic */ void onPostExecute(Object x0) {
            zze((Integer) x0);
        }

        protected Integer zzc(Void... voidArr) {
            try {
                ProviderInstaller.installIfNeeded(this.zzsm);
                return Integer.valueOf(0);
            } catch (GooglePlayServicesRepairableException e) {
                return Integer.valueOf(e.getConnectionStatusCode());
            } catch (GooglePlayServicesNotAvailableException e2) {
                return Integer.valueOf(e2.errorCode);
            }
        }

        protected void zze(Integer num) {
            if (num.intValue() == 0) {
                this.zzbbD.onProviderInstalled();
                return;
            }
            this.zzbbD.onProviderInstallFailed(num.intValue(), ProviderInstaller.zzagU.zza(this.zzsm, num.intValue(), "pi"));
        }
    }

    public interface ProviderInstallListener {
        void onProviderInstallFailed(int i, Intent intent);

        void onProviderInstalled();
    }

    static {
        zzagU = GoogleApiAvailability.getInstance();
        zzqf = new Object();
        zzbbC = null;
    }

    public static void installIfNeeded(Context context) throws GooglePlayServicesRepairableException, GooglePlayServicesNotAvailableException {
        zzx.zzb((Object) context, (Object) "Context must not be null");
        zzagU.zzai(context);
        Context remoteContext = GooglePlayServicesUtil.getRemoteContext(context);
        if (remoteContext == null) {
            Log.e("ProviderInstaller", "Failed to get remote context");
            throw new GooglePlayServicesNotAvailableException(8);
        }
        synchronized (zzqf) {
            try {
                if (zzbbC == null) {
                    zzaV(remoteContext);
                }
                zzbbC.invoke(null, new Object[]{remoteContext});
            } catch (Exception e) {
                Log.e("ProviderInstaller", "Failed to install provider: " + e.getMessage());
                throw new GooglePlayServicesNotAvailableException(8);
            }
        }
    }

    public static void installIfNeededAsync(Context context, ProviderInstallListener listener) {
        zzx.zzb((Object) context, (Object) "Context must not be null");
        zzx.zzb((Object) listener, (Object) "Listener must not be null");
        zzx.zzcx("Must be called on the UI thread");
        new C05071(context, listener).execute(new Void[0]);
    }

    private static void zzaV(Context context) throws ClassNotFoundException, NoSuchMethodException {
        zzbbC = context.getClassLoader().loadClass("com.google.android.gms.common.security.ProviderInstallerImpl").getMethod("insertProvider", new Class[]{Context.class});
    }
}
