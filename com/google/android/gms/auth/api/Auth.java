package com.google.android.gms.auth.api;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import com.google.android.gms.auth.api.credentials.CredentialsApi;
import com.google.android.gms.auth.api.credentials.PasswordSpecification;
import com.google.android.gms.auth.api.credentials.internal.zzf;
import com.google.android.gms.auth.api.proxy.ProxyApi;
import com.google.android.gms.auth.api.signin.GoogleSignInApi;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.internal.zzd;
import com.google.android.gms.auth.api.signin.internal.zzk;
import com.google.android.gms.auth.api.signin.internal.zzl;
import com.google.android.gms.auth.api.signin.zzg;
import com.google.android.gms.auth.api.signin.zzh;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions.NoOptions;
import com.google.android.gms.common.api.Api.ApiOptions.Optional;
import com.google.android.gms.common.api.Api.zzb;
import com.google.android.gms.common.api.Api.zzc;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.internal.zzko;
import com.google.android.gms.internal.zzkp;
import com.google.android.gms.internal.zzkq;
import com.google.android.gms.internal.zzkt;
import com.google.android.gms.internal.zzku;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzlb;
import java.util.Collections;
import java.util.List;

public final class Auth {
    public static final Api<AuthCredentialsOptions> CREDENTIALS_API;
    public static final CredentialsApi CredentialsApi;
    public static final Api<GoogleSignInOptions> GOOGLE_SIGN_IN_API;
    public static final GoogleSignInApi GoogleSignInApi;
    public static final Api<zza> PROXY_API;
    public static final ProxyApi ProxyApi;
    public static final zzc<zzkx> zzTW;
    public static final zzc<zzf> zzTX;
    public static final zzc<zzkq> zzTY;
    public static final zzc<zzl> zzTZ;
    public static final zzc<zzd> zzUa;
    public static final zzc<zzku> zzUb;
    private static final com.google.android.gms.common.api.Api.zza<zzkx, zza> zzUc;
    private static final com.google.android.gms.common.api.Api.zza<zzf, AuthCredentialsOptions> zzUd;
    private static final com.google.android.gms.common.api.Api.zza<zzkq, NoOptions> zzUe;
    private static final com.google.android.gms.common.api.Api.zza<zzku, NoOptions> zzUf;
    private static final com.google.android.gms.common.api.Api.zza<zzl, zzh> zzUg;
    private static final com.google.android.gms.common.api.Api.zza<zzd, GoogleSignInOptions> zzUh;
    public static final Api<zzh> zzUi;
    public static final Api<NoOptions> zzUj;
    public static final Api<NoOptions> zzUk;
    public static final zzko zzUl;
    public static final zzg zzUm;
    public static final com.google.android.gms.auth.api.consent.zza zzUn;

    /* renamed from: com.google.android.gms.auth.api.Auth.1 */
    static class C06861 extends com.google.android.gms.common.api.Api.zza<zzkx, zza> {
        C06861() {
        }

        public zzkx zza(Context context, Looper looper, com.google.android.gms.common.internal.zzf com_google_android_gms_common_internal_zzf, zza com_google_android_gms_auth_api_Auth_zza, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            return new zzkx(context, looper, com_google_android_gms_common_internal_zzf, com_google_android_gms_auth_api_Auth_zza, connectionCallbacks, onConnectionFailedListener);
        }
    }

    /* renamed from: com.google.android.gms.auth.api.Auth.2 */
    static class C06872 extends com.google.android.gms.common.api.Api.zza<zzf, AuthCredentialsOptions> {
        C06872() {
        }

        public zzf zza(Context context, Looper looper, com.google.android.gms.common.internal.zzf com_google_android_gms_common_internal_zzf, AuthCredentialsOptions authCredentialsOptions, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            return new zzf(context, looper, com_google_android_gms_common_internal_zzf, authCredentialsOptions, connectionCallbacks, onConnectionFailedListener);
        }
    }

    /* renamed from: com.google.android.gms.auth.api.Auth.3 */
    static class C06883 extends com.google.android.gms.common.api.Api.zza<zzkq, NoOptions> {
        C06883() {
        }

        public /* synthetic */ zzb zza(Context context, Looper looper, com.google.android.gms.common.internal.zzf com_google_android_gms_common_internal_zzf, Object obj, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            return zzc(context, looper, com_google_android_gms_common_internal_zzf, (NoOptions) obj, connectionCallbacks, onConnectionFailedListener);
        }

        public zzkq zzc(Context context, Looper looper, com.google.android.gms.common.internal.zzf com_google_android_gms_common_internal_zzf, NoOptions noOptions, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            return new zzkq(context, looper, com_google_android_gms_common_internal_zzf, connectionCallbacks, onConnectionFailedListener);
        }
    }

    /* renamed from: com.google.android.gms.auth.api.Auth.4 */
    static class C06894 extends com.google.android.gms.common.api.Api.zza<zzku, NoOptions> {
        C06894() {
        }

        public /* synthetic */ zzb zza(Context context, Looper looper, com.google.android.gms.common.internal.zzf com_google_android_gms_common_internal_zzf, Object obj, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            return zzd(context, looper, com_google_android_gms_common_internal_zzf, (NoOptions) obj, connectionCallbacks, onConnectionFailedListener);
        }

        public zzku zzd(Context context, Looper looper, com.google.android.gms.common.internal.zzf com_google_android_gms_common_internal_zzf, NoOptions noOptions, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            return new zzku(context, looper, com_google_android_gms_common_internal_zzf, connectionCallbacks, onConnectionFailedListener);
        }
    }

    /* renamed from: com.google.android.gms.auth.api.Auth.5 */
    static class C06905 extends com.google.android.gms.common.api.Api.zza<zzl, zzh> {
        C06905() {
        }

        public zzl zza(Context context, Looper looper, com.google.android.gms.common.internal.zzf com_google_android_gms_common_internal_zzf, zzh com_google_android_gms_auth_api_signin_zzh, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            return new zzl(context, looper, com_google_android_gms_common_internal_zzf, com_google_android_gms_auth_api_signin_zzh, connectionCallbacks, onConnectionFailedListener);
        }
    }

    /* renamed from: com.google.android.gms.auth.api.Auth.6 */
    static class C06916 extends com.google.android.gms.common.api.Api.zza<zzd, GoogleSignInOptions> {
        C06916() {
        }

        public zzd zza(Context context, Looper looper, com.google.android.gms.common.internal.zzf com_google_android_gms_common_internal_zzf, GoogleSignInOptions googleSignInOptions, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            return new zzd(context, looper, com_google_android_gms_common_internal_zzf, googleSignInOptions, connectionCallbacks, onConnectionFailedListener);
        }

        public List<Scope> zza(GoogleSignInOptions googleSignInOptions) {
            return googleSignInOptions == null ? Collections.emptyList() : googleSignInOptions.zzmu();
        }

        public /* synthetic */ List zzn(Object obj) {
            return zza((GoogleSignInOptions) obj);
        }
    }

    public static final class AuthCredentialsOptions implements Optional {
        private final String zzUo;
        private final PasswordSpecification zzUp;

        public static class Builder {
            private PasswordSpecification zzUp;

            public Builder() {
                this.zzUp = PasswordSpecification.zzUO;
            }
        }

        public Bundle zzlU() {
            Bundle bundle = new Bundle();
            bundle.putString("consumer_package", this.zzUo);
            bundle.putParcelable("password_specification", this.zzUp);
            return bundle;
        }

        public PasswordSpecification zzma() {
            return this.zzUp;
        }
    }

    public static final class zza implements Optional {
        private final Bundle zzUq;

        public Bundle zzmb() {
            return new Bundle(this.zzUq);
        }
    }

    static {
        zzTW = new zzc();
        zzTX = new zzc();
        zzTY = new zzc();
        zzTZ = new zzc();
        zzUa = new zzc();
        zzUb = new zzc();
        zzUc = new C06861();
        zzUd = new C06872();
        zzUe = new C06883();
        zzUf = new C06894();
        zzUg = new C06905();
        zzUh = new C06916();
        PROXY_API = new Api("Auth.PROXY_API", zzUc, zzTW);
        CREDENTIALS_API = new Api("Auth.CREDENTIALS_API", zzUd, zzTX);
        zzUi = new Api("Auth.SIGN_IN_API", zzUg, zzTZ);
        GOOGLE_SIGN_IN_API = new Api("Auth.GOOGLE_SIGN_IN_API", zzUh, zzUa);
        zzUj = new Api("Auth.ACCOUNT_STATUS_API", zzUe, zzTY);
        zzUk = new Api("Auth.CONSENT_API", zzUf, zzUb);
        ProxyApi = new zzlb();
        CredentialsApi = new com.google.android.gms.auth.api.credentials.internal.zzd();
        zzUl = new zzkp();
        zzUm = new zzk();
        GoogleSignInApi = new com.google.android.gms.auth.api.signin.internal.zzc();
        zzUn = new zzkt();
    }

    private Auth() {
    }
}
