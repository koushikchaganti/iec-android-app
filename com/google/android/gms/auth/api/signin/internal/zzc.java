package com.google.android.gms.auth.api.signin.internal;

import android.accounts.Account;
import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInApi;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.PendingResults;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzmo;
import java.util.HashSet;

public class zzc implements GoogleSignInApi {

    private abstract class zza<R extends Result> extends com.google.android.gms.internal.zzlx.zza<R, zzd> {
        final /* synthetic */ zzc zzVV;

        public zza(zzc com_google_android_gms_auth_api_signin_internal_zzc, GoogleApiClient googleApiClient) {
            this.zzVV = com_google_android_gms_auth_api_signin_internal_zzc;
            super(Auth.zzUa, googleApiClient);
        }
    }

    /* renamed from: com.google.android.gms.auth.api.signin.internal.zzc.1 */
    class C12181 extends zza<GoogleSignInResult> {
        final /* synthetic */ GoogleApiClient zzVT;
        final /* synthetic */ GoogleSignInOptions zzVU;
        final /* synthetic */ zzc zzVV;

        /* renamed from: com.google.android.gms.auth.api.signin.internal.zzc.1.1 */
        class C11931 extends zza {
            final /* synthetic */ C12181 zzVW;

            C11931(C12181 c12181) {
                this.zzVW = c12181;
            }

            public void zza(GoogleSignInAccount googleSignInAccount, Status status) throws RemoteException {
                if (googleSignInAccount != null) {
                    zzn.zzae(this.zzVW.zzVT.getContext()).zzb(googleSignInAccount, this.zzVW.zzVU);
                }
                this.zzVW.zzb(new GoogleSignInResult(googleSignInAccount, status));
            }
        }

        C12181(zzc com_google_android_gms_auth_api_signin_internal_zzc, GoogleApiClient googleApiClient, GoogleApiClient googleApiClient2, GoogleSignInOptions googleSignInOptions) {
            this.zzVV = com_google_android_gms_auth_api_signin_internal_zzc;
            this.zzVT = googleApiClient2;
            this.zzVU = googleSignInOptions;
            super(com_google_android_gms_auth_api_signin_internal_zzc, googleApiClient);
        }

        protected void zza(zzd com_google_android_gms_auth_api_signin_internal_zzd) throws RemoteException {
            ((zzg) com_google_android_gms_auth_api_signin_internal_zzd.zzqs()).zza(new C11931(this), this.zzVU);
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzn(status);
        }

        protected GoogleSignInResult zzn(Status status) {
            return new GoogleSignInResult(null, status);
        }
    }

    /* renamed from: com.google.android.gms.auth.api.signin.internal.zzc.2 */
    class C12192 extends zza<Status> {
        final /* synthetic */ GoogleApiClient zzVT;
        final /* synthetic */ zzc zzVV;

        /* renamed from: com.google.android.gms.auth.api.signin.internal.zzc.2.1 */
        class C11941 extends zza {
            final /* synthetic */ C12192 zzVX;

            C11941(C12192 c12192) {
                this.zzVX = c12192;
            }

            public void zzl(Status status) throws RemoteException {
                this.zzVX.zzb(status);
            }
        }

        C12192(zzc com_google_android_gms_auth_api_signin_internal_zzc, GoogleApiClient googleApiClient, GoogleApiClient googleApiClient2) {
            this.zzVV = com_google_android_gms_auth_api_signin_internal_zzc;
            this.zzVT = googleApiClient2;
            super(com_google_android_gms_auth_api_signin_internal_zzc, googleApiClient);
        }

        protected void zza(zzd com_google_android_gms_auth_api_signin_internal_zzd) throws RemoteException {
            ((zzg) com_google_android_gms_auth_api_signin_internal_zzd.zzqs()).zzb(new C11941(this), this.zzVV.zza(this.zzVT));
        }

        protected Status zzb(Status status) {
            return status;
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzb(status);
        }
    }

    /* renamed from: com.google.android.gms.auth.api.signin.internal.zzc.3 */
    class C12203 extends zza<Status> {
        final /* synthetic */ GoogleApiClient zzVT;
        final /* synthetic */ zzc zzVV;

        /* renamed from: com.google.android.gms.auth.api.signin.internal.zzc.3.1 */
        class C11951 extends zza {
            final /* synthetic */ C12203 zzVY;

            C11951(C12203 c12203) {
                this.zzVY = c12203;
            }

            public void zzm(Status status) throws RemoteException {
                this.zzVY.zzb(status);
            }
        }

        C12203(zzc com_google_android_gms_auth_api_signin_internal_zzc, GoogleApiClient googleApiClient, GoogleApiClient googleApiClient2) {
            this.zzVV = com_google_android_gms_auth_api_signin_internal_zzc;
            this.zzVT = googleApiClient2;
            super(com_google_android_gms_auth_api_signin_internal_zzc, googleApiClient);
        }

        protected void zza(zzd com_google_android_gms_auth_api_signin_internal_zzd) throws RemoteException {
            ((zzg) com_google_android_gms_auth_api_signin_internal_zzd.zzqs()).zzc(new C11951(this), this.zzVV.zza(this.zzVT));
        }

        protected Status zzb(Status status) {
            return status;
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzb(status);
        }
    }

    private GoogleSignInOptions zza(GoogleApiClient googleApiClient) {
        return ((zzd) googleApiClient.zza(Auth.zzUa)).zzmL();
    }

    private OptionalPendingResult<GoogleSignInResult> zza(GoogleApiClient googleApiClient, GoogleSignInOptions googleSignInOptions) {
        Log.d("GoogleSignInApiImpl", "trySilentSignIn");
        return new zzmo(googleApiClient.zza(new C12181(this, googleApiClient, googleApiClient, googleSignInOptions)));
    }

    private boolean zza(Account account, Account account2) {
        return account == null ? account2 == null : account.equals(account2);
    }

    public Intent getSignInIntent(GoogleApiClient client) {
        zzx.zzy(client);
        return ((zzd) client.zza(Auth.zzUa)).zzmK();
    }

    public GoogleSignInResult getSignInResultFromIntent(Intent data) {
        if (data == null || (!data.hasExtra("googleSignInStatus") && !data.hasExtra("googleSignInAccount"))) {
            return null;
        }
        GoogleSignInAccount googleSignInAccount = (GoogleSignInAccount) data.getParcelableExtra("googleSignInAccount");
        Status status = (Status) data.getParcelableExtra("googleSignInStatus");
        if (googleSignInAccount != null) {
            status = Status.zzaeX;
        }
        return new GoogleSignInResult(googleSignInAccount, status);
    }

    public PendingResult<Status> revokeAccess(GoogleApiClient client) {
        zzn.zzae(client.getContext()).zzmZ();
        for (GoogleApiClient zzoF : GoogleApiClient.zzoE()) {
            zzoF.zzoF();
        }
        return client.zzb(new C12203(this, client, client));
    }

    public PendingResult<Status> signOut(GoogleApiClient client) {
        zzn.zzae(client.getContext()).zzmZ();
        for (GoogleApiClient zzoF : GoogleApiClient.zzoE()) {
            zzoF.zzoF();
        }
        return client.zzb(new C12192(this, client, client));
    }

    public OptionalPendingResult<GoogleSignInResult> silentSignIn(GoogleApiClient client) {
        GoogleSignInOptions zza = zza(client);
        Result zza2 = zza(client.getContext(), zza);
        return zza2 != null ? PendingResults.zzb(zza2, client) : zza(client, zza);
    }

    public GoogleSignInResult zza(Context context, GoogleSignInOptions googleSignInOptions) {
        Log.d("GoogleSignInApiImpl", "getSavedSignInResultIfEligible");
        zzx.zzy(googleSignInOptions);
        zzn zzae = zzn.zzae(context);
        GoogleSignInOptions zzmX = zzae.zzmX();
        if (zzmX == null || !zza(zzmX.getAccount(), googleSignInOptions.getAccount()) || googleSignInOptions.zzmz()) {
            return null;
        }
        if ((googleSignInOptions.zzmy() && (!zzmX.zzmy() || !googleSignInOptions.zzmB().equals(zzmX.zzmB()))) || !new HashSet(zzmX.zzmu()).containsAll(new HashSet(googleSignInOptions.zzmu()))) {
            return null;
        }
        GoogleSignInAccount zzmW = zzae.zzmW();
        return (zzmW == null || zzmW.zzb()) ? null : new GoogleSignInResult(zzmW, Status.zzaeX);
    }
}
