package com.google.android.gms.auth.api.credentials.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.Auth.AuthCredentialsOptions;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.CredentialRequest;
import com.google.android.gms.auth.api.credentials.CredentialRequestResult;
import com.google.android.gms.auth.api.credentials.CredentialsApi;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.auth.api.credentials.PasswordSpecification;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.internal.zzlx.zzb;

public final class zzd implements CredentialsApi {

    private static class zza extends zza {
        private zzb<Status> zzVc;

        zza(zzb<Status> com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_common_api_Status) {
            this.zzVc = com_google_android_gms_internal_zzlx_zzb_com_google_android_gms_common_api_Status;
        }

        public void zzg(Status status) {
            this.zzVc.zzr(status);
        }
    }

    /* renamed from: com.google.android.gms.auth.api.credentials.internal.zzd.1 */
    class C12141 extends zze<CredentialRequestResult> {
        final /* synthetic */ CredentialRequest zzUY;
        final /* synthetic */ zzd zzUZ;

        /* renamed from: com.google.android.gms.auth.api.credentials.internal.zzd.1.1 */
        class C11921 extends zza {
            final /* synthetic */ C12141 zzVa;

            C11921(C12141 c12141) {
                this.zzVa = c12141;
            }

            public void zza(Status status, Credential credential) {
                this.zzVa.zzb(new zzc(status, credential));
            }

            public void zzg(Status status) {
                this.zzVa.zzb(zzc.zzh(status));
            }
        }

        C12141(zzd com_google_android_gms_auth_api_credentials_internal_zzd, GoogleApiClient googleApiClient, CredentialRequest credentialRequest) {
            this.zzUZ = com_google_android_gms_auth_api_credentials_internal_zzd;
            this.zzUY = credentialRequest;
            super(googleApiClient);
        }

        protected void zza(Context context, zzi com_google_android_gms_auth_api_credentials_internal_zzi) throws RemoteException {
            com_google_android_gms_auth_api_credentials_internal_zzi.zza(new C11921(this), this.zzUY);
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzi(status);
        }

        protected CredentialRequestResult zzi(Status status) {
            return zzc.zzh(status);
        }
    }

    /* renamed from: com.google.android.gms.auth.api.credentials.internal.zzd.2 */
    class C12152 extends zze<Status> {
        final /* synthetic */ zzd zzUZ;
        final /* synthetic */ Credential zzVb;

        C12152(zzd com_google_android_gms_auth_api_credentials_internal_zzd, GoogleApiClient googleApiClient, Credential credential) {
            this.zzUZ = com_google_android_gms_auth_api_credentials_internal_zzd;
            this.zzVb = credential;
            super(googleApiClient);
        }

        protected void zza(Context context, zzi com_google_android_gms_auth_api_credentials_internal_zzi) throws RemoteException {
            com_google_android_gms_auth_api_credentials_internal_zzi.zza(new zza(this), new SaveRequest(this.zzVb));
        }

        protected Status zzb(Status status) {
            return status;
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzb(status);
        }
    }

    /* renamed from: com.google.android.gms.auth.api.credentials.internal.zzd.3 */
    class C12163 extends zze<Status> {
        final /* synthetic */ zzd zzUZ;
        final /* synthetic */ Credential zzVb;

        C12163(zzd com_google_android_gms_auth_api_credentials_internal_zzd, GoogleApiClient googleApiClient, Credential credential) {
            this.zzUZ = com_google_android_gms_auth_api_credentials_internal_zzd;
            this.zzVb = credential;
            super(googleApiClient);
        }

        protected void zza(Context context, zzi com_google_android_gms_auth_api_credentials_internal_zzi) throws RemoteException {
            com_google_android_gms_auth_api_credentials_internal_zzi.zza(new zza(this), new DeleteRequest(this.zzVb));
        }

        protected Status zzb(Status status) {
            return status;
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzb(status);
        }
    }

    /* renamed from: com.google.android.gms.auth.api.credentials.internal.zzd.4 */
    class C12174 extends zze<Status> {
        final /* synthetic */ zzd zzUZ;

        C12174(zzd com_google_android_gms_auth_api_credentials_internal_zzd, GoogleApiClient googleApiClient) {
            this.zzUZ = com_google_android_gms_auth_api_credentials_internal_zzd;
            super(googleApiClient);
        }

        protected void zza(Context context, zzi com_google_android_gms_auth_api_credentials_internal_zzi) throws RemoteException {
            com_google_android_gms_auth_api_credentials_internal_zzi.zza(new zza(this));
        }

        protected Status zzb(Status status) {
            return status;
        }

        protected /* synthetic */ Result zzc(Status status) {
            return zzb(status);
        }
    }

    public PendingResult<Status> delete(GoogleApiClient client, Credential credential) {
        return client.zzb(new C12163(this, client, credential));
    }

    public PendingResult<Status> disableAutoSignIn(GoogleApiClient client) {
        return client.zzb(new C12174(this, client));
    }

    public PendingIntent getHintPickerIntent(GoogleApiClient client, HintRequest request) {
        zzx.zzb((Object) client, (Object) "client must not be null");
        zzx.zzb((Object) request, (Object) "request must not be null");
        zzx.zzb(client.zza(Auth.CREDENTIALS_API), (Object) "Auth.CREDENTIALS_API must be added to GoogleApiClient to use this API");
        AuthCredentialsOptions zzmm = ((zzf) client.zza(Auth.zzTX)).zzmm();
        PasswordSpecification zzma = (zzmm == null || zzmm.zzma() == null) ? PasswordSpecification.zzUO : zzmm.zzma();
        return PendingIntent.getActivity(client.getContext(), GamesStatusCodes.STATUS_REQUEST_UPDATE_PARTIAL_SUCCESS, zzb.zza(client.getContext(), request, zzma), DriveFile.MODE_READ_ONLY);
    }

    public PendingResult<CredentialRequestResult> request(GoogleApiClient client, CredentialRequest request) {
        return client.zza(new C12141(this, client, request));
    }

    public PendingResult<Status> save(GoogleApiClient client, Credential credential) {
        return client.zzb(new C12152(this, client, credential));
    }
}
