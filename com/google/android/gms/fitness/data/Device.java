package com.google.android.gms.fitness.data;

import android.content.Context;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.provider.Settings.Secure;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzoo;
import com.google.android.gms.internal.zzps;
import com.softelite.testapp.BuildConfig;

public final class Device implements SafeParcelable {
    public static final Creator<Device> CREATOR;
    public static final int TYPE_CHEST_STRAP = 4;
    public static final int TYPE_PHONE = 1;
    public static final int TYPE_SCALE = 5;
    public static final int TYPE_TABLET = 2;
    public static final int TYPE_UNKNOWN = 0;
    public static final int TYPE_WATCH = 3;
    private final int mVersionCode;
    private final int zzZU;
    private final String zzabv;
    private final String zzauA;
    private final String zzauB;
    private final String zzauC;
    private final int zzauD;

    static {
        CREATOR = new zzh();
    }

    Device(int versionCode, String manufacturer, String model, String version, String uid, int type, int platformType) {
        this.mVersionCode = versionCode;
        this.zzauA = (String) zzx.zzy(manufacturer);
        this.zzauB = (String) zzx.zzy(model);
        this.zzabv = BuildConfig.FLAVOR;
        this.zzauC = (String) zzx.zzy(uid);
        this.zzZU = type;
        this.zzauD = platformType;
    }

    public Device(String manufacturer, String model, String uid, int type) {
        this(manufacturer, model, BuildConfig.FLAVOR, uid, type, TYPE_UNKNOWN);
    }

    public Device(String manufacturer, String model, String version, String uid, int type, int platformType) {
        this(TYPE_PHONE, manufacturer, model, BuildConfig.FLAVOR, uid, type, platformType);
    }

    public static Device getLocalDevice(Context context) {
        int zzaF = zzoo.zzaF(context);
        return new Device(Build.MANUFACTURER, Build.MODEL, VERSION.RELEASE, zzaB(context), zzaF, TYPE_TABLET);
    }

    private boolean zza(Device device) {
        return zzw.equal(this.zzauA, device.zzauA) && zzw.equal(this.zzauB, device.zzauB) && zzw.equal(this.zzabv, device.zzabv) && zzw.equal(this.zzauC, device.zzauC) && this.zzZU == device.zzZU && this.zzauD == device.zzauD;
    }

    private static String zzaB(Context context) {
        return Secure.getString(context.getContentResolver(), "android_id");
    }

    private boolean zztO() {
        return zztN() == TYPE_PHONE;
    }

    public int describeContents() {
        return TYPE_UNKNOWN;
    }

    public boolean equals(Object that) {
        return this == that || ((that instanceof Device) && zza((Device) that));
    }

    public String getManufacturer() {
        return this.zzauA;
    }

    public String getModel() {
        return this.zzauB;
    }

    String getStreamIdentifier() {
        Object[] objArr = new Object[TYPE_WATCH];
        objArr[TYPE_UNKNOWN] = this.zzauA;
        objArr[TYPE_PHONE] = this.zzauB;
        objArr[TYPE_TABLET] = this.zzauC;
        return String.format("%s:%s:%s", objArr);
    }

    public int getType() {
        return this.zzZU;
    }

    public String getUid() {
        return this.zzauC;
    }

    public String getVersion() {
        return this.zzabv;
    }

    int getVersionCode() {
        return this.mVersionCode;
    }

    public int hashCode() {
        Object[] objArr = new Object[TYPE_SCALE];
        objArr[TYPE_UNKNOWN] = this.zzauA;
        objArr[TYPE_PHONE] = this.zzauB;
        objArr[TYPE_TABLET] = this.zzabv;
        objArr[TYPE_WATCH] = this.zzauC;
        objArr[TYPE_CHEST_STRAP] = Integer.valueOf(this.zzZU);
        return zzw.hashCode(objArr);
    }

    public String toString() {
        Object[] objArr = new Object[TYPE_CHEST_STRAP];
        objArr[TYPE_UNKNOWN] = getStreamIdentifier();
        objArr[TYPE_PHONE] = this.zzabv;
        objArr[TYPE_TABLET] = Integer.valueOf(this.zzZU);
        objArr[TYPE_WATCH] = Integer.valueOf(this.zzauD);
        return String.format("Device{%s:%s:%s:%s}", objArr);
    }

    public void writeToParcel(Parcel parcel, int flags) {
        zzh.zza(this, parcel, flags);
    }

    public int zztN() {
        return this.zzauD;
    }

    public String zztP() {
        return zztO() ? this.zzauC : zzps.zzdr(this.zzauC);
    }
}
