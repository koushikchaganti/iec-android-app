package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzx;
import com.softelite.testapp.BuildConfig;

public final class Application implements SafeParcelable {
    public static final Creator<Application> CREATOR;
    public static final Application zzatV;
    private final int mVersionCode;
    private final String zzSp;
    private final String zzabv;
    private final String zzatW;

    static {
        zzatV = new Application(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE, String.valueOf(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE), null);
        CREATOR = new zza();
    }

    Application(int versionCode, String packageName, String version, String domainName) {
        this.mVersionCode = versionCode;
        this.zzSp = (String) zzx.zzy(packageName);
        this.zzabv = BuildConfig.FLAVOR;
        this.zzatW = domainName;
    }

    public Application(String packageName, String version, String domainName) {
        this(1, packageName, BuildConfig.FLAVOR, domainName);
    }

    private boolean zza(Application application) {
        return this.zzSp.equals(application.zzSp) && zzw.equal(this.zzabv, application.zzabv) && zzw.equal(this.zzatW, application.zzatW);
    }

    public static Application zzde(String str) {
        return zze(str, null, null);
    }

    public static Application zze(String str, String str2, String str3) {
        return GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE.equals(str) ? zzatV : new Application(str, str2, str3);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object that) {
        return this == that || ((that instanceof Application) && zza((Application) that));
    }

    public String getPackageName() {
        return this.zzSp;
    }

    public String getVersion() {
        return this.zzabv;
    }

    int getVersionCode() {
        return this.mVersionCode;
    }

    public int hashCode() {
        return zzw.hashCode(this.zzSp, this.zzabv, this.zzatW);
    }

    public String toString() {
        return String.format("Application{%s:%s:%s}", new Object[]{this.zzSp, this.zzabv, this.zzatW});
    }

    public void writeToParcel(Parcel parcel, int flags) {
        zza.zza(this, parcel, flags);
    }

    public String zzty() {
        return this.zzatW;
    }
}
