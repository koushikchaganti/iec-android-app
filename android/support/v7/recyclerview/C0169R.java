package android.support.v7.recyclerview;

import com.softelite.testapp.C0577R;

/* renamed from: android.support.v7.recyclerview.R */
public final class C0169R {

    /* renamed from: android.support.v7.recyclerview.R.attr */
    public static final class attr {
        public static final int layoutManager = 2130772093;
        public static final int reverseLayout = 2130772095;
        public static final int spanCount = 2130772094;
        public static final int stackFromEnd = 2130772096;
    }

    /* renamed from: android.support.v7.recyclerview.R.dimen */
    public static final class dimen {
        public static final int item_touch_helper_max_drag_scroll_per_frame = 2131296360;
    }

    /* renamed from: android.support.v7.recyclerview.R.id */
    public static final class id {
        public static final int item_touch_helper_previous_elevation = 2131558406;
    }

    /* renamed from: android.support.v7.recyclerview.R.styleable */
    public static final class styleable {
        public static final int[] RecyclerView;
        public static final int RecyclerView_android_orientation = 0;
        public static final int RecyclerView_layoutManager = 1;
        public static final int RecyclerView_reverseLayout = 3;
        public static final int RecyclerView_spanCount = 2;
        public static final int RecyclerView_stackFromEnd = 4;

        static {
            RecyclerView = new int[]{16842948, C0577R.attr.layoutManager, C0577R.attr.spanCount, C0577R.attr.reverseLayout, C0577R.attr.stackFromEnd};
        }
    }
}
